package fabulous.ane
{

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.StatusEvent;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.system.Capabilities;
import flash.utils.ByteArray;
import flash.filesystem.File;
import flash.utils.Dictionary;


public class FabulousANE {
		public static const ERROR_TYPE_SETUP:String = "ERROR_TYPE_SETUP";
		public static const ERROR_TYPE_LOAD_FILE:String = "ERROR_TYPE_LOAD_FILE";

		private static var _logCallback:Function;
		private static var _errorCallback:Function;
		private static var _dispatcher:EventDispatcher;

		public static var FULLSCREEN_AD_LOADED:String = "fullScreenAdLoaded";
		public static var FULLSCREEN_AD_FAILED:String = "fullScreenAdFailed";
		public static var BANNER_AD_LOADED:String = "bannerAdLoaded";
		public static var BANNER_AD_FAILED:String = "bannerAdFailed";

		public static const LOAD_FILE_COMPLETED:String = "loadFileCompleted";
		public static const LOAD_FILE_ERROR:String = "loadFileError";

		private static var _loadFileCompletedSignalsMap:SignalsMap;
		private static var _loadFileErrorSignalsMap:SignalsMap;

		public function FabulousANE(logCallback:Function) {
			throw "Static class. Cannot instantiate.";
		}

		public static function init(logCallback:Function, errorCallback:Function):void {
			_logCallback = logCallback;
			_errorCallback = errorCallback;
			_dispatcher = new EventDispatcher();
			_loadFileCompletedSignalsMap = new SignalsMap();
			_loadFileErrorSignalsMap = new SignalsMap();
		}

		public static function log(msg:String):void {
			_logCallback("[FabulousANE] " + msg);
		}

		public static function logError(type:String, msg:String):void {
			_errorCallback(type, "[FabulousANE] " + msg);
		}

		public static function get dispatcher():EventDispatcher {
			return _dispatcher;
		}

		public static function getUDID():String {
			return "desktop-udid";
		}

		public static function getDeviceModel():String {
			return "desktop,desktop";
		}
		
		public static function getDeviceManufacturer():String {
			return "pc";
		}

		public static function getDeviceMemory():Number {
			return 0;
		}

		public static function getPreferredLanguage():String {
			return "xx";
		}

		public static function sendMail(subject:String, message:String, to:String, attachmentData:ByteArray, attachmentMime:String, attachmentName:String):Boolean {
			return false;
		}

		public static function vibrate():void {
		}

		public static function scheduleNotification(name:String, message:String, timeout:int):void {
		}

		public static function cancelNotification(name:String):void {
		}

		public static function showNativeAlert(title:String, message:String, cancelLabel:String, otherLabels:Array, callbacks:Array):void {
		}

		public static function prepareFullScreenAds():void {
		}

		public static function showFullScreenAd():void {
		}

		public static function prepareBannerAd():void {
		}

		public static function showBannerAd():void {
		}

		public static function hideBannerAd():void {
		}

		public static function share(title:String, message:String, attachmentName:String = null, attachmentMimeType:String = null, attachment:ByteArray = null):void {
		}

		public static function loadFileSync(path:String):ByteArray {
			var fileStream:FileStream = new FileStream();
			fileStream.open(new File(path), FileMode.READ);
			var byteArray:ByteArray = new ByteArray();
			byteArray.length = fileStream.bytesAvailable;
			fileStream.readBytes(byteArray, 0, byteArray.length);
			fileStream.close();
			return byteArray;
		}

		public static function loadFileAsync(path:String, onFileLoadCompleted:Function, onFileLoadError:Function = null):void {
			var isAlreadyLoading:Boolean = _loadFileCompletedSignalsMap.has(path);
			_loadFileCompletedSignalsMap.get(path).addOnce(onFileLoadCompleted);
			if (onFileLoadError != null) {
				_loadFileErrorSignalsMap.get(path).addOnce(onFileLoadError);
			}
			if (!isAlreadyLoading) {
				var urlLoader:FabulousANEURLLoader = new FabulousANEURLLoader();
				urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
				urlLoader.addEventListener(Event.COMPLETE, onUrlLoaderComplete);
				urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onUrlLoaderError);
				urlLoader.addEventListener(IOErrorEvent.DISK_ERROR, onUrlLoaderError);
				urlLoader.load(new URLRequest(path));
			}
		}

		private static function onUrlLoaderComplete(e:Event):void {
			var urlLoader:FabulousANEURLLoader = e.target as FabulousANEURLLoader;
			if (_loadFileCompletedSignalsMap.has(urlLoader.url)) {
				_loadFileCompletedSignalsMap.get(urlLoader.url).post(urlLoader.data as ByteArray);
			} else {
				logError(ERROR_TYPE_SETUP, "No callback for loaded file: " + urlLoader.url);
			}
			removeUrlLoaderListeners(urlLoader);
		}

		private static function onUrlLoaderError(e:IOErrorEvent):void {
			var urlLoader:FabulousANEURLLoader = e.target as FabulousANEURLLoader;
			if (_loadFileErrorSignalsMap.has(urlLoader.url)) {
				_loadFileErrorSignalsMap.get(urlLoader.url).post(e.toString());
			}
			removeUrlLoaderListeners(urlLoader);
			logError(ERROR_TYPE_LOAD_FILE, "Error loading file: " + e.toString() + " for url: " + urlLoader.url);
		}

		private static function removeUrlLoaderListeners(urlLoader:FabulousANEURLLoader):void {
			_loadFileCompletedSignalsMap.dispose(urlLoader.url);
			_loadFileErrorSignalsMap.dispose(urlLoader.url);
			urlLoader.removeEventListener(Event.COMPLETE, onUrlLoaderComplete);
			urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, onUrlLoaderError);
			urlLoader.removeEventListener(IOErrorEvent.DISK_ERROR, onUrlLoaderError);
		}
		
		public static function getAppExternalStorageDir():String {
			return File.applicationStorageDirectory.resolvePath("files/").nativePath;
		}
		public static function getUserEmail():String {
			return "user email not available on desktop";
		}
		
		public static function get storeType():String {
			return "store type not available on dekstop";
		}
		
		public static function get systemSdkVersion():int {
			return -1;
		}
		
		public static function get appKeyHash():String {
			return "key hash not available on desktop";
		}
	}
}

import flash.net.URLLoader;
import flash.net.URLRequest;

class FabulousANEURLLoader extends URLLoader {
	private var _urlRequest:URLRequest;

	public function FabulousANEURLLoader(urlRequest:URLRequest = null):void {
		super(urlRequest);
		_urlRequest = urlRequest;
	}

	override public function load(urlRequest:URLRequest):void {
		_urlRequest = urlRequest;
		super.load(urlRequest);
	}

	public function get urlRequest():URLRequest {
		return _urlRequest;
	}

	public function get url():String {
		return _urlRequest != null ? _urlRequest.url : null;
	}
}

import flash.utils.Dictionary;

class SignalsMap {

	private var _map:Dictionary;

	public function SignalsMap() {
		_map = new Dictionary();
	}

	public function get(id:Object):Signal {
		_map[id] ||= new Signal();
		return _map[id];
	}

	public function has(id:Object):Boolean {
		return _map[id] != null;
	}

	public function dispose(id:Object):void {
		if (_map[id] != null) {
			(_map[id] as Signal).clean();
			delete _map[id];
		}
	}
}

class Signal {
	internal var _listeners:Vector.<SignalListener>;

	public function Signal() {
	}

	public function add(func:Function):void {
		addExpiring(func, -1);
	}

	public function addOnce(func:Function):void {
		addExpiring(func, 1);
	}

	public function addExpiring(func:Function, life:int):void {
		var sl:SignalListener = new SignalListener();
		sl._func = func;
		sl._life = life;
		_listeners ||= new Vector.<SignalListener>();
		_listeners.push(sl);
	}

	public function remove(func:Function):void {
		if (_listeners) {
			for (var i:int = 0; i < _listeners.length; ++i) {
				if (_listeners[i]._func == func) {
					_listeners.splice(i, 1);
				}
			}
			if (_listeners.length == 0) {
				_listeners = null;
			}
		}
	}

	public function hasListener(func:Function):Boolean {
		if (_listeners) {
			for (var i:int = 0; i < _listeners.length; ++i) {
				if (_listeners[i]._func == func) {
					return true;
				}
			}
		}
		return false;
	}

	public function clean():void {
		_listeners = null;
	}

	public function get numListeners():int {
		return _listeners != null ? _listeners.length : 0;
	}

	public function cleanExpiring():void {
		if (_listeners) {
			for (var i:int = _listeners.length - 1; i >= 0; --i) {
				if (_listeners[i]._life >= 0) {
					_listeners.splice(i, 1);
				}
			}
		}
	}

	public function post(arg:Object = null):void {
		var i:int;
		if (_listeners) {
			if (arg !== null) {
				for (i = 0;  _listeners && i < _listeners.length; ++i) {
					_listeners[i]._func(arg);
				}
			} else {
				for (i = 0; _listeners && i < _listeners.length; ++i) {
					_listeners[i]._func();
				}
			}

			// secondary check is necessary, because last listener may be removed within its observer
			if (_listeners) {
				for (i = _listeners.length - 1; i >= 0; --i) {
					if (_listeners[i]._life > 0) {
						if (--_listeners[i]._life == 0) {
							_listeners.splice(i, 1);
						}
					}
				}
				if (_listeners.length == 0) {
					_listeners = null;
				}
			}
		}
	}
}

class SignalListener {
	public var _func:Function;
	public var _life:int;

	public function SignalListener() {
	}
}
