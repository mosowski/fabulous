﻿package fabulous.ane {

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.StatusEvent;
import flash.external.ExtensionContext;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.utils.ByteArray;
import flash.system.Capabilities;
import flash.utils.Dictionary;


public class FabulousANE {
		public static const ERROR_TYPE_SETUP:String = "ERROR_TYPE_SETUP";
		public static const ERROR_TYPE_LOAD_FILE:String = "ERROR_TYPE_LOAD_FILE";

		private static var _context:ExtensionContext;		
		private static var _logCallback:Function;
		private static var _errorCallback:Function;
		private static var _dispatcher:EventDispatcher;
		private static var _nativeAlertCallbacks:Array;

		public static var FULLSCREEN_AD_LOADED:String = "fullScreenAdLoaded";
		public static var FULLSCREEN_AD_FAILED:String = "fullScreenAdFailed";
		public static var BANNER_AD_LOADED:String = "bannerAdLoaded";
		public static var BANNER_AD_FAILED:String = "bannerAdFailed";

		private static const LOAD_FILE_COMPLETED:String = "loadFileCompleted";
		private static const LOAD_FILE_ERROR:String = "loadFileError";

		private static var _loadFileCompletedSignalsMap:SignalsMap;
		private static var _loadFileErrorSignalsMap:SignalsMap;

		public function FabulousANE() {
			throw "Static class. Cannot instantiate.";
		}

		public static function init(logCallback:Function, errorCallback:Function):void {
			_logCallback = logCallback;
			_errorCallback = errorCallback;
			_dispatcher = new EventDispatcher();
			_loadFileCompletedSignalsMap = new SignalsMap();
			_loadFileErrorSignalsMap = new SignalsMap();

			_context = ExtensionContext.createExtensionContext("fabulous.ane", null);
			if (!_context)
			{
				logError(ERROR_TYPE_SETUP, "Problem with Fabulous ANE initialization.");
				return;
			}

			_context.addEventListener(StatusEvent.STATUS, onStatus);

			_context.call("init");
		}

		public static function log(msg:String):void {
			_logCallback("[FabulousANE] " + msg);
		}

		public static function logError(type:String, msg:String):void {
			_errorCallback(type, "[FabulousANE] " + msg);
		}

		public static function get dispatcher():EventDispatcher {
			return _dispatcher;
		}

		public static function getUDID():String {
			return _context.call("getUDID") as String;
		}

		public static function getDeviceModel():String {
			return _context.call("getDeviceModel") as String;
		}
		
		public static function getDeviceManufacturer():String {
			return _context.call("getDeviceManufacturer") as String;
		}

		public static function getDeviceMemory():Number {
			return _context.call("getDeviceMemory") as Number;
		}

		public static function getPreferredLanguage():String {
			return _context.call("getPreferredLanguage") as String;
		}

		public static function sendMail(subject:String, message:String, to:String, attachmentData:ByteArray, attachmentMime:String, attachmentName:String):Boolean {
			return _context.call("sendMail", subject, message, to, attachmentData, attachmentMime, attachmentName) as Boolean;
		}

		public static function vibrate():void {
			_context.call("vibrate");
		}

	
		public static function scheduleNotification(name:String, message:String, timeout:int):void {
			_context.call("scheduleNotification", name, message, timeout);
		}

		public static function cancelNotification(name:String):void {
			_context.call("cancelNotification", name);
		}
	
		public static function showNativeAlert(title:String, message:String, cancelLabel:String, otherLabels:Array, callbacks:Array):void {
			_nativeAlertCallbacks = callbacks;
			_context.call("showNativeAlert", title, message, cancelLabel, otherLabels);
		}
		
		private static function onStatus(event:StatusEvent):void {
			if (event.code == "log") {
				log(event.level);
			} else if (event.code == "nativeAlertButtonClicked") {
				_nativeAlertCallbacks[int(event.level)]();
				_nativeAlertCallbacks = null;
			} else if (event.code == LOAD_FILE_COMPLETED) {
				var url:String = event.level;
				if (_loadFileCompletedSignalsMap.has(url)) {
					_loadFileCompletedSignalsMap.get(url).post(getLoadedFile(url));
				} else {
					logError(ERROR_TYPE_SETUP, "No callback for loaded file: " + url);
				}
				cleanupLoadFileSignals(url);
			} else if (event.code == LOAD_FILE_ERROR) {
				var error:Object = JSON.parse(event.level);
				if (_loadFileErrorSignalsMap.has(error.url)) {
					_loadFileErrorSignalsMap.get(error.url).post(error.message);
				}
				cleanupLoadFileSignals(error.url);
				logError(ERROR_TYPE_LOAD_FILE, "Error loading file: " + error.message + " for url: " + error.url);
			} else {
				_dispatcher.dispatchEvent(event)
			}
		}

		private static function cleanupLoadFileSignals(url:String):void {
			_loadFileCompletedSignalsMap.dispose(url);
			_loadFileErrorSignalsMap.dispose(url);
			disposeLoadedFile(url);
		}

		public static function prepareFullScreenAds():void {
			_context.call("prepareFullScreenAds");
		}

		public static function showFullScreenAd():void {
			_context.call("showFullScreenAd");
		}

		public static function prepareBannerAd():void {
			_context.call("prepareBannerAd");
		}

		public static function showBannerAd():void {
			_context.call("showBannerAd");
		}

		public static function hideBannerAd():void {
			_context.call("hideBannerAd");
		}

		public static function share(title:String, message:String, attachmentName:String = null, attachmentMimeType:String = null, attachment:ByteArray = null):void {
			_context.call("share", title, message, attachmentName, attachmentMimeType, attachment);
		}

		public static function loadFileSync(path:String):ByteArray {
			return _context.call("loadFileSync", path) as ByteArray;
		}

		public static function loadFileAsync(path:String, onFileLoadCompleted:Function, onFileLoadError:Function = null):void {
			var isAlreadyLoading:Boolean = _loadFileCompletedSignalsMap.has(path);
			_loadFileCompletedSignalsMap.get(path).addOnce(onFileLoadCompleted);
			if (onFileLoadError != null) {
				_loadFileErrorSignalsMap.get(path).addOnce(onFileLoadError);
			}
			if (!isAlreadyLoading) {
				_context.call("loadFileAsync", path);
			}
		}

		private static function getLoadedFile(path:String):ByteArray {
			return _context.call("getLoadedFile", path, new ByteArray()) as ByteArray;
		}

		private static function disposeLoadedFile(path:String):void {
			_context.call("disposeLoadedFile", path);
		}
		
		public static function getAppExternalStorageDir():String {
			return _context.call("getAppExternalStorageDir") as String;
		}

		public static function getUserEmail():String {
			return _context.call("getUserEmail") as String;
		}
		
		public static function get storeType():String {
			if (Capabilities.manufacturer.indexOf('Android') > -1)
			{
				return _context.call("getStoreType") as String;
			}
			else if (Capabilities.manufacturer.indexOf('iOS') > -1)
			{
				return "AppStore"
			}
			else
			{
				return null;
			}
		}

		public static function get systemSdkVersion():int {
			if (Capabilities.manufacturer.indexOf('iOS') > -1) {
				return -1;
			} else {
				return _context.call("systemSdkVersion") as int;
			}
		}

		public static function get appKeyHash():String {
			if (Capabilities.manufacturer.indexOf('iOS') > -1) {
				return "key hash not available on iOS";
			} else {
				return _context.call("appKeyHash") as String;
			}
		}
	}
}

import flash.utils.Dictionary;

class SignalsMap {

	private var _map:Dictionary;

	public function SignalsMap() {
		_map = new Dictionary();
	}

	public function get(id:Object):Signal {
		_map[id] ||= new Signal();
		return _map[id];
	}

	public function has(id:Object):Boolean {
		return _map[id] != null;
	}

	public function dispose(id:Object):void {
		if (_map[id] != null) {
			(_map[id] as Signal).clean();
			delete _map[id];
		}
	}
}

class Signal {
	internal var _listeners:Vector.<SignalListener>;

	public function Signal() {
	}

	public function add(func:Function):void {
		addExpiring(func, -1);
	}

	public function addOnce(func:Function):void {
		addExpiring(func, 1);
	}

	public function addExpiring(func:Function, life:int):void {
		var sl:SignalListener = new SignalListener();
		sl._func = func;
		sl._life = life;
		_listeners ||= new Vector.<SignalListener>();
		_listeners.push(sl);
	}

	public function remove(func:Function):void {
		if (_listeners) {
			for (var i:int = 0; i < _listeners.length; ++i) {
				if (_listeners[i]._func == func) {
					_listeners.splice(i, 1);
				}
			}
			if (_listeners.length == 0) {
				_listeners = null;
			}
		}
	}

	public function hasListener(func:Function):Boolean {
		if (_listeners) {
			for (var i:int = 0; i < _listeners.length; ++i) {
				if (_listeners[i]._func == func) {
					return true;
				}
			}
		}
		return false;
	}

	public function clean():void {
		_listeners = null;
	}

	public function get numListeners():int {
		return _listeners != null ? _listeners.length : 0;
	}

	public function cleanExpiring():void {
		if (_listeners) {
			for (var i:int = _listeners.length - 1; i >= 0; --i) {
				if (_listeners[i]._life >= 0) {
					_listeners.splice(i, 1);
				}
			}
		}
	}

	public function post(arg:Object = null):void {
		var i:int;
		if (_listeners) {
			if (arg !== null) {
				for (i = 0;  _listeners && i < _listeners.length; ++i) {
					_listeners[i]._func(arg);
				}
			} else {
				for (i = 0; _listeners && i < _listeners.length; ++i) {
					_listeners[i]._func();
				}
			}

			// secondary check is necessary, because last listener may be removed within its observer
			if (_listeners) {
				for (i = _listeners.length - 1; i >= 0; --i) {
					if (_listeners[i]._life > 0) {
						if (--_listeners[i]._life == 0) {
							_listeners.splice(i, 1);
						}
					}
				}
				if (_listeners.length == 0) {
					_listeners = null;
				}
			}
		}
	}
}

class SignalListener {
	public var _func:Function;
	public var _life:int;

	public function SignalListener() {
	}
}
