package fabulous.ane;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Vibrator;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;

import com.adobe.fre.FREArray;
import com.adobe.fre.FREByteArray;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREInvalidObjectException;
import com.adobe.fre.FREObject;
import com.adobe.fre.FRETypeMismatchException;
import com.adobe.fre.FREWrongThreadException;

import org.OpenUDID.OpenUDID_manager;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

public class Fabulous {

    public static Activity _activity;
    public static FREContext _context;

    private static String TAG = "Fabulous";

    public static final String GOOGLE_PLAY_STORE_TYPE = "GooglePlay";
    public static final String GOOGLE_INSTALLER_PACKAGE_NAME = "com.android.vending";

    public static final String AMAZON_STORE_TYPE = "AmazonStore";
    public static final String AMAZON_INSTALLER_PACKAGE_NAME = "com.amazon.venezia";

    public static final String LOAD_FILE_COMPLETED = "loadFileCompleted";
    public static final String LOAD_FILE_ERROR = "loadFileError";

    private static Map<String, byte[]> mFiles = new HashMap<>();

    public static void init() {
        OpenUDID_manager.sync(_activity);
    }

    public static String getUDID() {
        return OpenUDID_manager.getOpenUDID();
    }

    public static String getDeviceModel() {
        return "android," + Build.MANUFACTURER + "," + Build.MODEL;
    }

    public static void cancelNotification(String name) {

    }

    public static void scheduleNotification(String name, String message, int timeout) {

    }

    public static String getUserEmail() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(Initializer.context.getActivity()).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }
        return "";
    }

    public static String getStoreType() {
        String installerPackageName = _activity.getPackageManager().getInstallerPackageName(_activity.getPackageName());

        if (installerPackageName != null && installerPackageName.equals(AMAZON_INSTALLER_PACKAGE_NAME)) {
            return AMAZON_STORE_TYPE;
        }

        return GOOGLE_PLAY_STORE_TYPE;
    }

    public static double getDeviceMemory() {
        ActivityManager actManager = (ActivityManager) _activity.getSystemService(Context.ACTIVITY_SERVICE);
        MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        long totalMemory = memInfo.totalMem;
        return (double) totalMemory;
    }

    public static void share(String subject, String message, String attachmentName, String attachmentMimeType, FREByteArray attachment) throws IllegalStateException, FREInvalidObjectException, FREWrongThreadException, IOException {
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        if (attachmentMimeType != null) {
            shareIntent.setType(attachmentMimeType);
        } else {
            shareIntent.setType("text/plain");
        }
        if (attachment != null && canSaveAttachment()) {
            shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, saveByteArray(attachment, attachmentName));
        }
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
        _activity.startActivity(Intent.createChooser(shareIntent, "Share via"));
    }

    private static boolean canSaveAttachment() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT || doesUserHavePermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private static boolean doesUserHavePermission(String permission) {
        return _activity.checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    private static Uri saveByteArray(FREByteArray byteArray, String fileName) throws IllegalStateException, FREInvalidObjectException, FREWrongThreadException, IOException {
        byteArray.acquire();
        ByteBuffer bytes = byteArray.getBytes();
        byteArray.release();

        File file = new File(_activity.getExternalCacheDir(), fileName);
        file.getParentFile().mkdirs();
        FileOutputStream out = new FileOutputStream(file);
        byte[] b = new byte[bytes.capacity()];
        bytes.get(b, 0, bytes.capacity());
        out.write(b);
        out.flush();
        out.close();
        return Uri.parse("file://" + file.getAbsolutePath());
    }

    public static String getAppExternalStorageDir(Context context) {
        return new File(_activity.getExternalFilesDir(null), "").getAbsolutePath();
    }

    public static String getPreferredLanguage() {
        return Locale.getDefault().getLanguage().substring(0, 2).toLowerCase();
    }

    public static void vibrate() {
        Vibrator v = (Vibrator) _activity.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(400);
    }

    public static String getAppKeyHash() {
        String key = null;
        try {
            String packageName = _activity.getApplicationContext().getPackageName();
            PackageInfo info = _activity.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
            }
        } catch (NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    public static Boolean sendMail(FREObject[] arg1) throws FREWrongThreadException, FREInvalidObjectException, FRETypeMismatchException, FileNotFoundException, IOException, ActivityNotFoundException {
        //arg1: [subject:String, message:String, to:String, attachmentData:ByteArray, attachmentMime:String, attachmentName:String];
        String subject, message, to, attachmentMime, attachmentName;
        subject = arg1[0].getAsString();
        message = arg1[1].getAsString();
        to = arg1[2].getAsString();
        attachmentMime = arg1[4].getAsString();
        attachmentName = arg1[5].getAsString();
        Intent i = new Intent(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, message);
        i.putExtra(Intent.EXTRA_STREAM, saveByteArray((FREByteArray) arg1[3], attachmentName));
        i.setType(attachmentMime);
        _activity.startActivity(Intent.createChooser(i, "Send mail"));
        return true;
    }

    @SuppressLint("NewApi")
    public static void showNativeAlert(final FREContext freCtx, FREObject[] arg1) throws FREWrongThreadException, FRETypeMismatchException, FREInvalidObjectException {
        String title, message, cancelLabel;
        title = arg1[0].getAsString();
        message = arg1[1].getAsString();
        cancelLabel = arg1[2].getAsString();
        FREArray otherLabels = (FREArray) arg1[3];
        AlertDialog.Builder alertDialog;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            alertDialog = new AlertDialog.Builder(_activity, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            alertDialog = new AlertDialog.Builder(_activity, AlertDialog.THEME_HOLO_DARK);
        } else {
            alertDialog = new AlertDialog.Builder(_activity);
        }
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setNegativeButton(cancelLabel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                freCtx.dispatchStatusEventAsync(
                        "nativeAlertButtonClicked",
                        "0");
            }
        });
        if (otherLabels.getLength() > 0) {
            alertDialog.setPositiveButton(otherLabels.getObjectAt(0).getAsString(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    freCtx.dispatchStatusEventAsync(
                            "nativeAlertButtonClicked",
                            "1");
                }
            });
        }
        if (otherLabels.getLength() > 1) {
            alertDialog.setNeutralButton(otherLabels.getObjectAt(1).getAsString(), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    freCtx.dispatchStatusEventAsync(
                            "nativeAlertButtonClicked",
                            "2");
                }
            });
        }
        if (otherLabels.getLength() > 2) {
            Log.w(TAG, "android native alert show only 3 buttons");
        }
        alertDialog.show();
    }

    public static FREByteArray loadFileSync(Context context, String url) {
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(url, AssetManager.ACCESS_BUFFER);
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            FREByteArray byteArray = FREByteArray.newByteArray();
            byteArray.setProperty("length", FREObject.newObject(buffer.length));
            byteArray.acquire();
            byteArray.getBytes().put(buffer);
            byteArray.release();
            return byteArray;
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static void loadFileAsync(FREContext context, String url) {
        LoadFileTask loadFileTask = LoadFileTask.createFromUrl(context, url);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            loadFileTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            loadFileTask.execute();
        }
    }

    public synchronized static FREByteArray getLoadedFile(String url, FREByteArray byteArray) {
        try {
            byte[] bytes = mFiles.get(url);
            byteArray.setProperty("length", FREObject.newObject(bytes.length));
            byteArray.acquire();
            byteArray.getBytes().put(bytes);
            byteArray.release();
            return byteArray;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized static void disposeLoadedFile(String url) {
        mFiles.remove(url);
    }

    private synchronized static void onLoadFileAsyncCompleted(String url, byte[] bytes) {
        mFiles.put(url, bytes);
    }

    private static void signalLoadFileError(String url, String error) {
        try {
            _context.dispatchStatusEventAsync(LOAD_FILE_ERROR,
                    new JSONObject().put("url", url).put("message", error).toString());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static Map<String, FREFunction> getAPIMap() {
        Map<String, FREFunction> functions = new HashMap<String, FREFunction>();

        functions.put("init", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                _context = freCtx;
                _activity = freCtx.getActivity();
                Fabulous.init();
                return null;
            }
        });

        functions.put("getUDID", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getUDID());
                } catch (FREWrongThreadException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("getDeviceModel", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getDeviceModel());
                } catch (FREWrongThreadException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("getDeviceMemory", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getDeviceMemory());
                } catch (FREWrongThreadException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("getPreferredLanguage", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getPreferredLanguage());
                } catch (FREWrongThreadException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("sendMail", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.sendMail(arg1));
                } catch (Throwable e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("vibrate", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                Fabulous.vibrate();
                return null;
            }
        });

        functions.put("cancelNotification", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    Fabulous.cancelNotification(arg1[0].getAsString());
                    return null;
                } catch (Throwable e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("scheduleNotification", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    Fabulous.scheduleNotification(arg1[0].getAsString(), arg1[1].getAsString(), arg1[2].getAsInt());
                    return null;
                } catch (Throwable e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("share", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    Fabulous.share(arg1[0].getAsString(), arg1[1].getAsString(), arg1[2].getAsString(), arg1[3].getAsString(), (FREByteArray) arg1[4]);
                    return null;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });

        functions.put("showNativeAlert", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    Fabulous.showNativeAlert(freCtx, arg1);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("getUserEmail", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getUserEmail());
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("getAppExternalStorageDir", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getAppExternalStorageDir(freCtx.getActivity()));
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("getStoreType", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getStoreType());
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("getDeviceManufacturer", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Build.MANUFACTURER);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("systemSdkVersion", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Build.VERSION.SDK_INT);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("appKeyHash", new FREFunction() {
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return FREObject.newObject(Fabulous.getAppKeyHash());
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("loadFileSync", new FREFunction() {
            @Override
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                try {
                    return Fabulous.loadFileSync(freCtx.getActivity(), arg1[0].getAsString());
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("loadFileAsync", new FREFunction() {
            @Override
            public FREObject call(FREContext freCtx, FREObject[] arg1) {
                String url = null;
                try {
                    url = arg1[0].getAsString();
                    Fabulous.loadFileAsync(freCtx, url);
                } catch (Throwable e) {
                    e.printStackTrace();
                    Fabulous.signalLoadFileError(url, e.toString());
                }
                return null;
            }
        });

        functions.put("getLoadedFile", new FREFunction() {
            @Override
            public FREObject call(FREContext freContext, FREObject[] freObjects) {
                try {
                    return Fabulous.getLoadedFile(freObjects[0].getAsString(), (FREByteArray) freObjects[1]);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        functions.put("disposeLoadedFile", new FREFunction() {
            @Override
            public FREObject call(FREContext freContext, FREObject[] freObjects) {
                try {
                    Fabulous.disposeLoadedFile(freObjects[0].getAsString());
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        return functions;
    }

    abstract private static class LoadFileTask extends AsyncTask<Void, Void, byte[]> {
        protected FREContext mFREContext;
        protected Context mContext;
        protected String mUrl;

        public static LoadFileTask createFromUrl(FREContext context, String url) {
            if (url.indexOf("http") == 0) {
                return new LoadUrlFileTask(context, url);
            } else if (url.indexOf("file") == 0) {
                return new LoadDiskFileTask(context, url);
            } else {
                return new LoadAssetFileTask(context, url);
            }
        }

        public LoadFileTask(FREContext context, String url) {
            super();
            mFREContext = context;
            mContext = mFREContext.getActivity().getApplicationContext();
            mUrl = url;
        }

        protected void onPostExecute(byte[] result) {
            if (result != null) {
                try {
                    Fabulous.onLoadFileAsyncCompleted(mUrl, result);
                    mFREContext.dispatchStatusEventAsync(LOAD_FILE_COMPLETED, mUrl);
                } catch (Throwable e) {
                    Fabulous.signalLoadFileError(mUrl, e.toString());
                    e.printStackTrace();
                }
            }
        }

        private static class LoadAssetFileTask extends LoadFileTask {

            public LoadAssetFileTask(FREContext context, String url) {
                super(context, url);
            }

            protected byte[] doInBackground(Void... args) {
                InputStream inputStream = null;
                try {
                    inputStream = mContext.getAssets().open(mUrl, AssetManager.ACCESS_BUFFER);
                    byte[] buffer = new byte[inputStream.available()];
                    inputStream.read(buffer);
                    return buffer;
                } catch (Throwable e) {
                    Fabulous.signalLoadFileError(mUrl, e.toString());
                    e.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null;
            }
        }

        private static class LoadUrlFileTask extends LoadFileTask {

            public LoadUrlFileTask(FREContext context, String url) {
                super(context, url);
            }

            protected byte[] doInBackground(Void... args) {
                InputStream inputStream = null;
                try {
                    URL url = new URL(mUrl);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setConnectTimeout(5000);
                    connection.setReadTimeout(30000);
                    inputStream = connection.getInputStream();
                    return IOUtils.toByteArray(inputStream);
                } catch (Throwable e) {
                    Fabulous.signalLoadFileError(mUrl, e.toString());
                    e.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null;
            }
        }

        private static class LoadDiskFileTask extends LoadFileTask {

            public LoadDiskFileTask(FREContext context, String url) {
                super(context, url);
            }

            protected byte[] doInBackground(Void... args) {
                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(Uri.parse(mUrl).getPath());
                    return IOUtils.toByteArray(inputStream);
                } catch (Throwable e) {
                    Fabulous.signalLoadFileError(mUrl, e.toString());
                    e.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null;
            }
        }
    }
}
