package fabulous.ane;

import java.util.Map;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;

public class FabulousContext extends FREContext {
	@Override
	public void dispose() {
		Initializer.context = null;
	}

	@Override
	public Map<String, FREFunction> getFunctions() {
		return Fabulous.getAPIMap();
	}
}
