package fabulous.ane;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;

public class Initializer implements FREExtension {
	public static FREContext context;

	public FREContext createContext(String extId) {
		return context = new FabulousContext();
	}

	public void dispose() {
		context = null;
	}

	public void initialize() {}

	public static void log(String message) {
		if (context != null && message != null) {
 		   	context.dispatchStatusEventAsync("log", message);
		}
	}
}
