#import "FlashRuntimeExtensions.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <iAd/iAd.h>
#import "ASIHTTPRequest.h"

@interface Fabulous : NSObject

+ (UIViewController*)getViewController;
+ (void)init;
+ (void)dispatchEvent:(NSString *)event withMessage:(NSString *)message;
+ (void)log:(NSString *)string, ...;
+ (NSString*) getUDID;
+ (NSString*) getDeviceModel;
+ (double) getDeviceMemory;
+ (NSString*) getPreferredLanguage;
+ (bool) sendMail:(NSString *)subject message:(NSString *)message to:(NSString*)to attachment:(NSData *)attachment attachmentMime:(NSString *)attachmentMime attachmentName:(NSString *)attachmentName;
+ (void) vibrate;
+ (void) scheduleNotification:(NSString*)name withMessage:(NSString*)message withTimeout:(NSDate*)fireDate;
+ (void) cancelNotification:(NSString*)name;
+ (void) showNativeAlert:(NSString*)title message:(NSString*)message cancelLabel:(NSString*)cancelLabel labels:(NSArray*)labels;
+ (void) prepareFullScreenAds;
+ (void) showFullScreenAd;
+ (void) prepareBannerAd;
+ (void) showBannerAd;
+ (void) hideBannerAd;

+ (void) scheduleAllNotifications;
+ (void) onApplicationDidEnterBackground:(UIApplication *)application ;
+ (void) onApplicationWillTerminate:(UIApplication *)application;
+ (void) onApplicationDidBecomeActive:(UIApplication *)application;

+ (FREObject *) loadFileSync:(NSString *)url withByteArray:(FREObject *)byteArray;
+ (void) loadFileAsync:(NSString *)url;
+ (FREObject *) getLoadedFile:(NSString *)url withByteArray:(FREObject *)byteArray;
+ (void) disposeLoadedFile:(NSString *)url;

@end

@interface MailerDelegate : NSObject<MFMailComposeViewControllerDelegate>
@end

@interface NativeAlertDelegate: NSObject<UIAlertViewDelegate>
@end

@interface FullScreenAdDelegate: NSObject<ADInterstitialAdDelegate>
@property (nonatomic, retain) ADInterstitialAd *interstitial;
@property (nonatomic, assign) bool isLoaded;
@property (nonatomic, assign) bool isLoading;
@end


@interface BannerAdDelegate : NSObject<ADBannerViewDelegate>
@property (nonatomic, strong) ADBannerView* bannerView;
@property (nonatomic, assign) bool isLoading;
@end

@interface ASIHTTPRequestDelegate : NSObject
@property (nonatomic, readonly) NSString *url;
- (id) initWithUrl:(NSString *)url;
- (void) requestFinished:(ASIHTTPRequest *)request;
- (void) requestFailed:(ASIHTTPRequest *)request;
@end


//----------------- Helpers
NSString *DebugFREObjectToString(FREObject object);
NSArray *DebugFREArrayToStringArray(FREObject object);

//----------------- ANE API Functions
FREObject Fabulous_init(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_getUDID(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_getDeviceModel(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_getDeviceMemory(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_getDeviceManufacturer(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_getPreferredLanguage(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_sendMail(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_vibrate(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_scheduleNotification(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_cancelNotification(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_showNativeAlert(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_prepareFullScreenAds(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_showFullScreenAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_prepareBannerAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_showBannerAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_hideBannerAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_share(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_getUserEmail(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_loadFileSync(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_loadFileAsync(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_getLoadedFile(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);
FREObject Fabulous_disposeLoadedFile(FREContext context, void* functionData, uint32_t argc, FREObject argv[]);

//----------------- ANE Initialization Functions
void ANEContextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctionsToTest, const FRENamedFunction** functionsToSet);
void ANEContextFinalizer(FREContext ctx);
void ANEInitializer(void** extDataToSet, FREContextInitializer* ctxInitializerToSet, FREContextFinalizer* ctxFinalizerToSet);
void ANEFinalizer(void *extData);
