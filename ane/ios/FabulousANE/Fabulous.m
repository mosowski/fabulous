#import "Fabulous.h"
#import "OpenUDID.h"
#import "ASIHTTPRequest.h"

#import <Foundation/Foundation.h>
#import <AdSupport/AdSupport.h>
#import <AudioToolbox/AudioToolbox.h>

#import <sys/utsname.h>
#import <mach/mach.h>
#import <mach/mach_host.h>

FREContext FabulousCtx = nil;

@implementation Fabulous

static MailerDelegate *mailerDelegate;
static NativeAlertDelegate *nativeAlertDelegate;
static FullScreenAdDelegate *fullScreenAdDelegate;
static BannerAdDelegate *bannerAdDelegate;
static UIViewController *viewController;
static UIApplication *application;
static UIWindow *window;
static NSMutableDictionary *notifications;
static UIView *rootView;
static NSMutableDictionary *files;

+ (UIViewController*)getViewController {
    return viewController;
}

+ (void)init {
    mailerDelegate = [[MailerDelegate alloc] init];
    nativeAlertDelegate = [[NativeAlertDelegate alloc] init];
    fullScreenAdDelegate = [[FullScreenAdDelegate alloc] init];
    bannerAdDelegate = [[BannerAdDelegate alloc] init];
    
    application = [UIApplication sharedApplication];
    window = [application keyWindow];
    viewController = [window rootViewController];
    rootView = [viewController view];
    notifications = [[NSMutableDictionary alloc] init];
    files = [[NSMutableDictionary alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:[self class] selector:@selector(onApplicationDidEnterBackground:) name: UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:[self class] selector:@selector(onApplicationWillTerminate:) name: UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:[self class] selector:@selector(onApplicationDidBecomeActive:) name: UIApplicationDidBecomeActiveNotification object:nil];
}

+ (void)dispatchEvent:(NSString *)event withMessage:(NSString *)message {
    NSString *messageText = message ? message : @"";
    FREDispatchStatusEventAsync(FabulousCtx, (const uint8_t *)[event UTF8String], (const uint8_t *)[messageText UTF8String]);
}


+ (void)log:(NSString *)format, ... {
    @try {
        va_list args;
        va_start(args, format);
        NSString *string = [[NSString alloc] initWithFormat:format arguments:args];
        NSLog(@"%@", string);
        [Fabulous dispatchEvent:@"log" withMessage:string];
    } @catch (NSException *exception) {
        NSLog(@"[Fabulous] Couldn't log message. Exception: %@", exception);
    }
    
}

+ (NSString*) getUDID {
    return [OpenUDID value];
    //    if (!NSClassFromString(@"ASIdentifierManager")) {
    //        return [OpenUDID value];
    //    }
    //    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}


+ (NSString*) getDeviceModel {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithFormat:@"ios,%@", [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]];
}

+ (NSString*) getDeviceManufacturer {
    return @"Apple";
}

+ (double) getDeviceMemory {
    mach_port_t hostPort;
    mach_msg_type_number_t hostSize;
    vm_size_t pageSize;
    hostPort = mach_host_self();
    hostSize = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(hostPort, &pageSize);
    vm_statistics_data_t vmStat;
    if (host_statistics(hostPort, HOST_VM_INFO, (host_info_t)&vmStat, &hostSize) != KERN_SUCCESS) {
        return -1; // problem z pobraniem statow
    }
    return (double)(vmStat.free_count * pageSize);
}

+ (NSString*) getPreferredLanguage {
    return [[NSLocale preferredLanguages] objectAtIndex: 0];
}

+ (bool) sendMail:(NSString *)subject message:(NSString *)message to:(NSString*)to attachment:(NSData *)attachment attachmentMime:(NSString *)attachmentMime attachmentName:(NSString *)attachmentName {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *composer = [[MFMailComposeViewController alloc] init];
        composer.mailComposeDelegate = mailerDelegate;
        [composer setSubject:subject];
        [composer setMessageBody:message isHTML:NO ];
        [composer setToRecipients:[NSArray arrayWithObject:to]];
        if (attachment != nil) {
            [composer addAttachmentData:attachment mimeType:attachmentMime fileName:attachmentName];
        }
        [viewController presentModalViewController:composer animated:YES];
        [composer release];
        return true;
    } else {
        return false;
    }
}

+ (void) vibrate {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}


+ (void) scheduleNotification:(NSString*)name withMessage:(NSString*)message withTimeout:(NSDate*)fireDate {
    [Fabulous log:@"scheduleLocalNotification %@ %@",name,message];
    NSMutableDictionary *notificationInfo = [[NSMutableDictionary alloc] init];
    [notificationInfo setValue:message forKey:@"message"];
    [notificationInfo setValue:fireDate forKey:@"fireDate"];
    [notifications setValue:notificationInfo forKey:name];
}

+ (void) cancelNotification:(NSString*)name {
    [notifications setValue:nil forKey:name];
}

+ (void) showNativeAlert:(NSString*)title message:(NSString*)message cancelLabel:(NSString*)cancelLabel labels:(NSArray*)labels {
    UIAlertView *popup = [[UIAlertView alloc] initWithTitle:title message:message delegate:nativeAlertDelegate cancelButtonTitle:cancelLabel otherButtonTitles:nil];
    for (int i=0; i<[labels count]; ++i) {
        [popup addButtonWithTitle:[labels objectAtIndex:i]];
    }
    [popup show];
}

+ (void) prepareFullScreenAds {
    if (!fullScreenAdDelegate.isLoaded && !fullScreenAdDelegate.isLoading) {
        if (fullScreenAdDelegate.interstitial != nil) {
            [fullScreenAdDelegate.interstitial release];
        }
        fullScreenAdDelegate.isLoading = true;
        fullScreenAdDelegate.interstitial = [[ADInterstitialAd alloc] init];
        [Fabulous log:@"Fabulous ANE: Started loading fullScreen ad."];
        
        fullScreenAdDelegate.interstitial.delegate = fullScreenAdDelegate;
    }
}

+ (void) showFullScreenAd {
    if (fullScreenAdDelegate.isLoaded) {
        [fullScreenAdDelegate.interstitial presentFromViewController:viewController];
    }
}

+ (void) prepareBannerAd {
    if (!bannerAdDelegate.isLoading) {
        ADBannerView* bannerView = bannerAdDelegate.bannerView;
        if (bannerView != nil) {
            if ([bannerView superview] != nil) {
                [bannerView removeFromSuperview];
            }
            [bannerView release];
        }
        
        CGRect frame = CGRectZero;
        frame.size = [ADBannerView sizeFromBannerContentSizeIdentifier:ADBannerContentSizeIdentifierPortrait];
        frame.origin = CGPointMake(0.0, 0.0);
        
        bannerAdDelegate.isLoading = true;
        bannerView = [[ADBannerView alloc] initWithFrame: frame];
        bannerView.delegate = bannerAdDelegate;
        [bannerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        
        bannerAdDelegate.bannerView = bannerView;
    }
}

+ (void) showBannerAd {
    if (!bannerAdDelegate.isLoading) {
        ADBannerView* bannerView = bannerAdDelegate.bannerView;
        if ([bannerView superview] != nil) {
            [bannerView removeFromSuperview];
        }
        
        [[viewController view] addSubview:bannerView];
        [Fabulous log:@"Fabulous ANE: Displaying a banner ad."];
    }
}

+ (void) hideBannerAd {
    ADBannerView* bannerView = bannerAdDelegate.bannerView;
    if (bannerView != nil) {
        if ([bannerView superview] != nil) {
            [bannerView removeFromSuperview];
        }
        [bannerView release];
        bannerAdDelegate.bannerView = nil;
    }
}

+ (void) scheduleAllNotifications {
    for (id key in notifications) {
        NSMutableDictionary *info = [notifications objectForKey:key];
        if (info != nil) {
            NSDate *fireDate = [info objectForKey:@"fireDate"];
            if ([fireDate timeIntervalSinceNow] > 0) {
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                notification.fireDate = [info objectForKey:@"fireDate"];
                notification.alertBody = [info objectForKey:@"message"];
                [application scheduleLocalNotification: notification];
            }
        }
    }
}

+ (void) share:(NSString*)message withTitle:(NSString*)title withImage:(NSString*)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    NSArray *postItems = @[message, image];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]
                                            initWithActivityItems:postItems
                                            applicationActivities:nil];
    [activityVC setValue:title forKey:@"subject"];
    activityVC.excludedActivityTypes = @[UIActivityTypeSaveToCameraRoll, UIActivityTypeCopyToPasteboard, UIActivityTypePrint, UIActivityTypeAirDrop, UIActivityTypeAssignToContact];
    UIViewController *mainViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [mainViewController presentViewController:activityVC animated:YES completion:nil];
}

+ (void) onApplicationDidEnterBackground:(UIApplication *)application {
    // [Fabulous scheduleAllNotifications];
}
+ (void) onApplicationWillTerminate:(UIApplication *)application {
    
}
+ (void) onApplicationDidBecomeActive:(UIApplication *)_application {
    //  [application cancelAllLocalNotifications];
}

+ (FREObject *) copyDataToFREByteArray:(NSData *)data withByteArray:(FREObject *)byteArrayObject {
    if (data != nil) {
        FREObject length;
        FREByteArray byteArray;
        FRENewObjectFromUint32((uint32_t)data.length, &length);
        FRESetObjectProperty(byteArrayObject, (uint8_t*)"length", length, NULL);
        FREAcquireByteArray(byteArrayObject, &byteArray);
        memcpy(byteArray.bytes, data.bytes, data.length);
        FREReleaseByteArray(byteArrayObject);
        return byteArrayObject;
    } else {
        return nil;
    }
}

+ (NSData *) readDataFromUrl:(NSString *)url error:(NSError **)error {
    NSString *path = nil;
    if ([url hasPrefix:@"file"]) {
        path = [[NSURL URLWithString:url] path];
    } else {
        path = [[NSBundle mainBundle] pathForResource:url ofType:nil];
    }
    if (path != nil) {
        return [NSData dataWithContentsOfFile:path options:NSDataReadingMappedIfSafe error:error];
    } else {
        *error = [[NSError alloc] initWithDomain:NSCocoaErrorDomain code:NSFileNoSuchFileError
                                        userInfo:@{NSLocalizedDescriptionKey:
                                                       [NSString stringWithFormat:@"NSFileNoSuchFileError: File does not exist: %@", url]}];
        return nil;
    }
}

+ (void) loadFileFromUrl:(NSString *)url {
    NSError *error = nil;
    NSData *data = [Fabulous readDataFromUrl:url error:&error];
    if (error != nil) {
        [Fabulous onFileLoadError:url withMessage:[error localizedDescription]];
    } else {
        [Fabulous onFileLoadCompleted:url withData:data];
    }
}

+ (FREObject *) loadFileSync:(NSString *)urlString withByteArray:(FREObject *) byteArrayObject {
    NSData *data = nil;
    if ([urlString hasPrefix:@"http"]) {
        NSURL *url = [NSURL URLWithString:urlString];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        if (error != nil) {
            [Fabulous onFileLoadError:urlString withMessage:[error localizedDescription]];
        } else {
            data = [request responseData];
        }
    } else {
        NSError *error = nil;
        data = [Fabulous readDataFromUrl:urlString error:&error];
        if (error != nil) {
            [Fabulous onFileLoadError:urlString withMessage:[error localizedDescription]];
        }
        
    }
    return [Fabulous copyDataToFREByteArray:data withByteArray: byteArrayObject];
}

+ (void) loadFileAsync:(NSString *)urlString {
    if ([urlString hasPrefix:@"http"]) {
        NSURL *url = [NSURL URLWithString:urlString];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:[[ASIHTTPRequestDelegate alloc] initWithUrl:urlString]];
        [request startAsynchronous];
    } else {
        UIApplication * application = [UIApplication sharedApplication];
        if([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]) {
            __block UIBackgroundTaskIdentifier background_task;
            background_task = [application beginBackgroundTaskWithExpirationHandler:^ {
                [Fabulous onFileLoadError:urlString withMessage:@"Operation timed out"];
                [application endBackgroundTask: background_task];
                background_task = UIBackgroundTaskInvalid;
            }];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [Fabulous loadFileFromUrl:urlString];
                [application endBackgroundTask: background_task];
                background_task = UIBackgroundTaskInvalid; 
            });
        }
        else {
            [Fabulous loadFileFromUrl:urlString];
        }
    }
}

+ (FREObject *) getLoadedFile:(NSString *)url withByteArray:(FREObject *)byteArray {
    NSData *data;
    @synchronized (files) {
        data = (NSData *)[files objectForKey:url];
    }
    return [Fabulous copyDataToFREByteArray:data withByteArray:byteArray];
}

+ (void) disposeLoadedFile:(NSString *)url {
    @synchronized (files) {
        [files removeObjectForKey:url];
    }
}

+ (void) onFileLoadCompleted:(NSString *)url withData:(NSData *)data {
    @synchronized (files) {
        [files setObject:data forKey:url];
    }
    [Fabulous dispatchEvent:@"loadFileCompleted" withMessage:url];
}

+ (void) onFileLoadError:(NSString *)url withMessage:(NSString *)errorMessage {
    [Fabulous dispatchEvent:@"loadFileError" withMessage:[Fabulous toJSON:@{@"url":url, @"message":errorMessage}]];
}

+ (NSString *) toJSON:(id) object {
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:0 error:&error];
    NSString* json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return error != nil ? nil : json;
}

@end

@implementation ASIHTTPRequestDelegate : NSObject
- (id) initWithUrl:(NSString *)url {
    self = [super init];
    _url = url;
    return self;
}
- (void) requestFinished:(ASIHTTPRequest *)request {
    [Fabulous onFileLoadCompleted:_url withData:[request responseData]];
}

- (void) requestFailed:(ASIHTTPRequest *)request {
    [Fabulous onFileLoadError:_url withMessage:[[request error] localizedDescription]];
}
@end


@implementation MailerDelegate
-(void) mailComposeController: (MFMailComposeViewController*)controller didFinishWithResult: (MFMailComposeResult)result error:(NSError*)error {
    [[Fabulous getViewController] dismissModalViewControllerAnimated:YES];
}
@end

@implementation NativeAlertDelegate
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [Fabulous dispatchEvent:@"nativeAlertButtonClicked" withMessage:[NSString stringWithFormat:@"%ld",(long)buttonIndex]];
}
@end

@implementation FullScreenAdDelegate
@synthesize interstitial;
@synthesize isLoaded;
@synthesize isLoading;
-(void)interstitialAdDidLoad:(ADInterstitialAd*)interstitialAd {
    [Fabulous log:@"Fabulous ANE: FullScreen iAd loaded."];
    isLoaded = true;
    isLoading = false;
    [Fabulous dispatchEvent:@"fullScreenAdLoaded" withMessage:@""];
}
-(void)interstitialAdDidUnload:(ADInterstitialAd*)interstitialAd {
    [Fabulous log:@"Fabulous ANE: FullScreen iAd unloaded."];
    isLoaded = false;
}
-(void)interstitialAdActionDidFinish:(ADInterstitialAd*)interstitialAd {
    [Fabulous log:@"Fabulous ANE: FullScreen iAd finished."];
    isLoaded = false;
}
-(void)interstitialAd:(ADInterstitialAd*)interstitialAd didFailWithError:(NSError*)error {
    [Fabulous log:@"Fabulous ANE: FullScreen iAd loading failed."];
    isLoading = false;
    [Fabulous dispatchEvent:@"fullScreenAdFailed" withMessage:@""];
}
@end

@implementation BannerAdDelegate
@synthesize bannerView;
@synthesize isLoading;
-(void)bannerViewDidLoadAd:(ADBannerView*)bannerAdView {
    [Fabulous log:@"Fabulous ANE: Banner iAd loaded."];
    isLoading = false;
    [Fabulous dispatchEvent:@"bannerAdLoaded" withMessage:@""];
}
-(void)bannerView:(ADBannerView*)bannerAdView didFailToReceiveAdWithError:(NSError*)error {
    [Fabulous log:@"Fabulous ANE: Banner iAd loading failed."];
    isLoading = false;
    [Fabulous dispatchEvent:@"bannerAdFailed" withMessage:@""];
}
@end


//----------------------------- Helpers
NSString *DebugFREObjectToString(FREObject object) {
    uint32_t stringLength;
    const uint8_t *rawString;
    NSString *result = nil;
    if (FREGetObjectAsUTF8(object, &stringLength, &rawString) == FRE_OK)  {
        result = [NSString stringWithUTF8String:(char*)rawString];
    } else {
        [Fabulous log:@"Fabulous ANE Error. Cannot convert FREObject to NSString"];
    }
    return result;
}

NSArray *DebugFREArrayToStringArray(FREObject object) {
    uint32_t arrayLength;
    NSMutableArray *tmp = [[NSMutableArray alloc] init];
    
    FREGetArrayLength(object, &arrayLength);
    for (int i=0; i < arrayLength; ++i) {
        FREObject element;
        FREGetArrayElementAt(object, i, &element);
        [tmp addObject:DebugFREObjectToString(element)];
    }
    return [NSArray arrayWithArray:tmp];
}


//----------------------------- ANE API
FREObject Fabulous_init(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous init];
    return nil;
}

FREObject Fabulous_getUDID(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString* udid = [Fabulous getUDID];
    FREObject result;
    FRENewObjectFromUTF8((uint32_t)udid.length, (const uint8_t *)[udid UTF8String], &result);
    return result;
}

FREObject Fabulous_getDeviceModel(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString* model = [Fabulous getDeviceModel];
    FREObject result;
    FRENewObjectFromUTF8((uint32_t)model.length, (const uint8_t *)[model UTF8String], &result);
    return result;
}

FREObject Fabulous_getDeviceManufacturer(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString* manufacturer = [Fabulous getDeviceManufacturer];
    FREObject result;
    FRENewObjectFromUTF8((uint32_t)manufacturer.length, (const uint8_t *)[manufacturer UTF8String], &result);
    return result;
}

FREObject Fabulous_getDeviceMemory(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    FREObject result;
    FRENewObjectFromDouble([Fabulous getDeviceMemory], &result);
    return result;
}

FREObject Fabulous_getPreferredLanguage(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString* str = [Fabulous getPreferredLanguage];
    FREObject result;
    FRENewObjectFromUTF8((uint32_t)str.length, (const uint8_t *)[str UTF8String], &result);
    return result;
}

FREObject Fabulous_sendMail(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString *subject = nil;
    NSString *message = nil;
    NSString *to = nil;
    FREByteArray byteArray;
    NSData* attachmentData = nil;
    NSString *attachmentMime = nil;
    NSString *attachmentName = nil;
    FREObject result;
    
    subject = DebugFREObjectToString(argv[0]);
    message = DebugFREObjectToString(argv[1]);
    to = DebugFREObjectToString(argv[2]);
    
    if (FREAcquireByteArray(argv[3], &byteArray) == FRE_OK)  {
        attachmentData = [NSData dataWithBytes:byteArray.bytes length:byteArray.length];
        FREReleaseByteArray(argv[3]);
        
        attachmentMime = DebugFREObjectToString(argv[4]);
        attachmentName = DebugFREObjectToString(argv[5]);
    }
    
    bool success = [Fabulous sendMail:subject message:message to:to attachment:attachmentData attachmentMime:attachmentMime attachmentName:attachmentName];
    FRENewObjectFromBool(success, &result);
    
    return result;
}

FREObject Fabulous_vibrate(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous vibrate];
    return nil;
}

FREObject Fabulous_scheduleNotification(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString *name= nil;
    NSString *message = nil;
    int32_t timeout = 0;
    name = DebugFREObjectToString(argv[0]);
    message = DebugFREObjectToString(argv[1]);
    FREGetObjectAsInt32(argv[2], &timeout);
    
    
    
    [Fabulous scheduleNotification:name withMessage:message withTimeout:[[NSDate alloc] initWithTimeIntervalSinceNow:timeout]];
    return nil;
}



FREObject Fabulous_cancelNotification(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString *name= nil;
    name = DebugFREObjectToString(argv[0]);
    [Fabulous cancelNotification:name];
    return nil;
}


FREObject Fabulous_showNativeAlert(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString *title = nil;
    NSString *message = nil;
    NSString *cancelLabel= nil;
    NSArray *otherLabels = [[NSArray alloc] init];
    title = DebugFREObjectToString(argv[0]);
    message = DebugFREObjectToString(argv[1]);
    cancelLabel = DebugFREObjectToString(argv[2]);
    otherLabels = DebugFREArrayToStringArray(argv[3]);
    [Fabulous showNativeAlert:title message:message cancelLabel:cancelLabel labels:otherLabels];
    return nil;
}

FREObject Fabulous_prepareFullScreenAds(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous prepareFullScreenAds];
    return nil;
}

FREObject Fabulous_showFullScreenAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous showFullScreenAd];
    return nil;
}

FREObject Fabulous_prepareBannerAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous prepareBannerAd];
    return nil;
}

FREObject Fabulous_showBannerAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous showBannerAd];
    return nil;
}

FREObject Fabulous_hideBannerAd(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous hideBannerAd];
    return nil;
}

FREObject Fabulous_share(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    NSString *title = DebugFREObjectToString(argv[0]);
    NSString *message = DebugFREObjectToString(argv[1]);
    NSString *image = DebugFREObjectToString(argv[2]);
    [Fabulous share:message withTitle:title withImage:image];
    return nil;
}

FREObject Fabulous_getUserEmail(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    return nil;
}

FREObject Fabulous_loadFileSync(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    return [Fabulous loadFileSync:DebugFREObjectToString(argv[0]) withByteArray:argv[1]];
}

FREObject Fabulous_loadFileAsync(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous loadFileAsync:DebugFREObjectToString(argv[0])];
    return nil;
}

FREObject Fabulous_getLoadedFile(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    return [Fabulous getLoadedFile:DebugFREObjectToString(argv[0]) withByteArray:(FREObject *) argv[1]];
}

FREObject Fabulous_disposeLoadedFile(FREContext context, void* functionData, uint32_t argc, FREObject argv[]) {
    [Fabulous disposeLoadedFile:DebugFREObjectToString(argv[0])];
    return nil;
}


//----------------------------- ANE Initialization
void ANEContextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctionsToTest, const FRENamedFunction** functionsToSet) {
    NSInteger numAPIFunctions = 22;
    *numFunctionsToTest = (uint32_t)numAPIFunctions;
    
    FRENamedFunction* func = (FRENamedFunction*) malloc(sizeof(FRENamedFunction) * numAPIFunctions);
    
    func[0].name = (const uint8_t*) "init";
    func[0].functionData = NULL;
    func[0].function = &Fabulous_init;
    
    func[1].name = (const uint8_t*) "getUDID";
    func[1].functionData = NULL;
    func[1].function = &Fabulous_getUDID;
    
    func[2].name = (const uint8_t*) "getDeviceModel";
    func[2].functionData = NULL;
    func[2].function = &Fabulous_getDeviceModel;
    
    func[3].name = (const uint8_t*) "getDeviceMemory";
    func[3].functionData = NULL;
    func[3].function = &Fabulous_getDeviceMemory;
    
    func[4].name = (const uint8_t*) "getPreferredLanguage";
    func[4].functionData = NULL;
    func[4].function = &Fabulous_getPreferredLanguage;
    
    func[5].name = (const uint8_t*) "sendMail";
    func[5].functionData = NULL;
    func[5].function = &Fabulous_sendMail;
    
    func[6].name = (const uint8_t*) "vibrate";
    func[6].functionData = NULL;
    func[6].function = &Fabulous_vibrate;
    
    func[7].name = (const uint8_t*) "scheduleNotification";
    func[7].functionData = NULL;
    func[7].function = &Fabulous_scheduleNotification;
    
    func[8].name = (const uint8_t*) "cancelNotification";
    func[8].functionData = NULL;
    func[8].function = &Fabulous_cancelNotification;
    
    func[9].name = (const uint8_t*) "showNativeAlert";
    func[9].functionData = NULL;
    func[9].function = &Fabulous_showNativeAlert;
    
    func[10].name = (const uint8_t*) "prepareFullScreenAds";
    func[10].functionData = NULL;
    func[10].function = &Fabulous_prepareFullScreenAds;
    
    func[11].name = (const uint8_t*) "showFullScreenAd";
    func[11].functionData = NULL;
    func[11].function = &Fabulous_showFullScreenAd;
    
    func[12].name = (const uint8_t*) "prepareBannerAd";
    func[12].functionData = NULL;
    func[12].function = &Fabulous_prepareBannerAd;
    
    func[13].name = (const uint8_t*) "showBannerAd";
    func[13].functionData = NULL;
    func[13].function = &Fabulous_showBannerAd;
    
    func[14].name = (const uint8_t*) "hideBannerAd";
    func[14].functionData = NULL;
    func[14].function = &Fabulous_hideBannerAd;
    
    func[15].name = (const uint8_t*) "share";
    func[15].functionData = NULL;
    func[15].function = &Fabulous_share;
    
    func[16].name = (const uint8_t*) "getUserEmail";
    func[16].functionData = NULL;
    func[16].function = &Fabulous_getUserEmail;
    
    func[17].name = (const uint8_t*) "getDeviceManufacturer";
    func[17].functionData = NULL;
    func[17].function = &Fabulous_getDeviceManufacturer;
    
    func[18].name = (const uint8_t*) "loadFileSync";
    func[18].functionData = NULL;
    func[18].function = &Fabulous_loadFileSync;
    
    func[19].name = (const uint8_t*) "loadFileAsync";
    func[19].functionData = NULL;
    func[19].function = &Fabulous_loadFileAsync;
    
    func[20].name = (const uint8_t*) "getLoadedFile";
    func[20].functionData = NULL;
    func[20].function = &Fabulous_getLoadedFile;
    
    func[21].name = (const uint8_t*) "disposeLoadedFile";
    func[21].functionData = NULL;
    func[21].function = &Fabulous_disposeLoadedFile;
    
    *functionsToSet = func;
    
    FabulousCtx = ctx;
}

void ANEContextFinalizer(FREContext ctx) {
}

void ANEInitializer(void** extDataToSet, FREContextInitializer* ctxInitializerToSet, FREContextFinalizer* ctxFinalizerToSet)
{
    *extDataToSet = NULL;
    *ctxInitializerToSet = &ANEContextInitializer;
    *ctxFinalizerToSet = &ANEContextFinalizer;
}

void ANEFinalizer(void *extData) {
}
