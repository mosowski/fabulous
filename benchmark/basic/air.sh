function compile
{
	if [[ $# -ne 0 ]]
	then
		echo "compile:"
	else
		for COMMAND_ID in ${!PRE_COMPILE_COMMANDS[*]}
		do 
			${PRE_COMPILE_COMMANDS[$COMMAND_ID]}
		done

		echo "Compiling..."
		echo "Flash SDK path: $AIR_SDK"

		echo "Compiler: "`$AIR_SDK/bin/amxmlc -version`
		$AIR_SDK/bin/amxmlc $COMPILER_OPTIONS -debug=$DEBUG \
			-load-config "$AIR_SDK/frameworks/$AIR_CONFIG" \
			$MAIN_CLASS -output $SWF \
			$SOURCE_PATHS \
			$LIBRARY_PATHS
	fi
}

function launch 
{
	if [[ $# -ne 1 ]]
	then
		echo "launch <DEVICE_SIZE>"
		echo "where"
		echo "  DEVICE_SIZE: android | iphone4 | ipad | ipad3 | iphone5 | desktop"
	else
		if [[ $1 == "desktop" ]]
		then
			$AIR_SDK/bin/adl -profile extendedDesktop $APP_DESCRIPTOR $BINDIR $EXTDIR
		else
			size_android="480x816:480x854"
			size_galaxys2="480x762:480x800"
			size_galaxys3="720x1220:720x1280"
			size_iphone3="320x460:320x480"
			size_iphone4="640x920:640x960"
			size_iphone5="640x1096:640x1136"
			size_ipad="768x1004:768x1024"
			size_ipad3="1536x2008:1536x2048"
			size_square="800x800:800x800"

			SCREEN_CHOICE="size_$1"

			EXTDIR_ADL=""
			if [[ "$EXTDIR" != "" ]]
			then
				EXTDIR_ADL="$EXTDIR/adl"
			fi	

			$AIR_SDK/bin/adl -profile extendedMobileDevice -screensize ${!SCREEN_CHOICE} $EXTDIR_ADL $APP_DESCRIPTOR $BINDIR
		fi
	fi
}

function install_ios {
	echo "Installing: iOS"
	$AIR_SDK/bin/adt -installApp -platform ios -package $PACKAGE_NAME.ipa
}

function install_android {
	echo "Installing: Android"
	$AIR_SDK/bin/adt -installApp -platform android -package $PACKAGE_NAME.apk
}

function install {
	if [[ $# -ne 1 ]]
	then
		echo "usage: install <PLATFORM>"
		echo "  PLATFORM : ios | android"
	else
		PLATFORM=$1

		echo "Installing package..."

		install_$PLATFORM
			
		echo "Done."
	fi
}

function platform_ios 
{
	echo "Platform: iOS"
	$AIR_SDK/bin/adt -package -target $PACKAGE_TYPE -sampler -storetype pkcs12 -keystore "ios-cert/$IOS_CERT_NAME.p12" -storepass $IOS_CERT_PASS -provisioning-profile "ios-cert/$IOS_CERT_NAME.mobileprovision" $PACKAGE_NAME $PACKAGE_FILES $EXTDIR
}

function platform_simulator 
{
	echo "Platform: iOS Simulator"
	$AIR_SDK/bin/adt -package -target $PACKAGE_TYPE -sampler -storetype pkcs12 -keystore "ios-cert/$IOS_CERT_NAME.p12" -storepass $IOS_CERT_PASS -provisioning-profile "ios-cert/$IOS_CERT_NAME.mobileprovision" $PACKAGE_NAME $PACKAGE_FILES -platformsdk $PLATFORM_SDK $EXTDIR
	echo "Installing application on Simulator..."
	$AIR_SDK/bin/adt -installApp -platform ios -platformsdk $PLATFORM_SDK -device ios-simulator -package $PACKAGE_NAME.ipa
	echo "Launching..."
	$AIR_SDK/bin/adt -launchApp -platform ios -platformsdk $PLATFORM_SDK -device ios-simulator -appid $APP_ID
}

function platform_android 
{
	echo "Platform: Android"
	$AIR_SDK/bin/adt -package -target $PACKAGE_TYPE -storetype pkcs12 -keystore "android-cert/$AND_CERT_NAME" -storepass $AND_CERT_PASS $PACKAGE_NAME $PACKAGE_FILES $EXTDIR
}

function platform_desktop
{
	echo "Platform: Desktop"
	$AIR_SDK/bin/adt -package -storetype pkcs12 -keystore "desktop-cert/$DESKTOP_CERT_NAME" -storepass $DESKTOP_CERT_PASS -target $PACKAGE_TYPE $PACKAGE_NAME $PACKAGE_FILES $EXTDIR
}

function build_swc {
	LIB_DIR=$1
	LIB_NAME=$2
	OUTDATED=false
	LIB_TIMESTAMP=0

	echo "Building SWC for $1: $2"

	if [ -f $LIB_NAME ];
	then
		LIB_TIMESTAMP=`stat -f '%m' $LIB_NAME`
	fi

	for TIMESTAMP in $(find $LIB_DIR -type f | xargs stat -f '%m');
	do 
		if [ $TIMESTAMP -gt $LIB_TIMESTAMP ];
		then
			OUTDATED=true
		fi
	done 

	if $OUTDATED ;
	then
		echo "Lib $LIB_NAME requires recompilation."
		echo "Compiler: "`$AIR_SDK/bin/acompc -version`
		$AIR_SDK/bin/acompc $COMPILER_OPTIONS -debug=$DEBUG \
			-include-sources $LIB_DIR \
			-output $LIB_NAME
	fi
}

function package 
{
	if [[ $# -ne 2 ]]
	then
		echo "usage: package <PLATFORM> <PACKAGE_TYPE>"
		echo "  PLATFORM : ios | simlator | android | desktop"
   		echo "  PACKAGE_TYPE : ipa-ad-hoc | ipa-test | ipa-test-interpreter | ipa-test-interpreter-simulator | apk | apk-debug | air"
	else
		PLATFORM=$1
		PACKAGE_TYPE=$2
	
		VERSION=`cat version | tail -n 1`
		VERSION=$((VERSION + 1))
		echo $VERSION > version
		sed 's#<versionNumber>.*#<versionNumber>1.'$VERSION'</versionNumber>#' $APP_DESCRIPTOR > $APP_DESCRIPTOR".tmp"
		mv $APP_DESCRIPTOR".tmp" $APP_DESCRIPTOR

		echo "Packaging AIR..."
		echo "Build target: $PACKAGE_TYPE"
		echo "Version: 1.$VERSION"

		platform_$PLATFORM
			
		echo "Done."
	fi
}


if [[ $# -le 1 ]]
then
	echo "usage: air.sh <APPCONFIG> <COMMAND> [ params ]"
else

	CONFIG_SCRIPT=$1
	source $CONFIG_SCRIPT

	$2 "${@:3}"
fi
