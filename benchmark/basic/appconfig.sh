export AIR_SDK="path to your air sdk"

export APP_ID="com.foo.bar"
export PACKAGE_NAME="BasicBenchmark"

export AND_CERT_NAME="###"
export AND_CERT_PASS="###"

export IOS_CERT_NAME=":)"
export IOS_CERT_PASS=":)"

export AIR_CONFIG="airmobile-config.xml"

export APP_DESCRIPTOR="./descriptor.xml"
export BINDIR="./bin"
export SWF="$BINDIR/game.swf"

export PACKAGE_FILES="$APP_DESCRIPTOR -C $BINDIR game.swf -C $BINDIR static"

export DEBUG="false"

export COMPILER_OPTIONS="-advanced-telemetry -mobile -optimize -compress=false -swf-version=22"

export PRE_COMPILE_COMMANDS=()
export SOURCE_PATHS="-source-path ../../src"
export LIBRARY_PATHS=""
export MAIN_CLASS="./src/Main.as"
export EXTDIR=""

export PLATFORM_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator6.1.sdk"


