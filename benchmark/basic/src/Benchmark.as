package {

	import fabulous.*;

	public class Benchmark {

		public var text:DebugTextNode;

		public var layer:Node;
		public var items:Vector.<Node>;

		public function Benchmark() {
			layer = new Node();
			F.rootNode.addChild(layer);

			text = new DebugTextNode(F.device, 100, 30);
			F.rootNode.addChild(text);

			items = new Vector.<Node>();
		}

		public function tick():void {
			if (F.dt < 1/30) {
				var item:Node = spawnText();
				item.x = Math.random() * F.device.screenWidth;
				item.y = Math.random() * F.device.screenHeight;
				layer.addChild(item);
				items.push(item);
			} else if (F.dt > 1/29 && items.length > 0) {
				layer.removeChild(items[items.length - 1]);
				items.pop();
			}
			moveItems();
			text.text = items.length.toString();
		}

		public function moveItems():void {
			for (var i:int = 0; i < items.length; ++i) {
				items[i].x += Math.random() * 1 - 0.5;
				items[i].y += Math.random() * 1 - 0.5;
				items[i].rotation += Math.random() * 0.1 - 0.05;
			}
		}

		public function spawnQuad():Node {
			var item:QuadNode = new QuadNode(F.device.materialMgr.quadMaterial);
			item.texture = F.getTexture("sprite.png");
			item.width = 10;
			item.height = 10;
			//item.pivotX = 5;
			//item.pivotY = 5;
			return item;
		}

		public var textMaterial:Material;
		public function spawnText():Node {
			if (textMaterial == null) {
				textMaterial = F.materialBasicDefault;
				//(textMaterial as MaterialDistanceFieldOutline).color = 0xFFFF00FF;
				//(textMaterial as MaterialDistanceFieldOutline).outlineColor = 0xFF00FFFF;
			}
			var item:TextNode = new TextNode(textMaterial);
			item.font = F.getFont("chunkfive");
			item.fontSize = 40;
			item.downscaleToLimits = true;
			item.widthLimit = 200;
			item.heightLimit = 40;
			item.text = (9999 +Math.random() * 90000).toFixed(0);
			return item;
		}
	}
}

