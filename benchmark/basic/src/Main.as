package 
{
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.ui.MultitouchInputMode;
	import flash.ui.Multitouch;
	
	import fabulous.*;

	[SWF(backgroundColor="0xffffff", width=320, height=480,frameRate=60)]
	public class Main extends flash.display.Sprite {
		public var benchmark:Benchmark;
		public function Main() {
			super();
			Multitouch.inputMode =  MultitouchInputMode.TOUCH_POINT;
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.frameRate = 60;

			F.configure(stage, "high", "static", null, "fontsLib.cfg", null);
			F.engineStarted.add(onEngineStarted);
			F.start();
		}

		private function onEngineStarted():void {
			F.acquireDevice();
			F.device.setClearColor();
			F.ticked.add(onTicked);

			benchmark = new Benchmark();
		}

		private function onTicked():void {
			benchmark.tick();

			F.device.clear();
			F.device.renderScene();
			F.device.present();
		}
	}
}
