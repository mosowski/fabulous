export AIR_SDK="<<path-to-your-air-sdk>>"

export BINDIR="./bin"
export SWF="$BINDIR/game.swf"

export DEBUG="true"

export COMPILER_OPTIONS="-advanced-telemetry  -mobile -optimize -compress=false -swf-version=24"

export PRE_COMPILE_COMMANDS=()

export SOURCE_PATHS="-source-path ../../src"
export LIBRARY_PATHS=""
export MAIN_CLASS="src/Main.as"

