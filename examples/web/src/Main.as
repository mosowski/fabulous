package 
{
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Vector3D;
	import flash.ui.MultitouchInputMode;
	import flash.ui.Multitouch;
	
	import fabulous.F;

	import fabulous3d.Camera;
	import fabulous3d.GameObject;
	import fabulous3d.Library;
	import fabulous3d.Material;
	import fabulous3d.Mesh;
	import fabulous3d.MeshFilter;
	import fabulous3d.MeshRenderer;
	import fabulous3d.OBJMeshReader;
	import fabulous3d.Quaternion;

	[SWF(backgroundColor="0xffffff", width=320, height=480,frameRate=60)]
	public class Main extends flash.display.Sprite {
		private var cube:GameObject;

		public function Main() {
			super();
			Multitouch.inputMode =  MultitouchInputMode.TOUCH_POINT;
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.frameRate = 60;

			F.configure(stage, true, "high", "static", null, null, null);
			F.engineStarted.add(onEngineStarted);
			F.start();
		}

		private function onEngineStarted():void {
			F.acquireDevice();
			F.device.setClearColor();
			F.ticked.add(onTicked);

			var cam:GameObject = new GameObject();
			cam.addComponent(Camera);
			cam.getComponent(Camera).setAsActiveCamera();
			cam.getComponent(Camera).aspect = F.screenWidth / F.screenHeight;
			cam.transform.translate(0,0,-10);

			F.loadTextTo("static/cube.obj", function(obj:String):void {
				var mesh:Mesh = OBJMeshReader.load(obj);
				Library.addMesh("Fishery", mesh);

				cube = new GameObject();
				var filter:MeshFilter = cube.addComponent(MeshFilter);
				filter.mesh = mesh;

				var renderer:MeshRenderer = cube.addComponent(MeshRenderer);
				renderer.materialCount = 1;

				var material:Material = new Material();
				material.shader = Library.getShader("unlitTexture");
				material.tex0 = F.getTexture("cubeTex.png");
				renderer.setMaterial(0, material);
			});
		}

		private function onTicked():void {
			if (cube != null) {
				cube.transform.rotateBy(Quaternion.fromAxisAngle(new Vector3D(0, 1, 0), F.dt));
			}

			F.device.clear();
			F.device.renderScene();
			F.device.present();
		}
	}
}
