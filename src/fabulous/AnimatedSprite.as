package fabulous 
{
	/**
	 * ...
	 * @author ...
	 */
	public class AnimatedSprite extends Sprite 
	{
		private var _animationName:String;
		private var _frames:Vector.<SpriteSheetFrame>;
		private var _fps:Number = 30;
		private var _elapsedTime:Number = 0.0;
		private var _isPlaying:Boolean;
		private var _frameNo:int = -1;
		private var _delay:Number;
		
		public static function create(animationName:String, fps:Number = 30):AnimatedSprite {
			var frames:Vector.<SpriteSheetFrame> = new Vector.<SpriteSheetFrame>();
			var i:int = 0;
			do {
				var frame:SpriteSheetFrame = G.getSpriteFrame(animationName + "_" + Utils.addLeadingZeros(String(i++), 4) + ".png");
				if (frame != null) {
					frames.push(frame);
				}
			} while (frame != null);
			var animatedSprite:AnimatedSprite = new AnimatedSprite(animationName, frames, fps);
			return animatedSprite;
		}
		
		public function AnimatedSprite(animationName:String, frames:Vector.<SpriteSheetFrame>, fps:Number) {
			super(F.materialBasic);
			_animationName = animationName;
			_frames = frames;
			_frame = _frames[0];
			_fps = fps;
		}
		
		override internal function tick(device:Device):void {
			super.tick(device);
			if (_isPlaying) {
				_elapsedTime += device._dt;
				var previousFrameNo:int = _frameNo;
				_frameNo = int((_elapsedTime * _fps) % _frames.length);
				if (_frameNo < previousFrameNo && _delay > 0) {
					stop();
					F.waitAndDoOnce(_delay, play);
				} else if (_frameNo != previousFrameNo) {
					frame = _frames[_frameNo];
				}
			}
		}
		
		public function play():void {
			if (_node != null) {
				_node.isVisible = true;
			}
			_isPlaying = true;
		}
		
		public function pause():void {
			_isPlaying = false;
		}
		
		public function stop():void {
			if (_node != null) {
				_node.isVisible = false;
			}
			_isPlaying = false;
			_elapsedTime = 0;
			_frameNo = -1;
		}
		
		override public function set width(value:Number):void {
			for (var i:int = _frames.length - 1; i >= 0; --i) {
				_frames[i]._imageWidth = value;
			}
			super.width = value;
		}
		
		override public function set height(value:Number):void {
			for (var i:int = _frames.length - 1; i >= 0; --i) {
				_frames[i]._imageHeight = value;
			}
			super.height = value;
		}
		
		public function get delay():Number {
			return _delay;
		}
		
		public function set delay(value:Number):void {
			_delay = value;
		}
	}

}