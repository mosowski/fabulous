package fabulous {
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.utils.ByteArray;
	
	public class BaseLoader {
		internal static var loaderQueue:LoaderQueue;

		internal var _completed:Signal;
		internal var _crashed:Signal;

		internal var _device:Device;
		internal var _name:String;
		internal var _url:String;
		internal var _isLoaded:Boolean;

		public function BaseLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null, onError:Function = null) {
			_device = device;
			_name = name;
			_url = url;		
			_completed = new Signal();
			_crashed = new Signal();
			_isLoaded = false;
			if (onComplete != null) {
				_completed.addOnce(onComplete);
			}
			if (onError != null) {
				_crashed.addOnce(onError);
			}
		}

		public function get name():String {
			return _name;
		}

		public function load():void {
		}

		internal function queue():void {
			_device._loaderMgr._loaderQueue.add(this);
		}

		public function callAfterLoad(cb:Function):void {
			if (_isLoaded) {
				cb(_name);
			} else {
				_completed.addOnce(cb);
			}
		}

		protected function onLoaderError(errorMessage:String):void {
			if (!wasDisposed) {
				Log.e("Loader \"" + _name + "\" for url " + _url + " failed with error: " + errorMessage);
				_crashed.post(_name);
			}
		}

		protected function onLoaderComplete():void {
			if (!wasDisposed) {
				_isLoaded = true;
				_completed.post(_name);
			}
		}

		public function getChild(name:String):BaseLoader {
			return null;
		}

		public function unload():void {
			_isLoaded = false;
		}

		public function dispose():void {
			unload();
			if (_completed != null) {
				_completed.clean();
				_completed = null;
			}
			if (_crashed != null) {
				_crashed.clean();
				_crashed = null;
			}
			_device = null;
		}

		public function get wasDisposed():Boolean {
			return _device == null;
		}
	}
}
