package fabulous {
	import flash.utils.Dictionary;
	
	public class BatchedLoader extends BaseLoader {
		internal var _children:Vector.<BaseLoader>;
		internal var _childrenByName:Dictionary;
		internal var _hasLoaderStarted:Boolean;
		internal var _hasStopped:Boolean;

		public var stopOnError:Boolean;

		internal var _childCompleted:Signal;

		public function BatchedLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null) {
			super(device, name, url, onComplete);
			_children = new Vector.<BaseLoader>();
			_childrenByName = new Dictionary(true);
			_childCompleted = new Signal();
		}

		public function addChild(loader:BaseLoader):void {
			_children.push(loader);
			_childrenByName[loader.name] = loader;
			if (_hasLoaderStarted) {
				loader._completed.addOnce(onChildLoaderComplete);
				loader.load();
			}
		}

		public function addChildren(loaders:Array):void {
			for (var i:int = 0; i < loaders.length; ++i) {
				_children.push(loaders[i]);
				_childrenByName[loaders[i].name] = loaders[i];
			}
			if (_hasLoaderStarted) {
				for (i = 0; i < loaders.length; ++i) {
					loaders[i]._completed.addOnce(onChildLoaderComplete);
					loaders[i].load();
				}
			}
		}

		override public function getChild(name:String):BaseLoader {
			return _childrenByName[name];
		}

		override public function load():void {
			super.load();
			if (_children.length == 0) {
				_completed.post(_name);
			} else {
				var childrenCopy:Vector.<BaseLoader> = _children.slice();
				for (var i:int = 0; i < childrenCopy.length; ++i) {
					childrenCopy[i]._completed.addOnce(onChildLoaderComplete);
					childrenCopy[i]._crashed.addOnce(onChildLoaderError);
					childrenCopy[i].load();
				}
			}
		}

		private function removeChild(childName:String):void {
			for (var i:int = 0; i < _children.length; ++i) {
				if (_children[i].name == childName) {
					_children.splice(i, 1);
					break;
				}
			}
		}

		private function onChildLoaderError(childName:String):void {
			removeChild(childName);
			if (stopOnError) {
				_hasStopped = true;
				onLoaderError("Child load error: " + childName);
			}
		}


		private function onChildLoaderComplete(childName:String):void {
			if (!_hasStopped) {
				removeChild(childName);
				_childCompleted.post(childName);
				if (_children.length == 0) {
					onLoaderComplete();
				}
			}
		}

		override public function unload():void {
			if (_childrenByName != null) {
				for each(var child:BaseLoader in _childrenByName) {
					child.unload();
				}
			}
		}

		override public function dispose():void {
			if (_childrenByName != null) {
				for each(var child:BaseLoader in _childrenByName) {
					child.dispose();
				}
				_childrenByName = null;
				_children = null;
				super.dispose();
				_childCompleted.clean();
				_childCompleted = null;
			}
		}
	}

}
