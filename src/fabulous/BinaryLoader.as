package fabulous {
import fabulous.ane.FabulousANE;

import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
import flash.events.StatusEvent;
import flash.net.ObjectEncoding;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	public class BinaryLoader extends BaseLoader {
		internal var _inflated:Boolean;
		internal var _data:ByteArray;

		public function BinaryLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null, onError:Function = null) {
			super(device, name, url, onComplete, onError);
		}

		override public function load():void {
			super.load();
			FabulousANE.loadFileAsync(_url, onLoadFileCompleted, onLoadFileError);
		}

		private function onLoadFileCompleted(data:ByteArray):void {
			if (!wasDisposed) {
				_data = data;
				onLoaderComplete();
			}
		}

		private function onLoadFileError(errorMessage:String):void {
			if (!wasDisposed) {
				onLoaderError(errorMessage);
			}
		}

		public function get data():ByteArray {
			return _data;
		}

		public function get compressedByteArray():ByteArray {
			if (!_inflated) {
				_data.inflate();
				_inflated = true;
			}
			return _data;
		}

		public function getAMF3():Object {
			_data.objectEncoding = ObjectEncoding.AMF3;
			_data.position = 0;
			return _data.readObject();
		}

		public function getCompressedAMF3():Object {
			_data.objectEncoding = ObjectEncoding.AMF3;
			_data.position = 0;
			if (!_inflated) {
				_data.inflate();
				_inflated = true;
			}
			return _data.readObject();
		}

		override public function unload():void {
			super.unload();
			if (_data != null) {
				_data.clear();
				_data = null;
			}
		}
	}

}
