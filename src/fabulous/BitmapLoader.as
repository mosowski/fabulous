package fabulous {
import flash.display.Bitmap;
import flash.display.Loader;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.system.ImageDecodingPolicy;
import flash.system.LoaderContext;

public class BitmapLoader extends BinaryLoader {
		private static const IMG_CONTEXT:LoaderContext = new LoaderContext();
		{
			IMG_CONTEXT.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD;
		}

		internal var _loader:Loader;

		public function BitmapLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null, onError:Function = null) {
			super(device, name, url, onComplete, onError);
			_loader = new Loader();
		}

		override protected function onLoaderComplete():void {
			addBitmapLoaderListeners();
			try {
				_loader.loadBytes(_data, IMG_CONTEXT);
			} catch (e:Error) {
				onLoaderError(e.message);
			}
		}

		public function getBitmap():Bitmap {
			return _loader.content as Bitmap;
		}

		private function addBitmapLoaderListeners():void {
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onBitmapLoaderComplete);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onBitmapLoaderError);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.DISK_ERROR, onBitmapLoaderError);
		}

		private function removeBitmapLoaderListeners():void {
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onBitmapLoaderComplete);
			_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onBitmapLoaderError);
			_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.DISK_ERROR, onBitmapLoaderError);
		}

		protected function onBitmapLoaderComplete(e:Event):void {
			removeBitmapLoaderListeners();
			super.onLoaderComplete();
		}

		protected function onBitmapLoaderError(e:IOErrorEvent):void {
			removeBitmapLoaderListeners();
			super.onLoaderError(e.toString());
		}

		override public function unload():void {
			super.unload();
			if (_loader != null) {
				if (_loader.content != null && (_loader.content as Bitmap).bitmapData) {
					(_loader.content as Bitmap).bitmapData.dispose();
				}
				_loader.unload();
				try {
					_loader.close();
				} catch (e:Error) {}
				_loader.unloadAndStop();
				removeBitmapLoaderListeners();
				_loader = null;
			}
		}
	}

}
