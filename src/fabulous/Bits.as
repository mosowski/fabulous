package fabulous {

	public class Bits {

		public static function isPow2(n:int):Boolean {
			return n && !(n & (n-1));
		}

		public static function nextPow2(n:int):int {
			--n;
			n |= n >> 1;
			n |= n >> 2;
			n |= n >> 4;
			n |= n >> 8;
			n |= n >> 16;
			++n;
			return n;
		}
		
		public static function prevPow2(n:int):int {
			return nextPow2(n) >> 1;
		}
	}
}

