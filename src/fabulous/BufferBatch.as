package fabulous {

	import flash.geom.Rectangle;

	public class BufferBatch {
		public var _buffer:BufferBatchBuffer;
		public var _firstIndex:int;
		public var _numIndices:int;
		public var _material:Material;
		public var _texture:Texture;
		public var _maskRect:Rectangle;

		public function BufferBatch() {
			_maskRect = new Rectangle();
		}
	}

}
