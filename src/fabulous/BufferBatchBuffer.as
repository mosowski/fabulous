package fabulous {
	public class BufferBatchBuffer {
		internal var _device:Device;

		internal var _ib:IndexBuffer;
		internal var _vbs:Vector.<VertexBuffer>;

		internal var _numQueuedIndices:int;

		public static const VBTYPE_NONE:int = 0;
		public static const VBTYPE_VECTOR:int = 1;
		public static const VBTYPE_BYTEARRAY:int = 2;

		public function BufferBatchBuffer(device:Device, numVertexBuffers:int, vbSize:int, vbType:int, ibSize:int, vbFormat:Vector.<String>) {
			_device = device;
			_ib = new IndexBuffer(device);
			_ib.setSize(ibSize);
			_vbs = new Vector.<VertexBuffer>(numVertexBuffers, true);
			if ( vbType == VBTYPE_VECTOR ) {
				_vbs[0] = new VectorVertexBuffer(device);
			} else if (vbType == VBTYPE_BYTEARRAY ) {
				_vbs[0] = new ByteArrayVertexBuffer(device);
			}
			if ( vbType != VBTYPE_NONE ) {
				_vbs[0].setSizeAndFormat(vbSize, vbFormat);
			}
			_numQueuedIndices = 0;
		}
		
		internal function recreate():void {
			_ib.recreate();
			for (var i:int = 0; i < _vbs.length; ++i) {
				_vbs[i].recreate();
			}
		}

		internal function upload():void {
			for (var i:int = 0; i < _vbs.length; ++i) {
				_vbs[i].upload();
			}
			_ib.upload();
		}

		internal function reset():void {
			for (var i:int = 0; i < _vbs.length; ++i) {
				_vbs[i].position = 0;
			}
			_ib.position = 0;
			_numQueuedIndices = 0;
		}

		internal function bind():void {
			var offset:int = 0;
			for (var i:int = 0; i < _vbs.length; ++i) {
				var vb:VertexBuffer = _vbs[i];
				vb.bind(offset);
				offset += vb._format.length;
			}
		}

		internal function unbind():void {
			var offset:int = 0;
			for (var i:int = 0; i < _vbs.length; ++i) {
				var vb:VertexBuffer = _vbs[i];
				vb.unbind(offset);
				offset += vb._format.length;
			}
		}

	}

}
