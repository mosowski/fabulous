package fabulous {	  
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.geom.Rectangle;
	
	public class BufferBatcher {
		internal var _device:Device;

		internal var _quadBuffer:BufferBatchBuffer;
		internal var _unorderedBuffer:BufferBatchBuffer;
		internal var _maskedQuadBuffer:BufferBatchBuffer;

		internal var _renderQueue:Vector.<BufferBatch>;
		internal var _currentBufferBatchId:int;
		internal var _currentBufferBatch:BufferBatch;

		internal var _currentTexture:Texture;
		internal var _currentMaterial:Material;
		internal var _currentBuffer:BufferBatchBuffer;
		internal var _currentMaskRect:Rectangle;

		internal var _buffers:Vector.<BufferBatchBuffer>;

		public function BufferBatcher(device:Device) {
			_device = device;
			_renderQueue = new Vector.<BufferBatch>();
		}

		internal function init():void {
			_quadBuffer = new BufferBatchBuffer(_device, 1, VertexBuffer.MAX_SIZE, BufferBatchBuffer.VBTYPE_BYTEARRAY, IndexBuffer.MAX_SIZE, new <String>[Context3DVertexBufferFormat.FLOAT_2, Context3DVertexBufferFormat.FLOAT_2, Context3DVertexBufferFormat.FLOAT_1]);
			_unorderedBuffer = new BufferBatchBuffer(_device, 1, VertexBuffer.MAX_SIZE, BufferBatchBuffer.VBTYPE_BYTEARRAY, IndexBuffer.MAX_SIZE, new <String>[Context3DVertexBufferFormat.FLOAT_2, Context3DVertexBufferFormat.FLOAT_2, Context3DVertexBufferFormat.FLOAT_1]);
			_maskedQuadBuffer = new BufferBatchBuffer(_device, 1, VertexBuffer.MAX_SIZE, BufferBatchBuffer.VBTYPE_BYTEARRAY, IndexBuffer.MAX_SIZE, new <String>[Context3DVertexBufferFormat.FLOAT_2, Context3DVertexBufferFormat.FLOAT_2, Context3DVertexBufferFormat.FLOAT_2]);

			_buffers = new Vector.<BufferBatchBuffer>();
			_buffers.push(_quadBuffer, _unorderedBuffer, _maskedQuadBuffer);
		}
		
		internal function recreate():void {
			_quadBuffer.recreate();
			_unorderedBuffer.recreate();
			_maskedQuadBuffer.recreate();
			for each (var bufferBatch:BufferBatch in _renderQueue) {
				bufferBatch._buffer.recreate();
			}
		}

		internal function beginBatch(buffer:BufferBatchBuffer, material:Material, texture:Texture):void {
			_currentMaterial = material;
			_currentTexture = texture;
			_currentBuffer = buffer;
			_currentMaskRect = _device.currentMaskRect;

			if (_currentBufferBatchId >= _renderQueue.length) {
				_renderQueue[_currentBufferBatchId] = new BufferBatch();
			}
			_currentBufferBatch = _renderQueue[_currentBufferBatchId];
			_currentBufferBatch._firstIndex = buffer._numQueuedIndices;
			_currentBufferBatch._material = material;
			_currentBufferBatch._texture = texture;
			_currentBufferBatch._buffer = buffer;
			_currentBufferBatch._maskRect.copyFrom(_currentMaskRect);
		}

		internal function flushQueue():void {
			closeCurrentBatch();

			var i:int;

			var currentBuffer:BufferBatchBuffer = null;

			var renderBatch:BufferBatch;
			for (i = 0; i < _currentBufferBatchId; ++i) {
				renderBatch = _renderQueue[i];
				if (renderBatch._numIndices > 0) {
					if (currentBuffer != renderBatch._buffer) {
						if (currentBuffer) {
							currentBuffer.unbind();
						}
						currentBuffer = renderBatch._buffer;
						currentBuffer.upload();
						currentBuffer.bind();
					}

					if (renderBatch._texture._isLoaded) {
						renderBatch._material.bind(_device, renderBatch);

						_device._ctx.setScissorRectangle(renderBatch._maskRect);
						_device._ctx.drawTriangles(currentBuffer._ib._indexBuffer3d, renderBatch._firstIndex, renderBatch._numIndices / 3);
						_device._stats._drawnTriangles += renderBatch._numIndices / 3;
						_device._stats._drawCalls++;

						_device.clearSamplers();
					}
				}
			}
			if (currentBuffer) {
				currentBuffer.unbind();
			}
		}

		internal function resetBatching():void {
			_currentBufferBatchId = 0;
			_currentBufferBatch = null;
			_currentMaterial = null;
			_currentTexture = null;
			_currentMaskRect = null;
			for (var i:int = 0; i < _buffers.length; ++i) {
				_buffers[i].reset();
			}
		}

		internal function closeCurrentBatch():void {
			if (!_currentBufferBatch) {
				return;
			}
			if (_currentBufferBatch._buffer) {
				_currentBufferBatch._numIndices = _currentBufferBatch._buffer._numQueuedIndices - _currentBufferBatch._firstIndex;
			} else {
				_currentBufferBatch._numIndices = 0;
			}
			_currentBufferBatchId++;
		}

		internal function queue(buffer:BufferBatchBuffer, material:Material, texture:Texture, indexRangeLength:int):void {
			if (buffer._numQueuedIndices + indexRangeLength > IndexBuffer.MAX_SIZE) {
				flushQueue();
				resetBatching();
			}

			if (_currentMaterial == null
			|| (_currentMaterial
				&& (texture != _currentTexture || material != _currentMaterial || buffer != _currentBuffer || !_device.currentMaskRect.equals(_currentMaskRect)))) {
				closeCurrentBatch();
				beginBatch(buffer, material, texture);
			}
			buffer._numQueuedIndices += indexRangeLength;
		}
	}
}
