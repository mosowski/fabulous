package fabulous {
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	import flash.system.ApplicationDomain;
	import avm2.intrinsics.memory.sf32;
	import avm2.intrinsics.memory.si32;

	public class ByteArrayVertexBuffer extends VertexBuffer {

		internal var _data:ByteArray;

		public function ByteArrayVertexBuffer(device:Device) {
			super(device);
		}

		override internal function setSizeAndFormat(numVertices:int, format:Vector.<String>):void {
			super.setSizeAndFormat(numVertices, format);
			_data = new ByteArray();
			_data.endian = Endian.LITTLE_ENDIAN;
			_data.length = ApplicationDomain.MIN_DOMAIN_MEMORY_LENGTH;
			for (var i:int = 0; i < _numVertices * _data32PerVertex; ++i) {
				_data.writeFloat(0);
			}
			_changeRangeEnd = numVertices;
			upload();
		}

		
		override internal final function upload():void {
			if (_changeRangeBegin < _changeRangeEnd) {
				_vertexBuffer3d.uploadFromByteArray(_data, _changeRangeBegin * _data32PerVertex * 4, _changeRangeBegin, _changeRangeEnd - _changeRangeBegin);
				_changeRangeBegin = _numVertices;
				_changeRangeEnd = 0;
			}
		}

		/** Call it before writing to the *consecutive* vertices in the buffer, but 
		  * just after setting the right position. */
		override internal function beginWrite():void {
			super.beginWrite();
			if (_device._currentDomainMemoryObject != _data) {
				ApplicationDomain.currentDomain.domainMemory = _device._currentDomainMemoryObject =  _data;
			}
		}

		override internal function endWrite():void {
			super.endWrite();
		}

		
		override internal final function writeFloat(value:Number):void {
			sf32(value, _position*4);
			++_position;
		}

		
		internal final function writeUnsignedInt(value:uint):void {
			si32(value, _position*4);
			++_position;
		}

		
		override internal final function set position(value:int):void {
			_position = value;
			_data.position = _position * 4;
		}

		
		override internal final function get position():int {
			return _position;
		}
	}

}
