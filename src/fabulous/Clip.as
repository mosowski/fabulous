package fabulous  {
public class Clip extends Renderable {
		internal var _clipData:ClipData;

		internal var _animationName:String;
		internal var _animationData:ClipAnimationData;
		internal var _currentFrame:Number;
		internal var _repeats:int;
		internal var _rewindAtEnd:Boolean;

		internal var _material:Material;

		internal var _animationFinished:Signal;
		internal var _loopFinished:Signal;
		internal var _labelReached:Signal;
		internal var _clipDataLoaded:Signal;

		internal var _playbackSpeed:Number;

		internal static var _renderer:Renderer;

		public function Clip(clipData:ClipData, material:Material) {
			_clipData = clipData;
			_animationFinished = new Signal();
			_loopFinished = new Signal();
			_labelReached = new Signal();
			_clipDataLoaded = new Signal();
			_playbackSpeed = 1.0;

			if (_clipData && !_clipData.isLoaded) {
				_clipData._clipDataLoaded.addOnce(onClipDataLoadedListener);
			}
			_material = material;
		}

		
		override internal final function get renderer():Renderer {
			return _renderer;
		}

		override internal function tick(device:Device):void {
			if (_repeats != 0) {
				var previousFrame:Number = _currentFrame;
				_currentFrame = (_currentFrame + _playbackSpeed * device._playbackScale) % _animationData._totalFrames;
				var frameNumber:int = (int)(_currentFrame);
				if (_animationData._labels[frameNumber]) {
					_labelReached.post(_animationData._labels[frameNumber]);
				}
				if (_currentFrame < previousFrame) {
					_repeats--;
					_loopFinished.post();
					if (_repeats == 0) {
						_animationFinished.post();
						if (_rewindAtEnd) {
							_currentFrame = 0;
						} else {
							_currentFrame = _animationData._totalFrames - 1;
						}
					}
				}
			}
		}

		public function playAnimation(animationName:String, startFrame:int = 0, repeats:int = -1, rewindAtEnd:Boolean = false):void {
			_animationName = animationName;
			_animationData = _clipData.getAnimationData(animationName);
			_currentFrame = startFrame;
			_repeats = repeats;
			_rewindAtEnd = rewindAtEnd;
		}

		
		override public final function get hitmask():Hitmask {
			return _animationData._hitmask;
		}

		
		override public final function get radius():Number {
			return _animationData._radius;
		}

		
		public final function get width():Number {
			return _animationData._bounds.width;
		}

		
		public final function get height():Number {
			return _animationData._bounds.height;
		}

		
		public final function get numMetaNodes():int {
			return _animationData.numMetaNodes;
		}

		
		public final function getMetaNode(index:int):MetaNode {
			return _animationData.getMetaNode(index, _currentFrame);
		}

		
		public final function set playbackSpeed(value:Number):void {
			_playbackSpeed = value;
		}

		
		public final function get playbackSpeed():Number {
			return _playbackSpeed;
		}

		
		public final function get totalFrames():int {
			return _animationData._totalFrames;
		}

		
		public final function get currentFrame():int {
			return (int)(_currentFrame);
		}
		
		public final function set currentFrame(v:int):void {
			_currentFrame = v;
		}

		public final function get repeats():int {
			return _repeats;
		}
		public final function set repeats(v:int):void {
			_repeats = v;
		}

		
		public final function get clipData():ClipData {
			return _clipData;
		}

		
		public final function get clipAnimationData():ClipAnimationData {
			return _animationData;
		}

		internal function onClipDataLoadedListener():void {
			if (_animationName) {
				playAnimation(_animationName, _currentFrame, _repeats, _rewindAtEnd);
			}
			_clipDataLoaded.post();
		}

		
		public final function set material(value :Material):void {
			_material = value;
		}

		
		public final function get material():Material {
			return _material;
		}

		
		public final function get animationFinished():Signal {
			return _animationFinished;
		}

		
		public final function get loopFinished():Signal {
			return _loopFinished;
		}

		
		public final function get labelReached():Signal {
			return _labelReached;
		}

		
		public final function get clipDataLoaded():Signal {
			return _clipDataLoaded;
		}

		override public function dispose():void {
			super.dispose();
			_clipData = null;
			_animationData = null;
			_material = null;
			if (_animationFinished != null) {
				_animationFinished.clean();
				_animationFinished = null;
			}
			if (_loopFinished != null) {
				_loopFinished.clean();
				_loopFinished = null;
			}
			if (_labelReached != null) {
				_labelReached.clean();
				_labelReached = null;
			}
			if (_clipDataLoaded != null) {
				_clipDataLoaded.clean();
				_clipDataLoaded = null;
			}
		}

	}

}
