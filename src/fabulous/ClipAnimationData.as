package fabulous {	

	import flash.geom.Rectangle;
	
	public class ClipAnimationData {
		internal var _totalFrames:Number;

		internal var _frames:Vector.<ClipFrame>;
		internal var _bounds:Rectangle;
		internal var _radius:Number;
		internal var _hitmask:Hitmask;
		internal var _labels:Array;
		internal var _metaNodesData:Vector.<MetaNodeData>;

		internal static const ERR_NO_HITMASK_FOR_ANIMATION:String = "NoHitmaskForAnimation";

		public function ClipAnimationData() {
		}

		internal function fromObject(object:Object, device:Device):void {
			var i:int;

			_totalFrames = object.frames.length;

			_frames = new Vector.<ClipFrame>(_totalFrames);
			for (i = 0; i < _totalFrames; ++i) {
				_frames[i] = new ClipFrame();
				_frames[i].fromObject(object.frames[i], device);
			}

			if ("bounds" in object) {
				_bounds = new Rectangle(object.bounds[0], object.bounds[1], object.bounds[2] - object.bounds[0], object.bounds[3] - object.bounds[1]);
			} else {
				throw "No bounds in clip.";
			}

			_radius =  Math.max( -_bounds.left, -_bounds.top, _bounds.right, _bounds.bottom);

			_hitmask = new Hitmask();
			if ("hitmask" in object) {
				if (object.hitmask != null) {
					_hitmask.fromObject(object.hitmask);
				}
			}

			_labels = [];
			if ("labels" in object) {
				for (i = 0; i < object.labels.length; ++i) {
					_labels[object.labels[i][0]] = object.labels[i][1];
				}
			}

			if ("metaNodes" in object) {
				_metaNodesData = new Vector.<MetaNodeData>(object.metaNodes.length, true);
				for (i = 0; i <  object.metaNodes.length; ++i) {
					var metaNodeData:MetaNodeData = _metaNodesData[i] = new MetaNodeData();
					metaNodeData.fromObject(object.metaNodes[i]);
				}
			} else {
				_metaNodesData = new Vector.<MetaNodeData>();
			}
		}

		internal function get numMetaNodes():int {
			return _metaNodesData.length;
		}

		internal function getMetaNode(index:int, frameNo:int):MetaNode {
			return _metaNodesData[index].getMetaNodeForFrame(frameNo);
		}

		public function get frames():Vector.<ClipFrame> {
			return _frames;
		}

	}

}
