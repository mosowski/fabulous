package fabulous {
	import flash.utils.Dictionary;
	
	public class ClipDataManager {
		internal var _device:Device;
		internal var _clipDatas:Dictionary;

		internal var _clipDataConfig:Dictionary;

		internal var _unloadedClipDataSource:Object;
		internal static const UNLOADED_CLIP_DATA_NAME:String = "__unloadedClipData__";
		internal static const UNLOADED_ANIMATION_DATA_NAME:String = "__unloadedAnimationData__";

		public function ClipDataManager(device:Device) {
			_device = device;
			_clipDatas = new Dictionary();
		}

		internal function init():void {
			_unloadedClipDataSource = {
				animations: { }
			};
			_unloadedClipDataSource.name = UNLOADED_CLIP_DATA_NAME;
			_unloadedClipDataSource.animations[UNLOADED_ANIMATION_DATA_NAME] = {
				totalFrames: 1,
				frames: [
					{
						texture: TextureManager.UNLOADED_TEXTURE_NAME,
						vertices: [ ],
						indices: [ ]
					}
				],
				hitmask: {
					path: [ ],
					indices: [ ]
				},
				labels: [ ],
				frameTexture: [  ],
				metaNodes: [ ],
				bounds: [0, 0, 0, 0]
			};
		}

		public function loadClipDataConfigFromObject(object:Object):void {
			_clipDataConfig = new Dictionary();
			for (var clipDataName:String in object) {
				_clipDataConfig[clipDataName] = object[clipDataName];
			}
		}

		internal function loadClipDataFromObject(object:Object):void {
			var clipData:ClipData = new ClipData();
			clipData.fromObject(object, _device);
			_clipDatas[clipData._name] = clipData;
		}

		public function getClipData(name:String):ClipData {
			var clipData:ClipData = _clipDatas[name];
			if (!clipData) {
				clipData = _clipDatas[name] = new ClipData();
				clipData.fromObject(_unloadedClipDataSource, _device);
				if (!_clipDataConfig[name]) {
					throw "No clipData for name '" + name + "'.";
				} else if (!_clipDataConfig[name][_device._deviceProfile]) {
					throw "No clipData for profile '" + _device._deviceProfile + "' for '" + name + "'.";
				} else {
					var path:String = _device.getPath(_clipDataConfig[name][_device._deviceProfile]);

					_device._loaderMgr.startTextLoader(path, path,
						function(ldrName:String):void {
							clipData.fromObject(_device._loaderMgr.getTextLoader(ldrName).getJSON(), _device);
							_device._loaderMgr.disposeLoader(ldrName);
						}
					);
				}
			}
			return _clipDatas[name];
		}

		public function getClipDataNames():Vector.<String> {
			var names:Vector.<String> = new Vector.<String>();
			for (var name:String in _clipDataConfig) {
				names.push(name);
			}
			return names;
		}

	}

}
