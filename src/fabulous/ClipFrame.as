package fabulous {
	
	public class ClipFrame {
		internal var _texture:Texture;
		internal var _vertices:Vector.<Number>;
		internal var _indices:Vector.<int>;

		public function ClipFrame() {
		}

		public function fromObject(object:Object, device:Device):void {
			_vertices = Vector.<Number>(object.vertices);
			if ("indices" in object) {
				_indices = Vector.<int>(object.indices);
			} else {
				_indices = new Vector.<int>();
				for (var i:int = 0; i < _vertices.length/5; i+=4) {
					_indices.push(i, i + 1, i + 2, i + 1, i + 2, i +3);
				}
			}
			_texture = device._textureMgr.loadTexture(object.texture);
		}

		public function getX(i:int):Number {
			return _vertices[i*5];
		}
		public function getY(i:int):Number {
			return _vertices[i*5+1];
		}

		public function get numVertices():int {
			return _vertices.length/5;
		}


	}

}
