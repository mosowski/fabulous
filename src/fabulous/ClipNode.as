package fabulous {
	import flash.geom.Rectangle;

	public class ClipNode extends Node {
		private var _clip:Clip;
		private var _animationFinished:Signal;

		public function ClipNode(clip:Clip = null) {
			_animationFinished = new Signal();
			if (clip) {
				setClip(clip);
			}
		}

		public function setClip(newClip:Clip):void {
			if (_clip) {
				_clip.animationFinished.remove(onAnimationFinished);
				removeRenderable(_clip);
			}
			_clip = newClip;
			if (_clip) {
				_clip.animationFinished.add(onAnimationFinished);
				addRenderable(_clip);
			}
		}

		public final function playAnimation(animationName:String, startFrame:int = 0, repeats:int = -1, rewindAtEnd:Boolean = false):void {
			if (_clip) {
				_clip.playAnimation(animationName, startFrame, repeats, rewindAtEnd);
			}
		}

		
		public final function set playbackSpeed(value:Number):void {
			if (_clip) {
				_clip.playbackSpeed = value;
			}
		}

		private function onAnimationFinished():void {
			_animationFinished.post();
		}

		
		public final function get animationFinished():Signal {
			return _animationFinished;
		}

		
		public final function get width():Number {
			return derivedScaleX * _clip.width;
		}

		
		public final function get height():Number {
			return derivedScaleY * _clip.height;
		}

		
		public final function get currentFrame():int {
			return _clip.currentFrame;
		}
		
		public final function set currentFrame(v:int):void {
			_clip.currentFrame = v;
		}

		public final function get repeats():int {
			return _clip.repeats;
		}
		public final function set repeats(v:int):void {
			_clip.repeats = v;
		}

		public function get clipData():ClipData {
			return _clip.clipData;
		}

		public function get clipAnimationData():ClipAnimationData {
			return _clip.clipAnimationData;
		}

		override public function dispose():void {
			super.dispose();
			if (_clip != null) {
				_clip.dispose();
				_clip = null;
			}
			if (_animationFinished != null) {
				_animationFinished.clean();
				_animationFinished = null;
			}
		}
	}

}
