package fabulous {

	import flash.geom.Matrix;

	public final class ClipRenderer extends Renderer {
		internal var _buffer:BufferBatchBuffer;

		public function ClipRenderer(device:Device) {
			super(device);
		}

		override internal function init():void {
			_buffer = _device._bufferBatcher._unorderedBuffer;
		}

		override internal function processRenderable(renderable:Renderable):void {
			var clip:Clip = renderable as Clip;
			var mtx:Matrix = clip._node._mtx;
			var animationData:ClipAnimationData = clip._animationData;
			var currentFrame:int = clip._currentFrame;
			var alpha:Number = clip._node._derivedAlpha;

			if (animationData) {
				var vertexBuffer:ByteArrayVertexBuffer = _buffer._vbs[0] as ByteArrayVertexBuffer;
				var indexBuffer:IndexBuffer = _buffer._ib;

				var _frame:ClipFrame = animationData._frames[currentFrame];

				var i:int;
				var frameIndices:Vector.<int> = _frame._indices;
				var numFrameIndices:int = frameIndices.length;
				_device._bufferBatcher.queue(_buffer, clip._material, _frame._texture, numFrameIndices);
				indexBuffer.beginWrite();
				for (i = 0; i < numFrameIndices; ++i) {
					indexBuffer.writeIndex(vertexBuffer.vertexPosition + frameIndices[i]);
				}
				indexBuffer.endWrite();

				var vertexData:Vector.<Number> = _frame._vertices;
				var numFrameVerticesFloats:int = vertexData.length;

				vertexBuffer.beginWrite();
				for (i = 0; i < numFrameVerticesFloats; i+= 5) {
					vertexBuffer.writeFloat(vertexData[i] * mtx.a + vertexData[int(i+1)] * mtx.c + mtx.tx);
					vertexBuffer.writeFloat(vertexData[i] * mtx.b + vertexData[int(i+1)] * mtx.d + mtx.ty);
					vertexBuffer.writeFloat(vertexData[int(i+2)]);
					vertexBuffer.writeFloat(vertexData[int(i+3)]);
					vertexBuffer.writeFloat(vertexData[int(i+4)] * alpha);
				}

				vertexBuffer.endWrite();
			}
		}

	}
}

