package fabulous {
	import flash.display3D.Context3DTextureFormat;
	import flash.utils.ByteArray;
	import flash.events.Event;
	
	public class CompressedTexture extends Texture {

		internal var _atfOffset:int;

		public function CompressedTexture(device:Device) {
			super(device);
		}

		public function loadFromByteArray(source:ByteArray):void {
			if (_isLoading) {
				throw "Trying to upload source to texture being loaded.";
			}

			if (_handle != null && _handle != _device._textureMgr._unloadedFallbackTextureHandle) {
				_handle.dispose();
			}

			var signature:String = String.fromCharCode(source[0], source[1], source[2]);
			if (signature != "ATF") {
				throw "Invalid ATF.";
			}

			_atfOffset = 6;
			if (source[_atfOffset] == 255) {
				_atfOffset+= 6;
			}
			
			_width = Math.pow(2, source[_atfOffset+1]);
			_height = Math.pow(2, source[_atfOffset+2]);


			_sourceWidth = _width;
			_sourceHeight = _height;

			var textureFormat:String = getTextureFormat(source);
			if (textureFormat == Context3DTextureFormat.BGRA) {
				_numTextureBytes = _width * _height * 4;
			} else {
				_numTextureBytes = source.length;
			}

			_isCompressed = textureFormat != Context3DTextureFormat.BGRA;
			_hasAlpha = textureFormat != Context3DTextureFormat.COMPRESSED;

			_handle = _device._ctx.createTexture(_width, _height, textureFormat, false);
			_handle.uploadCompressedTextureFromByteArray(source, 0);
			Log.i("[CompressedTexture] Uploaded " + _name + ", " + _width + ", " + _height);
			_device._numTextureBytes += _numTextureBytes;
			_isLoaded = true;
			_loaded.post();
		}

		private function getTextureFormat(source:ByteArray):String
		{
			switch (source[_atfOffset+0])
			{
				case 0:
				case 1: return Context3DTextureFormat.BGRA;
				case 2:
				case 3: return Context3DTextureFormat.COMPRESSED;
				case 4:
				case 5: return Context3DTextureFormat.COMPRESSED_ALPHA;
				default: return null;
			}
		}

		override public function recreate():void {
			super.recreate();
			_device.textureMgr.loadCompressedTextureTo(this, _name);
		}
	}
}
