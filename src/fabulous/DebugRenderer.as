package fabulous {
	import flash.display.Sprite;
	
	public class DebugRenderer extends Renderer {
		internal var _sprite:flash.display.Sprite;

		public function DebugRenderer(device:Device) {
			super(device);

			_sprite = new flash.display.Sprite();
			_sprite.mouseEnabled = false;
			_sprite.mouseChildren = false;
			device._stage.addChild(_sprite);
		}

		override internal function beginFrame():void {
			super.beginFrame();
			_sprite.removeChildren();
		}

		override internal function endFrame():void {
			super.endFrame();
		}

		override internal function processRenderable(renderable:Renderable):void {
			var debugSprite:DebugSprite = renderable as DebugSprite;
			debugSprite._sprite.transform.matrix = debugSprite._node._mtx;
			debugSprite._sprite.alpha = debugSprite._node._derivedAlpha;
			_sprite.addChild(debugSprite._sprite);
			debugSprite.updateBounds();
		}

	}

}
