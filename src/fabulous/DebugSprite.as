	package fabulous {
	import flash.display.Sprite;
	import flash.geom.Rectangle;	
	
	public class DebugSprite extends Renderable {
		internal var _sprite:flash.display.Sprite;
		internal var _hitmask:Hitmask;
		internal var _bounds:Rectangle;
		internal var _radius:Number;

		internal static var _renderer:Renderer;

		public function DebugSprite() {
			_sprite = new flash.display.Sprite();
			_hitmask = new Hitmask();
			_hitmask.makeQuad();
			_bounds = new Rectangle(0, 0, 1, 1);
			_radius = 1;
		}

		
		override internal final function get renderer():Renderer {
			return _renderer;
		}

		public function get sprite():flash.display.Sprite {
			return _sprite;
		}

		
		override public final function get radius():Number {
			return _radius;
		}

		override public function get hitmask():Hitmask {
			return _hitmask;
		}

		internal function updateBounds():void {
			_bounds = _sprite.getBounds(_sprite);
			_hitmask._path[0].setTo(_bounds.left, _bounds.top);
			_hitmask._path[1].setTo(_bounds.left, _bounds.bottom);
			_hitmask._path[2].setTo(_bounds.right, _bounds.bottom);
			_hitmask._path[3].setTo(_bounds.right, _bounds.top);
			_radius = Math.max( -_bounds.left, -_bounds.top, _bounds.right, _bounds.bottom);
		}

		override public function dispose():void {
			super.dispose();
			_sprite = null;
			_hitmask = null;
			_bounds = null;
		}
	}

}
