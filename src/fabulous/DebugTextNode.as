package fabulous {
	
	public class DebugTextNode extends Node {
		internal var _text:Text;
		internal var _colorMaterial:MaterialTexSubConst;

		public function DebugTextNode(device:Device, w:int, h:int) {
			_colorMaterial = new MaterialTexSubConst(0,0,0,0);
			_text  = new Text(_colorMaterial);
			_text.font = device.fontMgr.debugFont;
			_text.widthLimit = w;
			_text.heightLimit = h;
			_text.wordWrap = true;
			_text.downscaleToLimits = false;
			_text.upscaleToLimits = false;
			_text.fontSize = 11;

			addRenderable(_text);
		}

		public function set width(value:Number):void {
			_text.widthLimit = value;
		}

		public function get width():Number {
			return _text.widthLimit;
		}

		public function set height(value:Number):void {
			_text.heightLimit = value;
		}

		public function get height():Number {
			return _text.heightLimit;
		}

		public function set text(value:String):void {
			_text.text = value;
		}

		public function get text():String {
			return _text.text;
		}

		public function set color(hexColor:int):void {
			_colorMaterial.setInvColorFromRGB(hexColor, 1);
		}

		override public function dispose():void {
			super.dispose();
			if (_text != null) {
				_text.dispose();
				_text = null;
			}
			_colorMaterial = null;
		}
	}

}
