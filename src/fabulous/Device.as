package fabulous {
	import flash.system.Capabilities;
	import flash.system.TouchscreenType;

	import flash.geom.Rectangle;
	import flash.display.Stage;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Program3D;
	import flash.display3D.textures.Texture;

	import flash.events.Event;	  
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;    

	import flash.utils.ByteArray;	 
	import flash.utils.Endian;
	import flash.utils.getTimer;

    import flash.sensors.Accelerometer;
    import flash.events.AccelerometerEvent;
	
	import fabulous3d.F3d;

	public class Device {
		internal var _ctx:Context3D;
		internal var _stage:Stage;
		internal var _screenWidth:int;
		internal var _screenHeight:int;
		internal var _wasBackBufferConfigured:Boolean;

		internal var _loaderMgr:LoaderManager;
		internal var _textureMgr:TextureManager;
		internal var _materialMgr:MaterialManager;
		internal var _clipDataMgr:ClipDataManager;
		internal var _fontMgr:FontManager;
		internal var _spriteSheetMgr:SpriteSheetManager;

		internal var _assetRootDir:String;
		internal var _deviceProfile:String;

		internal var _bufferBatcher:BufferBatcher;
		internal var _quadBatcher:QuadBatcher;

		internal var _clipRenderer:ClipRenderer;
		internal var _spriteRenderer:SpriteRenderer;
		internal var _maskedSpriteRenderer:MaskedSpriteRenderer;
		internal var _textRenderer:TextRenderer;
		internal var _shapeRenderer:ShapeRenderer;
		internal var _particleRenderer:ParticleRenderer;
		internal var _debugRenderer:DebugRenderer;
		internal var _quadRenderer:QuadRenderer;
		internal var _renderers:Vector.<Renderer>;

		internal var _screenConstants:ByteArray;

		internal var _rootNode:Node;
		internal var _touches:Array;
		internal var _numTouches:int;
		internal var _numMaxTouches:int;
		internal var _minimalSwipeDistance:Number;

		internal var _stats:Stats;

		internal var _clearR:Number;
		internal var _clearG:Number;
		internal var _clearB:Number;

		internal var _maskStack:MaskStack;

		internal var _tweener:Tweener;

		internal var _context3dCreated:Signal;
		internal var _context3dRecreated:Signal;
		internal var _ticked:Signal;

		internal var _lastFrameTime:Number;
		internal var _desiredFPS:Number;
		internal var _dt:Number;
		internal var _playbackScale:Number;
		internal var _currentTime:Number;

		internal var _currentDomainMemoryObject:ByteArray;

		internal var _numTextureBytes:uint;
		internal var _isActivated:Boolean;
		internal var _activated:Signal;
		internal var _contextActivated:Signal;
		internal var _deactivated:Signal;
		internal var _invokedApp:Signal;

        internal var _accelerometer:Accelerometer;
        internal var _accelerometerUpdated:Signal;

		internal var _is3dEnabled:Boolean;
		
		internal var _functionsToPerformNextFrame:Vector.<Function> = new Vector.<Function>();

		public function Device(stage:Stage, enable3d:Boolean=false) {
			_stage = stage;

			_context3dCreated = new Signal();
			_context3dRecreated = new Signal();
			_ticked = new Signal();

			_loaderMgr = new LoaderManager(this);
			_textureMgr = new TextureManager(this);
			_materialMgr = new MaterialManager(this);
			_clipDataMgr = new ClipDataManager(this);
			_fontMgr = new FontManager(this);
			_spriteSheetMgr = new SpriteSheetManager(this);

			_rootNode = new Node();

			_bufferBatcher = new BufferBatcher(this);
			_quadBatcher = new QuadBatcher(this);

			Clip._renderer = _clipRenderer = new ClipRenderer(this);
			Sprite._renderer = _spriteRenderer = new SpriteRenderer(this);
			MaskedSprite._renderer = _maskedSpriteRenderer = new MaskedSpriteRenderer(this);
			Text._renderer = _textRenderer = new TextRenderer(this);
			DebugSprite._renderer = _debugRenderer = new DebugRenderer(this);
			Shape._renderer = _shapeRenderer = new ShapeRenderer(this);
			Particles._renderer = _particleRenderer = new ParticleRenderer(this);
			Quad._renderer = _quadRenderer = new QuadRenderer(this);
			_renderers = new Vector.<Renderer>();
			_renderers.push(_clipRenderer, _spriteRenderer, _maskedSpriteRenderer, _textRenderer, _debugRenderer, _shapeRenderer, _particleRenderer, _quadRenderer);

			_screenConstants = new ByteArray();
			_screenConstants.endian = Endian.LITTLE_ENDIAN;

			_touches = [ ];
			_numMaxTouches = 5;
			_minimalSwipeDistance = Math.max(_stage.stageWidth, _stage.stageHeight) / 20;

			_stats = new Stats();

			_clearR = _clearG = _clearB = 1;

			_tweener = new Tweener();

			_currentTime = getTimer() / 1000;
			_lastFrameTime = _currentTime;
			_desiredFPS = stage.frameRate;
			_dt = 0;
			_playbackScale = 1;
			_isActivated = true;
			_activated = new Signal();
			_contextActivated = new Signal();
			_deactivated = new Signal();
			_invokedApp = new Signal();

            if (Accelerometer.isSupported) {
                _accelerometer = new Accelerometer();
                _accelerometer.addEventListener(AccelerometerEvent.UPDATE, onAccelerometerUpdate);
            }
            _accelerometerUpdated = new Signal();

			_stage.addEventListener(Event.RESIZE, onStageResize);
			_stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);

			try {
				var nativeApplication:* = flash.utils.getDefinitionByName("flash.desktop.NativeApplication");
				var invokeEvent:* = flash.utils.getDefinitionByName("flash.events.InvokeEvent");
				nativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, onDeactivate);
				nativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, onActivate);
				nativeApplication.nativeApplication.addEventListener(invokeEvent.INVOKE, onInvoke);
			} catch(e) {
				trace("[NonAirPlatform] " + e.toString());
			}

			_is3dEnabled = enable3d;
		}

		public function onEnterFrame(e:Event):void {
			_currentTime = getTimer() / 1000;
			_dt = _currentTime - _lastFrameTime;
			_lastFrameTime = _currentTime;
			_playbackScale = _desiredFPS * _dt;

			for (var i:int = 0; i < _functionsToPerformNextFrame.length; ++i) {
				_functionsToPerformNextFrame[i]();
			}
			_functionsToPerformNextFrame.length = 0;
			
			_tweener.tick();
			_ticked.post();
		}

		private function onDeactivate(e:Event):void {
			_isActivated = false;
			Log.i("[Device] Deactivated");
			_deactivated.post();
		}

		private function onActivate(e:Event):void {
			_isActivated = true;
			Log.i("[Device] Activated");
			_activated.post();
			trySignalContextActive();
		}

		private function onInvoke(e:*):void {
			Log.i("[Device] Invoked app");
			_invokedApp.post(e);
		}

		private function onAccelerometerUpdate(e:AccelerometerEvent):void {
			_accelerometerUpdated.post({x: e.accelerationX, y: e.accelerationY, z:e.accelerationZ });
		}

		public function get accelerometerUpdated():Signal {
			return _accelerometerUpdated;
		}

		public function set accelerometerUpdateRate(v:Number):void {
			if (_accelerometer) {
				_accelerometer.setRequestedUpdateInterval(v*1000);
			}
		}
		
		public function callWhenContextActive(cb:Function):void {
			if (isContextActive) {
				cb();
			} else {
				_contextActivated.addOnce(cb);
			}
		}

		internal function callWhenActive(cb:Function):void {
			if (_activated) {
				cb();
			} else {
				_activated.addOnce(cb);
			}
		}
		
		private function trySignalContextActive():void {
			if (isContextActive) {
				_contextActivated.post();
			}
		}

		public function requestContext3d():void {
			_stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, onContext3dCreate);
			_stage.stage3Ds[0].requestContext3D();
		}

		public function setContext3d(ctx:Context3D, enableErrorChecking:Boolean):void {
			Log.i("[Device.as] setContext3d");
			_ctx = ctx;
			_ctx.enableErrorChecking = enableErrorChecking;
			_wasBackBufferConfigured = false;
			trySignalContextActive();
		}

		private function onContext3dCreate(e:Event):void {
			Log.d("[Device.as] onContext3dCreate");
			_stage.stage3Ds[0].removeEventListener(Event.CONTEXT3D_CREATE, onContext3dCreate);
			_stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, onContext3dRecreated);
			setContext3d(_stage.stage3Ds[0].context3D, true);
			initEngine();
		}

		private function initEngine():void {
			reshape();

			_textureMgr.init();
			_materialMgr.init();
			_clipDataMgr.init();
			_fontMgr.init();
			_spriteSheetMgr.init();

			_bufferBatcher.init();
			_quadBatcher.init();

			for each (var renderer:Renderer in _renderers) {
				renderer.init();
			}

			if (_is3dEnabled) {
				F3d.init(this);
			}

			_context3dCreated.post();
		}

		private function onContext3dRecreated(e:Event):void {
			Log.d("[Device.as] onContext3dRecreated");
			setContext3d(_stage.stage3Ds[0].context3D, true);
			reshape();
			_materialMgr.recreate();
			_textureMgr.recreate();
			_fontMgr.recreate();
			_bufferBatcher.recreate();
			_quadBatcher.recreate();
			_context3dRecreated.post();
			if (_is3dEnabled) {
				F3d.recreate();
			}
		}
		
		internal function get isContextActiveAndConfigured():Boolean {
			return isContextActive && _wasBackBufferConfigured;
		}

		internal function get isContextActive():Boolean {
			return _isActivated && isContextValid;
		}
		
		internal function get isContextValid():Boolean {
			return _ctx && _ctx.driverInfo != "Disposed";
		}
		
		private function onStageResize(e:Event):void {
			reshape();
		}

		public function reshape():void {
			_screenWidth = _stage.stageWidth;
			_screenHeight = _stage.stageHeight;
			Log.i("[Device] reshape, isContextActive: " + isContextActive);
			if (isContextActive) {
				try {
					_ctx.configureBackBuffer(_screenWidth, _screenHeight, 0, _is3dEnabled);
					_wasBackBufferConfigured = true;
				} catch (e:Error) {
					_wasBackBufferConfigured = false;
				}
			} else {
				callWhenContextActive(reshape);
			}

			_maskStack = new MaskStack(this);

			_screenConstants.position = 0;
			_screenConstants.writeFloat(2 / _screenWidth);
			_screenConstants.writeFloat(-2 / _screenHeight);
			_screenConstants.writeFloat(-1);
			_screenConstants.writeFloat(1);
		}

		public function get ctx():Context3D {
			return _ctx;
		}

		public function get numMaxTouches():int {
			return _numMaxTouches;
		}
		public function set numMaxTouches(v:int):void {
			_numMaxTouches = v;
		}
		
		public final function get screenWidth():int {
			return _screenWidth;
		}

		
		public final function get screenHeight():int {
			return _screenHeight;
		}

		
		public final function get dt():Number {
			return _dt;
		}

		
		public final function get currentTime():Number {
			return _currentTime;
		}

		
		public final function set assetRootDir(value:String):void {
			_assetRootDir = value;
		}

		
		public final function get assetRootDir() : String {
			return _assetRootDir;
		}

		
		public final function getPath(path:String):String {
			return _assetRootDir + "/" + path;
		}

		
		public final function get loaderMgr():LoaderManager {
			return _loaderMgr;
		}

		
		public final function get textureMgr():TextureManager {
			return _textureMgr;
		}

		
		public final function get materialMgr():MaterialManager {
			return _materialMgr;
		}

		
		public final function get clipDataMgr():ClipDataManager {
			return _clipDataMgr;
		}

		
		public final function get fontMgr():FontManager {
			return _fontMgr;
		}

		
		public final function get spriteSheetMgr():SpriteSheetManager {
			return _spriteSheetMgr;
		}

		
		public final function set deviceProfile(value:String):void {
			_deviceProfile = value;
		}

		
		public final function get deviceProfile():String {
			return _deviceProfile;
		}

		
		public final function get rootNode():Node {
			return _rootNode;
		}

		
		public final function get tweener():Tweener {
			return _tweener;
		}

		
		public final function get context3dCreated():Signal {
			return _context3dCreated;
		}

		
		public final function get context3dRecreated():Signal {
			return _context3dRecreated;
		}


		public final function get activated():Signal {
			return _activated;
		}

		public final function get deactivated():Signal {
			return _deactivated;
		}		
		
		public final function get ticked():Signal {
			return _ticked;
		}

		public final function get invokedApp():Signal {
			return _invokedApp;
		}

		public final function get numTextureBytes():uint {
			return _numTextureBytes;
		}

		public function get isDesktop():Boolean {
			return Capabilities.manufacturer == "Adobe Windows" 
				|| Capabilities.manufacturer == "Adobe Macintosh";
		}

		public function setupStageInputListeners():void {
			if (isDesktop) {
				_stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				_stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				_stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			}
			_stage.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
			_stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
			_stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
		}

		private function onMouseDown(event:MouseEvent):void {
			_stage.dispatchEvent(new TouchEvent(TouchEvent.TOUCH_BEGIN, event.bubbles, event.cancelable, 0, false, event.localX, event.localY));
		}

		private function onMouseMove(event:MouseEvent):void {
			if (_numTouches > 0) {
				_stage.dispatchEvent(new TouchEvent(TouchEvent.TOUCH_MOVE, event.bubbles, event.cancelable, 0, false, event.localX, event.localY));
			}
		}

		private function onMouseUp(event:MouseEvent):void {
			_stage.dispatchEvent(new TouchEvent(TouchEvent.TOUCH_END, event.bubbles, event.cancelable, 0, false, event.localX, event.localY));
		}

		private function onTouchBegin(event:TouchEvent):void {
			if (_numTouches < _numMaxTouches) {
				var touch:Touch = _touches[event.touchPointID] = new Touch(event.touchPointID, event.stageX, event.stageY);
				_numTouches++;
				var node:Node = _rootNode.getHitNode(event.stageX, event.stageY, Node.HITMASK_TOUCH);
				touch._touchedNode = node || _rootNode;

				if (touch._touchedNode._touchBegan) {
					touch._touchedNode._touchBegan.post(touch);
				}

				node = _rootNode.getHitNode(event.stageX, event.stageY, Node.HITMASK_SWIPE_X);
				touch._swipedXNode = node || _rootNode;

				node = _rootNode.getHitNode(event.stageX, event.stageY, Node.HITMASK_SWIPE_Y);
				touch._swipedYNode = node || _rootNode;
			}
		}

		private function onTouchMove(event:TouchEvent):void {
			var touch:Touch = _touches[event.touchPointID];
			if (touch) {
				touch.addMovePosition(event.stageX, event.stageY);

				if (touch._touchedNode._touchMoved) {
					touch._touchedNode._touchMoved.post(touch);
				}

				if (touch._swipeStarted) {
					if (touch._swipedXNode && touch._swipedXNode._swipeMoved) {
						touch._swipedXNode._swipeMoved.post(touch);
					}
					if (touch._swipedYNode && touch._swipedYNode._swipeMoved && touch._swipedYNode != touch._swipedXNode) {
						touch._swipedYNode._swipeMoved.post(touch);
					}
				} else if (touch._swipeDistance >= _minimalSwipeDistance) {
					touch._swipeStarted = true;
					if (touch._swipedXNode && touch._swipedXNode._swipeBegan) {
						touch._swipedXNode._swipeBegan.post(touch);
					}
					if (touch._swipedYNode && touch._swipedYNode._swipeBegan && touch._swipedYNode != touch._swipedXNode) {
						touch._swipedYNode._swipeBegan.post(touch);
					}
				}
			}

		}

		private function onTouchEnd(event:TouchEvent):void {
			var touch:Touch = _touches[event.touchPointID];
			if (touch) {
				_numTouches--;
				delete _touches[event.touchPointID];
				if (touch._touchedNode._touchEnded) {
					touch._touchedNode._touchEnded.post(touch);
				}

				if (touch._swipedXNode && touch._swipedXNode._swipeEnded) {
					touch._swipedXNode._swipeEnded.post(touch);
				}

				if (touch._swipedYNode && touch._swipedYNode._swipeEnded && touch._swipedYNode != touch._swipedXNode) {
					touch._swipedYNode._swipeEnded.post(touch);
				}

				if (!touch._swipeStarted && touch._touchedNode._tapped) {
					touch._touchedNode._tapped.post(event);
				}
			}
		}

		
		public final function get numTouches():int {
			return _numTouches;
		}

		public final function get stats():Stats {
			return _stats;
		}

		internal function get currentMaskRect():Rectangle {
			return _maskStack._topRect;
		}

		
		public final function setClearColor(r:Number = 0.3, g:Number = 0.4, b:Number = 0.5):void {
			_clearR = r;
			_clearG = g;
			_clearB = b;
		}

		public final function configure():void {
			if (!_wasBackBufferConfigured) {
				reshape();
			}
		}
		
		public final function clear():void {
			if (isContextActiveAndConfigured) { 
				_ctx.clear(_clearR, _clearG, _clearB);
			}
		}

		
		public final function present():void {
			if (isContextActiveAndConfigured) { 
				_ctx.present();
			}
		}

		public function renderScene():void {
			if (isContextActiveAndConfigured) {
				if (_is3dEnabled) {
					F3d.tick();
					resetStateVariables();
				}

				var renderer:Renderer;
				_stats.reset();
				for each (renderer in _renderers) {
					renderer.beginFrame();
				}
				_bufferBatcher.resetBatching();
				_quadBatcher.beginFrame();

				_rootNode.update();
				renderNode(_rootNode);

				for each (renderer in _renderers) {
					renderer.endFrame();
				}
				_bufferBatcher.flushQueue();
				_quadBatcher.flush();
				_quadBatcher._numDrawnQuads = 0;

				_textureMgr.tick();
			}
		}

		internal function renderNode(node:Node):void {
			var i:int;
			var numRenderables:int;			   
			var renderable:Renderable;
			var radius:Number;
			var isClipped:Boolean;
			_stats._visibleNodes++;
			if (node.isVisible) {
				if (node._mask != null) {
					_maskStack.push(
						node._mask.left * node._mtx.a + node._mtx.tx,
						node._mask.top * node._mtx.d + node._mtx.ty,
						node._mask.right * node._mtx.a + node._mtx.tx,
						node._mask.bottom * node._mtx.d + node._mtx.ty
					);
				}
				if (node._renderables) {
					numRenderables = node._renderables.length;
					for (i = 0; i < numRenderables; ++i) {
						renderable = node._renderables[i];
						if (renderable) {
							renderable.tick(this);
							var renderer:Renderer = renderable.renderer;
							radius = renderable.radius * 1.42;
							if (((node.derivedScaleX > 0
								&& node.derivedX + radius * node._mtx.a > 0
								&& node.derivedX - radius * node._mtx.a < screenWidth)
								|| (node._mtx.a < 0
								&& node.derivedX - radius * node._mtx.a > 0
								&& node.derivedX + radius * node._mtx.a < screenWidth))
							   &&
							   ((node._mtx.d > 0
								&& node.derivedY + radius * node._mtx.d  > 0
								&& node.derivedY - radius * node._mtx.d< screenHeight)
								|| (node._mtx.d < 0
								&& node.derivedY - radius * node._mtx.d  > 0
								&& node.derivedY + radius * node._mtx.d < screenHeight))
							) {
								_stats._visibleRenderables++;
								renderer.processRenderable(renderable);
							} else {
								isClipped = true;
								_stats._clippedRenderables++;
							}
						}
					}
				}

				if (node._children && !(node._isClippingChildren && isClipped)) {
					for (var it:NodeList = node._children; it; it = it._next) {
						renderNode(it._elem);
					}
				}
				if (node._mask != null) {
					_maskStack.pop();
				}
			}
		}

		internal const _currentTextureHandles:Vector.<flash.display3D.textures.Texture> = new Vector.<flash.display3D.textures.Texture>(4);
		internal const _currentTextures:Vector.<fabulous.Texture> = new Vector.<fabulous.Texture>(4);
		internal const _currentReadyTextures:Vector.<fabulous.Texture> = new Vector.<fabulous.Texture>(4);
		internal var _currentProgram:Program3D;
		internal var _sourceBlendFactor:String;
		internal var _destBlendFactor:String;

		public function resetStateVariables():void {
			var i:int;
			for (i = 0; i < 4; ++i) {
				_currentTextureHandles[i] = null;
				_currentTextures[i] = null;
				_ctx.setTextureAt(i, null);
			}
			_currentProgram = null;
			_ctx.setProgram(null);
			_sourceBlendFactor = Context3DBlendFactor.ONE;
			_destBlendFactor = Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA;
			_ctx.setBlendFactors(_sourceBlendFactor, _destBlendFactor);
		}

		
		internal final function setTextureAt(samplerIndex:int, texture:fabulous.Texture):void {
			var handle:flash.display3D.textures.Texture = null;
			if (texture != null) {
				handle = texture._handle;			
			}
			if (_currentTextureHandles[samplerIndex] != handle) {
				_ctx.setTextureAt(samplerIndex, handle);
				_currentTextureHandles[samplerIndex] = handle;
				_currentTextures[samplerIndex] = texture;
			}
		}

		internal final function clearSamplers():void {
			setTextureAt(0, null);
			setTextureAt(1, null);
			setTextureAt(2, null);
			setTextureAt(3, null);
		}

		
		internal final function setShader(shader:Shader, samplerOptions:int):void {
			var program3d:Program3D = shader.getProgram3dForSamplerOptions(shader.getSamplerOptionsForTextures(_currentTextures, samplerOptions));
			if (_currentProgram != program3d) {
				_ctx.setProgram(program3d);
				_currentProgram = program3d;
			}
		}

		
		internal final function setBlendFactors(src:String, dest:String):void {
			if (_sourceBlendFactor != src || _destBlendFactor != dest) {
				_sourceBlendFactor = src;
				_destBlendFactor = dest;
				_ctx.setBlendFactors(src, dest);
			}
		}

	}

}
