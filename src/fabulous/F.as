package fabulous {
import flash.display.Stage;
import flash.utils.getTimer;

// A static proxy class created to make engine more user friendly
	public class F {
		private static var _device:Device;

		private static var _initializer:Initializer;

		// hack - we can't import classes within same package, so we force compiler to compile them in that way
		private static var _classes:Array = [
			BaseLoader, BatchedLoader, BinaryLoader, BitmapLoader, Bits, BufferBatch, BufferBatchBuffer,
			ByteArrayVertexBuffer, Clip, ClipAnimationData, ClipData, ClipDataManager, ClipFrame, ClipNode,
			ClipRenderer, CompressedTexture, DebugFont, DebugRenderer, DebugSprite, DebugTextNode, Device,
			Font, FontManager, Geom, Glyph, Hitmask, IndexBuffer, Initializer, LoaderManager, LoaderQueue,
			Log, Logger, MaskStack, Material, MaterialBasic, MaterialBasicAlpha, MaterialBlende, MaterialDistanceField,
			MaterialDistanceFieldOutline, MaterialGrayscale, MaterialHSL, MaterialLinearParticles, MaterialManager,
			MaterialMul, MaterialParticles, MaterialSaturate, MaterialSilhouette, MaterialTexSubConst, 
			MetaNode, MetaNodeData, Node, NodeList, ParticleRenderer, Particles, ParticleSpawnerBehaviour,
			ParticleSpawnerCircle, ParticleSpawnerNova, ParticleSpawnerPoint, ParticleSpawnerUniformBehaviour,
			Quad, QuadBatcher, QuadMaterial, QuadNode, QuadRenderer, Renderable, Renderer, Shader, ShaderSamplerOptions,
			Shape, ShapeRenderer, Shape, ShapeRenderer, Signal, SignalsMap, SocketLogger, Sprite, SpriteNode,
			SpriteRenderer, SpriteSheet, SpriteSheetFrame, SpriteSheetManager, Stats, Text, TextLoader, TextNode,
			TextRenderer, Texture, TextureManager, Touch, TraceLogger, Tweener, TweenItem, UncompressedTexture,
			VectorVertexBuffer, VertexBuffer
		];
		
		public static function get device():Device {
			return _device;
		}

		public static function configure(stage:Stage, enable3d:Boolean, profile:String, assetRootDir:String, clipsLib:Object, fontsLib:Object, spriteSheetsLib:Object):void {
			_initializer = new Initializer(stage, enable3d, profile, assetRootDir, clipsLib, fontsLib, spriteSheetsLib);
		}

		public static function start():void {
			Log.addLogger(new TraceLogger());
			_initializer.completed.addOnce(acquireDevice);
			_initializer.start();			
		}
		
		public static function acquireDevice():void {			
			_device = _initializer.device;
		}

		public static function get engineStarted():Signal {
			return _initializer.completed;
		}
		
		public static function get ticked():Signal {
			return _device.ticked;
		}

		public static function get screenWidth():Number {
			return _device._screenWidth;
		}

		public static function get screenHeight():Number {
			return _device._screenHeight;
		}

		public static function get exactTime():Number {
			return getTimer() / 1000;
		}

		public static function get dt():Number {
			return _device.dt;
		}
		
		public static function log(msg:String):void {
			Log.i(msg);
		}

		public static function get rootNode():Node {
			return _device.rootNode;
		}

		public static function get tweener():Tweener {
			return _device.tweener;
		}

		public static function getTexture(path:String):Texture {
			return _device.textureMgr.loadTexture(path);			
		}
		
		public static function lazyLoadTexture(path:String, loadTimeout:int, alternativePath: String = null):Texture {
			return _device.textureMgr.lazyLoadTexture(path, loadTimeout, alternativePath);
		}

		public static function textureLoaderCompleted(texture:Texture):Signal {
			return _device.textureMgr.loaderCompleted.get(texture);
		}
		
		public static function textureLoaderCrashed(texture:Texture):Signal {
			return _device.textureMgr.loaderCrashed.get(texture);
		}

		public static function disposeTexture(path:String):void {
			_device.textureMgr.disposeTexture(path);
		}

		public static function disposeTextureWhenNotUsed(path:String):void {
			_device.textureMgr.disposeTextureWhenNotUsed(path);
		}

		public static function getClipData(name:String):ClipData {
			return _device.clipDataMgr.getClipData(name);
		}

		public static function getFont(name:String):Font {
			return _device.fontMgr.getFont(name);
		}

		public static function getSheet(name:String):SpriteSheet {
			return _device.spriteSheetMgr.getSheet(name);
		}

		public static function getSheets():Vector.<SpriteSheet> {
			return _device.spriteSheetMgr.getSheets();
		}

		public static function get materialBasicDefault():MaterialBasic {
			return _device.materialMgr.materialBasicDefault;
		}

		public static function get materialBasic():MaterialBasic {
			return _device.materialMgr.materialBasicDefault;
		}

		public static function get materialBasicAlphaDefault():MaterialBasicAlpha {
			return _device.materialMgr.materialBasicAlphaDefault;
		}

		public static function get materialBasicAlpha():MaterialBasicAlpha {
			return _device.materialMgr.materialBasicAlphaDefault;
		}

		public static function get quadMaterial():QuadMaterial {
			return _device.materialMgr.quadMaterial;
		}
		
		public static function get materialGreyscale():MaterialGrayscale {
			return _device.materialMgr.materialGreyscale;
		}

		public static function getLoader(name:String):BaseLoader {
			return _device.loaderMgr.getLoader(name);
		}

		public static function loadText(url:String, onComplete:Function, loaderName:String = null):void {
			loaderName ||= url;
			_device.loaderMgr.startTextLoader(loaderName, url, function(loaderName:String):void {
				onComplete(loaderName);
				_device.loaderMgr.disposeLoader(loaderName);
			});
		}

		public static function loadTextTo(url:String, onComplete:Function, loaderName:String = null):void {
			loadText(url, function(ldrName:String):void {
				onComplete((getLoader(ldrName) as TextLoader).getString());
			}, loaderName);
		}

		public static function loadBinary(url:String, onComplete:Function, loaderName:String = null):void {
			loaderName ||= url;
			_device.loaderMgr.startBinaryLoader(loaderName, url, function(ldrName:String):void {
				onComplete();
				_device.loaderMgr.disposeLoader(ldrName);
			});
		}

		public static function loadBitmap(url:String, onComplete:Function, loaderName:String):void {
			loaderName ||= url;
			_device.loaderMgr.startBitmapLoader(loaderName, url, function(ldrName:String):void {
				onComplete();
				_device.loaderMgr.disposeLoader(ldrName);
			});
		}

		public static function loadBatch(loaderName:String, onComplete:Function):BatchedLoader {
			return _device.loaderMgr.startBatchedLoader(loaderName, function(ldrName:String):void {
				onComplete();
				_device.loaderMgr.disposeLoader(ldrName);
			});
		}
		
		public static function waitAndDoOnce(time:Number, onDelayFunction:Function):TweenItem {
			return tweener.tween(tweener, { }, { }, time, tweener.linearEase, onDelayFunction);
		}
		
		public static function performFunctionNextFrame(functionToPerform:Function):void {
			_device._functionsToPerformNextFrame.push(functionToPerform);
		}
	}
}
