package fabulous {
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileStream;
	import flash.filesystem.FileMode;
	
	public class FileLogger extends Logger {
		private static var _instance:FileLogger;

		private var _file:File;
		private var _fileStream:FileStream;

		public static function get instance():FileLogger {
			return _instance;
		}

		public static function create(logPath:String, fileName:String, addDateToLogs:Boolean = false):FileLogger {
			_instance = new FileLogger(logPath, fileName);
			_instance.addDateToLogs(addDateToLogs);
			return instance;
		}

		public function FileLogger(logPath:String, fileName:String) {
			super();
			if (_instance != null) {
				throw new Error("FileLogger is singleton");
			}
			_file = new File(logPath + "/" + fileName );
			_file.preventBackup = true;
			NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, close);
		}

		override public function print(msg:String):void {
			if (_fileStream == null) {
				_fileStream = new FileStream();
				_fileStream.open(_file, FileMode.APPEND);
			}
			_fileStream.writeUTFBytes(msg + "\n");
		}

		private function close(e:Event = null):void {
			if (_fileStream != null) {
				_fileStream.close();
				_fileStream = null;
			}
		}

		public function flush():void {
			close();
		}
	}
}
