package fabulous {
	public class Font {
		internal var _device:Device;
		internal var _name:String;
		internal var _texture:Texture;
		internal var _glyphs:Array;
		internal var _lineHeight:Number;
		internal var _baseSize:Number;
		internal var _isLoaded:Boolean;

		internal var _fontLoaded:Signal;

		public function Font(device:Device) {
			_device = device;
			_glyphs = [];
			_lineHeight = 0;
			_baseSize = 1;
			_texture = null;
			_isLoaded = false;
			_fontLoaded = new Signal();
		}

		internal function fromObject(object:Object):void {
			_name = object.name;
			_lineHeight = object.lineHeight;
			_baseSize = object.base;
			for each (var glyph:Array in object.glyphs) {
				_glyphs[glyph[0]] = new Glyph(glyph[1], glyph[2], glyph[3], glyph[4], glyph[5], glyph[6], glyph[7]);
			}
			_texture = _device.textureMgr.loadTexture(object.texture);
			if (_texture._isLoaded) {
				calculateGlyphsUv();
			} else {
				_texture._loaded.addOnce(calculateGlyphsUv);
			}
		}

		internal function calculateGlyphsUv():void {
			for each (var glyph:Glyph in _glyphs) {
				glyph._uvLeft = glyph._x / _texture._width;
				glyph._uvTop = glyph._y / _texture._height;
				glyph._uvRight = (glyph._x + glyph._width) / _texture._width;
				glyph._uvBottom = (glyph._y + glyph._height) / _texture._height;
			}
			_isLoaded = true;
			_fontLoaded.post();
		}

		public function callAfterLoad(f:Function):void {
			if (_isLoaded) {
				f();
			} else {
				_fontLoaded.addOnce(f);
			}
		}

		public function get name():String {
			return _name;
		}

		public function get lineHeight():Number {
			return _lineHeight;
		}

		public function get baseSize():Number {
			return _baseSize;
		}

		public function get fontLoaded():Signal {
			return _fontLoaded;
		}
	}

}
