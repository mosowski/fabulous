package fabulous {
import flash.display.BitmapData;
import flash.utils.Dictionary;
	public class FontManager {
		internal var _device:Device;
		internal var _fonts:Dictionary;
		internal var _fontsConfig:Dictionary;
		internal var _fontByName:Dictionary;

		internal var _debugFont:Font;

		public function FontManager(device:Device) {
			_device = device;
			_fontByName = new Dictionary();
		}

		public function init():void {
			var debugFontBitmapData:BitmapData = DebugFont.getDebugFontBitmapData();
			_device.textureMgr.createTextureFromBitmapData("__debugFontBitmap__", debugFontBitmapData);
			debugFontBitmapData.dispose();
			_debugFont = new Font(_device);
			_debugFont.fromObject(DebugFont.debugFontAsset);
		}
		
		public function recreate():void {
			var debugFontBitmapData:BitmapData = DebugFont.getDebugFontBitmapData();
			(_debugFont._texture as UncompressedTexture).loadFromBitmapData(debugFontBitmapData);
			debugFontBitmapData.dispose();
		}

		public function loadFontsConfigFromObject(object:Object):void {
			_fontsConfig = new Dictionary();
			for (var fontName:String in object) {
				Log.i("Registered font " + fontName);
				_fontsConfig[fontName] = object[fontName][_device._deviceProfile];
			}
			Log.i("Font Manager config loaded.");
		}

		public function getFont(name:String):Font {
			var font:Font = _fontByName[name];
			if (!font) {
				if (!_fontsConfig[name]) {
					throw "No font config for name '" + name + "'.";
				} else {
					font = _fontByName[name] = new Font(_device);
					var path:String = _device.getPath(_fontsConfig[name]);
					_device.loaderMgr.startTextLoader(path, path,
						function (ldrName:String):void {
							font.fromObject(_device.loaderMgr.getTextLoader(ldrName).getJSON());
							_device.loaderMgr.disposeLoader(ldrName);
						}
					);
				}
			}
			return font;
		}

		public function get debugFont():Font {
			return _debugFont;
		}

	}

}
