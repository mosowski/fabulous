package fabulous {
	import flash.geom.Point;
	
	public class Geom {
		
		
		public static function cross(a:Point, b:Point):Number {
			return a.x * b.y - a.y * b.x;
		}
		
		
		public static function cross2(ax:Number, ay:Number, bx:Number, by:Number):Number {
			return ax * by - ay * bx;
		}
		
		
		public static function dot(a:Point, b:Point):Number {
			return a.x * b.x + a.y * b.y;
		}
		
		
		public static function dot2(ax:Number, ay:Number, bx:Number, by:Number):Number {
			return ax * bx + ay * by;
		}
		
		
		public static function sgn(x:Number):Number {
			return x > 0 ? 1 : -1;
		}
		
		
		public static function intersection(out:Point, a:Point, b:Point, c:Point, d:Point):Boolean {
			var bx:int = b.x - a.x;
			var by:int = b.y - a.y;
			var dx:int = d.x - c.x;
			var dy:int = d.y - c.y;
			var bdotd:Number = bx * dy - by * dx;
			if (bdotd == 0) {
				return false;
			}
			var cx:int = c.x - a.x;
			var cy:int = c.y - a.y;
			var t:Number = (cx * dy - cy * dx) / bdotd;
			if (t > 1 || t < 0) {
				return false;
			}
			var u:Number = (cx * by - cy * bx) / bdotd;
			if (u > 1 || u < 0) {
				return false;
			}
			out.x = a.x + t * bx;
			out.y = a.y + t * by;
			return true;
		}

		public static function polygonArea(path:Vector.<Point>):Number {
			var n:int = path.length;
			var a:Number = 0.0;
			for (var p:int = n - 1, q:int = 0; q < n; p = q++) {
				a += path[p].x * path[q].y - path[q].x * path[p].y;
			}
			return a * 0.5;
		}

		
		public static function insideTriangle(ax:Number, ay:Number, bx:Number, by:Number, cx:Number, cy:Number, px:Number, py:Number):Boolean {
			return cross2(px - ax, py - ay, ax - bx, ay - by) >= 0
				&& cross2(px - bx, py - by, bx - cx, by - cy) >= 0
				&& cross2(px - cx, py - cy, cx - ax, cy - ay) >= 0;
		}

		/**
		 * Performs a classical ear-clipping triangulation.
		 * @param out
		 * @param path
		 * @return
		 */
		public static function triangulate(out:Vector.<uint>, path:Vector.<Point>):Boolean {
			out.length = 0;
			var n:int = path.length;
			if (n < 3) {
				return false;
			}
			
			var verts:Vector.<uint> = new Vector.<uint>();
			var v:int;
			
			if (polygonArea(path) > 0) {
				for (v = 0; v < n; v++) {
					verts[v] = v;
				}
			} else {
				for (v = 0; v < n; v++) {
					verts[v] = (n - 1) - v;
				}
			}
			
			var nv:int = n;
			
			var count:int = 2 * nv;
			var m:int;
			for (m = 0, v = nv - 1; nv > 2; ) {
				if (count-- <= 0) {
					return false;
				}
				
				var u:int = v;
				if (nv <= u) {
					u = 0;
				}
				v = u + 1;
				if (nv <= v) {
					v = 0;
				}
				var w:int = v + 1;
				if (nv <= w) {
					w = 0;
				}
				
				if (triangulateSnip(path, u, v, w, nv, verts)) {
					var a:int, b:int, c:int, s:int, t:int;
					
					a = verts[u];
					b = verts[v];
					c = verts[w];
					
					out.push(a);
					out.push(b);
					out.push(c);
					m++;
					
					for (s = v, t = v + 1; t < nv; s++, t++) {
						verts[s] = verts[t];
					}
					nv--;
					
					count = 2 * nv;
				}
			}
			
			return true;
		}
		
		private static function triangulateSnip(path:Vector.<Point>, u:int, v:int, w:int, n:int, verts:Vector.<uint>):Boolean {
			var p:int;
			var ax:Number, ay:Number, bx:Number, by:Number;
			var cx:Number, cy:Number, px:Number, py:Number;
			
			ax = path[verts[u]].x;
			ay = path[verts[u]].y;
			
			bx = path[verts[v]].x;
			by = path[verts[v]].y;
			
			cx = path[verts[w]].x;
			cy = path[verts[w]].y;

			if (cross2(bx - ax, by - ay, cx - ax, cy - ay) < 0.00001) {
				return false;
			}

			for (p = 0; p < n; p++) {
				if ((p == u) || (p == v) || (p == w)) {
					continue;
				}
				px = path[verts[p]].x;
				py = path[verts[p]].y;
				if (insideTriangle(ax, ay, bx, by, cx, cy, px, py)) {
					return false;
				}
			}
			return true;
		}
	}

}
