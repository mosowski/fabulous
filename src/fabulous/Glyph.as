package fabulous {
	public class Glyph {
		internal var _x:int;
		internal var _y:int;
		internal var _width:int;
		internal var _height:int;
		internal var _xoffset:int;
		internal var _yoffset:int;
		internal var _xadvance:int;

		internal var _uvLeft:Number;
		internal var _uvTop:Number;
		internal var _uvRight:Number;
		internal var _uvBottom:Number;

		public function Glyph(x:int, y:int, w:int, h:int, xoffset:int, yoffset:int, xadvance:int) {
			_x = x;
			_y = y;
			_width = w;
			_height = h;
			_xoffset = xoffset;
			_yoffset = yoffset;
			_xadvance = xadvance;
			_uvLeft = 0;
			_uvTop = 0;
			_uvRight = 0;
			_uvBottom = 0;
		}

	}

}
