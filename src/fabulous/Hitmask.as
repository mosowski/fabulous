package fabulous {	  
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.display.Sprite;
	
	public class Hitmask {
		internal var _path:Vector.<Point>;
		internal var _indices:Vector.<uint>;

		public function Hitmask() {
			_path = new Vector.<Point>();
			_indices = new Vector.<uint>();
		}

		internal function fromObject(object:Object):void {
			_path = new Vector.<Point>(object.path.length / 2, true);
			var i:int;
			for (i = 0; i < object.path.length; i += 2) {
				_path[i / 2] = new Point(object.path[i], object.path[i + 1]);
			}

			_indices = Vector.<uint>(object.indices);
		}

		internal function makeQuad():void {
			_path = new Vector.<Point>(4, true);
			_path[0] = new Point();
			_path[1] = new Point();
			_path[2] = new Point();
			_path[3] = new Point();
			_indices = new Vector.<uint>(6);
			_indices[0] = 0;
			_indices[1] = 1;
			_indices[2] = 2;
			_indices[3] = 2;
			_indices[4] = 3;
			_indices[5] = 0;
		}

		
		public final function fromRectangle(rectangle:Rectangle):void {
			_path[0].setTo(rectangle.left, rectangle.top);
			_path[1].setTo(rectangle.left, rectangle.bottom);
			_path[2].setTo(rectangle.right, rectangle.bottom);
			_path[3].setTo(rectangle.right, rectangle.top);
		}

		public final function hitTest(x:Number, y:Number, translationX:Number, translationY:Number, scaleX:Number, scaleY:Number):Boolean {
			var i:int, j:int, inside:Boolean = false;
			x = (x - translationX) / scaleX;
			y = (y - translationY) / scaleY;
			for (i = _path.length - 1; i >= 0 ; j = i--) {
				if (((_path[i].y <= y && y < _path[j].y) || (_path[j].y <= y && y < _path[i].y))
				&& (x < (_path[j].x - _path[i].x) * (y - _path[i].y) / (_path[j].y - _path[i].y) + _path[i].x)) {
					inside = !inside;
				}
			}
			return inside;
		}

		
		public final function get numVertices():int {
			return _path.length;
		}

		public function drawToSprite(sprite:flash.display.Sprite):void {
			if (_path.length) {
				sprite.graphics.moveTo(_path[0].x, _path[0].y);
				for (var i:int = 1; i <= _path.length; ++i) {
					sprite.graphics.lineTo(_path[i % _path.length].x, _path[i % _path.length].y);
				}
			}
		}



	}

}
