package fabulous {
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Context3DBufferUsage;
	
	public class IndexBuffer {
		internal var _device:Device;
		internal var _indexBuffer3d:IndexBuffer3D;
		internal var _numIndices:int;
		internal var _changeRangeBegin:int;
		internal var _changeRangeEnd:int;
		internal var _data:Vector.<uint>;
		private var _position:int;
		
		public static const MAX_SIZE:int = 0x3FFF;

		public function IndexBuffer(device:Device) {
			_device = device;
		}
		
		public function recreate():void {
			setSize(_numIndices);
		}

		public function setSize(numIndices:int):void {
			if (_indexBuffer3d) {
				_indexBuffer3d.dispose();
			}
			_numIndices = numIndices;
			_indexBuffer3d = _device._ctx.createIndexBuffer(numIndices, Context3DBufferUsage.DYNAMIC_DRAW);

			_data = new Vector.<uint>(_numIndices, true);
			for (var i:int = 0, v:int = 0; i < _numIndices-6; i += 6, v += 4) {
				_data[i] = v;
				_data[i + 1] = v + 1;
				_data[i + 2] = v + 2;
				_data[i + 3] = v + 1;
				_data[i + 4] = v + 2;
				_data[i + 5] = v + 3;
			}
			_changeRangeBegin = 0;
			_position = 0;
			_changeRangeEnd = numIndices;
			upload();
		}

		
		public final function upload():void {
			if (_changeRangeBegin < _changeRangeEnd) {
				_indexBuffer3d.uploadFromVector(_data, _changeRangeBegin, _changeRangeEnd - _changeRangeBegin);
				_changeRangeBegin = _numIndices;
				_changeRangeEnd = 0;
			}
		}

		
		internal final function beginWrite():void {
			if (_position < _changeRangeBegin) {
				_changeRangeBegin = _position;
			}
		}

		
		internal final function endWrite():void {
			if (_position > _changeRangeEnd) {
				_changeRangeEnd = _position;
			}
		}

		
		internal final function writeIndex(value:int):void {
			_data[_position] = value;
			++_position;
		}

		
		internal final function set position(value:int):void {
			_position = value;
		}

		
		internal final function get position():int {
			return _position;
		}

	}

}
