package fabulous {
	import flash.display.Stage;
	public class Initializer {
		private var _clipsLib:Object;
		private var _fontsLib:Object;
		private var _spriteSheetsLib:Object;
		
		internal var _stage:Stage;
		internal var _device:Device;

		internal var _is3dEnabled:Boolean;
		internal var _profile:String;
		internal var _assetRootDir:String;
		internal var _clipsLibraryPath:String;
		internal var _fontsLibraryPath:String;
		internal var _spriteSheetsLibraryPath:String;

		internal var _clipsLibraryLoader:TextLoader;
		internal var _fontsLibraryLoader:TextLoader;
		internal var _spriteSheetsLibraryLoader:TextLoader;

		internal var _completed:Signal;

		public function Initializer(stage:Stage, enable3d:Boolean, profile:String, assetRootDir:String, clipsLib:Object, fontsLib:Object, spriteSheetsLib:Object) {
			_stage = stage;

			_is3dEnabled = enable3d;
			_profile = profile;
			_assetRootDir = assetRootDir;
			if (clipsLib is String) {
				_clipsLibraryPath = clipsLib as String;
			} else if (clipsLib != null) {
				_clipsLib = clipsLib;
			}
			if (fontsLib is String) {
				_fontsLibraryPath = fontsLib as String;
			} else if (fontsLib != null) {
				_fontsLib = fontsLib;
			}
			if (spriteSheetsLib is String) {
				_spriteSheetsLibraryPath = spriteSheetsLib as String;
			} else if (spriteSheetsLib != null) {
				_spriteSheetsLib = spriteSheetsLib;
			}

			_completed = new Signal();
		}

		public function start():void {
			_device = new Device(_stage, _is3dEnabled);
			_device.context3dCreated.addOnce(onContext3dCreated);
			_device.requestContext3d();
		}

		internal function onContext3dCreated():void {
			_device.deviceProfile = _profile;
			_device.assetRootDir = _assetRootDir;
			_device.setupStageInputListeners();

			var requiredFilesLoader:BatchedLoader = device.loaderMgr.startBatchedLoader("requiredFilesLoader", onRequiredFilesLoaded);
			if (_clipsLibraryPath) {
				_clipsLibraryLoader = new TextLoader(device, "clipsLibrary", _device.getPath(_clipsLibraryPath));
				requiredFilesLoader.addChild(_clipsLibraryLoader);
			} else if (_clipsLib) {
				_device.clipDataMgr.loadClipDataConfigFromObject(_clipsLib);
			}
			if (_fontsLibraryPath) {
				_fontsLibraryLoader = new TextLoader(device, "fontsLibrary", _device.getPath(_fontsLibraryPath));
				requiredFilesLoader.addChild(_fontsLibraryLoader);
			} else if (_fontsLib) {
				_device.fontMgr.loadFontsConfigFromObject(_fontsLib);
			}
			if (_spriteSheetsLibraryPath) {
				_spriteSheetsLibraryLoader = new TextLoader(device, "spriteSheetsLibrary", _device.getPath(_spriteSheetsLibraryPath));
				requiredFilesLoader.addChild(_spriteSheetsLibraryLoader);
			} else if (_spriteSheetsLib) {
				_device.spriteSheetMgr._loaded.add(onSpriteSheetsLoaded);
				_device.spriteSheetMgr.loadConfigFromObject(_spriteSheetsLib);		
			}
			requiredFilesLoader.load();
		}

		private function onRequiredFilesLoaded(ldrName:String):void {
			Log.i("Required engine configuration files loaded");

			if (_clipsLibraryLoader) {
				_device.clipDataMgr.loadClipDataConfigFromObject(_clipsLibraryLoader.getJSON());
			}
			if (_fontsLibraryLoader) {
				_device.fontMgr.loadFontsConfigFromObject(_fontsLibraryLoader.getJSON());
			}
			if (_spriteSheetsLibraryLoader) {
				_device.spriteSheetMgr._loaded.add(onSpriteSheetsLoaded);
				_device.spriteSheetMgr.loadConfigFromObject(_spriteSheetsLibraryLoader.getJSON());
			} else if (_spriteSheetsLib == null) {
				_completed.post();
			}

			_device.loaderMgr.disposeLoader(ldrName);
		}

		private function onSpriteSheetsLoaded():void {
			_completed.post();
		}

		public final function get completed():Signal {
			return _completed;
		}

		public final function get device():Device {
			return _device;
		}

	}

}
