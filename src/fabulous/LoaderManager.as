package fabulous {
	import flash.utils.Dictionary;

public class LoaderManager {
		internal var _device:Device;
		internal var _loaders:Dictionary;
		internal var _loadersByUrl:Dictionary;
		internal var _loaderQueue:LoaderQueue;

		public function LoaderManager(device:Device) {
			_device = device;
			_loaders = new Dictionary();
			_loadersByUrl = new Dictionary();
			_loaderQueue = new LoaderQueue(_device);
		}

		private function startLoader(name:String, target:*, onComplete:Function, loaderType:Class, onError:Function = null):BaseLoader {
			var loader:BaseLoader = _loaders[name];
			if (loader == null) {
				loader = _loaders[name] = new loaderType(_device, name, target, onComplete, onError);
				_loadersByUrl[target] = loader;
				loader.queue();
			} else {
				loader.callAfterLoad(onComplete);
			}
			return loader;
		}

		public function getLoader(nameOrUrl:String):BaseLoader {
			var loader:BaseLoader = _loaders[nameOrUrl];
			if (loader == null) {
				loader = _loadersByUrl[nameOrUrl];
			}
			return loader;
		}

		public function getTextLoader(nameOrUrl:String):TextLoader {
			return getLoader(nameOrUrl) as TextLoader;
		}
		
		public function hasLoader(nameOrUrl:String):Boolean {
			return getLoader(nameOrUrl) != null;
		}

		public function disposeLoader(nameOrUrl:String):void {
			var loader:BaseLoader = _loaders[nameOrUrl];
			if (loader != null) {
				loader.dispose();
				delete _loaders[nameOrUrl];
			}
		}

		public function startBitmapLoader(name:String, url:String, onComplete:Function = null, onError:Function = null):BitmapLoader {
			return startLoader(name, url, onComplete, BitmapLoader, onError) as BitmapLoader;
		}

		public function startBinaryLoader(name:String, url:String, onComplete:Function = null, onError:Function = null):BinaryLoader {
			return startLoader(name, url, onComplete, BinaryLoader, onError) as BinaryLoader;
		}

		public function startTextLoader(name:String, url:String, onComplete:Function = null, onError:Function = null):TextLoader {
			return startLoader(name, url, onComplete, TextLoader, onError) as TextLoader;
		}

		public function startBatchedLoader(name:String, onComplete:Function = null, children:Array = null):BatchedLoader {
			var batchedLoader:BatchedLoader = _loaders[name];			 
			if (batchedLoader == null) {
				batchedLoader = _loaders[name] = new BatchedLoader(_device, name, "", onComplete);
			}

			if (children) {
				batchedLoader.addChildren(children);
				if (batchedLoader._hasLoaderStarted == false) {
					batchedLoader._completed.add(onComplete);
					batchedLoader.load();
				}
			}
			return batchedLoader;
		}

	}

}
