package fabulous {
	import flash.utils.getTimer;
	
	public class LoaderQueue {
		private static const TIME_LIMIT:uint = 100;
		
		internal var _queue:Vector.<BaseLoader>;
		internal var _device:Device;
		
		public function LoaderQueue( device:Device ) {
			_queue = new Vector.<BaseLoader>();
			_device = device;
		}
		
		internal function add( loader:BaseLoader ):void {
			if ( !_queue.length ) {
				_device._ticked.add( processQueue );
			}
			
			if ( _queue.indexOf( loader ) == -1 ) {
				if ( loader is BitmapLoader ) {
					_queue.unshift(loader);
				}
				else {
					_queue.push(loader);
				}
			}
		}
		
		internal function processQueue():void {
			var start:uint = getTimer();
			var duration:uint;
			for ( var i:int = 0; i < _queue.length && duration < TIME_LIMIT; i++ ) {
				var loader:BaseLoader = _queue.shift();
				loader.load();
				duration = getTimer() - start;
				i--;
			}
			
			if ( !_queue.length ) {
				_device.ticked.remove( processQueue );
			}
		}
	}
}
