package fabulous {
	
	public class Log {
		internal static var _loggers:Vector.<Logger> = new Vector.<Logger>();
		internal static var _flags:uint = 0xFFFFFFF;

		public static const ERROR:uint = 1;
		public static const WARNING:uint = 2;
		public static const INFO:uint = 4;
		public static const DEBUG:uint = 8;

		public static function addLogger(logger:Logger):void {
			_loggers.push(logger);
			logger.init();
		}

		public static function d(...args):void {
			if (_flags & DEBUG) {
				var msg:String = "" ;
				for (var i:int = 0; i < args.length; ++i) {
					msg += args[i].toString() + (i != (args.length - 1) ? " " : "");
				}

				for (i = 0; i < _loggers.length; ++i) {
					_loggers[i].d(msg);
				}
			}
		}
		
		public static function i(...args):void {
			if (_flags & INFO) {
				var msg:String = "" ;
				for (var i:int = 0; i < args.length; ++i) {
					msg += args[i].toString() + (i != (args.length - 1) ? " " : "");
				}

				for (i = 0; i < _loggers.length; ++i) {
					_loggers[i].i(msg);
				}
			}
		}

		
		public static function e(...args):void {
			if (_flags & ERROR) {
				var msg:String = "" ;
				for (var i:int = 0; i < args.length; ++i) {
					msg += args[i].toString() + (i != (args.length - 1) ? " " : "");
				}
				for (i = 0; i < _loggers.length; ++i) {
					_loggers[i].e(msg);
				}
			}
		}

		
		public static function w(...args):void {
			if (_flags & WARNING) {
				var msg:String = "" ;
				for (var i:int = 0; i < args.length; ++i) {
					msg += args[i].toString() + (i != (args.length - 1) ? " " : "");
				}
				for (i = 0; i < _loggers.length; ++i) {
					_loggers[i].w(msg);
				}
			}
		}

	}

}
