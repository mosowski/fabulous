package fabulous {
	
	public class Logger {
		protected var _addDateToLogs:Boolean;
		
		public function Logger() {
			_addDateToLogs = false;
		}

		internal function init():void {
		}

		public function print(msg:String):void {
			throw "Not implemented.";
		}
		
		internal function d(msg:String):void {
			print("D\\ " + date + msg);
		}

		
		internal function i(msg:String):void {
			print("I\\ " + date + msg);
		}
		
		internal function e(msg:String):void {
			print("E\\ " + date + msg);
		}

		
		internal function w(msg:String):void {
			print("W\\ " + date + msg);
		}
		
		private function get date():String {
			return _addDateToLogs ? dateString()+ " " : "";
		}
		
		private function dateString():String {
			var date:Date = new Date();
			return addLeadingZerosToNumber(date.getUTCDate(), 2) + "-" + 
					addLeadingZerosToNumber(date.getUTCMonth()+1, 2) + " " + 
					addLeadingZerosToNumber(date.getUTCHours(), 2) + ":" + 
					addLeadingZerosToNumber(date.getUTCMinutes(), 2) + ":" + 
					addLeadingZerosToNumber(date.getUTCSeconds(), 2);
		}
		
		private function addLeadingZerosToNumber(value:Number, zeros:int):String {
			return (Math.pow(10, zeros) + value).toString().substr(-zeros);
		}
		
		public function addDateToLogs(value:Boolean):void {
			_addDateToLogs = value;
		}
	}

}
