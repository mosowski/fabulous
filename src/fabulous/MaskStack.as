package fabulous {

	import flash.geom.Rectangle;

	public class MaskStack {
		internal var _device:Device;
		internal var _rootMask:Rectangle;
		internal var _stack:Vector.<Rectangle>;
		internal var _topId:int;
		internal var _topRect:Rectangle;

		public function MaskStack(device:Device) {
			_device = device;
			_stack = new Vector.<Rectangle>();
			_topId = 0;
			_rootMask = new Rectangle(0, 0, _device._screenWidth, _device._screenHeight);
			_topRect = _rootMask;
			_stack.push(_rootMask);
		}

		public function push(left:Number, top:Number, right:Number, bottom:Number):void {
			var oldTopRect:Rectangle = _stack[_topId];
			++_topId;
			if (_topId == _stack.length) {
				_stack.push(new Rectangle());
			}
			_topRect = _stack[_topId];

			if (oldTopRect.left > left) {
				left = oldTopRect.left;
			}
			if (oldTopRect.right < right) {
				right = oldTopRect.right;
			}
			if (oldTopRect.top > top) {
				top = oldTopRect.top;
			}
			if (oldTopRect.bottom < bottom) {
				bottom = oldTopRect.bottom;
			}
			
			_topRect.setTo(left, top, right - left, bottom - top);
		}

		public function pop():void {
			--_topId;
			_topRect = _stack[_topId];
		}

		public function clear():void {
			_topId = 1;
		}
	}
}
