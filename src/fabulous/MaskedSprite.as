package fabulous 
{
	/**
	 * ...
	 * @author ...
	 */
	public class MaskedSprite extends Sprite 
	{
		internal static var _renderer:Renderer;
		internal var _materialMask:MaterialMask;
		
		public function MaskedSprite(material:MaterialMask) {
			super(material);
			_materialMask = material;
		}
		
		public function get materialMask():MaterialMask {
			return _materialMask;
		}
		
		public function set materialMask(value:MaterialMask):void {
			_material = value;
			_materialMask = value;
		}
		
		override public function get material():Material {
			return _materialMask;
		}
		
		override public function set material(value:Material):void {
			if (value is MaterialMask) {
				super.material = value;
			}
		}
		
		override internal function get renderer():Renderer {
			return _renderer;
		}
	}

}