package fabulous {

	public class Material {

		public function Material() {
		}

		internal function bind(device:Device, bufferBatch:BufferBatch):void {
			throw "Abstract";
		}

		internal function recreate():void {
			
		}
	}

}
