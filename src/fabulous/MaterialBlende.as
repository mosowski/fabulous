package fabulous {
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	public class MaterialBlende extends Material {
		internal static var _shader:Shader;

		internal var _blendeX:Number;
		internal var _blendeY:Number;
		internal var _blendeFalloff:Number;
		internal var _blendeRadius:Number;
		internal var _blendeParams:ByteArray;

		public function MaterialBlende() {
			_blendeParams = new ByteArray();
			_blendeParams.endian = Endian.LITTLE_ENDIAN;
			_blendeX = 0;
			_blendeY = 0;
			_blendeFalloff = 15;
			_blendeRadius = 0;
		}

		internal static function initShader(device:Device):void {
			_shader = new Shader(device);
			_shader.setCode(
				[
					// va0.xy: vertex position
					// va1.xy: vertex uv
					// va2.x: vertex alpha
					// vc0.xy: screen scale
					// vc0.zw: screen translation
					"mov vt0, va0",
					"mul vt0.xy, vt0.xy, vc0.xy",
					"add vt0.xy, vt0.xy, vc0.zw",
					"mov op, vt0",
					"mov v0, va1",
					"mov v1, va2",
					"mov v2, va0"
				].join("\n"),
				[
					// v0: vertex uv
					// v1: vertex alpha
					// v2: vertex pos
					// fs0: texture
					// fc0.xy: blende center
					// fc0.z: blende edge thickness
					// fc0.w: squared blende radius
					"tex ft0, v0, fs0 <sampler>",
					"sub ft1, v2, fc0",
					"add ft1.z, ft1.z, fc0.z",
					"dp3 ft1, ft1, ft1",
					"sqt ft1.x, ft1.x",
					"sub ft1.x, ft1.x, fc0.w",
					"div ft1.x, ft1.x, fc0.z",
					"sat ft1.x, ft1.x",
					"mul ft1.x, ft1.x, v1.x",
					"mul oc, ft0, ft1.xxxx"
				].join("\n")
			);
		}

		
		public final function set blendeX(value:Number):void {
			_blendeX = value;
		}

		
		public final function get blendeX():Number {
			return _blendeX;
		}

		
		public final function set blendeY(value:Number):void {
			_blendeY = value;
		}

		
		public final function get blendeY():Number {
			return _blendeY;
		}

		
		public final function set blendeFalloff(value:Number):void {
			_blendeFalloff = value;
		}

		
		public final function get blendeFalloff():Number {
			return _blendeFalloff;
		}

		
		public final function set blendeRadius(value:Number):void {
			_blendeRadius = value;
		}

		
		public final function get blendeRadius():Number {
			return _blendeRadius;
		}

		
		override internal final function bind(device:Device, batch:BufferBatch):void {
			device.setTextureAt(0, batch._texture);
			device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
			device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);

			_blendeParams.position = 0;
			_blendeParams.writeFloat(_blendeX);
			_blendeParams.writeFloat(_blendeY);
			_blendeParams.writeFloat(_blendeFalloff);
			_blendeParams.writeFloat(_blendeRadius);
			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _blendeParams, 0);
		}
		
		internal static function recreate():void 	{
			_shader.recreate();
		}
	}

}
