package fabulous {
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	public class MaterialDistanceField extends Material {
		internal static var _shader:Shader;

		internal var _color:ByteArray;
		internal var _dfParams:ByteArray;

		internal var _smoothing:Number;
		internal var _threshold:Number;

		public function MaterialDistanceField() {
			_smoothing = 0.1;
			_threshold = 0.5;

			_color = new ByteArray();
			_color.endian = Endian.LITTLE_ENDIAN;
			_color.writeFloat(1);
			_color.writeFloat(1);
			_color.writeFloat(1);
			_color.writeFloat(1);

			_dfParams = new ByteArray();
			_dfParams.endian = Endian.LITTLE_ENDIAN;
			updateParams();
		}

		internal static function initShader(device:Device):void {
			_shader = new Shader(device);
			_shader.setCode(
				[
					// va0.xy: vertex position
					// va1.xy: vertex uv
					// va2.x: vertex alpha
					// vc0.xy: screen scale
					// vc0.zw: screen translation
					"mov vt0, va0",
					"mul vt0.xy, vt0.xy, vc0.xy",
					"add vt0.xy, vt0.xy, vc0.zw",
					"mov op, vt0",
					"mov v0, va1",
					"mov v1, va2"
				].join("\n"),
				[
					// v0: vertex uv
					// v1: vertex alpha
					// fs0: texture
					// fc0: color to multiply by texture color
					// fc1: lower threshold, inv smoothing range
					"tex ft0, v0, fs0 <sampler>",
					"sub ft1.x, ft0.x, fc1.x",
					"mul ft1.x, ft1.x, fc1.y",
					"sat ft1.x, ft1.x",
					"mul ft0.xyzw, fc0.xyzw, ft1.xxxx",
					"mov oc, ft0"
				].join("\n")
			);
		}

		public final function set color(c:uint):void {
			_color.position = 0;
			_color.writeFloat(((c >> 16) & 255) / 255);
			_color.writeFloat(((c >> 8) & 255) / 255);
			_color.writeFloat((c & 255) / 255);
			_color.writeFloat(((c >> 24) & 255) / 255);
		}

		public final function set threshold(v:Number):void {
			_threshold = v;
			updateParams();
		}

		public final function set smoothing(v:Number):void {
			_smoothing = v;
			updateParams();
		}

		private function updateParams():void {
			_dfParams.position = 0;
			_dfParams.writeFloat(_threshold - _smoothing/2);
			_dfParams.writeFloat(1/_smoothing);
			_dfParams.writeFloat(0);
			_dfParams.writeFloat(0);
		}


		
		override internal final function bind(device:Device, batch:BufferBatch):void {
			device.setTextureAt(0, batch._texture);
			device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
			device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);

			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _color, 0);
			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 1, 1, _dfParams, 0);
		}
		
		internal static function recreate():void 	{
			_shader.recreate();
		}
	}

}
