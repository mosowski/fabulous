package fabulous {
	
	public class MaterialManager {
		internal var _device:Device;

		internal var _materialBasicDefault:MaterialBasic;
		internal var _materialBasicAlphaDefault:MaterialBasicAlpha;
		internal var _quadMaterial:QuadMaterial;
		internal var _materialGreyscale:MaterialGrayscale;

		public function MaterialManager(device:Device) {
			_device = device;
		}

		internal function init():void {
			MaterialBasic.initShader(_device);
			_materialBasicDefault = new MaterialBasic();

			MaterialBasicAlpha.initShader(_device);
			_materialBasicAlphaDefault = new MaterialBasicAlpha();

			MaterialGrayscale.initShader(_device);
			_materialGreyscale = new MaterialGrayscale();
			
			MaterialTexSubConst.initShader(_device);
			MaterialMul.initShader(_device);
			MaterialSaturate.initShader(_device);
			MaterialHSL.initShader(_device);
			MaterialBlende.initShader(_device);
			MaterialDistanceField.initShader(_device);
			MaterialDistanceFieldOutline.initShader(_device);
			MaterialLinearParticles.initShader(_device);
			MaterialSilhouette.initShader(_device);
			MaterialMask.initShader(_device);
			
			QuadMaterial.initShader(_device);
			_quadMaterial = new QuadMaterial();
		}
		
		internal function recreate():void {
			_materialBasicDefault.recreate();
			_materialBasicAlphaDefault.recreate();
			_materialGreyscale.recreate();
			MaterialTexSubConst.recreate();
			MaterialMul.recreate();
			MaterialSaturate.recreate();
			MaterialHSL.recreate();
			MaterialBlende.recreate();
			MaterialDistanceField.recreate();
			MaterialDistanceFieldOutline.recreate();
			MaterialLinearParticles.recreate();
			MaterialSilhouette.recreate();
			MaterialMask.recreate();
			_quadMaterial.recreate();
		}

		
		public final function get materialBasicDefault():MaterialBasic {
			return _materialBasicDefault;
		}

		
		public final function get materialBasicAlphaDefault():MaterialBasicAlpha {
			return _materialBasicAlphaDefault;
		}
		
		public function get quadMaterial():QuadMaterial {
			return _quadMaterial;
		}
		
		public final function get materialGreyscale():MaterialGrayscale {
			return _materialGreyscale;
		}

	}

}
