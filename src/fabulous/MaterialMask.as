package fabulous 
{
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	/**
	 * ...
	 * @author ...
	 */
	public class MaterialMask extends Material {
		internal static var _shader:Shader;
		internal var _maskFrame:SpriteSheetFrame;
		
		public function MaterialMask(maskFrame:SpriteSheetFrame) {
			_maskFrame = maskFrame;
		}
		
		internal static function initShader(device:Device):void {
			_shader = new Shader(device);
			_shader.setCode(
				[
					// va0.xy: vertex position
					// va1.xy: vertex uv
					// va2.x: vertex alpha
					// vc0.xy: screen scale
					// vc0.zw: screen translation
					"mov vt0, va0",
					"mul vt0.xy, vt0.xy, vc0.xy",
					"add vt0.xy, vt0.xy, vc0.zw",
					"mov op, vt0",
					"mov v0, va1",
					"mov v1, va2"
				].join("\n"),
				[
					// v0: vertex uv
					// v1: vertex alpha
					// fs0: texture
					"tex ft0, v1, fs0 <sampler_0>",
					"tex ft1, v0, fs1 <sampler_1>",
					"mul oc, ft1, ft0.wwww"
					//"tex ft0, v0, fs0 <sampler_0>",
					//"tex ft1, v1, fs1 <sampler_1>",
					//"mul oc, ft0, ft1.wwww"
				].join("\n")
			);
		}

		
		override internal final function bind(device:Device, batch:BufferBatch):void {
			device.setTextureAt(0, _maskFrame._texture);
			device.setTextureAt(1, batch._texture);
			device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
			device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
		}
		
		internal static function recreate():void {
			_shader.recreate();
		}
	}

}