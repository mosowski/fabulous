package fabulous {
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.utils.ByteArray;
import flash.utils.Dictionary;
import flash.utils.Endian;

	public class MaterialMul extends Material {
		internal static var _shader:Shader;

		private static var _colors:Dictionary = new Dictionary();

		internal var _color:ByteArray;

		private static function getColor(r:Number, g:Number, b:Number, a:Number):ByteArray {
			var color:uint = getColorAsUint(r, g, b, a);
			var colorByteArray:ByteArray = _colors[color];
			if (colorByteArray == null) {
				colorByteArray = new ByteArray();
				colorByteArray.endian = Endian.LITTLE_ENDIAN;
				colorByteArray.writeFloat(r);
				colorByteArray.writeFloat(g);
				colorByteArray.writeFloat(b);
				colorByteArray.writeFloat(a);
				_colors[color] = colorByteArray;
			} else {
				colorByteArray.position = 0;
			}
			return colorByteArray;
		}

		private static function getColorAsUint(r:Number, g:Number, b:Number, a:Number):uint {
			return (int(a * 255) << 24) | (int(r * 255) << 16) | (int(g * 255) << 8) | int(b * 255);
		}

		public function MaterialMul(r:Number, g:Number, b:Number, a:Number) {
			_color = getColor(r, g, b, a);
		}

		internal static function initShader(device:Device):void {
			_shader = new Shader(device);
			_shader.setCode(
				[
					// va0.xy: vertex position
					// va1.xy: vertex uv
					// va2.x: vertex alpha
					// vc0.xy: screen scale
					// vc0.zw: screen translation
					"mov vt0, va0",
					"mul vt0.xy, vt0.xy, vc0.xy",
					"add vt0.xy, vt0.xy, vc0.zw",
					"mov op, vt0",
					"mov v0, va1",
					"mov v1, va2"
				].join("\n"),
				[
					// v0: vertex uv
					// v1: vertex alpha
					// fs0: texture
					// fc0: color to multiply by texture color
					"tex ft0, v0, fs0 <sampler>",
					"mov ft1, fc0",
					"mul ft1.xyz, ft1.xyz, ft1.www",
					"mul oc, ft0, ft1"
				].join("\n")
			);
		}

		
		public final function setColor(r:Number, g:Number, b:Number, a:Number):void {
			_color = getColor(r, g, b, a);
		}

		public final function set intColor(c:uint):void {
			setColor(
				((c >> 16) & 255) / 255,
				((c >> 8) & 255) / 255,
				(c & 255) / 255,
				((c >> 24) & 255) / 255
			);
		}

		public final function set color(c:uint):void {
			intColor = c;
		}
		
		override internal final function bind(device:Device, batch:BufferBatch):void {
			device.setTextureAt(0, batch._texture);
			device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
			device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _color, 0);
		}
		
		internal static function recreate():void 	{
			_shader.recreate();
		}
	}

}
