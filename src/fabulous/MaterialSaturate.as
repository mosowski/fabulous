package fabulous {
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	public class MaterialSaturate extends Material {
		internal static var _shader:Shader;

		internal var _weights:ByteArray;

		public function MaterialSaturate(weight:Number=0) {
			_weights = new ByteArray();
			_weights.endian = Endian.LITTLE_ENDIAN;
			_weights.writeFloat(0.3);
			_weights.writeFloat(0.59);
			_weights.writeFloat(0.11);
			_weights.writeFloat(weight);
		}

		internal static function initShader(device:Device):void {
			_shader = new Shader(device);
			_shader.setCode(
				[
					// va0.xy: vertex position
					// va1.xy: vertex uv
					// va2.x: vertex alpha
					// vc0.xy: screen scale
					// vc0.zw: screen translation
					"mov vt0, va0",
					"mul vt0.xy, vt0.xy, vc0.xy",
					"add vt0.xy, vt0.xy, vc0.zw",
					"mov op, vt0",
					"mov v0, va1",
					"mov v1, va2"
				].join("\n"),
				[
					// v0: vertex uv
					// v1: vertex alpha
					// fs0: texture
					// fc0.xyz: desaturation consts
					// fc0.w: saturation weight
					"tex ft0, v0, fs0 <sampler>",
					"dp3 ft1, ft0.xyz, fc0.xyz",
					"sub ft0.xyz, ft0.xyz, ft1.xxx",
					"mul ft0.xyz, ft0.xyz, fc0.www",
					"add ft0.xyz, ft0.xyz, ft1.xxx",
					"mov oc, ft0"
				].join("\n")
			);
		}

		
		public final function set weight(v:Number):void {
			_weights.position = 12;
			_weights.writeFloat(v);
		}

		
		override internal final function bind(device:Device, batch:BufferBatch):void {
			device.setTextureAt(0, batch._texture);
			device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
			device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _weights, 0);
		}
		
		internal static function recreate():void 	{
			_shader.recreate();
		}
	}

}
