package fabulous {
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	public class MaterialSilhouette extends Material {
		internal static var _shader:Shader;

		internal var _color:ByteArray;

		public function MaterialSilhouette() {
			_color = new ByteArray();
			_color.endian = Endian.LITTLE_ENDIAN;
			_color.writeFloat(0);
			_color.writeFloat(0);
			_color.writeFloat(0);
			_color.writeFloat(0);
		}

		internal static function initShader(device:Device):void {
			_shader = new Shader(device);
			_shader.setCode(
				[
					// va0.xy: vertex position
					// va1.xy: vertex uv
					// va2.x: vertex alpha
					// vc0.xy: screen scale
					// vc0.zw: screen translation
					"mov vt0, va0",
					"mul vt0.xy, vt0.xy, vc0.xy",
					"add vt0.xy, vt0.xy, vc0.zw",
					"mov op, vt0",
					"mov v0, va1",
					"mov v1, va2"
				].join("\n"),
				[
					// v0: vertex uv
					// v1: vertex alpha
					// fs0: texture
					// fc0: silhouette color
					"tex ft0, v0, fs0 <sampler>",
					"mov ft0.xyz, fc0.xyz",
					"mov oc, ft0"
				].join("\n")
			);
		}

		public final function set color(c:uint):void {
			_color.position = 0;
			_color.writeFloat(((c >> 16) & 255) / 255);
			_color.writeFloat(((c >> 8) & 255) / 255);
			_color.writeFloat((c & 255) / 255);
			_color.writeFloat(((c >> 24) & 255) / 255);
		}
		
		override internal final function bind(device:Device, batch:BufferBatch):void {
			device.setTextureAt(0, batch._texture);
			device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
			device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
			device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _color, 0);
		}
		
		internal static function recreate():void {
			_shader.recreate();
		}
	}

}
