package fabulous {
	
	public class MetaNode {
		internal var _name:String;
		internal var _x:Number;
		internal var _y:Number;
		internal var _scaleX:Number;
		internal var _scaleY:Number;
		internal var _visible:Boolean;

		public function MetaNode() {
		}

		
		public final function get name():String {
			return _name;
		}

		
		public final function get x():Number {
			return _x;
		}

		
		public final function get y():Number {
			return _y;
		}

		
		public final function get scaleX():Number {
			return _scaleX;
		}

		
		public final function get scaleY():Number {
			return _scaleY;
		}

		
		public final function get isVisible():Boolean {
			return _visible;
		}

	}

}
