package fabulous {
	
	public class MetaNodeData {
		internal var _name:String;
		internal var _keyFramesNumbers:Vector.<int>;
		internal var _keyFrames:Vector.<MetaNode>;

		public function MetaNodeData() {
		}

		internal function fromObject(object:Object):void {
			_name = object.name;
			_keyFramesNumbers = Vector.<int>(object.keyFramesNumbers);
			_keyFrames = new Vector.<MetaNode>(_keyFramesNumbers.length, true);
			for (var i:int = 0; i < _keyFramesNumbers.length; ++i) {
				var metaNode:MetaNode = _keyFrames[i] = new MetaNode();
				metaNode._name = _name;
				metaNode._x = object.x[i];
				metaNode._y = object.y[i];
				metaNode._scaleX = object.scaleX[i];
				metaNode._scaleY = object.scaleY[i];
				metaNode._visible = object.visible[i] as Boolean;
			}
		}

		
		internal final function getMetaNodeForFrame(frameNo:int):MetaNode {
			for (var i:int = _keyFramesNumbers.length - 1; i >= 0; --i) {
				if (frameNo >= _keyFramesNumbers[i]) {
					return _keyFrames[i];
				}
			}
			return _keyFrames[_keyFramesNumbers.length - 1];
		}

	}

}
