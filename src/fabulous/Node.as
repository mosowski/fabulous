package fabulous {

	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	
	public class Node {
		internal var _x : Number;
		internal var _y : Number;
		internal var _scaleX : Number;
		internal var _scaleY : Number;
		internal var _alpha : Number;
		internal var _rotation : Number;

		internal var _derivedIsVisible : Boolean;
		internal var _derivedAlpha : Number;
		internal var _mtx:Matrix;

		internal var _hasTransformationChanged:Boolean;
		internal var _isVisible:Boolean;
		internal var _hitBitMask:uint;

		internal var _parent:Node;
		internal var _renderables:Vector.<Renderable>;
		internal var _children:NodeList;

		internal var _isClippingChildren:Boolean;
		internal var _mask:Rectangle;

		internal var _touchBegan:Signal;
		internal var _touchMoved:Signal;
		internal var _touchEnded:Signal;
		internal var _tapped:Signal;
		internal var _swipeBegan:Signal;
		internal var _swipeMoved:Signal;
		internal var _swipeEnded:Signal;
		internal var _userHitTest:Function;

		public static const HITMASK_ANY:uint = 1;
		public static const HITMASK_TOUCH:uint = 2;
		public static const HITMASK_SWIPE_X:uint = 4;
		public static const HITMASK_SWIPE_Y:uint = 8;

		public function Node() {
			_x = 0;
			_y = 0;
			_scaleX = 1;
			_scaleY = 1;
			_alpha = 1;
			_rotation = 0;
			_derivedAlpha = 1;
			_derivedIsVisible = true;
			_hasTransformationChanged = true;
			_isVisible = true;
			_isClippingChildren = true;
			_hitBitMask = 1;
			_mtx = new Matrix();
		}
		
		public function clone():Node {
			var clone:Node = new Node();
			clone.copyTransformationFrom(this);
			clone._alpha = _alpha;
			return clone;
		}

		
		public final function copyTransformationFrom(node:Node):void {
			_x = node._x;
			_y = node._y;
			_scaleX = node._scaleX;
			_scaleY = node._scaleY;
		}

		internal function update():void {
			if (_hasTransformationChanged) {
				revalidate();
			} else {
				for (var it:NodeList = _children; it; it = it._next) {
					it._elem.update();
				}
			}
		}

		public function revalidate():void {
			if (_parent) {
				if (_rotation == 0) {
					_mtx.setTo(_scaleX, 0, 0, _scaleY, _x, _y);
				} else {
					_mtx.identity();
					_mtx.scale(_scaleX, _scaleY);
					_mtx.rotate(_rotation);
					_mtx.tx += _x;
					_mtx.ty += _y;
				}
				_mtx.concat(_parent._mtx);

				_derivedAlpha = _parent._derivedAlpha * _alpha;

				_derivedIsVisible = _parent._derivedIsVisible && _isVisible;
			} else {
				_mtx.identity();
				_mtx.scale(_scaleX, _scaleY);
				_mtx.rotate(_rotation);
				_mtx.tx += _x;
				_mtx.ty += _y;
				_derivedAlpha = _alpha;
				_derivedIsVisible = _isVisible;
			}
			_hasTransformationChanged = false;

			for (var it:NodeList = _children; it; it = it._next) {
				it._elem.revalidate();
			}
		}

		public function addChild(child:Node):void {
			if (child._parent != null) {
				child.detachFromParent();
			}
			if (_children == null) {
				_children = new NodeList(child);
			} else {
				if (!_children.find(child)) {
					_children.append(child);
				}
			}
			child._parent = this;
			child.revalidate();
		}

		public function addChildAt(child:Node, index:int):void {
			if (child._parent != null) {
				throw "Node is already has a parent.";
			}
			if (_children == null) {
				_children = new NodeList(child);
			} else {
				if (!_children.find(child)) {
					_children = _children.insert(child, index);
				}
			}
			child._parent = this;
			child.revalidate();
		}

		public function removeChild(child:Node):void {
			var item:NodeList = _children.find(child);
			if (item) {
				item._elem._parent = null;
				if (item == _children) {
					_children = item._next;
					if (item._next) {
						item._next._prev = null;
					}
				} else {
					item.remove();
				}
			}
		}

		public function removeChildren():void {
			if (_children) {
				for (var it:NodeList = _children; it; it = it._next) {
					it._elem._parent = null;
				}
				_children = null;
			}
		}

		public function get numChildren():int {
			if (_children) {
				return _children.length;
			} else {
				return 0;
			}
		}

		[Inline]
		public final function getChild(index:int):Node {
			return _children.at(index)._elem;
		}

		[Inline]
		public final function get parent():Node {
			return _parent;
		}
		public final function set parent(v:Node):void {
			if (v != null) {
				v.addChild(this);
			} else {
				detachFromParent();
			}
		}

		[Inline]
		public final function detachFromParent():void {
			if (_parent) {
				_parent.removeChild(this);
			}
		}

		/**
		 * Adds renderable to the end of node's renderables list.
		 * @param	renderable
		 */
		public function addRenderable(renderable:Renderable):void {
			addRenderableAt(renderable, numRenderables);
		}

		/**
		 * Adds renderable at index position. If index is greater than current
		 * number of renderables, list is filled with preceeding nulls.
		 * @param	renderable
		 * @param	index
		 */
		public function addRenderableAt(renderable:Renderable, index:int):void {
			if (renderable._node != null) {
				renderable._node.removeRenderable(renderable);
			}
			if (_renderables == null) {
				_renderables = new Vector.<Renderable>(index + 1);
			} else {
				if (index >= _renderables.length) {
					_renderables.length = index + 1;
				}
			}
			renderable._node = this;
			_renderables[index] = renderable;
		}
		
		public function getRenderableIndex(renderable:Renderable):int {
			return _renderables != null ? _renderables.indexOf(renderable) : -1;
		}

		/**
		 * Removes renderable from list if found.
		 * @param	renderable
		 */
		public function removeRenderable(renderable:Renderable):void {
			var renderableIndex:int = getRenderableIndex(renderable);
			if (renderableIndex != -1) {
				_renderables[renderableIndex]._node = null;
				_renderables.splice(renderableIndex, 1);
			}
		}

		/**
		 * Returns number of renderables. Preceeding nulls are counted too.
		 */
		[Inline]
		public final function get numRenderables():int {
			if (_renderables) {
				return _renderables.length;
			} else {
				return 0;
			}
		}

		/**
		 * Returns renderable at specified index. It may return null if at
		 * index preceeding null is found.
		 * @param	index
		 * @return
		 */
		[Inline]
		public final function getRenderable(index:int):Renderable {
			return _renderables[index];
		}
		
		public final function hasRenderable(renderable:Renderable):Boolean {
			return getRenderableIndex(renderable) != -1;
		}

		/**
		 * Removes all renderables from Node
		 */
		public final function removeRenderables():void {
			if (_renderables) {
				for (var i:int = 0; i < _renderables.length; ++i) {
					_renderables[i]._node = null;
				}
				_renderables.length = 0;
			}
		}

		/**
		 * Enables receiving touch events by the node.
		 */
		public final function set isTouchEnabled(value:Boolean):void {
			if (value) {
				_hitBitMask |= HITMASK_TOUCH;
			} else {
				_hitBitMask &= ~HITMASK_TOUCH;
			}
		}
		public final function get isTouchEnabled():Boolean {
			return _hitBitMask & HITMASK_TOUCH;
		}

		/**
		 * Enables receiving swipe signals by the node.
		 */
		
		public final function set isSwipeXEnabled(value:Boolean):void {
			if (value) {
				_hitBitMask |= HITMASK_SWIPE_X;
			} else {
				_hitBitMask &= ~HITMASK_SWIPE_X;
			}
		}
		public final function get isSwipeXEnabled():Boolean {
			return _hitBitMask & HITMASK_SWIPE_X;
		}

		/**
		 * Enables receiving swipe signals by the node.
		 */
		
		public final function set isSwipeYEnabled(value:Boolean):void {
			if (value) {
				_hitBitMask |= HITMASK_SWIPE_Y;
			} else {
				_hitBitMask &= ~HITMASK_SWIPE_Y;
			}
		}
		public final function get isSwipeYEnabled():Boolean {
			return _hitBitMask & HITMASK_SWIPE_Y;
		}

		public final function set isClippingChildren(v:Boolean):void {
			_isClippingChildren = v;
		}
		public final function get isClippingChildren():Boolean {
			return _isClippingChildren;
		}

		[Inline]
		public final function get x():Number {
			return _x;
		}
		[Inline]
		public final function set x(value:Number):void {
			_x = value;
			_hasTransformationChanged = true;
		}

		[Inline]
		public final function get y():Number {
			return _y;
		}
		[Inline]
		public final function set y(value:Number):void {
			_y = value;
			_hasTransformationChanged = true;
		}

		[Inline]
		public final function get scaleX():Number {
			return _scaleX;
		}
		[Inline]
		public final function set scaleX(value:Number):void {
			_scaleX = value;
			_hasTransformationChanged = true;
		}

		[Inline]
		public final function get scaleY():Number {
			return _scaleY;
		}
		[Inline]
		public final function set scaleY(value:Number):void {
			_scaleY = value;
			_hasTransformationChanged = true;
		}

		[Inline]
		public final function get alpha():Number {
			return _alpha;
		}
		[Inline]
		public final function set alpha(value:Number):void {
			_alpha = value;
			_hasTransformationChanged = true;
		}

		[Inline]
		public final function get rotation():Number {
			return _rotation;
		}
		[Inline]
		public final function set rotation(value:Number):void {
			_rotation = value;
			_hasTransformationChanged = true;
		}

		[Inline]
		public final function get derivedX():Number {
			return _mtx.tx;
		}

		[Inline]
		public final function get derivedY():Number {
			return _mtx.ty;
		}

		[Inline]
		public final function get derivedScaleX():Number {
			return Math.sqrt(_mtx.a * _mtx.a + _mtx.b * _mtx.b);
		}

		[Inline]
		public final function get derivedScaleY():Number {
			return Math.sqrt(_mtx.c * _mtx.c + _mtx.d * _mtx.d);
		}

		[Inline]
		public final function get derivedAlpha():Number {
			return _derivedAlpha;
		}

		[Inline]
		public final function get mtx():Matrix {
			return _mtx;
		}
		
		[Inline]
		public final function get derivedRotation():Number {
			return Math.atan2(_mtx.b, _mtx.a);
		}

		[Inline]
		public final function get isVisible():Boolean {
			return _derivedIsVisible;
		}
		[Inline]
		public final function set isVisible(v:Boolean):void {
			if (_isVisible != v) {
				_isVisible = v;
				_hasTransformationChanged = true;
			}
		}

		public function getHitNode(x:Number, y:Number, mask:uint = 1):Node {
			var i:int, it:NodeList;
			var hitNode:Node = null;
			if (_derivedIsVisible) {
				if (_children) {
					for (it = _children; it._next; it = it._next) { }
					for (; it; it=it._prev) {
						hitNode = it._elem.getHitNode(x, y, mask);
						if (hitNode != null) {
							return hitNode;
						}
					}
				}

				if ((_hitBitMask & mask) && _renderables) {
					var hitmask:Hitmask;
					for (i =  _renderables.length - 1; i >= 0; --i) {
						if (_renderables[i]) {
							hitmask = _renderables[i].hitmask;
							if (hitmask != null) {
								if (hitmask.hitTest(x, y, derivedX, derivedY, derivedScaleX, derivedScaleY)) {
									if (_userHitTest == null || _userHitTest(x, y, mask)) {
										return this;
									}
								}
							}
						}
					}
				}
			}
			return null;
		}

		
		[Inline]
		public final function get touchBegan():Signal {
			_touchBegan ||= new Signal();
			return _touchBegan;
		}

		[Inline]
		public final function get touchMoved():Signal {
			_touchMoved ||= new Signal();
			return _touchMoved;
		}

		[Inline]
		public final function get touchEnded():Signal {
			_touchEnded ||= new Signal();
			return _touchEnded;
		}

		[Inline]
		public final function get tapped():Signal {
			_tapped ||= new Signal();
			return _tapped;
		}

		[Inline]
		public final function get swipeBegan():Signal {
			_swipeBegan ||= new Signal();
			return _swipeBegan;
		}

		[Inline]
		public final function get swipeMoved():Signal {
			_swipeMoved ||= new Signal();
			return _swipeMoved;
		}

		[Inline]
		public final function get swipeEnded():Signal {
			_swipeEnded ||= new Signal();
			return _swipeEnded;
		}

		public function set userHitTest(v:Function):void {
			_userHitTest = v;
		}
		public function get userHitTest():Function {
			return _userHitTest;
		}

		public function set mask(v:Rectangle):void {
			_mask = v;
		}
		public function get mask():Rectangle {
			_mask ||= new Rectangle();
			return _mask;
		}

		public function dispose():void {
			_mtx = null;
			_parent = null;
			if (_renderables != null) {
				for (var i:int = _renderables.length - 1; i >= 0; --i) {
					_renderables[i].dispose();
				}
			}
			if (_children != null) {
				_children.dispose();
				_children = null;
			}
			_mask = null;
			if (_touchBegan != null) {
				_touchBegan.clean();
				_touchBegan = null;
			}
			if (_touchMoved != null) {
				_touchMoved.clean();
				_touchMoved = null;
			}
			if (_touchEnded != null) {
				_touchEnded.clean();
				_touchEnded = null;
			}
			if (_tapped != null) {
				_tapped.clean();
				_tapped = null;
			}
			if (_swipeBegan != null) {
				_swipeBegan.clean();
				_swipeBegan = null;
			}
			if (_swipeMoved != null) {
				_swipeMoved.clean();
				_swipeMoved = null;
			}
			if (_swipeEnded != null) {
				_swipeEnded.clean();
				_swipeEnded = null;
			}
			_userHitTest = null;
		}
	}
}
