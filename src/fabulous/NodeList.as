package fabulous {
	
	public class NodeList {
		internal var _elem:Node;
		internal var _next:NodeList;
		internal var _prev:NodeList;

		public function NodeList(elem:Node = null, prev:NodeList = null) {
			_elem = elem;
			_prev = prev;
			_next = null;
		}

		
		public final function append(elem:Node):void {
			var item:NodeList = this;
			while (item._next) {
				item = item._next;
			}
			item._next = new NodeList(elem, item);
		}

		
		/**
		 * Inserts element at specified index, shifting rest of existing elements if necessary.
		 * Returns new head of the list.
		 * @param elem
		 * @param index
		 * @return
		 */
		public final function insert(elem:Node, index:int):NodeList {
			var item:NodeList = this;
			var count:int = index;
			while (count-- > 0 && item._next) {
				item = item._next;
			}
			var newItem:NodeList;
			if (count > 0) {
				newItem = new NodeList(elem, item);
				item._next = newItem;
			} else {
				newItem = new NodeList(elem, item._prev);
				newItem._next = item;
				if (item._prev) {
					item._prev._next = newItem;
				}
				item._prev = newItem;
			}
			if (index == 0) {
				return newItem;
			} else {
				return this;
			}
		}

		
		public final function get length():int {
			var result:int = 0;
			var item:NodeList = this;
			while (item) {
				item = item._next;
				result++;
			}
			return result;
		}

		
		public final function clear():void {
			_elem = null;
			_prev = null;
			_next = null;
		}

		
		public final function remove():void {
			if (_next) {
				_next._prev = _prev;
			}
			if (_prev) {
				_prev._next = _next;
			}
		}

		
		public final function find(elem:Node):NodeList {
			var item:NodeList = this;
			while (item) {
				if (item._elem == elem) {
					return item;
				} else {
					item = item._next;
				}
			}
			return null;
		}

		
		public final function at(index:int):NodeList {
			var item:NodeList = this;
			while (index-- > 0) {
				item = item._next;
			}
			return item;
		}

		
		public final function addNext(elem:Node):void {
			var item:NodeList = new NodeList(elem, this);
			item._next = _next;
			_next = item;
		}

		
		public final function addPrev(elem:Node):void {
			var item:NodeList = new NodeList(elem, _prev);
			item._next = this;
			this._prev = item;
		}

		
		public final function removeAt(index:int):void {
			var item:NodeList = this;
			while (index-- > 0) {
				item = item._next;
			}
			item.remove();
		}

		
		public final function removeElem(elem:Node):void {
			var item:NodeList = this;
			while (item && item._elem != elem) {
				item = item._next;
			}
			if (item) {
				item.remove();
			}
		}

		public final function dispose():void {
			var item:NodeList = this;
			while (item) {
				item._elem.dispose();
				var next:NodeList = item._next;
				item._next = null;
				item._prev = null;
				item = next;
			}
		}
	}

}
