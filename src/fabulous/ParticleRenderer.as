package fabulous {	  
	
	public final class ParticleRenderer extends Renderer {

		public function ParticleRenderer(device:Device) {
			super(device);
		}

		override internal function processRenderable(renderable:Renderable):void {
			var particles:Particles = renderable as Particles;

			if (particles._texture && particles._spawner) {
				particles._bufferBatchBuffer.reset();
				_device._bufferBatcher.queue(particles._bufferBatchBuffer, particles._material, particles._texture, particles._quota * 6);
			}

		}

	}
}

