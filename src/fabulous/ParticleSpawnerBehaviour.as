package fabulous {
	public interface ParticleSpawnerBehaviour {
		function tick(particles:Particles,numParticles:Number):void;
		function spawn():void;
		function get life():Number;
		function get startX():Number;
		function get startY():Number;
		function get deltaX():Number;
		function get deltaY():Number;
		function get startSize():Number;
		function get deltaSize():Number;
		function get startRotation():Number;
		function get deltaRotation():Number;
		function get startColor():uint;
		function get endColor():uint;
	}

}
