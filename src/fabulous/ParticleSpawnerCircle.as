package fabulous {
	public class ParticleSpawnerCircle extends ParticleSpawnerUniformBehaviour {
		private var _minRadius:Number;
		private var _maxRadius:Number;

		public function ParticleSpawnerCircle() {
			super();
		}

		public function set minRadius(v:Number):void {
			_minRadius = v;
		}

		public function set maxRadius(v:Number):void {
			_maxRadius = v;
		}

		override public function spawn():void {
			var angle:Number = Math.random() * 2 * Math.PI;
			var radius:Number = _minRadius + Math.sqrt(Math.random()) * (_maxRadius - _minRadius);
			_originOffsetX = Math.sin(angle) * radius;
			_originOffsetY = Math.cos(angle) * radius;
			super.spawn();
		}

	}

}
