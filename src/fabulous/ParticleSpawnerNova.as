package fabulous {
	public class ParticleSpawnerNova extends ParticleSpawnerUniformBehaviour {

		internal var _minRadius:Number;
		internal var _maxRadius:Number;

		public function ParticleSpawnerNova() {
			super();
		}

		public function set minRadius(v:Number):void {
			_minRadius = v;
		}

		public function set maxRadius(v:Number):void {
			_maxRadius = v;
		}

		override public function spawn():void {
			var angle:Number = Math.random() * 2 * Math.PI;
			var radius:Number = _minRadius + Math.sqrt(Math.random()) * (_maxRadius - _minRadius);
			_originOffsetX = Math.sin(angle) * radius;
			_originOffsetY = Math.cos(angle) * radius;

			var vel:Number = minVel + Math.random() * (maxVel - minVel);

			_life = minLife + Math.random() * (maxLife - minLife);
			_startX = _spawnerX + _originOffsetX;
			_startY = _spawnerY + _originOffsetY;
			_deltaX = Math.sin(angle) * _life * vel;
			_deltaY = Math.cos(angle) * _life * vel;

			_spawnerX += _originStepX;
			_spawnerY += _originStepY;

			_startSize = sMinSize + Math.random() * (sMaxSize - sMinSize);
			_deltaSize = eMinSize + Math.random() * (eMaxSize - eMinSize) - _startSize;

			_startRotation = minRotation + Math.random() * (maxRotation - minRotation);
			_deltaRotation = (minOmega + Math.random() * ( maxOmega - minOmega)) * _life;

			var sColorFactor:Number = Math.random();
			var eColorFactor:Number = Math.random();
			var sR:Number = sMinR + sColorFactor * (sMaxR - sMinR);
			var sG:Number = sMinG + sColorFactor * (sMaxG - sMinG);
			var sB:Number = sMinB + sColorFactor * (sMaxB - sMinB);
			var sA:Number = sMinA + sColorFactor * (sMaxA - sMinA);
			var eR:Number = eMinR + eColorFactor * (eMaxR - eMinR);
			var eG:Number = eMinG + eColorFactor * (eMaxG - eMinG);
			var eB:Number = eMinB + eColorFactor * (eMaxB - eMinB);
			var eA:Number = eMinA + eColorFactor * (eMaxA - eMinA);

			_startColor = uint(sR * 255) | (uint(sG * 255) << 8) | (uint(sB * 255) << 16) | (uint(sA * 255) << 24);
			_endColor = uint(eR * 255) | (uint(eG * 255) << 8) | (uint(eB * 255) << 16) | (uint(eA * 255) << 24);
		}

	}

}
