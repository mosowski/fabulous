package fabulous {
	public class ParticleSpawnerUniformBehaviour implements ParticleSpawnerBehaviour {

		public var sMinR:Number;
		public var sMaxR:Number;
		public var sMinG:Number;
		public var sMaxG:Number;
		public var sMinB:Number;
		public var sMaxB:Number;
		public var sMinA:Number;
		public var sMaxA:Number;

		public var sMinSize:Number;
		public var sMaxSize:Number;

		public var eMinR:Number;
		public var eMaxR:Number;
		public var eMinG:Number;
		public var eMaxG:Number;
		public var eMinB:Number;
		public var eMaxB:Number;
		public var eMinA:Number;
		public var eMaxA:Number;

		public var eMinSize:Number;
		public var eMaxSize:Number;

		public var minLife:Number;
		public var maxLife:Number;
		public var minAngle:Number;
		public var maxAngle:Number;
		public var minVel:Number;
		public var maxVel:Number;
		public var minRotation:Number;
		public var maxRotation:Number;
		public var minOmega:Number;
		public var maxOmega:Number;

		internal var _life:Number;
		internal var _startX:Number;
		internal var _startY:Number;
		internal var _deltaX:Number;
		internal var _deltaY:Number;
		internal var _startSize:Number;
		internal var _deltaSize:Number;
		internal var _startRotation:Number;
		internal var _deltaRotation:Number;
		internal var _startColor:uint;
		internal var _endColor:uint;

		internal var _lastOriginX:Number;
		internal var _lastOriginY:Number;
		// used for particles initial position interpolation
		internal var _originStepX:Number;
		internal var _originStepY:Number;
		// used by spawners to control single particle spawn position
		internal var _originOffsetX:Number;
		internal var _originOffsetY:Number;
		// single particle interpolated initial position
		internal var _spawnerX:Number;
		internal var _spawnerY:Number;

		public function ParticleSpawnerUniformBehaviour() {
			_originOffsetX = 0;
			_originOffsetY = 0;
			_lastOriginX = 0;
			_lastOriginY = 0;
			_spawnerX = 0;
			_spawnerY = 0;
		}

		public function tick(particles:Particles, numParticles:Number):void {
			var originX:Number = (particles._node.derivedX - particles._anchorNode.derivedX)/particles._anchorNode.derivedScaleX;
			var originY:Number = (particles._node.derivedY - particles._anchorNode.derivedY)/particles._anchorNode.derivedScaleY;

			if (numParticles > 0) {
				_originStepX = (originX - _lastOriginX) / numParticles;
				_originStepY = (originY - _lastOriginY) / numParticles;
				_spawnerX = _lastOriginX;
				_spawnerY = _lastOriginY;
			} else {
				_spawnerX = originX;
				_spawnerY = originY;
			}
			_lastOriginX = originX;
			_lastOriginY = originY;
		}

		public function spawn():void {
			var angle:Number = minAngle + Math.random() * (maxAngle - minAngle);
			var vel:Number = minVel + Math.random() * (maxVel - minVel);

			_life = minLife + Math.random() * (maxLife - minLife);
			_startX = _spawnerX + _originOffsetX;
			_startY = _spawnerY + _originOffsetY;
			_deltaX = Math.sin(angle) * _life * vel;
			_deltaY = Math.cos(angle) * _life * vel;

			_spawnerX += _originStepX;
			_spawnerY += _originStepY;

			_startSize = sMinSize + Math.random() * (sMaxSize - sMinSize);
			_deltaSize = eMinSize + Math.random() * (eMaxSize - eMinSize) - _startSize;

			_startRotation = minRotation + Math.random() * (maxRotation - minRotation);
			_deltaRotation = (minOmega + Math.random() * ( maxOmega - minOmega)) * _life;

			var sColorFactor:Number = Math.random();
			var eColorFactor:Number = Math.random();
			var sR:Number = sMinR + sColorFactor * (sMaxR - sMinR);
			var sG:Number = sMinG + sColorFactor * (sMaxG - sMinG);
			var sB:Number = sMinB + sColorFactor * (sMaxB - sMinB);
			var sA:Number = sMinA + sColorFactor * (sMaxA - sMinA);
			var eR:Number = eMinR + eColorFactor * (eMaxR - eMinR);
			var eG:Number = eMinG + eColorFactor * (eMaxG - eMinG);
			var eB:Number = eMinB + eColorFactor * (eMaxB - eMinB);
			var eA:Number = eMinA + eColorFactor * (eMaxA - eMinA);

			_startColor = uint(sR * 255) | (uint(sG * 255) << 8) | (uint(sB * 255) << 16) | (uint(sA * 255) << 24);
			_endColor = uint(eR * 255) | (uint(eG * 255) << 8) | (uint(eB * 255) << 16) | (uint(eA * 255) << 24);
		}


		
		public final function get life():Number {
			return _life;
		}

		
		public final function get startX():Number {
			return _startX;
		}

		
		public final function get startY():Number {
			return _startY;
		}

		
		public final function get deltaX():Number {
			return _deltaX;
		}

		
		public final function get deltaY():Number {
			return _deltaY;
		}

		
		public final function get startSize():Number {
			return _startSize;
		}

		
		public final function get deltaSize():Number {
			return _deltaSize;
		}

		
		public final function get startRotation():Number {
			return _startRotation;
		}

		
		public final function get deltaRotation():Number {
			return _deltaRotation;
		}

		
		public final function get startColor():uint {
			return _startColor;
		}

		
		public final function get endColor():uint {
			return _endColor;
		}

	}

}
