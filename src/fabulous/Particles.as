package fabulous {
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.geom.Rectangle;	
	
	public class Particles extends Renderable {
		internal var _texture:Texture;
		internal var _material:MaterialParticles;
		internal var _device:Device;
		internal var _anchorNode:Node;

		internal var _vertexBuffer:ByteArrayVertexBuffer;
		internal var _bufferBatchBuffer:BufferBatchBuffer;

		internal var _hitmask:Hitmask;
		internal var _bounds:Rectangle;
		internal var _radius:Number;

		internal var _particlesToSpawn:Number;
		internal var _quota:int;
		internal var _spawnSlotId:int;
		internal var _expirationTimes:Vector.<Number>;

		internal var _lastSpawnerX:Number;
		internal var _lastSpawnerY:Number;

		internal var _spawner:ParticleSpawnerBehaviour;
		internal var _srcBlendFactor:String;
		internal var _destBlendFactor:String;

		internal var _spawnRate:Number;
		internal var _spawnLimit:Number;

		internal static const _offsets:Vector.<Number> = Vector.<Number>([ -0.5, -0.5, 0.5, -0.5, -0.5, 0.5, 0.5, 0.5]);

		internal static var _renderer:Renderer;

		public function Particles(material:MaterialParticles, device:Device) {
			_texture = null;
			_material = material;
			_material._particles = this;
			_device = device;

			_particlesToSpawn = 0;
			_spawnLimit = -1;
			_spawnSlotId = 0;
			_srcBlendFactor = Context3DBlendFactor.ONE;
			_destBlendFactor = Context3DBlendFactor.ONE;

			_hitmask = new Hitmask();
			_hitmask.makeQuad();

			_bounds = new Rectangle();
			_radius = 1000000;
		}

		
		override internal final function get renderer():Renderer {
			return _renderer;
		}

		public function set spawner(v:ParticleSpawnerBehaviour):void {
			_spawner = v;
			if (_spawner) {
				resetSpawner();
			}
		}

		public function get spawner():ParticleSpawnerBehaviour {
			return _spawner;
		}
		
		public function get anchorNode():Node {
			return _anchorNode;
		}

		public function set anchorNode(v:Node):void {
			_anchorNode = v;
		}

		public function set srcBlendFactor(v:String):void {
			_srcBlendFactor = v;
		}

		public function get srcBlendFactor():String {
			 return _srcBlendFactor;
		}

		public function set destBlendFactor(v:String):void {
			_destBlendFactor = v;
		}

		public function get destBlendFactor():String {
			 return _destBlendFactor;
		}

		public function resetSpawner():void {
			if (!_spawner || !_anchorNode) {
				_spawner.tick(this, 0);
			}
		}

		public function set quota(v:int):void {
			if (_quota != v) {
				_quota = v;

				_bufferBatchBuffer = new BufferBatchBuffer(_device, 1, quota * 4, BufferBatchBuffer.VBTYPE_BYTEARRAY, quota * 6, new <String>[
					Context3DVertexBufferFormat.FLOAT_4,
					Context3DVertexBufferFormat.FLOAT_4,
					Context3DVertexBufferFormat.FLOAT_4,
					Context3DVertexBufferFormat.BYTES_4,
					Context3DVertexBufferFormat.BYTES_4
				] );
				_vertexBuffer = _bufferBatchBuffer._vbs[0] as ByteArrayVertexBuffer;

				_expirationTimes = new Vector.<Number>(_quota);
			}
		}

		public function get quota():int {
			return _quota;
		}

		override internal function tick(device:Device):void {
			if (!_spawner || !_anchorNode) {
				return;
			}

			var numNewParticles:Number = spawnRate * _device.dt;
			if (_spawnLimit >= 0) {
				if (numNewParticles >= _spawnLimit) {
					numNewParticles = _spawnLimit;
					_spawnLimit = 0;
				} else {
					_spawnLimit -= numNewParticles;
				}
			}
			_particlesToSpawn += numNewParticles;


			var trials:int = _quota;
			var time:Number = _device._currentTime - _device.dt;

			_spawner.tick(this, _particlesToSpawn);

			while (trials-- && _particlesToSpawn >= 1) {
				if (time > _expirationTimes[_spawnSlotId]) {

					_spawner.spawn();

					var bornTime:Number = time + Math.random()*_device.dt;
					_expirationTimes[_spawnSlotId] = bornTime + _spawner.life;

					_vertexBuffer.vertexPosition = _spawnSlotId * 4;
					_vertexBuffer.beginWrite();
					for (var i:int = 0; i < 4; ++i) {
						// va0
						_vertexBuffer.writeFloat(bornTime);
						_vertexBuffer.writeFloat(_spawner.life);
						_vertexBuffer.writeFloat(_offsets[i * 2]);
						_vertexBuffer.writeFloat(_offsets[i * 2 + 1]);

						// va1
						_vertexBuffer.writeFloat(_spawner.startX);
						_vertexBuffer.writeFloat(_spawner.startY);
						_vertexBuffer.writeFloat(_spawner.deltaX);
						_vertexBuffer.writeFloat(_spawner.deltaY);

						// va2
						_vertexBuffer.writeFloat(_spawner.startSize);
						_vertexBuffer.writeFloat(_spawner.deltaSize);
						_vertexBuffer.writeFloat(_spawner.startRotation);
						_vertexBuffer.writeFloat(_spawner.deltaRotation);

						// va3
						_vertexBuffer.writeUnsignedInt(_spawner.startColor);
						// va4
						_vertexBuffer.writeUnsignedInt(_spawner.endColor);
					}
					_vertexBuffer.endWrite();
					_particlesToSpawn -= 1;

				}
				_spawnSlotId = (_spawnSlotId + 1) % _quota;
			}
			if (!trials) {
				_particlesToSpawn = 0;
			}

		}

		override public function dispose():void {
			super.dispose();
			_texture = null;
			_material = null;
			_device = null;
			_anchorNode = null;
			_vertexBuffer = null;
			_bufferBatchBuffer = null;
			_hitmask = null;
			_bounds = null;
			_expirationTimes = null
			_spawner = null;
		}

		
		override public final function get radius():Number {
			return _radius;
		}

		override public function get hitmask():Hitmask {
			return _hitmask;
		}

		
		public final function set texture(value:Texture):void {
			_texture = value;
		}
		
		public final function get texture():Texture {
			return _texture;
		}

		
		public final function set material(value:MaterialParticles):void {
			if (_material != value) {
				if (_material) {
					_material._particles = null;
				}
				_material = value;
				if (_material) {
					_material._particles = this;
				}
			}
		}

		
		public final function get material():MaterialParticles { 
			return _material;
		}

		public function get spawnRate():Number {
			return _spawnRate;
		}

		public function set spawnRate(value:Number):void {
			_spawnRate = value;
		}

		public function get spawnLimit():Number {
			return _spawnLimit;
		}

		public function set spawnLimit(value:Number):void {
			_spawnLimit = value;
		}
	}

}
