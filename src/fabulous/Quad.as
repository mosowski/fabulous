package fabulous {
	import flash.geom.Rectangle;	
	
	public class Quad extends Renderable {
		internal var _quadMaterial:QuadMaterial;
		internal var _texture:Texture;
		internal var _material:Material;

		internal var _uvX:Number;
		internal var _uvY:Number;
		internal var _uvWidth:Number;
		internal var _uvHeight:Number;

		internal var _hitmask:Hitmask;
		internal var _radius:Number;

		internal static var _renderer:Renderer;

		public function Quad(material:QuadMaterial) {
			_texture = null;
			_quadMaterial = material;

			_uvX = _uvY = 0;
			_uvWidth = _uvHeight = 1;

			_hitmask = new Hitmask();
			_hitmask.makeQuad();

			_radius = 1;
		}

		
		override internal final function get renderer():Renderer {
			return _renderer;
		}

		
		override public final function get hitmask():Hitmask {
			return _hitmask;
		}

		
		override public final function get radius():Number {
			return _radius;
		}

		
		public final function set texture(value:Texture):void {
			_texture = value;
		}

		
		public final function get texture():Texture {
			return _texture;
		}

		
		public final function set material(value :QuadMaterial):void {
			_quadMaterial = value;
		}

		
		public final function get material():QuadMaterial {
			return _quadMaterial;
		}

		public function setUV(x:Number, y:Number, w:Number, h:Number):void {
			_uvX = x;
			_uvY = y;
			_uvWidth =	w;
			_uvHeight = h;
		}	

		override public function dispose():void {
			super.dispose();
			_quadMaterial = null;
			_texture = null;
			_material = null;
			_hitmask = null;
		}
	}
}
