package fabulous {

	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import flash.geom.Matrix;

	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;

	import flash.system.ApplicationDomain;
	import avm2.intrinsics.memory.sf32;

	public class QuadBatcher {
		internal var _device:Device;

		internal var _ib:IndexBuffer;
		internal var _vb:VertexBuffer;
		
		internal var _params:ByteArray;

		internal var _numBatchedQuads:int;

		internal var _currentQuadId:int;
		internal var _currentQuadMaterial:QuadMaterial;
		internal var _currentTexture:Texture;

		internal var _numDrawnQuads:int;

		public function QuadBatcher(device:Device) {
			_device = device;

			_numBatchedQuads = 40;
			_numDrawnQuads = 0;
		}

		internal function init():void {
			_ib = new IndexBuffer(_device);
			_ib.setSize((_numBatchedQuads + 1) * 6);

			_vb = new ByteArrayVertexBuffer(_device);
			_vb.setSizeAndFormat(_numBatchedQuads * 4, new <String> [ 
				Context3DVertexBufferFormat.FLOAT_3, // quad indexes
				Context3DVertexBufferFormat.FLOAT_3  // x,y,1
			]);
			_params = new ByteArray();
			_params.endian = Endian.LITTLE_ENDIAN;
			_params.length = _numBatchedQuads * (3 * 4) * 4;

			_vb.beginWrite();
			_vb.position = 0;
			for (var i:int = 0; i < _numBatchedQuads; ++i) {
				_vb.writeFloat(i * 3);
				_vb.writeFloat(i * 3 + 1);
				_vb.writeFloat(i * 3 + 2);
				_vb.writeFloat(0);
				_vb.writeFloat(0);
				_vb.writeFloat(1.0);

				_vb.writeFloat(i * 3);
				_vb.writeFloat(i * 3 + 1);
				_vb.writeFloat(i * 3 + 2);
				_vb.writeFloat(1);
				_vb.writeFloat(0);
				_vb.writeFloat(1.0);

				_vb.writeFloat(i * 3);
				_vb.writeFloat(i * 3 + 1);
				_vb.writeFloat(i * 3 + 2);
				_vb.writeFloat(0);
				_vb.writeFloat(1);
				_vb.writeFloat(1.0);

				_vb.writeFloat(i * 3);
				_vb.writeFloat(i * 3 + 1);
				_vb.writeFloat(i * 3 + 2);
				_vb.writeFloat(1);
				_vb.writeFloat(1);
				_vb.writeFloat(1.0);
			}
			_vb.endWrite();

			_ib.upload();
			_vb.upload();
		}

		internal function beginFrame():void {
			_currentTexture = null;
		}

		internal function flush():void {
			if (_currentQuadId > 0) {
				_device.setTextureAt(0, _currentTexture);
				_device.setShader(_currentQuadMaterial.shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
				_device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);
				_device._ctx.setProgramConstantsFromByteArray(
					Context3DProgramType.VERTEX, 
					0, 
					_currentQuadId * 3,
					_params,
					0
				);
				_device._ctx.setProgramConstantsFromByteArray(
					Context3DProgramType.VERTEX, 
					120, 
					1, 
					_device._screenConstants, 
					0
				);

				_vb.bind(0);
				_device._ctx.drawTriangles(_ib._indexBuffer3d, 0, _currentQuadId * 2);
				_device.clearSamplers();
				_vb.unbind(0);

				_numDrawnQuads += _currentQuadId;
				_currentQuadId = 0;

			}
		}

		internal function queue(quadMaterial:QuadMaterial, quad:Quad):void {
			if (quad._texture != _currentTexture || quadMaterial != _currentQuadMaterial || _currentQuadId == _numBatchedQuads) {
				flush();
				_currentTexture = quad._texture;
				_currentQuadMaterial = quadMaterial;
			}
			var mtx:Matrix = quad._node._mtx;
			if (_device._currentDomainMemoryObject != _params) {
				ApplicationDomain.currentDomain.domainMemory = _device._currentDomainMemoryObject =  _params;
			}
			var pos:int = _currentQuadId * 3 * 4 * 4;

			sf32(mtx.a, pos); pos+=4;
			sf32(mtx.c, pos); pos+=4;
			sf32(mtx.tx, pos); pos+=4;
			sf32(0, pos); pos+=4;

			sf32(mtx.b, pos); pos+=4;
			sf32(mtx.d, pos); pos+=4;
			sf32(mtx.ty, pos); pos+=4;
			sf32(0, pos); pos+=4;

			sf32(quad._uvX, pos); pos+=4;
			sf32(quad._uvY, pos); pos+=4;
			sf32(quad._uvWidth, pos); pos+=4;
			sf32(quad._uvHeight, pos); pos+=4;

			_currentQuadId++;
		}
		
		internal function recreate():void {
			_ib.recreate();
			_vb.recreate();
		}
	}
}

