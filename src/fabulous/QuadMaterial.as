package fabulous {
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;

	public class QuadMaterial {
		internal static var _shader:Shader;

		public function QuadMaterial() {
		}

		internal static function initShader(device:Device):void {
			_shader = new Shader(device);
			_shader.setCode(
				[
					// vc0-vc119: quad params
					// va0.xyz: mtx row 1 id, mtx row 2 id, uv scale id
					// va1.xyz: vertex position
					// vc120.xy: screen scale
					// vc120.zw: screen translation
					"mov vt2, va1",
					"mov vt0, va1",
					"dp3 vt0.x, vt2.xyz, vc[va0.x].xyz",
					"dp3 vt0.y, vt2.xyz, vc[va0.y].xyz", 

					"mov vt1, vc[va0.z]",
					"mul vt1.zw, vt1.zw, va1.xyxy",
					"add vt1.xy, vt1.xy, vt1.zw",

					"mul vt0.xy, vt0.xy, vc120.xy",
					"add vt0.xy, vt0.xy, vc120.zw",

					"mov op, vt0",
					"mov v0, vt1"
				].join("\n"),
				[
					// v0: vertex uv
					// v1: vertex alpha
					// fs0: texture
					"tex oc, v0, fs0 <sampler>"
				].join("\n")
			);
			trace("QuadMaterial shader initialized.");
		}

		public function get shader():Shader {
			return _shader;
		}
		
		internal function recreate():void 	{
			_shader.recreate();
		}
	}
}
