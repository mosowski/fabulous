package fabulous {

	public class QuadNode extends Node {
		public var _pivotNode:Node;
		public var _quad:Quad;

		public var _width:Number;
		public var _height:Number;
		public var _pivotX:Number;
		public var _pivotY:Number;

		public function QuadNode(material:QuadMaterial) {
			super();
			_quad = new Quad(material);
			addRenderable(_quad);
			_pivotNode = new Node();
		}

		public function set width(v:Number):void {
			if (v != _width) {
				scaleX = _width = v;
			}
		}

		public function set height(v:Number):void {
			if (v != _height) {
				scaleY = _height = v;
			}
		}

		public function set pivotX(v:Number):void {
			if (v != _pivotX) {
				_pivotX = v;
				updatePivot();
			}
		}
		public function get pivotX():Number {
			return _pivotX;
		}

		public function set pivotY(v:Number):void {
			if (v != _pivotY) {
				_pivotY = v;
				updatePivot();
			}
		}
		public function get pivotY():Number {
			return _pivotY;
		}

		private function updatePivot():void {
			if (_pivotX == 0 && _pivotY == 0) {
				addRenderable(_quad);
				removeChild(_pivotNode);
				scaleX = _width;
				scaleY = _height;
			} else {
				if (!_pivotNode.parent) {
					_pivotNode.addRenderable(_quad);
					addChild(_pivotNode);
					scaleX = 1;
					scaleY = 1;
					_pivotNode.scaleX = _width;
					_pivotNode.scaleY = _height;
				}
				_pivotNode.x = -_pivotX;
				_pivotNode.y = -_pivotY;
			}
		}

		[Inline]
		public final function set texture(v:Texture):void {
			_quad.texture = v;
		}

		[Inline]
		public final function get texture():Texture {
			return _quad._texture;
		}

		public function setUV(x:Number, y:Number, w:Number, h:Number):void {
			_quad.setUV(x, y, w, h);
		}

		internal function setUVFromFrame(frame:SpriteSheetFrame = null):void {
			if (frame != null) {
				_quad.setUV(
					frame._left / _quad._texture._width,
					frame._top / _quad._texture._height,
					(frame._right - frame._left) / _quad._texture._width,
					(frame._bottom - frame._top) / _quad._texture._height
				);
				pivotX = frame._pivotX;
				pivotY = frame._pivotY;
			}
		}

		public function setTextureFromFrame(frame:SpriteSheetFrame):void {
			if (frame != null) {
				_quad._texture = frame._texture;
				_quad._texture.callAfterLoad(function():void {
					setUVFromFrame(frame);
				});
			}
		}

		override public function dispose():void {
			super.dispose();
			if (_pivotNode != null) {
				_pivotNode.dispose();
				_pivotNode = null;
			}
			if (_quad != null) {
				_quad.dispose();
				_quad = null;
			}
		}
	}
}
