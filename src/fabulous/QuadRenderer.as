package fabulous {	  
	
	public final class QuadRenderer extends Renderer {

		public function QuadRenderer(device:Device) {
			super(device);
		}

		override internal function processRenderable(renderable:Renderable):void {
			var quad:Quad = renderable as Quad;

			if (quad._texture) {
				_device._quadBatcher.queue(quad._quadMaterial, quad)
			}
		}

	}
}

