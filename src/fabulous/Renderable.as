package fabulous {

	public class Renderable {
		internal var _node:Node;

		public function Renderable():void {
		}

		public function get radius():Number {
			return 0;
		}

		public function get hitmask():Hitmask {
			return null;
		}

		internal function tick(device:Device):void {
		}

		internal function get renderer():Renderer {
			throw "Not implemented";
		}
		
		public final function get node():Node {
			return _node;
		}
		
		public final function set node(value:Node):void {
			_node = value;
		}

		public function dispose():void {
			_node = null;
		}
	}

}
