package fabulous {
	public class Renderer {
		internal var _device:Device;

		public function Renderer(device:Device) {
			_device = device;
		}

		internal function init():void {
		}

		internal function beginFrame():void {
		}

		internal function endFrame():void {
		}

		internal function processRenderable(renderable:Renderable):void {
		}

	}

}
