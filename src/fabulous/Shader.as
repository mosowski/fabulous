package fabulous {
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Program3D;	 
	
	public class Shader {
		internal var _device:Device;
		internal var _program3dBySamplerOptions:Array;

		internal var _vertexProgramCode:String;
		internal var _fragmentProgramCode:String;

		public function Shader(device:Device) {
			_device = device;
			_program3dBySamplerOptions = [];
		}

		
		internal final function setCode(vertexProgramCode:String, fragmentProgramCode:String):void {
			_vertexProgramCode = vertexProgramCode;
			_fragmentProgramCode = fragmentProgramCode;
		}

		internal final function getProgram3dForSamplerOptions(options:Vector.<int>):Program3D {
			var program3d:Program3D = _program3dBySamplerOptions[options];
			if (!program3d) {
				program3d = _program3dBySamplerOptions[options] = _device._ctx.createProgram();
				var vertexAgal:AGALMiniAssembler = new AGALMiniAssembler();
				vertexAgal.assemble(Context3DProgramType.VERTEX, _vertexProgramCode);
				var fragmentAgal:AGALMiniAssembler = new AGALMiniAssembler();
				var fragmentCode:String = _fragmentProgramCode.replace(/<sampler>/gi, ShaderSamplerOptions._samplerCodeByInt[options[0]]);
				for (var i:int = 0; i < options.length; ++i) {
					fragmentCode = fragmentCode.replace(new RegExp("<sampler_" + i + ">", "gi"), ShaderSamplerOptions._samplerCodeByInt[options[i]]);
				}
				fragmentAgal.assemble(Context3DProgramType.FRAGMENT, fragmentCode);
				try {
					program3d.upload(vertexAgal.agalcode, fragmentAgal.agalcode);
				} catch (er:Error) {
					Log.e("Shader contains errors.");
					Log.e(er.message);
					Log.i(_vertexProgramCode);
					Log.i(fragmentCode);
				}
			}
			return program3d;
		}

		
		internal final function getSamplerOptionsForTexture(texture:Texture, baseOptions:int):int {
			if (texture._isCompressed) {
				if (texture._hasAlpha) {
					return baseOptions + 2;
				} else {
					return baseOptions + 1;
				}
			} else {
				return baseOptions;
			}
		}
		
		internal final function getSamplerOptionsForTextures(textures:Vector.<Texture>, baseOptions:int):Vector.<int> {
			var samplerOptions:Vector.<int> = new Vector.<int>();
			for (var i:int = 0; i < textures.length; ++i) {
				if (textures[i] != null) {
					samplerOptions.push(getSamplerOptionsForTexture(textures[i], baseOptions));
				}
			}
			return samplerOptions;
		}

		public function recreate():void {
			_program3dBySamplerOptions = [];
		}

	}

}
