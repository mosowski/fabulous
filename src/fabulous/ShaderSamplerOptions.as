package fabulous {
	
	public class ShaderSamplerOptions {

		internal static const TEX_2D_NOMIP_LINEAR:int = 0;
		internal static const TEX_2D_NOMIP_NEAREST:int = 3;
		internal static const TEX_2D_TRILINEAR:int = 6;

		internal static const _samplerCodeByInt:Array = [
			"<2d, nomip, linear, clamp>",
			"<2d, nomip, linear, clamp, dxt1>",
			"<2d, nomip, linear, clamp, dxt5>",

			"<2d, nomip, nearest, clamp>",
			"<2d, nomip, nearest, clamp, dxt1>",
			"<2d, nomip, nearest, clamp, dxt5>",

			"<2d, miplinear, linear, clamp>",
			"<2d, miplinear, linear, clamp, dxt1>",
			"<2d, miplinear, linear, clamp, dxt5>"
		];

	}

}
