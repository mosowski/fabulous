package fabulous {
	import flash.geom.Point;
	import flash.geom.Rectangle;	

	public class Shape extends Renderable {
		internal var _texture:Texture;
		internal var _material:Material;

		internal var _hitmask:Hitmask;
		internal var _bounds:Rectangle;
		internal var _radius:Number;
		internal var _hasBoundsChanged:Boolean;
		internal var _useShapeAsHitmask:Boolean;

		internal var _positions:Vector.<Point>;
		internal var _vertices:Vector.<Number>;
		internal var _indices:Vector.<uint>;

		internal static var _renderer:Renderer;

		public function Shape(material:Material) {
			_material = material;
			_vertices = new Vector.<Number>();
			_indices = new Vector.<uint>();
			_bounds = new Rectangle();
			_useShapeAsHitmask = false;
			_hitmask = new Hitmask();
		}

		
		override internal final function get renderer():Renderer {
			return _renderer;
		}

		
		override public final function get hitmask():Hitmask {
			return _hitmask;
		}

		
		override public final function get radius():Number {
			return _radius;
		}

		public function setVertexCount(count:int):void {
			_vertices = new Vector.<Number>(count*5, true);
		}

		public function setTriangleCount(count:int):void {
			_indices = new Vector.<uint>(count * 3, true);
		}

		public function triangulate():void {
			updateBounds();
			_indices = new Vector.<uint>();
			Geom.triangulate(_indices, _hitmask._path);
		}

		
		public final function setPos(id:int, x:Number, y:Number):void {
			id *= 5;
			_vertices[id] = x;
			_vertices[id + 1] = y;
			_hasBoundsChanged = true;
		}

		
		public final function setPosUv(id:int, x:Number, y:Number, u:Number, v:Number):void {
			id *= 5;
			_vertices[id] = x;
			_vertices[id+1] = y;
			_vertices[id+2] = u;
			_vertices[id + 3] = v;
			_hasBoundsChanged = true;
		}

		
		public final function setPosUvAlpha(id:int, x:Number, y:Number, u:Number, v:Number, a:Number):void {
			id *= 5;
			_vertices[id] = x;
			_vertices[id+1] = y;
			_vertices[id+2] = u;
			_vertices[id+3] = v;
			_vertices[id + 4] = a;
			_hasBoundsChanged = true;
		}

		
		public final function setAlphaForAll(a:Number=1):void {
			for (var i:int = 0; i < _vertices.length; i += 5) {
				_vertices[i + 4] = a;
			}
		}

		/**
		 * Makes vertices from points. If you need to make vertices from raw coordinates, use
		 * setPath2.
		 * @param path
		 */
		
		public final function setPath(path:Vector.<Point>):void {
			setVertexCount(path.length);
			for (var i:int = 0; i < path.length; ++i) {
				_vertices[i * 5] = path[i].x;
				_vertices[i * 5 + 1] = path[i].y;
			}
			_hasBoundsChanged = true;
			triangulate();
		}

		/**
		 * Alternative method to make shape from vector of numbers arranged in pattern:
		 * [x,y,x,y,x,y,...]
		 * @param path
		 */
		
		public final function setPath2(path:Vector.<Number>):void {
			setVertexCount(path.length / 2);
			for (var i:int = 0; i < path.length; i+=2) {
				_vertices[i/2 * 5] = path[i];
				_vertices[i/2 * 5 + 1] = path[i+1];
			}
			_hasBoundsChanged = true;
			triangulate();
		}

		
		public final function setTriangle(id:int, a:int, b:int, c:int):void {
			id *= 3;
			_indices[id] = a;
			_indices[id + 1] = b;
			_indices[id + 2] = c;
		}

		/**
		 * Maps texture as a rectangle to the shape, generating uvs from vertex position.
		 * (u,v) = (scaleX * x + offX, scaleY * y + offY)
		 */
		
		public final function makePlanarUv(offX:Number=0, offY:Number=0, scaleX:Number=1, scaleY:Number=1, useCustomBounds:Boolean = false, boundsX:Number = 0, boundsY:Number = 0, boundsW:Number = 0, boundsH:Number = 0):void {
			if (!useCustomBounds) {
				updateBounds();
				boundsW = _bounds.width;
				boundsH = _bounds.height;
				boundsX = _bounds.x;
				boundsY = _bounds.y;
			}
			for (var i:int = 0; i < _vertices.length; i += 5) {
				_vertices[i + 2] = ((_vertices[i] - boundsX) / boundsW) * scaleX + offX;
				_vertices[i + 3] = ((_vertices[i + 1] - boundsY) / boundsH) * scaleY + offY;
			}
		}


		
		public final function set texture(value:Texture):void {
			_texture = value;
		}

		
		public final function get texture():Texture {
			return _texture;
		}

		
		public final function set material(value :Material):void {
			_material = value;
		}

		
		public final function get material():Material {
			return _material;
		}

		public function updateBounds():void {
			if (_hasBoundsChanged) {
				_bounds.x = _vertices[0];
				_bounds.y = _vertices[1];
				_bounds.width = 0;
				_bounds.height = 0;
				if (_hitmask._path.length != _vertices.length / 5) {
					_hitmask._path.length = _vertices.length / 5;
				}
				_hitmask._path[0] ||= new Point();
				_hitmask._path[0].setTo(_vertices[0], _vertices[1]);
				for (var i:int = 5; i < _vertices.length; i += 5) {
					if (_vertices[i] < _bounds.x) {
						_bounds.left = _vertices[i];
					} else if (_vertices[i] > _bounds.right) {
						_bounds.right = _vertices[i];
					}
					if (_vertices[i+1] < _bounds.y) {
						_bounds.top = _vertices[i+1];
					} else if (_vertices[i+1] > _bounds.bottom) {
						_bounds.bottom = _vertices[i+1];
					}					
					_hitmask._path[i/5] ||= new Point();
					_hitmask._path[i/5].setTo(_vertices[i], _vertices[i+1]);
				}
				_radius = Math.max(-_bounds.left, -_bounds.top, _bounds.right, _bounds.bottom);
				_hasBoundsChanged = false;
			}
		}

		override public function dispose():void {
			super.dispose();
			_texture = null;
			_material = null;
			_hitmask = null;
			_bounds = null;
			_positions = null;
			_vertices = null;
			_indices = null;
		}
	}

}
