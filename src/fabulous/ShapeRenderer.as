package fabulous {	  

	import flash.geom.Matrix;
	
	public final class ShapeRenderer extends Renderer {
		internal var _buffer:BufferBatchBuffer;

		public function ShapeRenderer(device:Device) {
			super(device);
		}

		override internal function init():void {
			_buffer = _device._bufferBatcher._unorderedBuffer;
		}

		override internal function processRenderable(renderable:Renderable):void {
			var shape:Shape = renderable as Shape;
			var mtx:Matrix = shape._node._mtx;
			var alpha:Number = shape._node._derivedAlpha;

			if (shape._texture && shape._vertices.length) {
				var vertexBuffer:ByteArrayVertexBuffer = _buffer._vbs[0] as ByteArrayVertexBuffer;
				var indexBuffer:IndexBuffer = _buffer._ib;

				var i:int, n:int;
				_device._bufferBatcher.queue(_buffer, shape._material, shape._texture, shape._indices.length);
				indexBuffer.beginWrite();
				for (i = 0; i < shape._indices.length; ++i) {
					indexBuffer.writeIndex(vertexBuffer.vertexPosition + shape._indices[i]);
				}
				indexBuffer.endWrite();

				// Note: Expressions within array access operator caused enormous garbage growth on iOS.
				n = shape._vertices.length;
				vertexBuffer.beginWrite();
				var vertexData:Vector.<Number> = shape._vertices;
				for (i = 0; i < n; i += 5) {
					vertexBuffer.writeFloat(vertexData[i] * mtx.a + vertexData[i+1] * mtx.c + mtx.tx);
					vertexBuffer.writeFloat(vertexData[i] * mtx.b + vertexData[i+1] * mtx.d + mtx.ty);
					vertexBuffer.writeFloat(vertexData[i+2]);
					vertexBuffer.writeFloat(vertexData[i+3]);
					vertexBuffer.writeFloat(vertexData[i+4] * alpha);
				}
				vertexBuffer.endWrite();
			}
		}
	}
}

