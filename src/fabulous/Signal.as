package fabulous {
	/**
	 * ...
	 * @author mosowski
	 */
	public class Signal {
		internal var _listeners:Vector.<SignalListener>;

		public function Signal() {
		}

		public function add(func:Function):void {
			addExpiring(func, -1);
		}

		public function addOnce(func:Function):void {
			addExpiring(func, 1);
		}

		public function addExpiring(func:Function, life:int):void {
			var sl:SignalListener = new SignalListener();
			sl._func = func;
			sl._life = life;
			_listeners ||= new Vector.<SignalListener>();
			_listeners.push(sl);
		}

		public function remove(func:Function):void {
			if (_listeners) {
				for (var i:int = 0; i < _listeners.length; ++i) {
					if (_listeners[i]._func == func) {
						_listeners.splice(i, 1);
					}
				}
				if (_listeners.length == 0) {
					_listeners = null;
				}
			}
		}

		public function hasListener(func:Function):Boolean {
			if (_listeners) {
				for (var i:int = 0; i < _listeners.length; ++i) {
					if (_listeners[i]._func == func) {
						return true;
					}
				}
			}
			return false;
		}

		public function clean():void {
			_listeners = null;
		}

		public function get numListeners():int {
			return _listeners != null ? _listeners.length : 0;
		}

		public function cleanExpiring():void {
			if (_listeners) {
				for (var i:int = _listeners.length - 1; i >= 0; --i) {
					if (_listeners[i]._life >= 0) {
						_listeners.splice(i, 1);
					}
				}
			}
		}

		public function post(arg:Object = null):void {
			var i:int;
			if (_listeners) {
				if (arg !== null) {
					for (i = 0;  _listeners && i < _listeners.length; ++i) {
						_listeners[i]._func(arg);
					}
				} else {
					for (i = 0; _listeners && i < _listeners.length; ++i) {
						_listeners[i]._func();
					}
				}

				// secondary check is necessary, because last listener may be removed within its observer
				if (_listeners) {
					for (i = _listeners.length - 1; i >= 0; --i) {
						if (_listeners[i]._life > 0) {
							if (--_listeners[i]._life == 0) {
								_listeners.splice(i, 1);
							}
						}
					}
					if (_listeners.length == 0) {
						_listeners = null;
					}
				}
			}
		}
	}
}


class SignalListener {
	public var _func:Function;
	public var _life:int;

	public function SignalListener() {
	}
}
