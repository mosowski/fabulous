package fabulous {

	import flash.utils.Dictionary;

	public class SignalsMap {

		private var _map:Dictionary;

		public function SignalsMap() {
			_map = new Dictionary();
		}

		public function get(id:Object):Signal {
			_map[id] ||= new Signal();
			return _map[id];
		}

		public function has(id:Object):Boolean {
			return _map[id] != null;
		}
		
		public function dispose(id:Object):void {
			if (_map[id] != null) {
				(_map[id] as Signal).clean();
				delete _map[id];
			}
		}
	}
}
