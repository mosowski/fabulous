package fabulous {
	import flash.events.Event;
	import flash.events.IOErrorEvent;	 
	import flash.net.XMLSocket;

	public class SocketLogger extends Logger {
		internal var _socket:XMLSocket;
		internal var _ip:String;
		internal var _port:int;
		internal var _queue:Vector.<String>;

		public function SocketLogger(ip:String, port:int) {
            super ();
			_ip = ip;
			_port = port;
			_queue = new Vector.<String>();
		}

		override internal function init():void {
			_socket = new XMLSocket(_ip, _port);
			_socket.addEventListener(Event.CONNECT, onConnected);
			_socket.addEventListener(IOErrorEvent.IO_ERROR, onError);
		}

		private function onError(e:Event):void {
			trace("Cannot connect to socket logger host.");
		}

		private function onConnected(e:Event):void {
			while (_queue.length) {
				print(_queue[0]);
				_queue.shift();
			}
		}

        override internal function d(msg:String):void {
            print("!SOS<showMessage key='debug'>" + format(msg) + "</showMessage>\n");
        }


        override internal function i(msg:String):void {
            print("!SOS<showMessage key='info'>" + format(msg) + "</showMessage>\n");
        }

        override internal function e(msg:String):void {
            print("!SOS<showMessage key='error'>" + format(msg) + "</showMessage>\n");
        }


        override internal function w(msg:String):void {
            print("!SOS<showMessage key='warning'>" + format(msg) + "</showMessage>\n");
        }
		
		override public final function print(msg:String):void {
			if (_socket.connected) {
				_socket.send(msg);
			} else {
				_queue.push(msg);
			}
		}

        private function format(msg:String):String {
            return msg.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        }
	}

}
