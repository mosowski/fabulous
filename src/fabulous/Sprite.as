package fabulous {
	import flash.geom.Rectangle;	
	
	public class Sprite extends Renderable {
		internal var _texture:Texture;
		internal var _material:Material;

		internal var _frame:SpriteSheetFrame;

		internal var _pivotX:Number;
		internal var _pivotY:Number;

		internal var _uvTop:Number;
		internal var _uvLeft:Number;
		internal var _uvBottom:Number;
		internal var _uvRight:Number;

		internal var _hitmask:Hitmask;
		internal var _bounds:Rectangle;
		internal var _radius:Number;

		internal static var _renderer:Renderer;

		public function Sprite(material:Material) {
			_texture = null;
			_material = material;
			_frame =  new SpriteSheetFrame(0, 0, 0, 0, 0, 0, 0, 0, TextureManager.UNLOADED_TEXTURE_NAME);

			_pivotX = 0;
			_pivotY = 0;

			_uvTop = _uvLeft = 0;
			_uvBottom = _uvRight = 1;

			_hitmask = new Hitmask();
			_hitmask.makeQuad();

			_bounds = new Rectangle();
		}
		
		public function clone():Sprite {
			var clone:Sprite = new Sprite(_material);
			clone._texture = _texture;
			clone._frame = _frame;
			clone._pivotX = _pivotX;
			clone._pivotY = _pivotY;
			clone._uvTop = _uvTop;
			clone._uvLeft = _uvLeft;
			clone._uvBottom = _uvBottom;
			clone._uvRight = _uvRight;
			clone._hitmask = _hitmask;
			clone._bounds = _bounds.clone();
			clone._radius = _radius;
			clone._node = _node.clone();
			if (_node.hasRenderable(this)) {
				clone._node.addRenderableAt(clone, _node.getRenderableIndex(this));
			}
			return clone;
		}
		
		override public final function get hitmask():Hitmask {
			return _hitmask;
		}
		
		override public final function get radius():Number {
			return _radius;
		}

		
		public final function get width():Number {
			return _frame._imageWidth;
		}
		public function set width(v:Number):void {
			if (_frame._imageWidth != v) {
				_frame._imageWidth = v;
				updateBounds();
			}
		}

		
		public final function get height():Number {
			return _frame._imageHeight;
		}
		public function set height(v:Number):void {
			if (_frame._imageHeight != v) {
				_frame._imageHeight = v;
				updateBounds();
			}
		}


		
		public final function set pivotX(value:Number):void {
			if (_pivotX != value) {
				_pivotX = value;
				updateBounds();
			}
		}

		
		public final function set pivotY(value:Number):void {
			if (_pivotY != value) {
				_pivotY = value;
				updateBounds();
			}
		}

		
		public final function get pivotX():Number {
			return _pivotX;
		}

		
		public final function get pivotY():Number {
			return _pivotY;
		}

		
		public final function set texture(value:Texture):void {
			_texture = value;
		}

		
		public final function get texture():Texture {
			return _texture;
		}

		
		public function set material(value :Material):void {
			_material = value;
		}

		
		public function get material():Material {
			return _material;
		}
		
		public function setUV(l:Number, t:Number, w:Number, h:Number):void {
			_uvLeft = l;
			_uvTop = t;
			_uvRight = l + w;
			_uvBottom = t + h;
			updateBounds();
		}	

		internal function setUVFromFrame(frame:SpriteSheetFrame = null):void {
			if (!frame) {
				frame = _frame;
			}
			_uvLeft = frame._left / _texture._width;
			_uvTop = frame._top / _texture._height;
			_uvRight = frame._right / _texture._width;
			_uvBottom = frame._bottom / _texture._height;
			_pivotX = frame._pivotX;
			_pivotY = frame._pivotY;
			updateBounds();
		}

		internal function updateBounds():void {
			_bounds.x = -_pivotX;
			_bounds.y = -_pivotY;
			_bounds.width = _frame._imageWidth;
			_bounds.height = _frame._imageHeight;
			_radius = Math.max(_pivotX, _pivotY, _frame._imageWidth -_pivotX, _frame.imageHeight - _pivotY);
			_hitmask.fromRectangle(_bounds);
		}

		public function setTextureFromFrame(frame:SpriteSheetFrame):void {
			if (frame) {
				this.frame = frame.clone();
			}
		}

		public final function set frame(value:SpriteSheetFrame):void {
			if (_frame != value) {
				_frame	= value;
				if (_frame) {
					_texture = _frame._texture;
					_texture.callAfterLoad(setUVFromFrame);
					updateBounds();
				}
			}
		}

		public final function get frame():SpriteSheetFrame {
			return _frame;
		}
		
		override internal function get renderer():Renderer {
			return _renderer;
		}

		override public function dispose():void {
			super.dispose();
			if (_texture != null) {
				_texture.clearLoadedListener(setUVFromFrame);
				_texture = null;
			}
			_material = null;
			_frame = null;
			_hitmask = null;
			_bounds = null;
		}
	}
}
