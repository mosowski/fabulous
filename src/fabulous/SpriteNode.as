package fabulous {
	public class SpriteNode extends Node {
		internal var _sprite:Sprite;

		public function SpriteNode(material:Material) {
			_sprite = new Sprite(material);
			addRenderable(_sprite);
		}

		
		public final function set pivotX(value:Number):void {
			_sprite.pivotX = value;
		}

		
		public final function set pivotY(value:Number):void {
			_sprite.pivotY = value;
		}

		
		public final function get pivotX():Number {
			return _sprite.pivotX;
		}

		
		public final function get pivotY():Number {
			return _sprite.pivotY;
		}

		
		public final function set frame(value:SpriteSheetFrame):void {
			_sprite.frame = value;
		}

		
		public final function get frame():SpriteSheetFrame {
			return _sprite.frame;
		}

		public final function get width():Number {
			return _sprite.width * scaleX;
		}
		public final function set width(v:Number):void {
			_sprite.width = v / scaleX;
		}

		public final function get height():Number {
			return _sprite.height * scaleY;
		}
		public final function set height(v:Number):void {
			_sprite.height = v / scaleY;
		}


		public final function set material(value:Material):void {
			_sprite.material = value;
		}
		public final function get material():Material {
			return _sprite.material;
		}

		public function get texture():Texture {
			return _sprite.texture;
		}
		public function set texture(v:Texture):void {
			_sprite.texture = v;
		}

		public function get sprite():Sprite {
			return _sprite;
		}

		override public function dispose():void {
			super.dispose();
			if (_sprite != null) {
				_sprite.dispose();
				_sprite = null;
			}
		}

	}

}
