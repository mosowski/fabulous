package fabulous {	  

	import flash.geom.Matrix;
	
	public final class SpriteRenderer extends Renderer {
		internal var _buffer:BufferBatchBuffer;

		public function SpriteRenderer(device:Device) {
			super(device);
		}

		override internal function init():void {
			_buffer = _device._bufferBatcher._quadBuffer;
		}

		override internal function processRenderable(renderable:Renderable):void {
			var sprite:Sprite = renderable as Sprite;
			var mtx:Matrix = sprite._node._mtx;
			var alpha:Number = sprite._node._derivedAlpha;
			var left:Number = sprite._bounds.left;
			var right:Number = sprite._bounds.right;
			var top:Number = sprite._bounds.top;
			var bottom:Number = sprite._bounds.bottom;
			//var bounds:Rectangle = sprite._bounds;

			if (sprite._texture) {
				sprite._texture.touch();
				var vertexBuffer:ByteArrayVertexBuffer = _buffer._vbs[0] as ByteArrayVertexBuffer;

				_device._bufferBatcher.queue(_buffer, sprite._material, sprite._texture, 6);

				vertexBuffer.beginWrite();

				vertexBuffer.writeFloat(mtx.a * left + mtx.c * top + mtx.tx);
				vertexBuffer.writeFloat(mtx.b * left + mtx.d * top + mtx.ty);
				vertexBuffer.writeFloat(sprite._uvLeft);
				vertexBuffer.writeFloat(sprite._uvTop);
				vertexBuffer.writeFloat(alpha);

				vertexBuffer.writeFloat(mtx.a * left + mtx.c * bottom + mtx.tx);
				vertexBuffer.writeFloat(mtx.b * left + mtx.d * bottom + mtx.ty);
				vertexBuffer.writeFloat(sprite._uvLeft);
				vertexBuffer.writeFloat(sprite._uvBottom);
				vertexBuffer.writeFloat(alpha);

				vertexBuffer.writeFloat(mtx.a * right + mtx.c * top + mtx.tx);
				vertexBuffer.writeFloat(mtx.b * right + mtx.d * top + mtx.ty);
				vertexBuffer.writeFloat(sprite._uvRight);
				vertexBuffer.writeFloat(sprite._uvTop);
				vertexBuffer.writeFloat(alpha);

				vertexBuffer.writeFloat(mtx.a * right + mtx.c * bottom + mtx.tx);
				vertexBuffer.writeFloat(mtx.b * right + mtx.d * bottom + mtx.ty);
				vertexBuffer.writeFloat(sprite._uvRight);
				vertexBuffer.writeFloat(sprite._uvBottom);
				vertexBuffer.writeFloat(alpha);

				vertexBuffer.endWrite();
			}

		}

	}
}

