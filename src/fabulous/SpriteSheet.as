package fabulous {	  
	import flash.utils.Dictionary;

	public class SpriteSheet {
		internal var _device:Device;
		internal var _name:String;
		internal var _frames:Dictionary;
		internal var _isLoaded:Boolean;

		internal var _loaded:Signal;

		public function SpriteSheet(device:Device) {
			_device = device;
			_frames = new Dictionary();
			_isLoaded = false;
			_loaded = new Signal();
		}

		internal function fromObject(object:Object):void {
			for (var spriteId:String in object) {
				if (_frames[spriteId]) {
					throw new Error("Conflicting sprite id '" + spriteId + "' in sprite sheet '" + _name + "'.");
				} else {
					var src:Object = object[spriteId];
					_frames[spriteId] = new SpriteSheetFrame(src.left, src.top, src.right, src.bottom, src.width, src.height, src.pivotX, src.pivotY, src.texture);
				}
			}
			_loaded.post();
		}
		
		public function hasFrame(spriteId:String):Boolean {
			return spriteId in _frames;
		}

		public function getFrame(spriteId:String):SpriteSheetFrame {
			var frame:SpriteSheetFrame = _frames[spriteId];
			if (frame) {
				frame._texture = _device._textureMgr.loadTexture(frame._textureId);
				return frame;
			} else {
				throw new Error("Sprite id '" + spriteId + "' not found in sprite sheet '" + _name + "'.");
			}
		}

		public function getAllTexturesIds():Vector.<String> {
			var texturesIds:Object = {};
			for (var id:String in _frames) {
				texturesIds[_frames[id]._textureId] = true;
			}
			var result:Vector.<String> = new Vector.<String>();
			for (id in texturesIds) {
				result.push(id);
			}
			return result;
		}

		public function get name():String {
			return _name;
		}
	}

}
