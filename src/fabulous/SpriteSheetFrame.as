package fabulous {
	public class SpriteSheetFrame {
		internal var _left:Number;
		internal var _top:Number;
		internal var _right:Number;
		internal var _bottom:Number;
		internal var _imageWidth:Number;
		internal var _imageHeight:Number;
		internal var _textureId:String;
		internal var _pivotX:Number;
		internal var _pivotY:Number;
		internal var _texture:Texture;

		public function SpriteSheetFrame(left:Number, top:Number, right:Number, bottom:Number, width:Number, height:Number, pivotX:Number, pivotY:Number, textureId:String) {
			_left = left;
			_top = top;
			_right = right;
			_bottom = bottom;
			_imageWidth = width;
			_imageHeight = height;
			_pivotX = pivotX;
			_pivotY = pivotY;
			_textureId = textureId;
		}

		public final function get imageWidth():Number {
			return _imageWidth;
		}

		public final function get imageHeight():Number {
			return _imageHeight;
		}

		public final function get uvLeft():Number {
			return _left / _texture._sourceWidth;
		}

		public final function get uvTop():Number {
			return _top / _texture._sourceHeight;
		}

		public final function get uvRight():Number {
			return _right / _texture._sourceWidth;
		}

		public final function get uvBottom():Number {
			return _bottom / _texture._sourceHeight;
		}

		public function get texture():Texture {
			return _texture;
		}

		public function clone():SpriteSheetFrame {
			var clone:SpriteSheetFrame = new SpriteSheetFrame(_left, _top, _right, _bottom, _imageWidth, _imageHeight, _pivotX, _pivotY, _textureId);
			clone._texture = _texture;
			return clone;
		}

	}

}
