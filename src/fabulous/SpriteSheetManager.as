package fabulous {
import flash.utils.Dictionary;

public class SpriteSheetManager {
		internal var _device:Device;
		internal var _spriteSheetsByName:Dictionary;
		internal var _spriteSheetsLoader:BatchedLoader;
		internal var _spriteSheets:Vector.<SpriteSheet>;

		internal var _loaded:Signal;

		public function SpriteSheetManager(device:Device) {
			_spriteSheets = new <SpriteSheet>[];
			_device = device;
			_loaded = new Signal();
		}

		public function init():void {

		}
		
		private function createLoaderCallback(sheetName:String):Function {
			return function (ldrName:String):void {
				var loader:TextLoader = _spriteSheetsLoader.getChild(ldrName) as TextLoader;
				_spriteSheetsByName[sheetName].fromObject((_spriteSheetsLoader.getChild(ldrName) as TextLoader).getJSON());
				loader.unload();
			};
		}

		public function loadConfigFromObject(object:Object):void {
			_spriteSheetsByName = new Dictionary();
			_spriteSheetsLoader = _device.loaderMgr.startBatchedLoader("spriteSheetsLoader", onAllSpriteSheetsLoaded);
			for (var name:String in object) {
				var spriteSheet:SpriteSheet = new SpriteSheet(_device);
				spriteSheet._name = name;
				_spriteSheetsByName[name] = spriteSheet;
				_spriteSheets.push(spriteSheet);
				var path:String = _device.getPath(object[name][_device._deviceProfile]);
				_spriteSheetsLoader.addChild(new TextLoader(_device, path, path, createLoaderCallback(name)));
			}
			_spriteSheetsLoader.load();
		}

		internal function onAllSpriteSheetsLoaded(ldrName:String):void {
			_loaded.post();
			_device.loaderMgr.disposeLoader(ldrName);
		}

		public function getSheet(name:String):SpriteSheet {
			return _spriteSheetsByName[name];
		}

		public function getSheets():Vector.<SpriteSheet> {
			return _spriteSheets;
		}
	}
}
