package fabulous {
	public class Stats {
		internal var _visibleNodes:int;
		internal var _visibleRenderables:int;
		internal var _drawnTriangles:int;
		internal var _drawCalls:int;
		internal var _clippedRenderables:int;
		public function Stats() {
		}

		internal function reset():void {
			_visibleNodes = 0;
			_visibleRenderables = 0;
			_drawnTriangles = 0;
			_clippedRenderables = 0;
			_drawCalls = 0;
		}

		public function get visibleNodes():int {
			return _visibleNodes;
		}
		public function get visibleRenderables():int {
			return _visibleRenderables;
		}
		public function get drawnTriangles():int {
			return _drawnTriangles;
		}
		public function get clippedRenderables():int {
			return _clippedRenderables;
		}
		public function get drawCalls():int {
			return _drawCalls;
		}

	}

}
