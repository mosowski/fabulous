package fabulous {	  
	import flash.geom.Rectangle;

	public class Text extends Renderable {
		internal var _font:Font;
		internal var _material:Material;

		internal var _hitmask:Hitmask;
		internal var _bounds:Rectangle;
		internal var _radius:Number;

		internal var _text:String;

		internal var _widthLimit:Number;
		internal var _heightLimit:Number;
		internal var _downscaleToLimits:Boolean;
		internal var _upscaleToLimits:Boolean;
		internal var _formatScale:Number;
		internal var _wordWrap:Boolean;
		internal var _fontRelativeSize:Number;
		internal var _fontPointSize:Number;

		internal var _characterOffsetsX:Vector.<Number>;
		internal var _characterOffsetsY:Vector.<Number>;

		internal var _vertexBuffer:VertexBuffer;
		internal var _isVertexBufferDirty:Boolean;

		internal var _horizontalAlignment:int;
		internal var _verticalAlignment:int;

		internal var _numLines:int;
		internal var _realHeight:int;

		internal var _screenSize:Number;

		public static const LEFT:int = 0;
		public static const CENTER:int = 1;
		public static const RIGHT:int = 2;
		public static const TOP:int = 0;
		public static const BOTTOM:int = 2;

		internal static var _renderer:Renderer;

		public function Text(material:Material) {
			_material = material;
			_bounds = new Rectangle();
			_hitmask = new Hitmask();
			_hitmask.makeQuad();
			_text = "";
			_formatScale = 1;
			_fontRelativeSize = 1;
			_fontPointSize = 1;
			_downscaleToLimits = false;
			_upscaleToLimits = false;
			_numLines = 0;
			_realHeight = 0;
			_screenSize = 1;
		}

		
		override internal final function get renderer():Renderer {
			return _renderer;
		}

		
		override public final function get hitmask():Hitmask {
			return _hitmask;
		}
		
		
		override public final function get radius():Number {
			return _radius;
		}

		
		public function get material():Material {
			return _material;
		}
		
		public function set material(v:Material):void {
			_material = v;
		}

		
		public final function set font(value:Font):void {
			if (_font != value) {
				_font = value;
				if (!_font._isLoaded) {
					_font._fontLoaded.addOnce(revalidate);
				} else {
					revalidate();
				}
			}
		}
		
		public final function get font():Font {
			return _font;
		}
		
		
		public final function set text(value:String):void {
			value ||= "";
			value = value.replace(new RegExp("\\\\n|<br>", "g"), String.fromCharCode(10));
			if (_text != value) {
				_text = value;
				revalidate();
			}
		}
		
		public final function get text():String {
			return _text;
		}

		/**
		 * If true, text will be scaled down to fit the widthLimit and heightLimit.
		 */
		
		public final function set downscaleToLimits(value:Boolean):void {
			if (value != _downscaleToLimits) {
				_downscaleToLimits = value;
				revalidate();
			}
		}
		
		public final function get downscaleToLimits():Boolean {
			return _downscaleToLimits;
		}

		/**
		 * If true, text will be scaled uo to fit the widthLimit and heightLimit.
		 * downscaleToLimits must be true to allow text upscaling.
		 */
		
		public final function set upscaleToLimits(value:Boolean):void {
			if (value != _upscaleToLimits) {
				_upscaleToLimits = value;
				revalidate();
			}
		}
		
		public final function get upscaleToLimits():Boolean {
			return _upscaleToLimits;
		}

		/**
		 * If true, words that doesn't fit the line will be wrapped to next line.
		 * If word is wider than line, it will be split up.
		 */
		
		public final function set wordWrap(value:Boolean):void {
			_wordWrap = value;
		}
		
		public function get wordWrap():Boolean {
			return _wordWrap;
		}

		
		public final function set widthLimit(value:Number):void {
			if (value != _widthLimit) {
				if (value < 0) {
					value = 0;
				}
				_widthLimit = value;
				revalidate();
			}
		}
		
		public final function get widthLimit():Number {
			return _widthLimit;
		}

		
		public final function set heightLimit(value:Number):void {
			if (value != _heightLimit) {
				if (value < 0) {
					value = 0;
				}
				_heightLimit = value;
				revalidate();
			}
		}
		
		public final function get heightLimit():Number {
			return _heightLimit;
		}

		/**
		 * Alignemnt of the text in horizontal space. Can be Text.LEFT, Text.CENTER or Text.RIGHT.
		 */
		
		public final function set horizontalAlignment(value:int):void {
			if (value != _horizontalAlignment) {
				_horizontalAlignment = value;
				revalidate();
			}
		}
		
		public final function get horizontalAlignment():int {
			return _horizontalAlignment;
		}

		/**
		 * Alignemnt of the text in vertical space. Can be Text.TOP, Text.CENTER or Text.BOTTOM.
		 */
		
		public final function set verticalAlignment(value:int):void {
			if (value != _verticalAlignment) {
				_verticalAlignment = value;
				revalidate();
			}
		}
		
		public final function get verticalAlignment():int {
			return _verticalAlignment;
		}

		/**
		 * Scale applied to text before doing the formatting.
		 */
		
		public final function set fontSize(value:Number):void {
			if (value != _fontPointSize) {
				_fontPointSize = value;
				revalidate();
			}
		}
		
		public final function get fontSize():Number {
			return _fontPointSize;
		}

		public final function get screenSize():Number {
			return _screenSize;
		}

		
		public final function getCharBoundaries(characterIndex:int):Rectangle {
			var glyph:Glyph = _font._glyphs[_text.charCodeAt(characterIndex)];
			if (glyph) {
				return new Rectangle(
					_characterOffsetsX[characterIndex],
					_characterOffsetsY[characterIndex],
					glyph._width * _formatScale * _fontRelativeSize,
					glyph._height * _formatScale * _fontRelativeSize
				);
			} else {
				return new Rectangle(
					_characterOffsetsX[characterIndex],
					_characterOffsetsY[characterIndex]
				);
			}

		}

		
		public final function getCharacterX(characterIndex:int):Number {
			return _characterOffsetsX[characterIndex];
		}

		
		public final function getCharacterY(characterIndex:int):Number {
			return _characterOffsetsY[characterIndex];
		}

		
		public function get numLines():int {
			return _numLines;
		}

		public function get realHeight():int {
			return _realHeight;
		}
		
		internal final function revalidateHorizontalAlignment(from:int, to:int, space:Number):void {
			for (var i:int = from; i < to; ++i) {
				if (_horizontalAlignment == CENTER) {
					_characterOffsetsX[i] += space / 2;
				} else if (_horizontalAlignment == RIGHT) {
					_characterOffsetsX[i] += space;
				}
			}
		}


		/**
		 * CAUTION: probably the most dirty piece of code in this engine
		 */
		internal function revalidate():void {
			if (!_font) {
				return;
			}

			if (_characterOffsetsX == null || _characterOffsetsY == null || _text.length != _characterOffsetsX.length) {
				if (_characterOffsetsX == null) {
					_characterOffsetsX = new Vector.<Number>(_text.length);
				} else {
					_characterOffsetsX.length = _text.length;
				}
				if (_characterOffsetsY == null) {
					_characterOffsetsY = new Vector.<Number>(_text.length);
				} else {
					_characterOffsetsY.length = _text.length;
				}
			}
			if (_font && _font._isLoaded) {
				_fontRelativeSize = _fontPointSize / _font._baseSize;
			} else {
				_fontRelativeSize = 1;
			}

			_numLines = 1;

			var i:int = 0;
			var glyph:Glyph;
			var altRelativeSize:Number = _fontRelativeSize;

			while (true) {
				var x:Number = 0;
				var y:Number = 0;
				var linePos:int = 0;
				var lineWidth:Number = 0;
				var wordPos:int = 0;
				var wordWidth:Number = 0;
				var wordX:Number = 0;
				var ws:Boolean = true;
				var pos:int = 0;
				while (pos != _text.length) {
					var charCode:int = _text.charCodeAt(pos);
					glyph = _font._glyphs[charCode];
					if (glyph) {
						if (_wordWrap) {
							if (charCode == 32) {
								ws = true;
							} else {
								if (ws) {
									wordPos = pos;
									wordX = x;
									wordWidth = 0;
									ws = false;
								}
								wordWidth += glyph._width * altRelativeSize;
								if (wordX + wordWidth > _widthLimit) {
									if (wordWidth < _widthLimit) {
										revalidateHorizontalAlignment(linePos, wordPos, _widthLimit - (x - wordWidth));
										pos = wordPos;
									} else {
										if (pos == wordPos) {
											break;
										}
										revalidateHorizontalAlignment(linePos, pos, _widthLimit - x);
									}
									x = 0;
									y += _font._lineHeight * altRelativeSize;
									linePos = pos;
									lineWidth = 0;
									ws = true;
									_numLines++;
									continue;
								}
								wordWidth = wordWidth - glyph._width * altRelativeSize + glyph._xadvance * altRelativeSize;
							}
						}
						_characterOffsetsX[pos] = x + glyph._xoffset * altRelativeSize;
						_characterOffsetsY[pos] = y + glyph._yoffset * altRelativeSize;

						x += glyph._xadvance * altRelativeSize;
					} else {
						if (charCode == 10) {
							revalidateHorizontalAlignment(linePos, pos, _widthLimit - x);
							x = 0;
							y += _font._lineHeight * altRelativeSize;
							linePos = pos;
							lineWidth = 0;
							_numLines++;
							ws = true;
						}
					}
					pos++;
				}
				revalidateHorizontalAlignment(linePos, pos, _widthLimit - x);
				var maxWidth:Number = 0;
				var minX:Number = -1;
				for (i = 0; i < _characterOffsetsX.length; ++i) {
					glyph = _font._glyphs[_text.charCodeAt(i)];
					if (glyph) {
						if (_characterOffsetsX[i] + glyph._width * altRelativeSize > maxWidth) {
							maxWidth = _characterOffsetsX[i] + glyph._width * altRelativeSize;
						}
					}
					if (minX == -1 || _characterOffsetsX[i] < minX) {
						minX = _characterOffsetsX[i];
					}
				}
				if (_horizontalAlignment == CENTER || _horizontalAlignment == RIGHT) {
					maxWidth -= minX;
					for (i = 0; i < _characterOffsetsX.length; ++i) {
						_characterOffsetsX[i] -= minX;
					}
				}
				var maxHeight:int = y + _font._lineHeight * altRelativeSize;
				if (!_downscaleToLimits || maxHeight <= _heightLimit) {
					break;
				} else {
					altRelativeSize *= 0.9;
				}
			}

			if (_downscaleToLimits && maxHeight > 0 && maxWidth > 0) {
				var vLimitScale:Number = _heightLimit / maxHeight;
				var hLimitScale:Number = _widthLimit / maxWidth;
				if (hLimitScale < vLimitScale) {
					_formatScale = hLimitScale;
				} else {
					_formatScale = vLimitScale;
				}

				if (!_upscaleToLimits && _formatScale > 1) {
					_formatScale = 1;
				}

				for (i = 0; i < _characterOffsetsX.length; ++i) {
					_characterOffsetsX[i] *= _formatScale;
					_characterOffsetsY[i] *= _formatScale;
				}
			} else {
				_formatScale = 1;
			}

			var horizontalOffset:Number = _widthLimit - maxWidth * _formatScale;
			if (_horizontalAlignment != LEFT) {
				if (_horizontalAlignment == CENTER) {
					horizontalOffset /= 2;
				}
			} else {
				horizontalOffset = 0;
			}

			var verticalOffset:Number = _heightLimit - maxHeight * _formatScale;
			if (_verticalAlignment != TOP) {
				if (_verticalAlignment == CENTER) {
					verticalOffset /= 2;
				}
			} else {
				verticalOffset = 0;
			}
			for (i = 0; i < _characterOffsetsY.length; ++i) {
				_characterOffsetsX[i] += horizontalOffset;
				_characterOffsetsY[i] += verticalOffset;
			}


			_realHeight = y + _font._lineHeight;
			_bounds.x = horizontalOffset;
			_bounds.y = verticalOffset;
			_bounds.width = maxWidth * _formatScale;
			_bounds.height = maxHeight * _formatScale;
			_radius = Math.max( -_bounds.left, -_bounds.top, _bounds.right, _bounds.bottom);
			_formatScale *= altRelativeSize / _fontRelativeSize;

			_hitmask.fromRectangle(_bounds);

			_isVertexBufferDirty = true;
		}

		override public function dispose():void {
			super.dispose();
			_font = null;
			_material = null;
			_hitmask = null;
			_bounds = null;
			_text = null;
			_characterOffsetsX = null;
			_characterOffsetsY = null;
			_vertexBuffer = null;
		}
	}

}
