package fabulous {
public class TextLoader extends BinaryLoader {
		private var _text:String;
		private var _json:Object;

		public function TextLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null, onError:Function = null) {
			super(device, name, url, onComplete, onError);
		}

		override protected function onLoaderComplete():void {
			_text = _data.readUTFBytes(_data.length);
			super.onLoaderComplete();
		}

		public function getString():String {
			return _text;
		}

		public function getJSON():Object {
			if (_json == null) {
				try {
					_json = JSON.parse(_text);
				} catch (e:Error) {
					Log.e("Error while parsing json string: '" + _text + "'");
					throw e;
				}
			}
			return _json;
		}

		override public function unload():void {
			super.unload();
			_json = null;
			_text = null;
		}
	}

}
