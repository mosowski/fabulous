package fabulous {

	public class TextNode extends Node {
		internal var _text:Text;

		public function TextNode(material:Material) {
			_text = new Text(material);
			_text.widthLimit = 1;
			_text.heightLimit = 1;
			_text.wordWrap = true;
			_text.downscaleToLimits = false;
			_text.upscaleToLimits = false;
			_text.fontSize = 10;
			addRenderable(_text);
		}

		
		public final function set font(value:Font):void {
			_text.font = value;
		}
		public final function get font():Font {
			return _text.font;
		}
		
		public final function set fontSize(value:Number):void {
			_text.fontSize = value;
		}
		public final function get fontSize():Number {
			return _text.fontSize;
		}

		public final function set downscaleToLimits(value:Boolean):void {
			_text.downscaleToLimits = value;
		}
		public final function get downscaleToLimits():Boolean {
			return _text.downscaleToLimits;
		}
		
		public final function set upscaleToLimits(value:Boolean):void {
			_text.upscaleToLimits = value;
		}

		public final function set widthLimit(value:Number):void {
			_text.widthLimit = value;
		}
		public final function get widthLimit():Number {
			return _text.widthLimit;
		}

		public final function set heightLimit(value:Number):void {
			_text.heightLimit = value;
		}
		public final function get heightLimit():Number {
			return _text.heightLimit;
		}

		public final function set horizontalAlignment(value:int):void {
			_text.horizontalAlignment = value;
		}
		public final function get horizontalAlignment():int {
			return _text.horizontalAlignment;
		}
		
		public final function set verticalAlignment(value:int):void {
			_text.verticalAlignment = value;
		}
		public final function get verticalAlignment():int {
			return _text.verticalAlignment;
		}

		public final function get wordWrap():Boolean{
			return _text.wordWrap;
		}
		public final function set wordWrap(v:Boolean):void {
			_text.wordWrap = v;
		}

		public final function get screenSize():Number {
			return _text.screenSize;
		}

		public final function set text(value:String):void {
			_text.text = value;
		}
		public final function get text():String {
			return _text.text;
		}

		public final function get realHeight():int {
			return _text.realHeight;
		}

		override public function dispose():void {
			super.dispose();
			if (_text != null) {
				_text.dispose();
				_text = null;
			}
		}
	}
}
