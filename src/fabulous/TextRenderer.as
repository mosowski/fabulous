package fabulous {	  

	import flash.geom.Matrix;
	
	public final class TextRenderer extends Renderer {
		internal var _buffer:BufferBatchBuffer;

		public function TextRenderer(device:Device) {
			super(device);
		}

		override internal function init():void {
			_buffer = _device._bufferBatcher._quadBuffer;
		}

		override internal function processRenderable(renderable:Renderable):void {
			var text:Text = renderable as Text;
			var mtx:Matrix = text._node._mtx;
			var glyphScale:Number = text._formatScale  * text._fontRelativeSize;
			var alpha:Number = text._node._derivedAlpha;
			var texture:Texture = text._font._texture;
			var material:Material = text._material;

			var left:Number;
			var top:Number;
			var right:Number;
			var bottom:Number;

			text._screenSize = text._font._baseSize * glyphScale;

			if (texture) {
				var charOffsetsX:Vector.<Number> = text._characterOffsetsX;
				var charOffsetsY:Vector.<Number> = text._characterOffsetsY;
				var vertexBuffer:ByteArrayVertexBuffer = _buffer._vbs[0] as ByteArrayVertexBuffer;

				vertexBuffer.beginWrite();

				for (var i:int = 0; i < text._text.length; ++i) {
					var glyph:Glyph = text._font._glyphs[text._text.charCodeAt(i)];
					if (glyph) {

						left = charOffsetsX[i];
						top = charOffsetsY[i];
						right = (charOffsetsX[i] + glyph._width * glyphScale);
						bottom = (charOffsetsY[i] + glyph._height * glyphScale);

						_device._bufferBatcher.queue(_buffer, material, texture, 6);

						vertexBuffer.writeFloat(mtx.a * left + mtx.b * top + mtx.tx);
						vertexBuffer.writeFloat(mtx.c * left + mtx.d * top + mtx.ty);
						vertexBuffer.writeFloat(glyph._uvLeft);
						vertexBuffer.writeFloat(glyph._uvTop);
						vertexBuffer.writeFloat(alpha);

						vertexBuffer.writeFloat(mtx.a * left + mtx.b * bottom + mtx.tx);
						vertexBuffer.writeFloat(mtx.c * left + mtx.d * bottom + mtx.ty);
						vertexBuffer.writeFloat(glyph._uvLeft);
						vertexBuffer.writeFloat(glyph._uvBottom);
						vertexBuffer.writeFloat(alpha);

						vertexBuffer.writeFloat(mtx.a * right + mtx.b * top + mtx.tx);
						vertexBuffer.writeFloat(mtx.c * right + mtx.d * top + mtx.ty);
						vertexBuffer.writeFloat(glyph._uvRight);
						vertexBuffer.writeFloat(glyph._uvTop);
						vertexBuffer.writeFloat(alpha);

						vertexBuffer.writeFloat(mtx.a * right + mtx.b * bottom + mtx.tx);
						vertexBuffer.writeFloat(mtx.c * right + mtx.d * bottom + mtx.ty);
						vertexBuffer.writeFloat(glyph._uvRight);
						vertexBuffer.writeFloat(glyph._uvBottom);
						vertexBuffer.writeFloat(alpha);

					}
				}

				vertexBuffer.endWrite();
			}
		}

	}
}

