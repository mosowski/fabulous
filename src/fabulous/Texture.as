package fabulous {
import flash.display3D.textures.Texture;
import flash.utils.Dictionary;

public class Texture {
		public static var COMPRESSION_NONE:int = 0;
		public static var COMPRESSION_DXT1:int = 1;
		public static var COMPRESSION_DXT5:int = 2;

		internal var _device:Device;
		internal var _name:String;
		internal var _alternativePath:String;
		internal var _loadedFromAlternativePath:Boolean;
		internal var _handle:flash.display3D.textures.Texture;
		internal var _isLoaded:Boolean;
		internal var _isLoading:Boolean;
		internal var _isCompressed:Boolean;
		internal var _hasAlpha:Boolean;
		internal var _width:int;
		internal var _height:int;
		internal var _sourceWidth:int;
		internal var _sourceHeight:int;
		internal var _numTextureBytes:int;
		internal var _wasVisibleInLastFrame:Boolean;
		internal var _lastExposureTime:Number;
		internal var _userReferences:Dictionary;

		internal var _loaded:Signal;
		internal var _disposed:Signal;

		public function Texture(device:Device) {
			_device = device;
			_isLoaded = false;
			_isLoading = false;
			_isCompressed = false;
			_hasAlpha = true;
			_wasVisibleInLastFrame = false;
			_lastExposureTime = 0;
			_loaded = new Signal();
			_disposed = new Signal();
			_userReferences = new Dictionary(true);
			_loadedFromAlternativePath = false;
		}

		public function addUser(user:Object):void {
			if (!(user in _userReferences)) {
				_userReferences[user] = 1;
			} else {
				_userReferences[user] += 1;
			}
		}

		public function removeUser(user:Object):void {
			CONFIG::debug {
				if (!(user in _userReferences)) {
					throw new Error("No such user: " + user + " for texture: " + _name);
				}
			}
			_userReferences[user] -= 1;
			if (_userReferences[user] <= 0) {
				delete _userReferences[user];
			}
		}

		internal function get numUsers():int {
			var num:int = 0;
			for (var k:Object in _userReferences) {
				num++;
			}
			return num;
		}

		internal function dispose():void {
			if (_handle && _handle != _device._textureMgr._unloadedFallbackTextureHandle) {
				_handle.dispose();
				_handle = null;
				_device._numTextureBytes -= _numTextureBytes;
				_device._textureMgr.initAsUnloaded(this);
				_isLoaded = false;
				_disposed.post(this);
			}
		}

		public function callAfterLoad(callback:Function):void {
			if (_isLoaded) {
				callback();
			} else {
				_loaded.addOnce(callback);
			}
		}
		
		public function clearLoadedListener(callback:Function):void {
			_loaded.remove(callback);
		}

		[Inline]
		public final function touch():void {
			_wasVisibleInLastFrame = true;
			_lastExposureTime = _device._currentTime;
		}

		[Inline]
		public final function get lastExposureTime():Number {
			return _lastExposureTime;
		}

		public function get sourceWidth():int {
			return _sourceWidth;
		}
		public function get sourceHeight():int {
			return _sourceHeight;
		}

		public function get name():String {
			return _name;
		}

		public function get disposed():Signal {
			return _disposed;
		}
		
		public function get alternativePath():String 
		{
			return _alternativePath;
		}
		
		public function get loadedFromAlternativePath():Boolean {
			return _loadedFromAlternativePath;
		}

		public function get isLoaded():Boolean {
			return _isLoaded;
		}

		public function get stage3dTexture():flash.display3D.textures.Texture {
			return _handle;
		}

		public function get compression():int {
			if (_isCompressed) {
				if (_hasAlpha) {
					return COMPRESSION_DXT5;
				} else {
					return COMPRESSION_DXT1;
				}
			} else {
				return COMPRESSION_NONE;
			}
		}
		
		public function recreate():void {
			dispose();
		}
	}

}
