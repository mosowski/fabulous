package fabulous {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	
	public class TextureManager {
		internal var _device:Device;
		internal var _texturesByName:Object;
		internal var _lazyTexturesWaitingTimes:Object;
		internal var _texturesToDispose:Object;

		internal var _unloadedFallbackTextureHandle:*; // star, to avoid ambigious types
		internal var _unloadedTexture:UncompressedTexture;
		internal var _unloadedTextureData:BitmapData;

		internal var _loaderCompleted:SignalsMap;
		internal var _loaderCrashed:SignalsMap;

		internal static const UNLOADED_TEXTURE_NAME:String = "__unloadedTexture__";
		
		private var _isTextureDownscaleAllowed:Boolean;

		public function TextureManager(device:Device) {
			_device = device;
			_texturesByName = { };
			_lazyTexturesWaitingTimes = { };
			_texturesToDispose = { };
			_loaderCompleted = new SignalsMap();
			_loaderCrashed = new SignalsMap();
		}

		internal function init():void {
			_unloadedTextureData ||= new BitmapData(4, 4, true, 0);
			_unloadedTexture ||= new UncompressedTexture(_device);
			_unloadedFallbackTextureHandle = null;
			_unloadedTexture.loadFromBitmapData(_unloadedTextureData);
			_texturesByName[UNLOADED_TEXTURE_NAME] = _unloadedTexture;
			_unloadedFallbackTextureHandle = _unloadedTexture._handle;
		}
		
		internal function recreate():void {
			init();
			for each (var texture:Texture in _texturesByName) {
				if (texture != _unloadedTexture && texture != _device.fontMgr._debugFont._texture) {
					texture.recreate();
				}
			}
		}

		internal function isCompressedPath(path:String):Boolean {
			return path.indexOf(".atf") != -1;
		}
		
		internal function isLibraryPath(path:String):Boolean {
			return path.indexOf("https://") == -1 && path.indexOf("http://") == -1 && path.indexOf("file://") == -1;
		}


		/**
		 * Returns texture resource if loaded, else cues proper loader which
		 * automatically uploads texture to GPU when loaded.
		 * @param	name
		 * @return
		 */
		public function loadTexture(path:String, alternativePath: String = null):Texture {
			return lazyLoadTexture(path, 0, alternativePath);
		}

		/**
		 * Returns texture resource if loadeds, else puts texture on waiting state.
		 * If texture is called by renderer by @minTimesVisible times, it's going to
		 * be loeaded.
		 */
		public function lazyLoadTexture(path:String, minTimesVisible:uint, alternativePath:String = null):Texture {
			var texture:Texture = _texturesByName[path];
			if (texture == null) {
				if (minTimesVisible == 0) {
					// support for loading textures by url
					if (isLibraryPath(path)) {
						path = _device.getPath(path);
					}
					if (isCompressedPath(path)) {
						return loadCompressedTexture(path, alternativePath);
					} else {
						return loadUncompressedTexture(path, alternativePath);
					}
				} else {
					if (isLibraryPath(path)) {
						path = _device.getPath(path);
					}
					if (isCompressedPath(path)) {
						texture = createCompressedTexture(path, alternativePath);
					} else {
						texture = createUncompressedTexture(path, alternativePath);
					}
					_lazyTexturesWaitingTimes[path] = minTimesVisible;
					return texture;
				}
			} else {
				return texture;
			}
		}

		internal function tick():void {
			for (var name:String in _lazyTexturesWaitingTimes) {
				var texture:Texture = _texturesByName[name];
				if (texture._wasVisibleInLastFrame) {
					texture._wasVisibleInLastFrame = false;
					var timeout:int = _lazyTexturesWaitingTimes[name]--;
					if (timeout <= 0) {
						delete _lazyTexturesWaitingTimes[name];
						if (texture is CompressedTexture) {
							loadCompressedTextureTo(texture as CompressedTexture, name);
						} else {
							loadUncompressedTextureTo(texture as UncompressedTexture, name);
						}
					}
				}
			}

			//TODO: It's wrong, do something smarter with signals
			for (name in _texturesToDispose) {
				texture = _texturesByName[name];
				var oldNumUsers:int = _texturesToDispose[name];
				var newNumUsers:int = texture.numUsers;
				if (newNumUsers == 0) {
					Log.i("[TextureManager] Disposing reference-counted texture " + name);
					disposeTextureImmediately(name);
					delete _texturesToDispose[name];
				} else if (newNumUsers > oldNumUsers) { // Texture gained new user, forget the removal
					delete _texturesToDispose[name];
				}
			}
		}

		/**
		 * Creates named texture resource and immediately uploads bitmapData as the source
		 * @param	name
		 * @param	bitmapData
		 * @return
		 */
		public function createTextureFromBitmapData(name:String, bitmapData:BitmapData):Texture {
			var texture:UncompressedTexture = _texturesByName[name] = new UncompressedTexture(_device);
			texture.loadFromBitmapData(bitmapData);
			texture._name = name;
			return texture;
		}

		/**
		 * Creates empty uncompressed texture resource.
		 * @param	texture
		 */
		public function createUncompressedTexture(name:String, alternativePath:String = null):UncompressedTexture {
			var texture:UncompressedTexture = _texturesByName[name] = new UncompressedTexture(_device);
			texture._name = name;
			texture._alternativePath = alternativePath;
			initAsUnloaded(texture);
			return texture;
		}

		/**
		 * Creates empty compressed texture resource.
		 * @param	name
		 * @return
		 */
		public function createCompressedTexture(name:String, alternativePath: String):CompressedTexture {
			var texture:CompressedTexture = _texturesByName[name] = new CompressedTexture(_device);
			texture._name = name;
			texture._alternativePath = alternativePath;
			initAsUnloaded(texture);
			return texture;
		}

		internal function initAsUnloaded(texture:Texture):void {
			texture._handle = _unloadedFallbackTextureHandle;
			texture._hasAlpha = _unloadedTexture._hasAlpha;
			texture._isCompressed = _unloadedTexture._isCompressed;
			texture._width = _unloadedTexture._width;
			texture._height = _unloadedTexture._height;
		}

		public function loadUncompressedTextureTo(texture:UncompressedTexture, path:String):void {
			if (texture._isLoading && _device._loaderMgr.hasLoader(path)) {
				Log.i("[TextureManager] Uncompressed texture already loading: " + path);
			} else {
				texture._isLoading = true;
				Log.i("[TextureManager] Loading uncompressed texture: " + path);
				_device._loaderMgr.startBitmapLoader(path, path,
					function(name:String):void {
						_device.callWhenContextActive(function():void {
							Log.i("[TextureManager] Uncompressed texture load completed for ", path);
							var loader:BitmapLoader = _device._loaderMgr.getLoader(path) as BitmapLoader;
							try {
								var bitmap:Bitmap = loader.getBitmap();
								texture._isLoading = false;
								texture.loadFromBitmapData(bitmap.bitmapData);
							}
							catch (e:Error) {
								Log.e("[TextureManager] Loaded texture was broken, " + e + ": " + texture.name + "\n" + e.getStackTrace());
								if (texture.alternativePath && texture.alternativePath != path) {
									Log.i("[TextureManager] Start loading texture from alternative path");
									texture._loadedFromAlternativePath = true;
									loadUncompressedTextureTo(texture, texture.alternativePath);
								}
								return;
							}
							_loaderCompleted.get(texture).post(loader);
							_loaderCrashed.dispose(texture);
							_device._loaderMgr.disposeLoader(path);
						});
					},
					function(name:String):void {
						Log.e("[TextureManager] Uncompressed texture load failed for ", path);
						if (texture.alternativePath && texture.alternativePath != path) {
							Log.i("[TextureManager] Start loading texture from alternative path");
							texture._loadedFromAlternativePath = true;
							loadUncompressedTextureTo(texture, texture.alternativePath);
						} else {
							_loaderCrashed.get(texture).post(_device._loaderMgr.getLoader(path));
							_loaderCompleted.dispose(texture);
						}
						_device._loaderMgr.disposeLoader(path);
					}
				);
			}
		}

		public function loadUncompressedTexture(path:String, alternativePath: String):Texture {
			var texture:UncompressedTexture = _texturesByName[path];					
			if (!texture) {
				texture = createUncompressedTexture(path, alternativePath);
				loadUncompressedTextureTo(texture, path);
			}
			return texture;
		}

		internal function loadCompressedTextureTo(texture:CompressedTexture, path:String):void {
			if (texture._isLoading && _device._loaderMgr.hasLoader(path)) {
				Log.i("[TextureManager] Compressed texture already loading: " + path);
			} else {
				texture._isLoading = true;
				Log.i("[TextureManager] Loading compressed texture: " + path);
				_device._loaderMgr.startBinaryLoader(path, path,
					function(name:String):void {
						_device.callWhenContextActive(function():void {
							Log.i("[TextureManager] Compressed texture load completed for ", path);
							var loader:BinaryLoader = _device._loaderMgr.getLoader(path) as BinaryLoader;
							try {
								var atfSource:ByteArray = loader.data;
								texture._isLoading = false;
								texture.loadFromByteArray(atfSource);
							}
							catch (e: * ) {
								Log.e("[TextureManager] Loaded texture was broken, " + e + ": " + texture.name + "\n" + e.getStackTrace());
								if (texture.alternativePath && texture.alternativePath != path) {
									Log.i("[TextureManager] Start loading texture from alternative path");
									texture._loadedFromAlternativePath = true;
									loadCompressedTextureTo(texture, texture.alternativePath);
								}
								return;
							}
							_loaderCompleted.get(texture).post(loader);
							_loaderCrashed.dispose(texture);
							_device._loaderMgr.disposeLoader(path);
						});
					},
					function(name:String):void {
						Log.e("[TextureManager] Compressed texture load failed for ", path);
						if (texture.alternativePath && texture.alternativePath != path) {
							Log.i("[TextureManager] Start loading texture from alternative path");
							texture._loadedFromAlternativePath = true;
							loadCompressedTextureTo(texture, texture.alternativePath);
						} else {
							_loaderCrashed.get(texture).post(_device._loaderMgr.getLoader(path));
							_loaderCompleted.dispose(texture);
						}
						_device._loaderMgr.disposeLoader(path);
					}
				);
			}
		}

		public function loadCompressedTexture(path:String, alternativePath: String):Texture {
			var texture:CompressedTexture = _texturesByName[path];
			if (!texture) {
				texture = createCompressedTexture(path, alternativePath);
				loadCompressedTextureTo(texture, path);
			}
			return texture;
		}

		/**
		 * Disposes named texture. Texture will can be used to rendering or reuploaded as ordinary unloaded texture.
		 * @param	name
		 */
		public function disposeTexture(path:String):void {
			Log.i("[TextureManager] Disposing " + path);
			var texture:Texture = _texturesByName[path];
			if (texture != null) {
				disposeTextureImmediately(path);
			}
		}

		public function disposeTextureWhenNotUsed(path:String):void {
			Log.i("[TextureManager] Disposing " + path);
			var texture:Texture = _texturesByName[path];
			if (texture != null) {
				var numUsers:int = texture.numUsers;
				if (numUsers == 0) {
					disposeTextureImmediately(path);
				} else {
					_texturesToDispose[path] = numUsers;
				}
			}
		}

		internal function disposeTextureImmediately(path:String):void {
			Log.i("[TextureManager] Destroyed " + path);
			var texture:Texture = _texturesByName[path];
			texture.dispose();
			delete _texturesByName[path];
			delete _lazyTexturesWaitingTimes[path];
			delete _texturesToDispose[path];
		}

		public function get loaderCompleted():SignalsMap {
			return _loaderCompleted;
		}

		public function get unloadedFallbackTextureHandle():* {
			return _unloadedTexture._handle;
		}
		
		public function get isTextureDownscaleAllowed():Boolean {
			return _isTextureDownscaleAllowed;
		}
		
		public function set isTextureDownscaleAllowed(value:Boolean):void {
			_isTextureDownscaleAllowed = value;
		}
		
		public function get loaderCrashed():SignalsMap {
			return _loaderCrashed;
		}
	}

}
