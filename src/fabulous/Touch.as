package fabulous {
	public class Touch {
		internal var _id:int;
		internal var _x:Number;
		internal var _y:Number;
		internal var _swipeX:Number;
		internal var _swipeY:Number;
		internal var _originalX:Number;
		internal var _originalY:Number;
		internal var _swipeDistance:Number;
		internal var _touchedNode:Node;
		internal var _swipedXNode:Node;
		internal var _swipedYNode:Node;
		internal var _swipeStarted:Boolean;

		public function Touch(id:int, x:Number, y:Number) {
			_id = id;
			_x = x;
			_y = y;
			_swipeX = 0;
			_swipeY = 0;
			_originalX = x;
			_originalY = y;
			_swipeDistance = 0;
			_swipeStarted = false;
		}

		public final function get id():int {
			return _id;
		}

		public final function get x():Number {
			return _x;
		}
		public final function get y():Number {
			return _y;
		}
		
		public final function get originalX():Number {
			return _originalX;
		}
		public final function get originalY():Number {
			return _originalY;
		}

		public final function get swipeX():Number {
			return _swipeX;
		}
		public final function get swipeY():Number {
			return _swipeY;
		}

		public final function get swipeDistance():Number {
			return _swipeDistance;
		}

		internal function addMovePosition(x:Number, y:Number):void {
			_swipeX = x-_x;
			_swipeY = y-_y;
			_swipeDistance += Math.sqrt(_swipeX*_swipeX + _swipeY*_swipeY);
			_x = x;
			_y = y;
		}

	}

}
