package fabulous {
	
	public class TraceLogger extends Logger {

		public function TraceLogger() {
		}

		
		override public final function print(msg:String):void {
			trace(msg);
		}

	}

}
