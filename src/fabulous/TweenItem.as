package fabulous {
	public class TweenItem {
		internal var _next:TweenItem;

		internal var _active:Boolean;
		internal var _id:int;
		internal var _object:Object;
		internal var _props:Array;
		internal var _from:Object;
		internal var _diff:Object;
		internal var _startTime:Number;
		internal var _duration:Number;
		internal var _ease:Function;
		internal var _onComplete:Function;

		public function TweenItem() {
			_next = null;
			_active = false;
			_object = null;
			_props = [];
		}

		public final function stop():void {
			_onComplete = null;
			_active = false;
			_object = null;
			_ease = null;
		}
	}

}
