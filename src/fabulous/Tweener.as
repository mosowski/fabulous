package fabulous {
	import flash.utils.getTimer;
	public class Tweener {
		internal var _tweenListHead:TweenItem;
		internal var _tweenListLast:TweenItem;
		internal var _time:Number;
		internal var _currentId:int;

		public function Tweener() {
			_tweenListHead = new TweenItem();
			_tweenListLast = _tweenListHead;
			_time = 0;
			_currentId = 0;
		}

		public final function tween(object:Object, from:Object, to:Object, duration:Number, ease:Function, onComplete:Function = null):TweenItem {
			var item:TweenItem = getTweenItem();
			item._active = true;
			item._id = _currentId++;
			item._object = object;
			item._props.length = 0;
			item._from = from;
			item._diff = to;
			item._startTime = _time;
			item._duration = duration;
			item._ease = ease;
			item._onComplete = onComplete;

			for (var prop:String in from) {
				item._props[item._props.length] = prop;
			}
			for (var i:int = 0; i < item._props.length; ++i) {
				item._diff[item._props[i]] -= from[item._props[i]];
				object[item._props[i]] = from[item._props[i]];
			}

			return item;
		}

		internal final function tick():void {
			_time = getTimer() / 1000;
			var q:Number;
			var i:int;
			var prop:String;
			var ease:Number;
			for (var item:TweenItem = _tweenListHead; item != null; item = item._next) {
				if (item._active) {
					q = (_time - item._startTime) / item._duration;
					if (q >= 1.0) {
						for (i = 0; i < item._props.length; ++i) {
							item._object[item._props[i]] = item._from[item._props[i]] + item._diff[item._props[i]];
						}

						if (item._onComplete != null) {
							item._onComplete();
							item._onComplete = null;
						}
						item._active = false;
						item._object = null;
						item._ease = null;
					} else {
						ease = item._ease(q);
						for (i = 0; i < item._props.length; ++i) {
							item._object[item._props[i]] = item._from[item._props[i]] + item._diff[item._props[i]] * ease;
						}
					}
				}
			}
		}

		internal final function getTweenItem():TweenItem {
			var item:TweenItem = _tweenListHead;
			while (item != null && item._active == true) {
				item = item._next;
			}
			if (!item) {
				item = new TweenItem();
				_tweenListLast._next = item;
				_tweenListLast = item;
			}
			return item;
		}

		public final function linearEase(q:Number):Number {
			return q;
		}

		public final function quadraticEase(q:Number):Number {
			return q*q;
		}

		public final function sqrtEase(q:Number):Number {
			return Math.sqrt(q);
		}

		public final function sineEase(q:Number):Number {
			return Math.sin(q * Math.PI * 0.5);
		}

		public final function mediumBounceEase(q:Number):Number {
			return Math.sin(q * Math.PI * 0.686403) * 1.2;
		}


	}

}

