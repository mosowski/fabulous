package fabulous {
	import flash.geom.Matrix;
	import flash.display.BitmapData;
	import flash.display3D.Context3DTextureFormat;

	public class UncompressedTexture extends Texture {

		public function UncompressedTexture(device:Device) {
			super(device);
		}

		/**
		 * Uploads bitmap as texture source to GPU. Disposes already uploaded
		 * texture; throws exception when trying to upload texture being loaded
		 * using texture manager.
		 * @param	bitmapData
		 */
		public function loadFromBitmapData(bitmapData:BitmapData):void {
			if (_isLoading) {
				throw "Trying to upload source to texture being loaded.";
			}

			if (_handle && _handle != _device._textureMgr._unloadedFallbackTextureHandle) {
				_handle.dispose();
			}

			_sourceWidth = bitmapData.width;
			_sourceHeight = bitmapData.height;

			if (!(Bits.isPow2(bitmapData.width) && Bits.isPow2(bitmapData.height))) {
				var nextWidth:int = Bits.nextPow2(bitmapData.width);
				var prevWidth:int = Bits.prevPow2(bitmapData.width);
				var nextHeight:int = Bits.nextPow2(bitmapData.height);
				var prevHeight:int = Bits.prevPow2(bitmapData.height);				
				if (_device.textureMgr.isTextureDownscaleAllowed) {
					_width = ((nextWidth - bitmapData.width) < (bitmapData.width - prevWidth)) ? nextWidth : prevWidth
					_height = ((nextHeight - bitmapData.height) < (bitmapData.height - prevHeight)) ? nextHeight : prevHeight
				} else {
					_width = Bits.nextPow2(bitmapData.width);
					_height = Bits.nextPow2(bitmapData.height);
				}

				if (_width > 2048) {
					_width = 2048;
				}
				if (_height > 2048) {
					_height = 2048;
				}

				Log.w("Texture " + _name + " is non-power-of-two ("+bitmapData.width+" x " + bitmapData.height+"). Upscaling to " + _width + " x " + _height);

				var tmpBitmap:BitmapData = new BitmapData(_width, _height, true, 0x0000000);
				tmpBitmap.draw(bitmapData, new Matrix(_width/bitmapData.width, 0, 0, _height/bitmapData.height), null, null, null, true);

				_handle = _device._ctx.createTexture(_width, _height, Context3DTextureFormat.BGRA, false);
				_handle.uploadFromBitmapData(tmpBitmap);
				tmpBitmap.dispose();
			} else {
				_width = bitmapData.width;
				_height = bitmapData.height;

				_handle = _device._ctx.createTexture(_width, _height, Context3DTextureFormat.BGRA, false);
				_handle.uploadFromBitmapData(bitmapData);
			}

			_numTextureBytes = _width * _height * 4;
			_device._numTextureBytes += _numTextureBytes;
			_isLoaded = true;
			_loaded.post();
		}
		
		override public function recreate():void {
			super.recreate();
			_device.textureMgr.loadUncompressedTextureTo(this, _name);
		}
	}
}
