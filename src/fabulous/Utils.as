package fabulous 
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author ...
	 */
	public class Utils {
		public static function addLeadingZeros(text:String, length:int):String {
			while (text.length < length) {
				text = "0" + text;
			}
			return text;
		}
	}

}

internal final class FakeClass { }