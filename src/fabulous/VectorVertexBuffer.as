package fabulous {
	public class VectorVertexBuffer extends VertexBuffer {
		internal var _data:Vector.<Number>;

		public function VectorVertexBuffer(device:Device) {
			super(device);
		}

		override internal function setSizeAndFormat(numVertices:int, format:Vector.<String>):void {
			super.setSizeAndFormat(numVertices, format);
			_data = new Vector.<Number>(_numVertices * _data32PerVertex, true);
			_changeRangeEnd = _numVertices;
			upload();
		}

		
		override internal final function upload():void {
			if (_changeRangeBegin <_changeRangeEnd) {
				_vertexBuffer3d.uploadFromVector(_data, _changeRangeBegin, _changeRangeEnd - _changeRangeBegin);
				_changeRangeBegin = _numVertices;
				_changeRangeEnd = 0;
			}
		}

		
		override internal final function writeFloat(f:Number):void {
			_data[_position] = f;
			++_position;
		}

		
		override internal final function set position(value:int):void {
			_position = value;
		}

		
		override internal final function get position():int {
			return _position;
		}

	}

}
