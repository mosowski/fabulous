package fabulous {
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.Context3DBufferUsage;
	
	public class VertexBuffer {
		internal var _device:Device;
		internal var _vertexBuffer3d:VertexBuffer3D;
		internal var _numVertices:int;
		internal var _data32PerVertex:int;
		internal var _format:Vector.<String>;
		internal var _formatOffsets:Vector.<int>;
		internal var _changeRangeBegin:int;
		internal var _changeRangeEnd:int;
		protected var _position:int;

		public static const MAX_SIZE:int = 0x3FFF;
		
		public function VertexBuffer(device:Device) {
			_device = device;
		}
		
		internal function recreate():void {
			setSizeAndFormat(_numVertices, _format);
		}

		internal function setSizeAndFormat(numVertices:int, format:Vector.<String>):void {
			if (_vertexBuffer3d) {
				_vertexBuffer3d.dispose();
			}

			_numVertices = numVertices;
			_format = format;
			_formatOffsets = new Vector.<int>();
			_data32PerVertex = 0;
			for (var i:int = 0; i < _format.length; ++i) {
				_formatOffsets.push(_data32PerVertex);
				if (_format[i] == Context3DVertexBufferFormat.FLOAT_1 || _format[i] == Context3DVertexBufferFormat.BYTES_4) {
					_data32PerVertex += 1;
				} else if (_format[i] == Context3DVertexBufferFormat.FLOAT_2) {
					_data32PerVertex += 2;
				} else if (_format[i] == Context3DVertexBufferFormat.FLOAT_3) {
					_data32PerVertex += 3;
				} else if (_format[i] == Context3DVertexBufferFormat.FLOAT_4) {
					_data32PerVertex += 4;
				}
			}
			_vertexBuffer3d = _device._ctx.createVertexBuffer(numVertices, _data32PerVertex, Context3DBufferUsage.DYNAMIC_DRAW);
			_position = 0;
		}

		internal function upload():void {
			throw new Error("Abstract");
		}

		
		internal final function bind(offset:int):void {
			for (var i:int = 0; i < _format.length; ++i) {
				_device._ctx.setVertexBufferAt(i + offset, _vertexBuffer3d, _formatOffsets[i], _format[i]);
			}
		}

		
		internal final function unbind(offset:int):void {
			for (var i:int = 0; i < _format.length; ++i) {
				_device._ctx.setVertexBufferAt(i + offset, null);
			}
		}

		/** Call it before writing to the *consecutive* vertices in the buffer, but 
		  * just after setting the right position. */
		internal function beginWrite():void {
			if (_position/ _data32PerVertex < _changeRangeBegin) {
				_changeRangeBegin = _position / _data32PerVertex;
			}
		}

		internal function endWrite():void {
			if (_position / _data32PerVertex > _changeRangeEnd) {
				_changeRangeEnd = _position / _data32PerVertex;
			}
		}

		internal function writeFloat(f:Number):void {
			throw new Error("Abstract");
		}

		internal function set position(value:int):void {
			_position = value;
		}

		
		internal final function set vertexPosition(value:int):void {
			position = value * _data32PerVertex;
		}

		internal function get position():int {
			return _position;
		}

		
		internal final function get vertexPosition():int {
			return _position / _data32PerVertex;
		}

	}

}
