package fabulous.animation 
{
	import fabulous.F;
	import fabulous.Signal;
	/**
	 * ...
	 * @author ...
	 */
	public class Animation {
		private var _isLooping:Boolean;
		private var _isPlaying:Boolean;
		private var _onFinishedSignal:Signal;
		
		public function Animation() {
			_onFinishedSignal = new Signal();
		}
		
		public function play(onFinished:Function = null):void {
			_isPlaying = true;
			if (onFinished != null) {
				_onFinishedSignal.addOnce(onFinished);
			}
		}
		
		public function stop():void {
			_isPlaying = false;
		}
		
		internal function reset():void {
			
		}
		
		protected final function finish():void {
			// we perform 1-frame delay not to start the animation again (if isLooping is true),
			// because it may cause an infinite loop in _onFinishedSignal
			F.performFunctionNextFrame(reallyFinish);
		}
		
		private function reallyFinish():void {
			if (_isLooping) {
				reset();
				play();
			} else {
				if (_onFinishedSignal != null) {
					_onFinishedSignal.post();
				}
			}			
		}
		
		public function get isLooping():Boolean {
			return _isLooping;
		}
		
		public function set isLooping(value:Boolean):void {
			_isLooping = value;
		}

		public function dispose():void {
			if (_onFinishedSignal != null) {
				_onFinishedSignal.clean();
				_onFinishedSignal = null;
			}
		}
	}

}