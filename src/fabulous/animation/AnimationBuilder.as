package fabulous.animation 
{
	import fabulous.structures.Stack;
	/**
	 * ...
	 * @author ...
	 */
	public class AnimationBuilder {
		private var _onWholeAnimationFinishedFunction:Function;
		private var _currentAnimationSet:AnimationSet;
		private var _animationSetStack:Stack;
		private var _mainAnimation:AnimationSet;
		
		public static function start():AnimationBuilder {
			return new AnimationBuilder();
		}
		
		public static function startParallel():AnimationBuilder {
			return start().beginParallel();
		}
		
		public static function startSequence():AnimationBuilder {
			return start().beginSequence();
		}
		
		public function beginParallel():AnimationBuilder {
			return beginAnimationSet(new ParallelAnimation());
		}
		
		public function beginSequence():AnimationBuilder {
			return beginAnimationSet(new SequentialAnimation());
		}
		
		private function beginAnimationSet(animationSet:AnimationSet):AnimationBuilder {
			if (_mainAnimation == null) {
				_mainAnimation = animationSet;
			}
			if (_currentAnimationSet != null) {
				_animationSetStack.push(_currentAnimationSet);
				_currentAnimationSet.addAnimation(animationSet);
			}
			_currentAnimationSet = animationSet;
			return this;
		}
		
		public function end():AnimationBuilder {
			_currentAnimationSet = _animationSetStack.pop();
			return this;
		}
		
		public function endSequence():AnimationBuilder {
			return end();
		}
		
		public function endParallel():AnimationBuilder {
			return end();
		}
		
		public function finishBuilding():Animation {
			return _mainAnimation;
		}
		
		public function addAnimation(animation:Animation):AnimationBuilder {
			_currentAnimationSet.addAnimation(animation);
			return this;
		}
		
		public function addTween(target:Object, startValues:Object, targetValues:Object, time:Number, transition:Function = null):AnimationBuilder {
			_currentAnimationSet.addAnimation(new TweenAnimation(target, startValues, targetValues, time, transition));
			return this;
		}
		
		public function addDelay(delay:Number):AnimationBuilder {
			_currentAnimationSet.addAnimation(new DelayAnimation(delay));
			return this;
		}
		
		public function AnimationBuilder() {
			_animationSetStack = new Stack()
		}
	}

}