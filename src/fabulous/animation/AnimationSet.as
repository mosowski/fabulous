package fabulous.animation 
{
	/**
	 * ...
	 * @author tomasz.jakubowski
	 */
	public class AnimationSet extends Animation 
	{
		protected var _animations:Vector.<Animation>;
		
		public function AnimationSet() {
			super();
			_animations = new Vector.<Animation>();
		}
		
		public function addAnimation(animation:Animation):void {
			_animations.push(animation);
		}
		
		override internal function reset():void {
			super.reset();
			for (var i:int = 0; i < _animations.length; ++i) {
				_animations[i].reset();
			}
		}

		override public function dispose():void {
			super.dispose();
			if (_animations != null) {
				for (var i:int = _animations.length - 1; i >= 0; --i) {
					_animations[i].dispose();
				}
				_animations = null;
			}
		}
	}

}