package fabulous.animation 
{
	import fabulous.F;
	import fabulous.TweenItem;
	/**
	 * ...
	 * @author ...
	 */
	public class DelayAnimation extends Animation {
		private var _delay:Number;
		private var _tween:TweenItem;
		
		public function DelayAnimation(delay:Number) {
			_delay = delay;
		}
		
		override public function play(onFinished:Function = null):void {
			super.play(onFinished);
			_tween = F.waitAndDoOnce(_delay, finish);
		}
		
		override public function stop():void {
			super.stop();
			if (_tween != null) {
				_tween.stop();
			}
		}

		override public function dispose():void {
			super.dispose();
			_tween = null;
		}
	}

}