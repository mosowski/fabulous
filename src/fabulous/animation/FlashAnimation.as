package fabulous.animation 
{
import fabulous.F;
import fabulous.Material;
import fabulous.MaterialBasic;
import fabulous.Node;
import fabulous.Signal;
import fabulous.Sprite;
import fabulous.SpriteSheetFrame;
import fabulous.TextLoader;

import flash.utils.Dictionary;

/**
	 * ...
	 * @author tomasz.jakubowski
	 */
	public class FlashAnimation extends Animation 
	{
		private var _name:String;
		private var _frameRate:int;
		private var _spritePrototypes:Dictionary;
		private var _allSprites:Dictionary;
		private var _layers:Vector.<AnimationLayer>;
		private var _useAlphaMaterial:Boolean;
		private var _animation:Animation;
		private var _parentNode:Node;
		private var _isLoading:Boolean;
		private var _isLoaded:Boolean;
		private var _loadedSignal:Signal;
		private var _material:Material;
		
		public function FlashAnimation(name:String, node:Node, material:Material) {
			super();
			_name = name;
			_loadedSignal = new Signal();
			_material = material;
			this.parentNode = node;
		}

		public function load(onLoaded:Function = null):FlashAnimation {
			if (!_isLoading && !_isLoaded) {
				if (onLoaded != null) {
					_loadedSignal.addOnce(onLoaded);
				}
				_isLoading = true;
				var path:String = "static/animations/" + _name + ".json";
				var that:FlashAnimation = this;
				F.device.loaderMgr.startTextLoader(path, path,
					function (ldrName:String):void {
						var loader:TextLoader = F.device.loaderMgr.getLoader(ldrName) as TextLoader;
						that.fromObject(loader.getJSON());
						_isLoaded = true;
						_isLoading = false;
						_loadedSignal.post();
						F.device.loaderMgr.disposeLoader(ldrName);
					}
				);
			} else if (onLoaded != null) {
				if (_isLoading) {
					_loadedSignal.addOnce(onLoaded);
				} else {
					onLoaded();
				}
			}
			return this;
		}
		
		override public function play(onFinished:Function = null):void {
			super.play(onFinished);
			load(playAnimation);
		}
		
		override internal function reset():void {
			super.reset();
			if (_animation != null) {
				_animation.reset();
			}
		}
		
		override public function stop():void {
			super.stop();
			if (_animation != null) {
				_animation.stop();
			}
		}
		
		private function fromObject(animationData:Object):void {
			_useAlphaMaterial = animationData.useAlphaMaterial;
			_frameRate = animationData.frameRate;
			_spritePrototypes = new Dictionary();
			_allSprites = new Dictionary();
			for (var i:int = 0; i < animationData.sprites.length; ++i) {
				var spriteData:Object = animationData.sprites[i];
				var sprite:Sprite = new Sprite(_useAlphaMaterial && _material is MaterialBasic ? F.device.materialMgr.materialBasicAlphaDefault : _material);
				var frame:SpriteSheetFrame = F.device.spriteSheetMgr.getSheet("gui").getFrame(spriteData.textureName);
				sprite.setTextureFromFrame(frame);
				sprite.width = 2 * frame.imageWidth;
				sprite.height = 2 * frame.imageHeight;
				var node:Node = new Node();
				node.addRenderable(sprite);
				node.x = spriteData.x;
				node.y = spriteData.y;
				_spritePrototypes[animationData.sprites[i].name] = sprite;
			}
			_layers = new Vector.<AnimationLayer>(animationData.layers.length);
			for (i = 0; i < _layers.length; ++i) {
				_layers[i] = new AnimationLayer(animationData.layers[i], _spritePrototypes, _allSprites);
			}
			addLayers();
			_animation = setupAnimation();
		}
		
		private function setupAnimation():Animation {
			var builder:AnimationBuilder = _layers.length > 1 ? AnimationBuilder.startParallel() : AnimationBuilder.start();
			for (var i:int = 0; i < _layers.length; ++i) {
				builder.beginSequence();
				var layer:AnimationLayer = _layers[i];
				for (var j:int = 0; j < layer.frames.length; ++j) {
					var frameNow:AnimationFrame = layer.frames[j];
					var frameThen:AnimationFrame = layer.frames[Math.min(j + 1, layer.frames.length - 1)];
					if (frameNow.elements.length > 1) {
						builder.beginParallel();
					}
					for (var k:int = 0; k < frameNow.elements.length; ++k) {
						var elementNow:AnimationElement = frameNow.elements[k];
						var elementThen:AnimationElement = frameThen.elements[k];
						builder.addTween(
							layer.symbols[elementNow.name],
							{
								x : elementNow.x,
								y : elementNow.y,
								scaleX : elementNow.scaleX,
								scaleY : elementNow.scaleY,
								rotation : elementNow.rotation,
								alpha : elementNow.alpha
							},
							{
								x : elementThen.x,
								y : elementThen.y,
								scaleX : elementThen.scaleX,
								scaleY : elementThen.scaleY,
								rotation : elementThen.rotation,
								alpha : elementThen.alpha
							},
							frameNow.duration / _frameRate
						);
					}
					if (frameNow.elements.length > 1) {
						builder.endParallel();
					}
				}
				builder.endSequence();
			}
			if (_layers.length > 1) {
				builder.endParallel();
			}
			return builder.finishBuilding();
		}
		
		private function addLayers():void {
			if (_layers != null) {
				for (var i:int = 0; i < _layers.length; ++i) {
					var layer:AnimationLayer = _layers[i];
					for each (var symbol:Node in layer.symbols) {
						if (_parentNode != symbol.parent) {
							if (_parentNode != null) {
								_parentNode.addChild(symbol);
							} else if (symbol != null) {
								symbol.detachFromParent();
							}
						}
					}
				}
			}
		}
		
		private function playAnimation():void {
			_animation.play(finish);
		}
		
		public function get parentNode():Node {
			return _parentNode;
		}
		
		public function set parentNode(value:Node):void {
			_parentNode = value;
			addLayers();
		}
		
		public function get material():Material {
			return _material;
		}
		
		public function set material(value:Material):void {
			_material = value;
			for each (var sprite:Sprite in _spritePrototypes) {
				sprite.material = _material;
			}
			for each (sprite in _allSprites) {
				sprite.material = _material;
			}
		}

		override public function dispose():void {
			super.dispose();
			_spritePrototypes = null;
			_allSprites = null;
			_layers = null;
			if (_animation != null) {
				_animation.dispose();
				_animation = null;
			}
			_parentNode = null;
			if (_loadedSignal != null) {
				_loadedSignal.clean();
				_loadedSignal = null;
			}
			_material = null;
		}
	}

}

import fabulous.Node;
import fabulous.Sprite;

import flash.utils.Dictionary;

final class AnimationLayer {
	private var _name:String;
	private var _frames:Vector.<AnimationFrame>;
	private var _symbols:Dictionary;
	
	public function AnimationLayer(data:Object, spritePrototypes:Dictionary, allSprites:Dictionary) {
		_name = data.name;
		_symbols = new Dictionary();
		_frames = new Vector.<AnimationFrame>(data.frames.length);
		for (var i:int = 0; i < _frames.length; ++i) {
			_frames[i] = new AnimationFrame(data.frames[i], this, spritePrototypes, allSprites);
		}
	}
	
	[Inline]
	public function get name():String {
		return _name;
	}
	
	[Inline]
	public function get frames():Vector.<AnimationFrame> {
		return _frames;
	}
	
	[Inline]
	public function get symbols():Dictionary {
		return _symbols;
	}
}

final class AnimationFrame {
	private var _index:int;
	private var _duration:int;
	private var _elements:Vector.<AnimationElement>;
	
	public function AnimationFrame(data:Object, layer:AnimationLayer, spritePrototypes:Dictionary, allSprites:Dictionary) {
		_index = data.index;
		_duration = data.duration;
		_elements = new Vector.<AnimationElement>(data.elements.length);
		for (var i:int = 0; i < _elements.length; ++i) {
			var element:AnimationElement = new AnimationElement(data.elements[i]);
			_elements[i] = element;
			if (layer.symbols[element.name] == null) {
				var node:Node = new Node();
				var sprite:Sprite = spritePrototypes[element.name].clone();
				node.addChild(sprite.node);	
				
				layer.symbols[element.name] = node;
				allSprites[element.name] = sprite;
				
				node.x = element.x;
				node.y = element.y;
				node.scaleX = element.scaleX;
				node.scaleY = element.scaleY;
				node.rotation = element.rotation;
				node.alpha = element.alpha;
			}
		}
	}
	
	[Inline]
	public function get index():int {
		return _index;
	}
	
	[Inline]
	public function get duration():int {
		return _duration;
	}
	
	[Inline]
	public function get elements():Vector.<AnimationElement> {
		return _elements;
	}
}

final class AnimationElement {
	private var _name:String;
	private var _scaleX:Number;
	private var _scaleY:Number;
	private var _x:Number;
	private var _y:Number;
	private var _rotation:Number;
	private var _alpha:Number;
	
	public function AnimationElement(data:Object) {
		_name = data.name;
		_scaleX = data.scaleX;
		_scaleY = data.scaleY;
		_x = data.x;
		_y = data.y;
		_rotation = data.rotation;
		_alpha = data.alpha;
	}
	
	[Inline]
	public function get name():String {
		return _name;
	}
	
	[Inline]
	public function get scaleX():Number {
		return _scaleX;
	}
	
	[Inline]
	public function get scaleY():Number {
		return _scaleY;
	}
	
	[Inline]
	public function get x():Number {
		return _x;
	}
	
	[Inline]
	public function get y():Number {
		return _y;
	}
	
	[Inline]
	public function get rotation():Number {
		return _rotation;
	}
	
	[Inline]
	public function get alpha():Number {
		return _alpha;
	}
}