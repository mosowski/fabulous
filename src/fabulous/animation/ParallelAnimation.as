package fabulous.animation 
{
	import fabulous.utils.Counter;
	/**
	 * ...
	 * @author tomasz.jakubowski
	 */
	public class ParallelAnimation extends AnimationSet 
	{
		private var _counter:Counter;
		
		public function ParallelAnimation() {
			super();
		}
		
		override public function play(onFinished:Function = null):void {
			super.play(onFinished);
			if (_counter == null) {
				_counter = new Counter(_animations.length);
			}
			_counter.register(finish);
			for (var i:int = 0; i < _animations.length; ++i) {
				_animations[i].play(_counter.count);
			}
		}
		
		override internal function reset():void {
			super.reset();
			_counter.clean();
		}
		
		override public function stop():void {
			for (var i:int = 0; i < _animations.length; ++i) {
				_animations[i].stop();
			}
			_counter.clean();
			super.stop();
		}

		override public function dispose():void {
			super.dispose();
			if (_counter != null) {
				_counter.dispose();
				_counter = null;
			}
		}
	}

}