package fabulous.animation 
{
	/**
	 * ...
	 * @author tomasz.jakubowski
	 */
	public class SequentialAnimation extends AnimationSet
	{
		private var _currentAnimationIndex:int = 0;
		private var _currentAnimation:Animation;
		
		public function SequentialAnimation() {
			super();
		}
		
		override public function play(onFinished:Function = null):void {
			super.play(onFinished);
			performAnimation();
		}
		
		override public function addAnimation(animation:Animation):void {
			super.addAnimation(animation);
		}
		
		override internal function reset():void {
			super.reset();
			_currentAnimationIndex = 0;
		}
		
		override public function stop():void {
			if (_currentAnimation != null) {
				_currentAnimation.stop();
				_currentAnimation = null;
				_currentAnimationIndex = 0;
			}
			super.stop();
		}
		
		private function performAnimation():void {
			if (_currentAnimationIndex < _animations.length) {
				_currentAnimation = _animations[_currentAnimationIndex++]
				_currentAnimation.play(performAnimation);
			} else {
				finish()
			}
		}

		override public function dispose():void {
			super.dispose();
			if (_currentAnimation != null) {
				_currentAnimation.dispose();
				_currentAnimation = null;
			}
		}
	}

}