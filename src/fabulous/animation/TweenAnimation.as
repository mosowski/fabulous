package fabulous.animation {
import fabulous.F;
import fabulous.TweenItem;

/**
 * ...
 * @author ...
 */
public class TweenAnimation extends Animation {
	private var _target:Object;
	private var _time:Number;
	private var _transitionFunction:Function;
	private var _tween:TweenItem;
	private var _fromValues:Object;
	private var _toValues:Object;
	private var _toValuesClone:Object;

	public function TweenAnimation(target:Object, fromValues:Object, targetValues:Object, time:Number, transitionFunction:Function = null) {
		_target = target;
		_fromValues = fromValues;
		_toValues = targetValues;
		_toValuesClone = {};
		_time = time;
		_transitionFunction = transitionFunction != null ? transitionFunction : F.tweener.linearEase;
	}

	override public function play(onFinished:Function = null):void {
		super.play(onFinished);
		cloneToValues();
		_tween = F.tweener.tween(
				_target,
				_fromValues,
				// these values are changed by Tweener, so in order to be able to launch this animation again we have to clone_toValuesClone,
				_toValuesClone,
				_time,
				_transitionFunction,
				finish
		);
	}

	public function tweenProperty(property:String, from:Number, to:Number):void {
		_fromValues[property] = from;
		_toValues[property] = to;
	}

	override public function stop():void {
		super.stop();
		if (_tween != null) {
			_tween.stop();
		}
	}

	private function cloneToValues():void {
		for (var key:String in _toValues) {
			_toValuesClone[key] = _toValues[key];
		}
	}

	override public function dispose():void {
		super.dispose();
		_target = null;
		_transitionFunction = null;
		_tween = null;
		_fromValues = null;
		_toValues = null;
		_toValuesClone = null;
	}
}

}