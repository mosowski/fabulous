package fabulous.structures 
{
	/**
	 * ...
	 * @author tomasz.jakubowski
	 */
	public class Stack 
	{
		private var _array:Array;
		
		public function Stack() {
			_array = [];
		}
		
		public function pop():* {
			return _array.pop();
		}
		
		public function push(element:*):void {
			_array.push(element);
		}
	}

}