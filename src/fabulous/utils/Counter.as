package fabulous.utils 
{
	import fabulous.Signal;
	/**
	 * ...
	 * @author tomasz.jakubowski
	 */
	public class Counter 
	{
		private var _maxCount:int;
		private var _currentCount:int;
		private var _finishedSignal:Signal;
		
		public function Counter(maxCount:uint) {
			_maxCount = maxCount;
			_finishedSignal = new Signal();
		}
		
		public function count():void {
			CONFIG::debug {
				if (_currentCount == _maxCount) {
					throw new Error("Counter reached value > maxValue");
				}
			}
			if (++_currentCount >= _maxCount) {
				_finishedSignal.post();
			}
		}
		
		public function register(callback:Function):void {
			_finishedSignal.addOnce(callback);
		}
		
		public function clean():void {
			_currentCount = 0;
			if (_finishedSignal != null) {
				_finishedSignal.clean();
			}
		}

		public function dispose():void {
			clean();
			_finishedSignal = null;
		}
	}

}