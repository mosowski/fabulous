package fabulous3d {
	import flash.geom.Matrix3D;

	import flash.display3D.Context3DClearMask;

	public class Camera extends Component {
		internal var _fov:Number;
		internal var _near:Number;
		internal var _far:Number;
		internal var _aspect:Number;

		internal var _clearColor:Boolean;
		internal var _clearDepth:Boolean;
		internal var _initialDepth:Number;

		internal var _projMtx:Matrix3D;
		internal var _viewMtx:Matrix3D;
		internal var _viewProjMtx:Matrix3D;
		internal var _mtxData:Vector.<Number>;

		internal var _isDirty:Boolean;

		public function Camera() {
			super();
			_near = 0.1;
			_far = 1000.0;
			_aspect = 1.0;
			_fov = 60.0;
			_clearColor = false;
			_clearDepth = false;
			_initialDepth = 1.0;
			_projMtx = new Matrix3D();
			_viewMtx = new Matrix3D();
			_viewProjMtx = new Matrix3D();
			_mtxData = new Vector.<Number>();
			_mtxData.length = 16;
			_isDirty = true;
		}

		internal function updateMatrix():void {
            var top:Number = _near * Math.tan(_fov * Math.PI / 360.0);
            var right:Number = top * _aspect;

            _mtxData[0] = _near / right;
            _mtxData[1] = 0;
            _mtxData[2] = 0;
            _mtxData[3] = 0;

            _mtxData[4] = 0;
            _mtxData[5] = _near / top;
            _mtxData[6] = 0;
            _mtxData[7] = 0;

            _mtxData[8] = 0;
            _mtxData[9] = 0;
            _mtxData[10] = (_far + _near) / (_far - _near);
            _mtxData[11] = 1;

            _mtxData[12] = 0;
            _mtxData[13] = 0;
            _mtxData[14] = -(_far * _near * 2) / (_far - _near);
            _mtxData[15] = 0;

            _projMtx.copyRawDataFrom(_mtxData);
		}

		override public function update():void {
			if (_isDirty) {
				updateMatrix();
				_isDirty = false;
			}
			if (transform._wasChanged) {
				_viewMtx.copyFrom(transform._mtx);
				_viewMtx.invert();
				_viewProjMtx.copyFrom(_viewMtx);
				_viewProjMtx.append(_projMtx);
			}
		}

		public function get near():Number {
			return _near;
		}

		public function set near(v:Number):void {
			_near = v;
			_isDirty = true;
		}

		public function get far():Number {
			return _far;
		}

		public function set far(v:Number):void {
			_far = v;
			_isDirty = true;
		}

		public function get fov():Number {
			return _fov;
		}

		public function set fov(v:Number):void {
			_fov = v;
			_isDirty = true;
		}

		public function get aspect():Number {
			return _aspect;
		}

		public function set aspect(v:Number):void {
			_aspect = v;
			_isDirty = true;
		}

		public function setAsActiveCamera():void {
			F3d._activeCamera = _gameObject;
		}

		public function clear():void {
			var clearMask:uint = Context3DClearMask.STENCIL;
			if (_clearColor) {
				clearMask |= Context3DClearMask.COLOR;
			}
			if (_clearDepth) {
				clearMask |= Context3DClearMask.DEPTH;
			}
			F3d.ctx.clear(0,0,0,1, _initialDepth, 0, clearMask);
		}
	}
}
