package fabulous3d {

	public class Component {
		internal var _gameObject:GameObject;

		public function Component() {
		}

		public function update():void {
		}

		public function lateUpdate():void {
		}

		public function get transform():Transform {
			return _gameObject._transform;
		}
	}
}
