package fabulous3d {
	import flash.utils.Dictionary;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;

	import flash.display3D.Context3D;
	import flash.display3D.Context3DCompareMode;	 
	import flash.display3D.Context3DBlendFactor;	 

	import fabulous.Device;

	public class F3d {
		internal static var _device:Device;
		internal static var _shaders:Dictionary;

		internal static var _gameObjects:Vector.<GameObject>;
		internal static var _newGameObjects:Vector.<GameObject>;
		internal static var _gameObjectsToDestroy:Vector.<GameObject>;

		internal static var _shaderQueue:Vector.<Shader>;
		internal static var _renderQueue:Dictionary;

		internal static var _activeCamera:GameObject;
		internal static var _modelViewProjMtx:Matrix3D;

		public static function init(device:Device):void {
			_device = device;

			_shaders = new Dictionary();
			_gameObjects = new Vector.<GameObject>();
			_newGameObjects = new Vector.<GameObject>();
			_gameObjectsToDestroy = new Vector.<GameObject>();

			_shaderQueue = new Vector.<Shader>();
			_renderQueue = new Dictionary();

			_modelViewProjMtx = new Matrix3D();

			Library.init();
		}

		public static function tick():void {
			initGameObjects();
			destroyGameObjects();
			ctx.setScissorRectangle(null);

			cleanRenderQueue();

			for (var i:int = 0; i < _gameObjects.length; ++i) {
				if (_gameObjects[i].transform._parent == null) {
					_gameObjects[i].update();
				}
			}

			renderGameObjects();

			cleanContext();
		}

		internal static function cleanContext():void {
			ctx.setVertexBufferAt(0, null);
			ctx.setVertexBufferAt(1, null);
			ctx.setTextureAt(0, null);
			ctx.setDepthTest(false, Context3DCompareMode.ALWAYS);
			ctx.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ZERO);
		}

		public static function recreate():void {
			Library.recreate();
		}

		internal static function get ctx():Context3D {
			return _device.ctx;
		}

		internal static function get fallbackTexture():* {
			return _device.textureMgr.unloadedFallbackTextureHandle;
		}

		internal static function initGameObjects():void {
			for (var i:int = 0; i < _newGameObjects.length; ++i) { 
				_gameObjects.push(_newGameObjects[i]);
			}
			_newGameObjects.length = 0;
		}
		
		internal static function destroyGameObjects():void {
			while (_gameObjectsToDestroy.length > 0) {
				var go:GameObject = _gameObjectsToDestroy.pop();
				var i:int = _gameObjects.indexOf(go);
				if (i >= 0) {
					_gameObjects.splice(i, 1);
				}
			}
		}

		internal static function addGameObject(go:GameObject):void {
			_newGameObjects.push(go);
		}

		internal static function destroyGameObject(go:GameObject):void {
			if (!go._isDestroyed) {
				_gameObjectsToDestroy.push(go);
				go._isDestroyed = true;
			}
		}

		internal static function addShader(s:Shader):void {
			if (_shaderQueue.indexOf(s) < 0) {
				_shaderQueue.push(s);
				_renderQueue[s] = new Vector.<Array>();
			}
			sortShaderQueue();
		}

		internal static function sortShaderQueue():void {
			_shaderQueue.sort(function(a:Shader, b:Shader):Number{
				return a._queue - b._queue;
			});
		}

		internal static function cleanRenderQueue():void {
			for each (var queue:Vector.<Array> in _renderQueue) {
				queue.length = 0;
			}
		}

		internal static function addToRenderQueue(go:GameObject, m:Material, mesh:Mesh, submesh:int):void {
			var queue:Vector.<Array> = _renderQueue[m._shader];
			queue.push([go, m, mesh, submesh]);
		}

		internal static function renderGameObjects():void {
			if (_activeCamera == null) {
				return;
			}

			var camera:Camera = _activeCamera.getComponent(Camera) as Camera;
			camera.clear();

			var viewProjMtx:Matrix3D = camera._viewProjMtx;
			for (var s:Shader in _renderQueue) {
				s.bind();

				var queue:Vector.<Array> = _renderQueue[s];

				for (var i:int = 0; i < queue.length; ++i) {
					var gameObject:GameObject = queue[i][0];
					var material:Material = queue[i][1];
					var mesh:Mesh = queue[i][2];
					var submesh:int = queue[i][3];

					_modelViewProjMtx.copyFrom(gameObject.transform._mtx);
					_modelViewProjMtx.append(viewProjMtx);

					s.setConstants(material, _modelViewProjMtx);
					mesh.bindBuffers();
					mesh.drawSubmesh(submesh);
				}
			}
		}
	}
}

