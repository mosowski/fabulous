package fabulous3d {

	public class GameObject {
		internal var _isDestroyed:Boolean;
		internal var _components:Vector.<Component>;
		internal var _transform:Transform;
		internal var _isEnabled:Boolean;

		public function GameObject() {
			_components = new Vector.<Component>();
			_transform = addComponent(Transform);
			_isEnabled = true;
			F3d.addGameObject(this);
		}

		public function addComponent(t:Class):* {
			var c:Component = new t();
			_components.push(c);
			c._gameObject = this;
			return c;
		}

		public function removeComponent(c:Component):* {
			var i:int = _components.indexOf(c);
			if (i >= 0) {
				_components.splice(i, 1);
				c._gameObject = null;
				return c;
			} else {
				return null;
			}
		}

		public function getComponent(t:Class):* {
			for (var i:int = 0; i < _components.length; ++i) {
				if (_components[i] is t) {
					return _components[i];
				}
			}
			return null;
		}

		internal function update():void {
			if (_isEnabled) {
				for (var i:int = 0; i < _components.length; ++i) {
					_components[i].update();
				}
			}
		}

		internal function lateUpdate():void {
			if (_isEnabled) {
				for (var i:int = 0; i < _components.length; ++i) {
					_components[i].lateUpdate();
				}
			}
		}

		public function get transform():Transform {
			return _transform;
		}

		public function get isEnabled():Boolean {
			return _isEnabled;
		}

		public function set isEnabled(v:Boolean):void {
			_isEnabled = v;
		}

		public static function destroy(go:GameObject):void {
			F3d.destroyGameObject(go);
		}
	}
}
