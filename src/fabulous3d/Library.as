package fabulous3d {

	public class Library {

		internal static var _shaders:Object;
		internal static var _meshes:Object;
		internal static var _materials:Object;

		internal static function init():void {
			_shaders = {};
			_meshes = {};
			_materials = {};

			_shaders["unlitTexture"] = new Shader();
			_shaders["unlitTexture"].setCode(
				[
					"m44 op, va0, vc0", // transform vertex
					"mov v0, va1" // copy texcoords to varying0 register
				].join("\n"),
				[
					"tex oc, v0, fs0 <2d, nomip, repeat, linear#compression#>" // read texture and save color to output
				].join("\n")
			);
		}

		public static function addShader(id:String, s:Shader):Shader{
			_shaders[id] = s;
			return s;
		}

		public static function getShader(id:String):Shader {
			if (id in _shaders) {
				return _shaders[id];
			} else {
				throw new Error("Shader not found: " + id);
			}
		}

		public static function addMesh(id:String, m:Mesh):Mesh {
			_meshes[id] = m;
			m._libraryId = id;
			return m;
		}

		public static function getMesh(id:String):Mesh {
			if (id in _meshes) {
				return _meshes[id];
			} else {
				throw new Error("Mesh not found: " + id);
			}
		}

		public static function addMaterial(id:String, m:Material):Material {
			_materials[id] = m;
			return m;
		}

		public static function getMaterial(id:String):Material {
			if (id in _materials) {
				return _materials[id];
			} else {
				throw new Error("Material not found: " + id);
			}
		}

		internal static function recreate():void {
			for each (var s:Shader in _shaders) {
				s.recreate();
			}
			for each (var m:Mesh in _meshes) {
				m.recreate();
			}
		}
	}
}
