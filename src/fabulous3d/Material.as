package fabulous3d {
	import flash.utils.Dictionary;

	import fabulous.Texture;

	public class Material {
		internal var _shader:Shader;
		internal var _tex0:Texture;

		internal var _params:Object;

		public function Material() {
			_params = {};
		}

		public function get shader():Shader {
			return _shader;
		}

		public function set shader(v:Shader):void {
			_shader = v;
		}	

		public function get tex0():Texture {
			return _tex0;
		}

		public function set tex0(v:Texture):void {
			_tex0 = v;
		}	

		public function get params():Object {
			return _params;
		}
		
		public function recreate():void {
			_shader.recreate();
		}
	}
}
