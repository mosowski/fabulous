package fabulous3d {
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Context3DBufferUsage;

	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.geom.Point;

	public class Mesh {
		private static const DEFAULT_VERTEX_COUNT:int = 10000;
		private static const DEFAULT_INDEX_BUFFER_LENGTH:int = 45000;
		private static const DEFAULT_SUBMESHES_COUNT:int = 2;
		private static const ZERO_INDEX_BUFFER_VECTOR:Vector.<uint> = new Vector.<uint>(DEFAULT_INDEX_BUFFER_LENGTH);
		
		internal var _submeshes:Vector.<Vector.<uint>>;
		internal var _submeshById:Object;
		
		internal var _submeshesLength:Vector.<uint>;
		private var _posLength:int;
		private var _uvLength:int;
		
		internal var _pos:Vector.<Number>;
		internal var _uv:Vector.<Number>;

		private var _maxPosbvLength:int;
		private var _maxUvvbLength:int;
		
		internal var _posvb:VertexBuffer3D;
		internal var _uvvb:VertexBuffer3D;
		internal var _submeshesibs:Vector.<IndexBuffer3D>;
		
		internal var _libraryId:String;
		private var _path:String;

		public function Mesh() {
			_submeshes = new Vector.<Vector.<uint>>();
			_submeshesibs = new Vector.<IndexBuffer3D>(DEFAULT_SUBMESHES_COUNT);
			_submeshesLength = new Vector.<uint>(DEFAULT_SUBMESHES_COUNT);
			_submeshById = {};
			_pos = new Vector.<Number>(3 * DEFAULT_VERTEX_COUNT);
			_uv = new Vector.<Number>(2 * DEFAULT_VERTEX_COUNT);
			createPosVertexBufferIfNeeded(true, DEFAULT_VERTEX_COUNT);
			createUvVertexBufferIfNeeded(true, DEFAULT_VERTEX_COUNT);
			_maxUvvbLength = DEFAULT_VERTEX_COUNT;
			for (var i:int = 0; i < DEFAULT_SUBMESHES_COUNT; ++i) {
				createIndexBufferIfNeeded(true, i, DEFAULT_INDEX_BUFFER_LENGTH);
			}
		}

		public function set vertices(v:Vector.<Vector3D>):void {
			_posLength = v.length * 3;
			if (_posLength > _pos.length) {
				_pos.length = _posLength;
			}
			for (var i:int = 0; i < v.length; ++i) {
				_pos[i*3] = v[i].x;
				_pos[i*3+1] = v[i].y;
				_pos[i*3+2] = v[i].z;
			}
		}

		public function set uv(v:Vector.<Point>):void {
			_uvLength = v.length * 2;
			if (_uvLength > _uv.length) {
				_uv.length = _uvLength;
			}
			for (var i:int = 0; i < v.length; ++i) {
				_uv[i*2] = v[i].x;
				_uv[i*2+1] = v[i].y;
			}
		}

		public function setIndices(v:Vector.<uint>, submesh:int):void {
			_submeshes[submesh] = v;
		}

		public function set submeshCount(v:int):void {
			_submeshes.length = v;
			for (var i:int = 0; i < v; ++i) {
				_submeshes[i] ||= new Vector.<uint>();
			}
		}
		
		public function get path():String {
			return _path;
		}
		
		public function set path(value:String):void {
			_path = value;
		}

		public function getSubmeshById(id:String):int {
			return _submeshById[id];
		}

		public function rebuildBuffers(force:Boolean = false):void {
			createPosVertexBufferIfNeeded(force, _pos.length / 3);
			_posvb.uploadFromVector(_pos, 0, _pos.length / 3);

			createUvVertexBufferIfNeeded(force, _uv.length / 2);
			_uvvb.uploadFromVector(_uv, 0, _uv.length / 2);
			
			if (_submeshesibs.length < _submeshes.length) {
				_submeshesibs.length = _submeshes.length;
			}
			
			for (var i:int = 0; i < _submeshes.length; ++i) {
				createIndexBufferIfNeeded(force, i, Math.max(DEFAULT_INDEX_BUFFER_LENGTH, _submeshes[i].length));
				_submeshesibs[i].uploadFromVector(_submeshes[i], 0, _submeshes[i].length);
			}
		}
		
		private function createPosVertexBufferIfNeeded(force:Boolean, length:int):void {
			if (force || _maxPosbvLength < length) {
				if (_posvb != null) {
					_posvb.dispose();
				}
				_posvb = F3d.ctx.createVertexBuffer(length, 3, Context3DBufferUsage.STATIC_DRAW);
				_maxPosbvLength = length;
			}
		}
		
		private function createUvVertexBufferIfNeeded(force:Boolean, length:int):void {
			if (force || _maxUvvbLength < length) {
				if (_uvvb != null) {
					_uvvb.dispose();
				}
				_uvvb = F3d.ctx.createVertexBuffer(length, 2, Context3DBufferUsage.STATIC_DRAW);
				_maxUvvbLength = length;
			}
		}
		
		private function createIndexBufferIfNeeded(force:Boolean, i:int, length:int):void {
			if (force || _submeshesLength[i] < length) {
				if (_submeshesibs[i] != null) {
					_submeshesibs[i].dispose();
				}
				_submeshesLength[i] = length;
				_submeshesibs[i] = F3d.ctx.createIndexBuffer(length, Context3DBufferUsage.STATIC_DRAW);
				_submeshesibs[i].uploadFromVector(ZERO_INDEX_BUFFER_VECTOR, 0, DEFAULT_INDEX_BUFFER_LENGTH);				
			}
		}

		internal function recreate():void {
			rebuildBuffers(true);
		}

		internal function bindBuffers():void {
			if (_posvb != null) {
				F3d.ctx.setVertexBufferAt(0, _posvb, 0, Context3DVertexBufferFormat.FLOAT_3);
			}
			if (_uvvb != null) {
				F3d.ctx.setVertexBufferAt(1, _uvvb, 0, Context3DVertexBufferFormat.FLOAT_2);
			}
		}

		internal function drawSubmesh(i:int):void {
			F3d.ctx.drawTriangles(_submeshesibs[i], 0, _submeshes[i].length / 3);
		}

		public function destroy():void {
			if (_libraryId != null) {
				delete Library._meshes[_libraryId];
				if (_posvb != null) {
					_posvb.dispose();
					_posvb = null;
				}
				if (_uvvb != null) {
					_uvvb.dispose();
					_uvvb = null;
				}
				_libraryId = null;
			}
		}
	}
}
