package fabulous3d {

	public class MeshFilter extends Component {

		internal var _mesh:Mesh;

		public function MeshFilter() {
			super();
		}

		public function set mesh(v:Mesh):void {
			_mesh = v;
		}
		
		public function get mesh():Mesh {
			return _mesh;
		}
	}

}
