package fabulous3d {

	public class MeshRenderer extends Component {

		internal var _materials:Vector.<Material>;

		public function MeshRenderer() {
			super();
			_materials = new Vector.<Material>();
		}

		public function set materials(v:Vector.<Material>):void {
			_materials = v;
		}

		public function setMaterial(index:int, m:Material):void {
			_materials[index] = m;
		}

		public function set materialCount(v:int):void {
			_materials.length = v;
		}

		override public function update():void {
			var meshFilter:MeshFilter = _gameObject.getComponent(MeshFilter);
			if (meshFilter == null) {
				return;
			}

			var mesh:Mesh = meshFilter._mesh;
			if (mesh == null) {
				return;
			}

			for (var i:int = 0; i < mesh._submeshes.length; ++i) {
				if (i < _materials.length && _materials[i] != null) {
					F3d.addToRenderQueue(_gameObject, _materials[i], mesh, i);
				}
			}
		}
	}
}
