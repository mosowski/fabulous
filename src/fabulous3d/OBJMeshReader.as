package fabulous3d {

	import flash.geom.Vector3D;
	import flash.geom.Point;

	//TODO: add vertex normal support to fabulous

	public class OBJMeshReader {

		public static function load(str:String, mesh:Mesh = null):Mesh {
			var lines:Array = str.split("\n");
			var vArr:Array = [];
			var vtArr:Array = [];
			var currentMtl:String = "";
			var vertexMap:Object = {};
			var submeshMap:Object = {};
			var submeshIds:Array = [];

			mesh ||= new Mesh();
			var vertices:Vector.<Vector3D> = new Vector.<Vector3D>();
			var submeshes:Vector.<Vector.<uint>> = new Vector.<Vector.<uint>>();
			var uvs:Vector.<Point> = new Vector.<Point>();

			// vertex with same pos, but different uv or nrm has to be split into two vertices,
			// so tuple made of attribute indices is a good candidate for an unique vertex identifier
			var getVertexIndex:Function = function(id:String):int {
				if (!(id in vertexMap)) {
					var ind:Array = id.split("/");
					var vi:int = parseInt(ind[0])-1;
					var vert:Vector3D = new Vector3D(
						vArr[vi][0],
						vArr[vi][1],
						vArr[vi][2]
					);
					var uv:Point = new Point(0,0);
					if (ind.length > 0 && ind[1] != "") {
						var vt:int = parseInt(ind[1])-1
						uv.x = vtArr[vt][0];
						uv.y = 1 - vtArr[vt][1];
					}
					var newVertexId:int = vertices.length;
					vertices.push(vert);
					uvs.push(uv);
					vertexMap[id] = newVertexId;
					return newVertexId;
				} else {
					return vertexMap[id];
				}
			};

			for (var i:int = 0; i < lines.length; ++i) {
				if (lines[i].charAt(0) == "#") {
					continue;
				}

				var tokens:Array = lines[i].split(" ");
				for (var j:int = tokens.length - 1; j >= 0; --j) {
					if (tokens[j] == "") {
						tokens.splice(j, 1);
					}
				}
				if (tokens[0] == "v") {
					vArr.push([ 
						parseFloat(tokens[1]), 
						parseFloat(tokens[2]), 
						parseFloat(tokens[3])
					]);
				} else if (tokens[0] == "vt") {
					vtArr.push([
						parseFloat(tokens[1]), 
						parseFloat(tokens[2])
					]);
				} else if (tokens[0] == "f") {
					var submesh:Vector.<uint> = submeshMap[currentMtl];
					if (!(currentMtl in submeshMap)) {
						submeshes.push(submeshMap[currentMtl] = submesh = new Vector.<uint>());
						submeshIds.push(currentMtl);
					}

					submesh.push(getVertexIndex(tokens[1]));
					submesh.push(getVertexIndex(tokens[2]));
					submesh.push(getVertexIndex(tokens[3]));

					if (tokens.length == 5 && (tokens[4] == "" || tokens[4] == "\r")) { //
						tokens.splice(4);
					}
					if (tokens.length > 4) {
						throw new Error("Non-triangle faces are not supported by fabulous.");
					}
				} else if (tokens[0] == "usemtl") {
					currentMtl = tokens[1].split("\r")[0];
				}
			}

			mesh.vertices = vertices;
			mesh.uv = uvs;
			mesh.submeshCount = submeshes.length;
			for (i = 0; i < submeshes.length; ++i) {
				mesh.setIndices(submeshes[i], i);
				mesh._submeshById[submeshIds[i]] = i;
			}
			mesh.rebuildBuffers();
			return mesh;
		}
	}
}
