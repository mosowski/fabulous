package fabulous3d {

	import flash.geom.*;

	public class Quaternion {
        private static var vecZero:Vector3D = new Vector3D();
        private static var matrixComponents:Vector.<Vector3D> = new <Vector3D>[vecZero, vecZero, vecZero];
        private static var axisAngleQuat:Quaternion = new Quaternion();

		public static var identity:Quaternion = new Quaternion();

		internal var _v : Vector3D;

        public function Quaternion(x:Number = 0, y:Number = 0, z:Number = 0, w:Number = 1) {
            _v = new Vector3D(x, y, z, w);
        }

        public function get x():Number {
            return _v.x;
        }

        public function set x(value:Number):void {
            _v.x = value;
        }

        public function get y():Number {
            return _v.y;
        }

        public function set y(value:Number):void {
            _v.y = value;
        }

        public function get z():Number {
            return _v.z;
        }

        public function set z(value:Number):void {
            _v.z = value;
        }

        public function get w():Number {
            return _v.w;
        }

        public function set w(value:Number):void {
            _v.w = value;
        }

        public function multiplyBy(b:Quaternion):void {
            this.setTo(_v.x * b._v.w + _v.w * b._v.x + _v.y * b._v.z - _v.z * b._v.y, _v.y * b._v.w + _v.w * b._v.y + _v.z * b._v.x - _v.x * b._v.z, _v.z * b._v.w + _v.w * b._v.z + _v.x * b._v.y - _v.y * b._v.x, _v.w * b._v.w - _v.x * b._v.x - _v.y * b._v.y - _v.z * b._v.z);
        }

        public function transformVector(vec:Vector3D):void {
            var ix:Number = _v.w * vec.x + _v.y * vec.z - _v.z * vec.y;
            var iy:Number = _v.w * vec.y + _v.z * vec.x - _v.x * vec.z;
            var iz:Number = _v.w * vec.z + _v.x * vec.y - _v.y * vec.x;
            var iw:Number = -_v.x * vec.x - _v.y * vec.y - _v.z * vec.z;

            vec.setTo(ix * _v.w + iw * -_v.x + iy * -_v.z - iz * -_v.y, iy * _v.w + iw * -_v.y + iz * -_v.x - ix * -_v.z, iz * _v.w + iw * -_v.z + ix * -_v.y - iy * -_v.x);
        }

        public function setTo(x:Number = 0, y:Number = 0, z:Number = 0, w:Number = 1):void {
            _v.x = x;
            _v.y = y;
            _v.z = z;
            _v.w = w;
        }

        public function copyFrom(q:Quaternion):void {
            _v.x = q.x;
            _v.y = q.y;
            _v.z = q.z;
            _v.w = q.w;
        }

        public function identity():void {
            _v.x = _v.y = _v.z = 0;
            _v.w = 1;
        }

        public function clone():Quaternion {
            return new Quaternion(_v.x, _v.y, _v.z, _v.w);
        }

        public function normalize():void {
            var len:Number = Math.sqrt(_v.x * _v.x + _v.y * _v.y + _v.z * _v.z + _v.w * _v.w);
            _v.x /= len;
            _v.y /= len;
            _v.z /= len;
            _v.w /= len;
        }

        public function toMatrix3D(mtx:Matrix3D):void {
            Quaternion.matrixComponents[1] = _v;
            mtx.recompose(Quaternion.matrixComponents, Orientation3D.QUATERNION);
        }

        public function setFromAxisAngle(axis:Vector3D, angle:Number):void {
            var sinAngle:Number = Math.sin(angle * 0.5);
            _v.x = sinAngle * axis.x;
            _v.y = sinAngle * axis.y;
            _v.z = sinAngle * axis.z;
            _v.w = Math.cos(angle * 0.5);
        }

        public function toAngleAxis(v:Vector3D):void {
            if (1.0 - _v.w * _v.w != 0) {
                v.setTo(_v.x / Math.sqrt(1.0 - _v.w * _v.w), _v.y / Math.sqrt(1.0 - _v.w * _v.w), _v.z / Math.sqrt(1.0 - _v.w * _v.w));
                v.w = 2.0 * Math.acos(_v.w);
            } else {
                v.setTo(1, 0, 0);
                v.w = 0;
            }
        }

        public function multiplyByAngleAxis(axis:Vector3D, angle:Number):void {
            axisAngleQuat.setFromAxisAngle(axis, angle);
            multiplyBy(axisAngleQuat);
        }

		public static function fromAxisAngle(axis:Vector3D, angle:Number):Quaternion {
			var q:Quaternion = new Quaternion();
			q.setFromAxisAngle(axis, angle);
			return q;
		}

        public static function fromToRotation(dest:Quaternion, a:Vector3D, b:Vector3D):void {
            var d:Number = a.dotProduct(b);
            var axis:Vector3D = new Vector3D();
            if (d >= 1.0) {
                dest.setTo(0.0, 0.0, 0.0, 1.0);
            } else if (d < (0.000001 - 1.0)) {
                axis = Vector3D.X_AXIS.crossProduct(a);
                if (axis.length < 0.000001)
                    axis = Vector3D.Y_AXIS.crossProduct(a);
                if (axis.length < 0.000001)
                    axis = Vector3D.Z_AXIS.crossProduct(a);
                axis.normalize();
                dest.setFromAxisAngle(axis, Math.PI);
            } else {
                var s:Number = Math.sqrt((1.0 + d) * 2.0);
                var sInv:Number = 1.0 / s;
                axis = a.crossProduct(b);
                dest.setTo(axis.x * sInv, axis.y * sInv, axis.z * sInv, s * 0.5);
            }
            dest.normalize();
        }
	}
}
