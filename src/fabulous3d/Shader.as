package fabulous3d {
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DCompareMode;	 
	import flash.display3D.Context3DBlendFactor;	 
	import flash.display3D.Program3D;	 

	import flash.geom.Matrix3D;

	import fabulous.AGALMiniAssembler;
	import fabulous.Log;

	public class Shader {
		public static var TRANSPARENT_QUEUE:int = 1000;
		public static var OPAQUE_QUEUE:int = 0;
		internal static var COMPRESSION_STR:Array = [ "", ",dxt1", ",dxt5" ];

		internal var _queue:int;
		internal var _programByCompression:Object;

		internal var _vertSrc:String;
		internal var _fragSrc:String;

		internal var _depthMask:Boolean;
		internal var _depthCompare:String;

		internal var _sourceBlend:String;
		internal var _destBlend:String;

		internal var _params:Object;

		public function Shader() {
			_queue = 0;
			_depthMask = true;
			_depthCompare = Context3DCompareMode.LESS;
			_sourceBlend = Context3DBlendFactor.ONE;
			_destBlend = Context3DBlendFactor.ZERO;
			_programByCompression = {};
			_params = {};
			F3d.addShader(this);
		}

		public function addParam(id:String, type:String, firstRegister:int):void {
			_params[id] = { 
				type: type,
				firstRegister: firstRegister
			};
		}

		public function setCode(vertSrc:String, fragSrc:String):void {
			_vertSrc = vertSrc;
			_fragSrc = fragSrc; 
		}

		public function getProgram(compression:int = 0):Program3D {
			if (compression in _programByCompression) {
				return _programByCompression[compression];
			} else {
				var program:Program3D = F3d.ctx.createProgram();
				var vertexAgal:AGALMiniAssembler = new AGALMiniAssembler();
				vertexAgal.assemble(Context3DProgramType.VERTEX, _vertSrc);
				var fragmentAgal:AGALMiniAssembler = new AGALMiniAssembler();
				var fragSrc:String = _fragSrc.replace(/#compression#/gi, COMPRESSION_STR[compression]);
				fragmentAgal.assemble(Context3DProgramType.FRAGMENT, fragSrc);
				_programByCompression[compression] = program;

				try {
					program.upload(vertexAgal.agalcode, fragmentAgal.agalcode);
					Log.i("[F3d::Shader] Compiled for format " + compression + ":\n" + fragSrc);
				} catch (er:Error) {
					Log.e("Shader contains errors.");
					Log.e(er.message);
					Log.i(_vertSrc);
					Log.i(_fragSrc);
				}
				return program;
			}
		}

		internal function recreate():void {
			setCode(_vertSrc, _fragSrc);
			_programByCompression = {};
		}

		public function set queue(v:int):void {
			if (_queue != v) {
				_queue = v;
				F3d.addShader(this);
			}
		}

		public function get queue():int {
			return _queue;
		}

		public function set sourceBlend(v:String):void {
			_sourceBlend = v;
		}

		public function get sourceBlend():String {
			return _sourceBlend;
		}

		public function set destBlend(v:String):void {
			_destBlend = v;
		}

		public function get destBlend():String {
			return _destBlend;
		}

		public function set depthMask(v:Boolean):void {
			_depthMask = v;
		}

		public function get depthMask():Boolean {
			return _depthMask;
		}

		public function set depthCompare(v:String):void {
			_depthCompare = v;
		}

		public function get depthCompare():String {
			return _depthCompare;
		}

		internal function bind():void {
			F3d.ctx.setDepthTest(_depthMask, _depthCompare);
			F3d.ctx.setBlendFactors(_sourceBlend, _destBlend);
		}

		internal function setConstants(m:Material, mvp:Matrix3D):void {
			// Ok. This just gets plainly stupid, because the effort made to 
			// group render calls by used program is wasted when mixing
			// compressed and uncompressed textures in materials of
			// the same shader. But I will leave it as it is, just to 
			// keep things simple. Adobe guys had three years to learn
			// the OpenGL and do this stuff right.
			if (m._tex0 != null) {
			   	if (m._tex0.isLoaded) {
					F3d.ctx.setTextureAt(0, m._tex0.stage3dTexture);
					F3d.ctx.setProgram(getProgram(m._tex0.compression));
				} else {
					F3d.ctx.setTextureAt(0, F3d.fallbackTexture);
					F3d.ctx.setProgram(getProgram());
				}
			} else {
				F3d.ctx.setTextureAt(0, null);
				F3d.ctx.setProgram(getProgram());
			}

			//TODO: Move texture parameters to _params too. Maybe global consts too?
			for (var paramId:String in _params) {
				if (!(paramId in m._params)) {
					throw new Error("Undefined shader parameter: " + paramId);
				} 
				F3d.ctx.setProgramConstantsFromVector(_params[paramId].type, _params[paramId].firstRegister, m._params[paramId]);
			}

			F3d.ctx.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX, 0, mvp, true);
		}
	}
}
