package fabulous3d {
	import fabulous.F;

	public class StopMotionAnimator extends Component {
		internal var _meshes:Vector.<Mesh>;
		internal var _times:Vector.<Number>;
		internal var _timer:Number;
		internal var _duration:Number;

		public function StopMotionAnimator() {
			super();
			_meshes = new Vector.<Mesh>();
			_times = new Vector.<Number>();
			_timer = 0;
			_duration = 0;
		}

		public function set numFrames(v:int):void {
			_meshes.length = v;
			_times.length = v;
		}

		public function get numFrames():int {
			return _times.length;
		}

		public function setFrame(i:int, t:Number, m:Mesh):void {
			_meshes[i] = m;
			_times[i] = t;
			findDuration();
		}

		internal function findDuration():void {
			_duration = 0;
			for (var i:int = 0; i < _times.length; ++i) {
				_duration += _times[i];
			}
		}

		override public function update():void {
			if (_duration == 0 || _times.length == 0) {
				return;
			}

			var filter:MeshFilter = _gameObject.getComponent(MeshFilter);
			if (filter == null) {
				return;
			}

			var modTimer:Number = _timer % _duration;

			for (var i:int = 0; i < _times.length; ++i) {
				if (modTimer < _times[i]) {
					filter.mesh = _meshes[i];
					break;
				} else {
					modTimer -= _times[i];
				}
			}

			_timer += F.dt;
		}
	}
}
