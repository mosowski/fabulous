package fabulous3d {

	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.geom.Rectangle;

	public class Transform extends Component {
		internal var _localPosition:Vector3D;
		internal var _localRotation:Quaternion;
		internal var _localScale:Vector3D;

		internal var _position:Vector3D;
		internal var _rotation:Quaternion;
		internal var _scale:Vector3D;

		internal var _mtx:Matrix3D;
		internal var _rotationAxis:Vector3D;

		internal var _parent:Transform;
		internal var _children:Vector.<Transform>;

		internal var _isDirty:Boolean;
		internal var _wasChanged:Boolean;

		public function Transform() {
			super();
			_localPosition = new Vector3D();
			_localRotation = new Quaternion();
			_localScale = new Vector3D(1,1,1);
			_position = new Vector3D();
			_rotation = new Quaternion();
			_scale = new Vector3D();
			_children = new Vector.<Transform>();
			_mtx = new Matrix3D();
			_rotationAxis = new Vector3D();
			_parent = null;
			_isDirty = true;
		}

		override public function update():void {
			_wasChanged = false;
			if (_isDirty) {
				if (_parent != null) {
					_position.copyFrom(_localPosition);
					_position.x *= _parent._scale.x;
					_position.y *= _parent._scale.y;
					_position.z *= _parent._scale.z;
					_parent._rotation.transformVector(_position);
					_position.incrementBy(_parent._position);

					_rotation.copyFrom(_localRotation);
					_rotation.multiplyBy(_parent._rotation);

					_scale.copyFrom(_parent._scale);
					_scale.x *= _localScale.x;
					_scale.y *= _localScale.y;
					_scale.z *= _localScale.z;
				} else {
					_position.copyFrom(_localPosition);
					_rotation.copyFrom(_localRotation);
					_scale.copyFrom(_localScale);
				}
				_isDirty = false;
				_wasChanged = true;

				_rotation.toAngleAxis(_rotationAxis);

				_mtx.identity();
				_mtx.appendScale(_scale.x, _scale.y, _scale.z);
			    _mtx.appendRotation(_rotationAxis.w * (180.0 / Math.PI), _rotationAxis);
				_mtx.appendTranslation(_position.x, _position.y, _position.z);

				for (var i:int = 0; i < _children.length; ++i) {
					_children[i]._isDirty = true;
				}
			}
			for (i = 0; i < _children.length; ++i) {
				_children[i]._gameObject.update();
			}
		}

		public function get localPosition():Vector3D {
			return _localPosition;
		}

		public function set localPosition(v:Vector3D):void {
			_localPosition = v;
			_isDirty = true;
		}

		public function get localRotation():Quaternion {
			return _localRotation;
		}

		public function set localRotation(v:Quaternion):void {
			_localRotation= v;
			_isDirty = true;
		}

		public function get localScale():Vector3D {
			return _localScale;
		}

		public function set localScale(v:Vector3D):void {
			_localScale = v;
			_isDirty = true;
		}

		public function rotateBy(q:Quaternion):void {
			_localRotation.multiplyBy(q);
			_isDirty = true;
		}

		public function translate(x:Number, y:Number, z:Number):void {
			_localPosition.x += x;
			_localPosition.y += y;
			_localPosition.z += z;
			_isDirty = true;
		}

		public function translateBy(v:Vector3D):void {
			_localPosition.incrementBy(v);
			_isDirty = true;
		}

		public function touch():void {
			_isDirty = true;
		}
	}
}
