rm -f asseter.jar
mkdir bin
javac -d bin -cp ./src:./libs/dom4j-1.6.1.jar:./libs/json-simple-1.1.1.jar  ./src/asseter/Main.java
cd ./bin
jar -xf ../libs/dom4j-1.6.1.jar
jar -xf ../libs/json-simple-1.1.1.jar
cd ..
jar -cfm asseter.jar asseter.mf  -C ./bin/ .
rm -rf bin
echo "Done."
