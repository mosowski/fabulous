package asseter;

import java.util.Enumeration;
import java.util.Hashtable;

import org.json.simple.JSONObject;


public class AssetLib {
	private Hashtable<String,Hashtable<String, String>> elements;
	
	public AssetLib() {
		elements = new Hashtable<String,Hashtable<String, String>>();
	}
	
	public void addAsset(String name, String profile, String path) {
		if (!elements.containsKey(name)) {
			elements.put(name, new Hashtable<String, String>());
		}
		Hashtable<String, String> element = elements.get(name);
		element.put(profile, path);
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getJSON() {
		JSONObject json = new JSONObject();
		Enumeration<String> it = elements.keys();
		while (it.hasMoreElements()) {
			String assetName = it.nextElement();
			JSONObject assetJson = new JSONObject();
			Hashtable<String, String> element = elements.get(assetName);
			Enumeration<String> jt = element.keys();
			while (jt.hasMoreElements()) {
				String profileName = jt.nextElement();
				assetJson.put(profileName, element.get(profileName));
			}
			json.put(assetName, assetJson);
		}
		return json;
	}
		
}
