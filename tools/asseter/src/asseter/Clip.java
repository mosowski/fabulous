package asseter;

import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.dom4j.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Clip {
	private String name;
	private String clipDir;
	private boolean boundsGiven;
	private Rect bounds;
	private Hashtable<String,Sprite> images;
	private SpriteSheet sheet; 
	private Vector<ClipAnimation> animations;
	
	public Clip(SpriteSheet sheet, String name, String clipDir) {
		this.name = name;
		this.sheet = sheet;
		this.clipDir = clipDir;
		
		boundsGiven = false;
		bounds = new Rect(Float.NaN, Float.NaN, Float.NaN, Float.NaN);

		images = new Hashtable<String, Sprite>();
		animations = new Vector<ClipAnimation>();
	}
	
	@SuppressWarnings("unchecked")
	public void fromXML(Element clipXml) {
		Element imagesXml = clipXml.element("images");
		for (Element imageXml : (List<Element>) imagesXml.elements("image")) {
			String imageId = imageXml.attributeValue("id");
			String imageFile = imageXml.attributeValue("path");
			BufferedImage img = G.readFileAsImage(G.srcDir +"/" + clipDir+  imageFile, G.pageWidth, G.pageHeight);
			String spriteName = imageId + "@" + name;
			Sprite sprite = new Sprite(spriteName, img, 0, 0);			
			images.put(imageId, sprite);
		}
		
		Element animationsXml = clipXml.element("animations");
		for (Element animationXml : (List<Element>) animationsXml.elements("animation")) {
			//int numFrames = Integer.parseInt(animationXml.attributeValue("frames"));
			String animationName = animationXml.attributeValue("name");
			ClipAnimation animation = new ClipAnimation(this, animationName);
			animation.fromXML(animationXml);
			animations.add(animation);
		}
	}
	
	public void processData() {
		for (ClipAnimation animation : animations) {
			animation.processData();
		}
	}
	
	public Sprite getImage(String id) {
		return images.get(id);
	}
	
	public String getName() {
		return name;
	}
		
	public void addImagesGroup(Vector<Sprite> sprites) {
		sheet.addSprites(sprites);
	}
	
	public void expandBounds(float x, float y) {
		if (!boundsGiven) {
			bounds.left = x;
			bounds.top = y;
			bounds.right = x;
			bounds.bottom = y;
			boundsGiven = true;
		}
		if (x < bounds.left) {
			bounds.left = x;
		}
		if (y < bounds.top) {
			bounds.top = y;
		}
		if (x > bounds.right) {
			bounds.right = x;
		}
		if (y > bounds.bottom) {
			bounds.bottom = y;
		}
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getJSON() {
		JSONObject json = new JSONObject();
		
		json.put("name", name);
		
		JSONArray boundsJson = new JSONArray();
		boundsJson.add(bounds.left);
		boundsJson.add(bounds.top);
		boundsJson.add(bounds.right);
		boundsJson.add(bounds.bottom);
		
		json.put("bounds", boundsJson);
		
		JSONObject animationsJson = new JSONObject();
		for (ClipAnimation animation : animations) {
			animationsJson.put(animation.getName(), animation.getJSON());
		}
		json.put("animations", animationsJson);
		return json;
	}
	
}
