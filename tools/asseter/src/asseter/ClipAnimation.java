package asseter;

import java.util.List;
import java.util.Vector;

import org.dom4j.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ClipAnimation {	
	private Clip clip;
	private String name;
	private Vector<ClipFrame> frames;
	
	public ClipAnimation(Clip clip, String name) {
		this.name = name;
		this.clip = clip;
		frames = new Vector<ClipFrame>();
	}
	
	@SuppressWarnings("unchecked")
	public void fromXML(Element animationXml) {
		for (Element frameXml : (List<Element>) animationXml.elements("frame")) {
			ClipFrame frame = new ClipFrame(clip);
			frame.fromXML(frameXml);
			frames.add(frame);
		}
	}
	
	public void processData() {
		for (ClipFrame frame : frames) {
			frame.createBuffersData();
		}
	}
	
	public String getName() {
		return name;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getJSON() {
		JSONObject animationJson = new JSONObject();
		
		
		JSONArray framesJson = new JSONArray();
		for (ClipFrame frame : frames) {
			framesJson.add(frame.getJSON());
		}
		
		animationJson.put("frames", framesJson);
		
		//TODO
		JSONArray labelsJson = new JSONArray();
		animationJson.put("labels", labelsJson);
		
		//TODO
		JSONArray metaNodes = new JSONArray();
		animationJson.put("metaNodes", metaNodes);
		
		//TODO
		JSONObject hitmaskJson = new JSONObject();
		JSONArray hitmaskPathJson = new JSONArray();
		JSONArray hitmaskIndicesJson = new JSONArray();
		hitmaskJson.put("path", hitmaskPathJson);
		hitmaskJson.put("indices", hitmaskIndicesJson);
		
		hitmaskJson.put("hitmask", hitmaskJson);
		
		return animationJson;
	}
}
