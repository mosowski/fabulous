package asseter;

import java.util.List;
import java.util.Vector;

import org.dom4j.Element;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ClipFrame {
	private Clip clip;
	private Vector<Integer> indexData;
	private Vector<Float> vertexData;
	private Vector<Sprite> vertexImage;
	private Element xml;
	private Page page;
	
	public ClipFrame(Clip clip) {
		this.clip = clip;
		indexData = new Vector<Integer>();
		vertexData = new Vector<Float>();
		vertexImage = new Vector<Sprite>();
	}
	
	// Remark: creating frames has to be split into two parts,
	// because at the moment of parsing geometry we don't know
	// sprite location in sprite sheets, so we are unable to 
	// calculate valid texture coordinates. asseter.Sprite sheet is
	// being flushed after collecting image groups.
	@SuppressWarnings("unchecked")
	public void fromXML(Element frameXml) {
		xml = frameXml;
		Vector<Sprite> imagesGroup = new Vector<Sprite>();
		
		int numVertices = Integer.parseInt(xml.attributeValue("vertices"));
		vertexImage.setSize(numVertices);
		
		for (Element geomXml : (List<Element>) xml.elements("geom")) {
			String imageId = geomXml.attributeValue("image");
			Sprite image = clip.getImage(imageId);
			imagesGroup.add(image);
			
			int numIndices = Integer.parseInt(geomXml.attributeValue("indices"));
			String indexText = geomXml.getText().trim();
			String[] indexNumbers = indexText.split(" ");
			for (int i = 0; i < numIndices; ++i) {
				int ind = Integer.parseInt(indexNumbers[i]);
				vertexImage.set(ind, image);
				indexData.add(ind);
			}
		}
		
		clip.addImagesGroup(imagesGroup);
		
	}
	
	@SuppressWarnings("unchecked")
	public void createBuffersData() {
		int numVertices = Integer.parseInt(xml.attributeValue("vertices"));
		String vertexText = xml.elementText("vertex").trim();
		String[] vertexNumbers = vertexText.split(" ");
		
		for (int i = 0; i < numVertices*5; i+=5) {
			float x = Float.parseFloat(vertexNumbers[i]);
			float y = Float.parseFloat(vertexNumbers[i+1]);
			clip.expandBounds(x, y);
			vertexData.add(x);
			vertexData.add(y);
			
			float u = Float.parseFloat(vertexNumbers[i+2]);
			float v = Float.parseFloat(vertexNumbers[i+3]);
			Sprite image = vertexImage.get(i/5);
			u = (image.getUVLeft() + u * image.getUVWidth()) / image.getPage().getWidth();
			v = (image.getUVTop() + v * image.getUVHeight()) / image.getPage().getHeight();
			vertexData.add(u);
			vertexData.add(v);
			vertexData.add(new Float(1.0));				
		}		
		
		for (Element geomXml : (List<Element>) xml.elements("geom")) {
			String imageId = geomXml.attributeValue("image");
			Sprite image = clip.getImage(imageId);
			
			assert(page == null || page == image.getPage());
			assert(image.getPage() != null);
			page = image.getPage();
			assert(image.getPage() != null);
		}
	}
	
	@SuppressWarnings("unchecked")
	public void addIndexDataToJSON(JSONArray json) {
		json.addAll(indexData);
	}
	
	public int getNumIndices() {
		return indexData.size();
	}
	
	public Page getPage() {
		return page;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getJSON() {
		JSONObject json = new JSONObject();
		
		json.put("texture", page.getFilePath());
		
		JSONArray indexJson = new JSONArray();
		for (int i : indexData) {
			indexJson.add(i);
		}
		json.put("indices", indexJson);
		
		JSONArray vertexDataJson = new JSONArray();
		for (Float f : vertexData) {
			vertexDataJson.add(f);
		}
		json.put("vertices", vertexDataJson);
		
		return json;
	}
		
}
