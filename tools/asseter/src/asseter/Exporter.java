package asseter;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Vector;
import java.util.Arrays;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;



public class Exporter {
	private AssetLib textureLib;
	private AssetLib spriteLib;
	private AssetLib clipLib;
	

	public Exporter(Element rootXml) {
		textureLib = new AssetLib();
		spriteLib = new AssetLib();
		clipLib = new AssetLib();
		
		processClips(rootXml.element("clips"));
		processSprites(rootXml.element("sprites"));
				
		G.writeStringToFile(spriteLib.getJSON().toJSONString(), G.outDir +"/spriteLib.json");
		G.writeStringToFile(clipLib.getJSON().toJSONString(), G.outDir +"/clipLib.json");
	}
	
	
	@SuppressWarnings("unchecked")
	public void processClips(Element clipsXml) { 
		for (Element groupXml : (List<Element>) clipsXml.elements("group")) {
			String groupName = groupXml.attributeValue("name");
			
			for (Element profileXml : (List<Element>) groupXml.elements("profile")) {
				String profileName = profileXml.attributeValue("name");
				//String postProc
				String pageWidthStr = profileXml.attributeValue("pageWidth");
				String pageHeightStr = profileXml.attributeValue("pageHeight");
				int pageWidth = G.pageWidth;
				int pageHeight = G.pageHeight;
				if (pageWidthStr != null) {
					pageWidth = Integer.parseInt(pageWidthStr);
				}
				if (pageHeightStr != null) {
					pageHeight = Integer.parseInt(pageHeightStr);
				}
				
				String profileDir = "clips/" + groupName + "/" + profileName;
				G.createDirectory(G.outDir + "/" + profileDir);
				
				SpriteSheet sheet = new SpriteSheet(profileDir, profileName, pageWidth, pageHeight);
				Vector<Clip> clips = new Vector<Clip>();
				for (Element clipXml : (List<Element>) profileXml.elements("clip")) {
					String clipName = clipXml.attributeValue("name");
					String clipFile = clipXml.attributeValue("file");
					
					String clipFileText = G.readFileAsString(G.srcDir + "/" + clipFile);
					try {
						Document clipXmlDoc = DocumentHelper.parseText(clipFileText);
						String clipDir = "";
						if (clipFile.indexOf("/") != -1) {
							clipDir = clipFile.substring(0, clipFile.indexOf("/"))+"/";
						}
						Clip clip = new Clip(sheet, clipName, clipDir);
						clip.fromXML(clipXmlDoc.getRootElement());
						clips.add(clip);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
				sheet.flush();
				
				for (Clip clip : clips) {
					clip.processData();
					String clipJsonPath = profileDir + "/" + clip.getName() + ".json";
					G.writeStringToFile(clip.getJSON().toJSONString(), G.outDir + "/" + profileDir +"/" + clip.getName() + ".json");
					clipLib.addAsset(clip.getName(), profileName, clipJsonPath);
				}
				
				sheet.savePagesToDir(G.outDir);
				
				sheet.addPagesToLib(textureLib);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void processSprites(Element spritesXml) {					
		for (Element groupXml : (List<Element>) spritesXml.elements("group")) {			
			String groupName = groupXml.attributeValue("name");
			
			for (Element profileXml : (List<Element>) groupXml.elements("profile")) {
				String[] profilesNames = profileXml.attributeValue("name").split(",");
				String postProcName = profileXml.attributeValue("postproc");
				String pageWidthStr = profileXml.attributeValue("pageWidth");
				String pageHeightStr = profileXml.attributeValue("pageHeight");
				int pageWidth = G.pageWidth;
				int pageHeight = G.pageHeight;
				if (pageWidthStr != null) {
					pageWidth = Integer.parseInt(pageWidthStr);
				}
				if (pageHeightStr != null) {
					pageHeight = Integer.parseInt(pageHeightStr);
				}

				System.out.println("Generating profiles: " + Arrays.toString(profilesNames));

				String profileName = profilesNames[0];

				String profileDir = "sprites/" + groupName + "/" + profileName;
				G.createDirectory(G.outDir + "/" + profileDir);
				
				SpriteSheet sheet = new SpriteSheet(profileDir, profileName, pageWidth, pageHeight);
											
				for (Element spriteXml : (List<Element>) profileXml.elements("sprite")) {
					String spriteName = spriteXml.attributeValue("name");
					String spriteFile = spriteXml.attributeValue("file");
					String pivotXStr = spriteXml.attributeValue("pivotX");
					String pivotYStr = spriteXml.attributeValue("pivotY");
					BufferedImage img = G.readFileAsImage(G.srcDir + "/" + spriteFile, pageWidth, pageHeight);
					img = Postprocess.postProc(postProcName, img);
					float pivotX = 0;
					float pivotY = 0;
					if (pivotXStr != null) {
						pivotX = Float.parseFloat(pivotXStr);
					}
					if (pivotYStr != null) {
						pivotY = Float.parseFloat(pivotYStr);
					}
					sheet.addSprite(spriteName, img, pivotX, pivotY);
				}
				
				sheet.flush();
				
				sheet.savePagesToDir(G.outDir);
				G.writeStringToFile(sheet.getJSON().toJSONString(), G.outDir + "/" + profileDir + "/desc.json");
				for (String subProfileName : profilesNames) {
					spriteLib.addAsset(groupName, subProfileName, profileDir +"/desc.json");
				}
				
				sheet.addPagesToLib(textureLib);
			}			
		}
	}
}
