package asseter;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import javax.imageio.ImageIO;

import org.dom4j.Document;


public class G {	

	public static Document configXml;
	
	public static int pageWidth;
	public static int pageHeight;
	
	public static String outDir;
	public static String srcDir;
		
	public static List<SpriteSheet> spriteSheets;
	
	public static String readFileAsString(String path) {
		try {
	        StringBuffer fileData = new StringBuffer(1000);
	        BufferedReader reader = new BufferedReader(new FileReader(path));
	        char[] buf = new char[1024];
	        int numRead=0;
	        while((numRead=reader.read(buf)) != -1){
	            String readData = String.valueOf(buf, 0, numRead);
	            fileData.append(readData);
	            buf = new char[1024];
	        }
	        reader.close();
	        return fileData.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	public static void writeStringToFile(String string, String path) {
		try {
			FileWriter writer = new FileWriter(path);
			writer.write(string);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static BufferedImage readFileAsImage(String path, int pageWidth, int pageHeight) {
		try {
			BufferedImage img = ImageIO.read(new File(path));
			if (img.getWidth() > pageWidth - 8 || img.getHeight() > pageHeight - 8) {
				System.out.println("! Warning: Image " + path + " was too big. Resized it to fit page size.");
				float scale = Math.min((float)(pageWidth - 8)/img.getWidth(), (float)(pageHeight - 8)/img.getHeight());
				return Postprocess.postProcResize(img, scale);
				/*
				BufferedImage scaledImg = new BufferedImage(Math.min(asseter.G.pageWidth-8, img.getWidth()), Math.min(asseter.G.pageHeight-8, img.getHeight()), img.getType());
				Graphics2D g = (Graphics2D)scaledImg.getGraphics();
				g.drawImage(img, 0, 0, scaledImg.getWidth(), scaledImg.getHeight(), null);
				g.dispose();
				g.setComposite(AlphaComposite.Src);
				g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
				g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);	
				return scaledImg;
				*/
			}
			return img;
		} catch (Exception e) {
			System.out.println("!!! Error loading image file " + path);
			e.printStackTrace();
			return null;
		}
	}
	
	public static void createDirectory(String path) {
		File file = new File(path);
		file.mkdirs();
	}
				
}
