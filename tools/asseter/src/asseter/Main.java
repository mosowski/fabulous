package asseter;

import org.dom4j.DocumentHelper;

public class Main {

    public static void main(String[] args) {
        if (args.length != 4) {
            System.out.println("Usage: exporter <config> <srcdir> <outdir> <page_size>");
        } else {
            try {
                G.configXml = DocumentHelper.parseText(G.readFileAsString(args[0]));
                G.srcDir = args[1];
                G.outDir = args[2];
                G.pageWidth = G.pageHeight = Integer.parseInt(args[3]);

                new Exporter(G.configXml.getRootElement());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
