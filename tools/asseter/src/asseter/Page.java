package asseter;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Hashtable;
import java.util.Vector;

import javax.imageio.ImageIO;

import org.json.simple.JSONObject;


public class Page {
	private PageNode node;
	private Hashtable<String, Sprite> sprites;
	private String filePath;
	
	public Page(int pageWidth, int pageHeight) {
		node = new PageNode(null, 0, 0, pageWidth, pageHeight);
		sprites = new Hashtable<String, Sprite>();
	}
	
	public boolean insertSprite(Sprite spr) {
		if (node.insert(spr) != null) {
			sprites.put(spr.getName(), spr);
			spr.setPage(this);
			return true;
		} else {
			return false;
		}
	}
	
	public void insertSprites(Vector<Sprite> sprites) {
		for (Sprite spr : sprites) {
			insertSprite(spr);
		}
	}
	
	public boolean canInsertSprites(Vector<Sprite> sprites) {		
		for (Sprite spr : sprites) {
			if (!node.canInsert(spr)) {
				return false;
			}
		}
		return true;
	}
		
	
	public void setFilePath(String path) {
		filePath = path;
	}
	
	public String getFilePath() {
		return filePath;
	}	
	
	public int getWidth() {
		return node.getWidth();
	}
	
	public int getHeight() {
		return node.getHeight();
	}
	
	public void saveToFile(String dir) {
		try {
			File outFile = new File(dir + "/" + filePath);
			BufferedImage image = node.generateImage();
			ImageIO.write(image, "png", outFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void addSpritesToJSON(JSONObject object) {
		for (Sprite sprite : sprites.values()) {
			JSONObject spriteJson = new JSONObject();
			spriteJson.put("left", sprite.getUVLeft());
			spriteJson.put("top", sprite.getUVTop());
			spriteJson.put("right", sprite.getUVRight());
			spriteJson.put("bottom", sprite.getUVBottom());
			
			spriteJson.put("texture", filePath);
			
			spriteJson.put("width", sprite.getWidth());
			spriteJson.put("height", sprite.getHeight());
			
			spriteJson.put("pivotX", sprite.getPivotX());
			spriteJson.put("pivotY", sprite.getPivotY());
			
			object.put(sprite.getName(), spriteJson);
		}
	}
	
}
