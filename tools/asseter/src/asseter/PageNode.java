package asseter;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Hashtable;


public class PageNode {
	private int pad;
	
	private int x;
	private int y;
	private int width;
	private int height;
	
	private PageNode left;
	private PageNode right;
	private Sprite sprite;
	
	private Hashtable<Integer, PageNode> hashedNodes;
	
	public PageNode(PageNode parent, int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		pad = 1;
		
		if (parent == null) {
			hashedNodes = new Hashtable<Integer, PageNode>();
		} else {
			hashedNodes = parent.hashedNodes;
		}
	}
	
	public PageNode insert(Sprite spr) {
		if (hashedNodes.containsKey(spr.getHash())) {
			return hashedNodes.get(spr.getHash());
		}
		
		if (sprite == null) {
			int w = spr.getWidth() + 2*pad;
			int h = spr.getHeight() + 2*pad;
			if (w <= width && h <= height) {
				sprite = spr;				
				hashedNodes.put(spr.getHash(), this);
				sprite.setUV(x+pad, y+pad,x+pad+sprite.getWidth(), y+pad+sprite.getHeight());
				if (w < width) {
					left = new PageNode(this, x + w, y, width - w, h);
				}
				if (h < height) {
					right = new PageNode(this, x, y + h, width, height - h);
				}
				return this;
			} else {
				return null;
			}
		} else {
			PageNode result = null;
			if (left != null) {
				result = left.insert(spr);
			}
			if (result != null) {
				return result;
			} else if (right != null) {
				result = right.insert(spr);
			} 
			return result;
		}
	}	
	
	public boolean canInsert(Sprite spr) {
		if (hashedNodes.containsKey(spr.getHash())) {
			return true;
		}
		if (sprite == null) {
			int w = spr.getWidth() + 2*pad;
			int h = spr.getHeight() + 2*pad;
			return w <= width && h <= height;
		} else {
			return (left != null && left.canInsert(spr)) || (right != null && right.canInsert(spr));
		}
	}
	
	public Sprite getSprite() {
		return sprite;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public BufferedImage generateImage() {
		System.out.println("Generating page...");
		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
				
		writeSpriteToGraphics(result.createGraphics());
		copyEdgePixels(result);
		
		return result;
	}
	
	private void writeSpriteToGraphics(Graphics2D g2d) {
		if (sprite != null) {
			g2d.drawImage(sprite.getImage(), x + pad, y + pad, null);
		}
		if (left != null) {
			left.writeSpriteToGraphics(g2d);
		}
		if (right != null) {
			right.writeSpriteToGraphics(g2d);
		}
	}
	
	private void copyEdgePixels(BufferedImage image) {
		if (sprite != null) {
			for (int i = (int) sprite.getUVLeft() - 1; i < sprite.getUVRight() + 1; ++i) {
				image.setRGB(i, (int) sprite.getUVTop() - 1, image.getRGB(Math.min(Math.max(i, (int) sprite.getUVLeft()), (int) sprite.getUVRight() - 1), (int) sprite.getUVTop()));
				image.setRGB(i, (int) sprite.getUVBottom(), image.getRGB(Math.min(Math.max(i, (int) sprite.getUVLeft()), (int) sprite.getUVRight() - 1), (int) sprite.getUVBottom() - 1));
			}

			for (int i = (int) sprite.getUVTop(); i < sprite.getUVBottom(); ++i) {
				image.setRGB((int) sprite.getUVLeft() - 1, i, image.getRGB((int) sprite.getUVLeft(), i));
				image.setRGB((int) sprite.getUVRight(), i, image.getRGB((int) sprite.getUVRight() - 1, i));
			}
		}
		if (left != null) {
			left.copyEdgePixels(image);
		}
		if (right != null) {
			right.copyEdgePixels(image);
		}
	}
}
