package asseter;

import java.awt.image.BufferedImage;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;

public class Postprocess {

	public static BufferedImage postProc(String func, BufferedImage img) {
		if (func.equals("none")) {
			return img;
		} else {
			String[] args = func.split(":");
			int i=0;
			while (i < args.length) {
				if (args[i].equals("resize")) {
					float scale = Float.parseFloat(args[i+1]);
					System.out.println("Applying postprocess: resize("+scale+")");
					img = postProcResize(img, scale);
					i+=2;
				} else {
					System.out.println("! Unknown postprocess function: [" + args[i] +"]");
					break;
				}
			}
			return img;
		}
	}

	public static BufferedImage postProcResize(BufferedImage img, float scale) {
		int newWidth = Math.max((int)(img.getWidth() * scale), 2);
		int newHeight = Math.max((int)(img.getHeight() * scale), 2);
		BufferedImage scaledImg = new BufferedImage(newWidth, newHeight, img.getType());
		Graphics2D g = (Graphics2D)scaledImg.getGraphics();
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);	
		g.drawImage(img, 0, 0, scaledImg.getWidth(), scaledImg.getHeight(), null);
		g.dispose();
		return scaledImg;
	}
}
