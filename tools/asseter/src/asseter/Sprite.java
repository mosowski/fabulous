package asseter;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;


public class Sprite {

	private String name;
	
	private BufferedImage image;	
	private Rectangle sourceRect;

	private Page page;
	
	private float uvLeft;
	private float uvTop;
	private float uvRight;
	private float uvBottom;
	
	private float pivotX;
	private float pivotY;
	
	private Integer hash;
	
	public Sprite(String name, BufferedImage image, float pivotX, float pivotY) {
		this.name = name;
		this.image = image;	
		this.pivotX = pivotX;
		this.pivotY = pivotY;
		sourceRect = new Rectangle();
				
		calculateImageHash();
	}
	
	private void calculateImageHash() {
		hash = 0;
		for (int i = 0; i < image.getWidth(); ++i) {
			for (int j = 0; j < image.getHeight(); ++j) {
				hash += image.getRGB(i, j);	
			}
		}
		hash += image.getWidth();
		hash += image.getHeight();
	}
	
	public void setRect(int x, int y, int width, int height) {
		sourceRect.setBounds(x, y, width, height);
	}
	
	public void setPage(Page page) {
		this.page = page;
	}	
	
	public Page getPage() {
		return page;
	}
	
	public Integer getHash() {
		return hash;
	}
	
	public String getName() {
		return name;
	}
	
	public int getWidth() {		
		return image.getWidth();
	}
	
	public int getHeight() {
		return image.getHeight();
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public float getUVLeft() {
		return uvLeft;
	}
	
	public float getUVTop() {
		return uvTop;
	}
	
	public float getUVRight() {
		return uvRight;
	}
	
	public float getUVBottom() {
		return uvBottom;
	}
	
	public float getUVWidth() {
		return uvRight - uvLeft;
	}
	
	public float getUVHeight() {
		return uvBottom - uvTop;
	}
	
	public void setUV(float left, float top, float right, float bottom) {
		uvLeft = left;
		uvTop = top;
		uvRight = right;
		uvBottom = bottom;
	}
	
	public float getPivotX() {
		return pivotX;
	}
	
	public float getPivotY() {
		return pivotY;	
	}
		
		
}
