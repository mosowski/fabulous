package asseter;

import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import java.util.HashMap;

import org.json.simple.JSONObject;


public class SpriteSheet {
	private String path;
	private String profile;	
	private HashMap<String,Sprite> spriteByName;
	private Vector<Page> pages;	
	private Vector<Vector<Sprite>> spriteQueue;	
	private int pageWidth;
	private int pageHeight;
		
	public SpriteSheet(String path, String profile, int pageWidth, int pageHeight) {
		this.path = path;
		this.profile = profile;
		this.pageWidth = pageWidth;
		this.pageHeight = pageHeight;
		pages = new Vector<Page>();	
		spriteQueue = new Vector<Vector<Sprite>>();
		spriteByName = new HashMap<String,Sprite>();
	}
	
	public Sprite addSprite(String spriteName, BufferedImage img, float pivotX, float pivotY) {
		if (spriteByName.get(spriteName) == null) {
			Sprite spr = new Sprite(spriteName, img, pivotX, pivotY);

			Vector<Sprite> sprVec = new Vector<Sprite>();
			sprVec.add(spr);
			spriteQueue.add(sprVec);
			return spr;
		} else {
			System.out.println("!Warning: Duplicated sprite found: " + spriteName);
			return spriteByName.get(spriteName);
		}
	}
	
	public void addSprites(Vector<Sprite> sprites) {
		Collections.sort(sprites, new Comparator<Sprite>() {
			@Override
			public int compare(Sprite o1, Sprite o2) {
				return o2.getHeight() - o1.getHeight();
			}
		});
		spriteQueue.add(sprites);
	}
	
	public void flush() {
		Collections.sort(spriteQueue, new Comparator<Vector<Sprite>>() {
			@Override
			public int compare(Vector<Sprite> o1, Vector<Sprite> o2) {
				return o2.get(0).getHeight() - o1.get(0).getHeight();
			}
		});
		for (Vector<Sprite> sprites : spriteQueue) {
			boolean insertResult = false;
			for (int i = 0; i < pages.size() && !insertResult; ++i) {
				if (pages.get(i).canInsertSprites(sprites)) {
					pages.get(i).insertSprites(sprites);
					insertResult = true;
				}				
			} 
			if (!insertResult) {
				Page newPage = new Page(pageWidth, pageHeight);
				pages.add(newPage);
				newPage.setFilePath(path + "/page" + String.valueOf(pages.size() + ".png"));
				if (!newPage.canInsertSprites(sprites)) {
					System.out.println("FATAL! No room for sprites! (" + sprites.size() +")");
					return;
				}
				newPage.insertSprites(sprites);				
			}		
		}	
		spriteQueue.clear();
	}
	
	public void savePagesToDir(String dir) {
		for (Page page : pages) {
			page.saveToFile(dir);		
		}
	}
	
	public void addPagesToLib(AssetLib lib) {
		for (Page page : pages) {
			lib.addAsset(page.getFilePath(), profile, page.getFilePath());
		}
	}
	
	public JSONObject getJSON() {
		JSONObject object = new JSONObject();
		for (int i = 0; i < pages.size(); ++i) {
			pages.get(i).addSpritesToJSON(object);
		}
		return object;
	}
}
