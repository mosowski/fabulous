

if [[ $# -ne 1 ]];
then
	echo "usage:"
	echo "png2jpegxr.sh <PngFile>"
else
	WORKDIR=`pwd`/`dirname $0`

	PNG=$1
	ATF=${PNG%.*}.jxr.atf

	$WORKDIR/png2atf -i $PNG -n 0,1 -q 30 -o $ATF

	echo "Converted $PNG to $ATF"
fi
