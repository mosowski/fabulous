

if [[ $# -ne 1 ]];
then
	echo "usage:"
	echo "png2pvr.sh <PngFile>"
else
	WORKDIR=`pwd`/`dirname $0`

	PNG=$1
	PVR=${PNG%.*}.pvr 
	ATF=${PNG%.*}.pvr.atf

	$WORKDIR/PVRTexToolCL -i $PNG -o $PVR -square + -pot + -rfilter cubic -p -m 2 -f PVRTC1_4 -q pvrtcbest
	$WORKDIR/pvr2atf -p $PVR -n 0,1 -o $ATF

	rm $PVR

	echo "Converted $PNG to $PVR"
fi
