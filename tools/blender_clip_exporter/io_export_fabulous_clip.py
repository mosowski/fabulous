import bpy
import json
import mathutils as Math
import os

bl_info = {
    "name": "Export Fabulous Clip",
    "author": "Mateusz Osowski",
    "version": (1, 0),
    "blender": (2, 6, 2),
    "api": 36079,
    "location": "File > Export > Fabulous Clip",
    "description": "Export Fabulous Clip",
    "category": "Import-Export"}

from bpy.props import (StringProperty,FloatProperty)

from bpy_extras.io_utils import (ExportHelper, axis_conversion)

class ExportMODEL(bpy.types.Operator, ExportHelper):
    bl_idname = "export_fabulus.clip"
    bl_label = 'Export Fabulous Clip'

    filename_ext = ".xml"
    filter_glob = StringProperty(
            default="*.xml",
            options={'HIDDEN'},
            )

    post_scale = FloatProperty(
            name = "PostScale",
            default = 10.0,
            min = 0.001,
            max = 1000.00
            )    

    def invoke(self, context, event):
        for sel in bpy.context.selected_objects:
            if sel.type == 'MESH':            
                modelname = sel.name
                
        if not self.filepath:
            blend_filepath = context.blend_data.filepath
            if not blend_filepath:
                blend_filepath = "~/untitled"
            else:
                blend_filepath = os.path.splitext (blend_filepath) [0]
                print(blend_filepath)

            self.filepath = os.path.dirname(blend_filepath) +'/'+ modelname + self.filename_ext
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
        
    def execute(self, context):
        keywords = self.as_keywords(ignore=("filter_glob","check_existing"))
        return save(self, context, self.post_scale, keywords['filepath'])

def menu_func_export(self, context):
    self.layout.operator(ExportMODEL.bl_idname, text="Fabulous Clip (.xml)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func_export)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)

if __name__ == "__main__":
    register()
    
#==============================================================================
import xml.etree.ElementTree as XML


class Clip:
    def __init__(self):
        self.animations = [ ]
        self.images = [ ]        
        
    def add_image(self, image):
        found = False
        for (path, id) in self.images:
            if path == image:
                found = True
                return id
        if not found:
            id= len(self.images)
            self.images.append((image, id))
            return id
    
    def to_xml(self):
        self.xml = XML.Element("clip")
        self.animations_xml = XML.SubElement(self.xml, "animations")
        for animation in self.animations:
            animation.to_xml(self.animations_xml)
        self.images_xml = XML.SubElement(self.xml, "images")
        for (path, id)  in self.images:
            image_xml = XML.SubElement(self.images_xml, "image")
            image_xml.set("path", path)
            image_xml.set("id", str(id))
        return self.xml
    
class Animation:
    def __init__(self):
        self.frames = [ ]        
        self.name = ""
        
    def to_xml(self, rootxml):
        self.xml = XML.SubElement(rootxml, "animation")
        self.xml.set("name", self.name)
        self.xml.set("lenght", str(len(self.frames)))
        for frame in self.frames:
            frame.to_xml(self.xml)
        
class Frame:
    def __init__(self):
        self.geoms = [ ]        
        self.vert_queue = [ ]
        self.vert_data = [ ]        
        self.faces = [ ]        

    def append_vert(self, vec, uv):
        self.vert_queue.append((vec,uv))
    
    def append_face(self, image_id, a, b, c):
        geom = None
        for g in self.geoms:
            if g.image_id == image_id:
                geom = g
                break
        if geom == None:
            geom = Geom()
            geom.image_id = image_id
            self.geoms.append(geom)            
        geom.faces.extend([a,b,c])
    
    def get_vert_hash(self, v):        
        # with assumption that same position means same uv and alpha
        return "%f %f" % (v[0], v[1])
        
    def prepare_geoms(self):
        #todo: sort faces by z
        dict = { }
        for (v,uv) in self.vert_queue:
            hash = self.get_vert_hash(v)
            if hash in dict:
                id = dict[hash]
            else:                                
                dict[hash] = len(self.vert_data)
                self.vert_data.append((int(v[0] * _post_scale * 1.001), -int(v[1] * _post_scale * 1.001), uv[0], 1.0 - uv[1], 1.0))
        for g in self.geoms:
            g.index = [ dict[self.get_vert_hash(v)] for v in g.faces ]
            
    def vertex_to_str(self):
        result = ""
        for (x,y,u,v,a) in self.vert_data:
            result += "%d %d %.7f %.7f %.3f " % (x,y,u,v,a)           
        return result
        
    def to_xml(self, parentxml):
        self.prepare_geoms()
        self.xml = XML.SubElement(parentxml, "frame")
        self.xml.set("vertices", str(len(self.vert_data)))
        self.vertex_xml = XML.SubElement(self.xml, "vertex")
        self.vertex_xml.text = self.vertex_to_str()  
        for geom in self.geoms:
            geom.to_xml(self.xml)
        
class Geom:
    def __init__(self):        
        self.faces = [ ]
        self.index = [ ]
        self.image_id = 0
    
    def index_to_str(self):
        result = ""
        for e in self.index:
            result += "%d " % (e)
        return result
        
    def to_xml(self, parentxml):
        self.xml = XML.SubElement(parentxml, "geom")
        self.xml.set("image", str(self.image_id))
        self.xml.set("indices", str(len(self.index)))
        self.xml.text = self.index_to_str()
                
class AnimRange:
    def __init__(self):
        self.begin = 0
        self.end = 0
        self.name = ""
               
def save(operator,context, post_scale, filepath=""):        
    anims = []
    global _post_scale 
    _post_scale = post_scale
    
    armature = None
    mesh_objects = [ ]
    
    for sel in bpy.context.selected_objects:
        if sel.type == "ARMATURE":            
            armature = bpy.data.objects[sel.name]
        if sel.type == "MESH":
            mesh_objects.append(bpy.data.objects[sel.name])
        
    assert len(mesh_objects) != 0, "Mesh not selected!"
    
    anim_ranges = { }
    for marker in bpy.context.scene.timeline_markers:
        anim_name = marker.name.split("_")[0]
        marker_type = marker.name.split("_")[1]        
        if not (anim_name in anim_ranges):
            anim_range = AnimRange()
            anim_range.name = anim_name
            anim_ranges[anim_name] = anim_range           
        else:            
            anim_range = anim_ranges[anim_name]
        if marker_type == "A":
            anim_range.begin = marker.frame
        if marker_type == "B":
            anim_range.end = marker.frame
    
    clip = Clip()
        
    for anim_range in anim_ranges.values():
        animation = Animation()
        animation.name = anim_range.name
        clip.animations.append(animation)
        print("Caputring animation " + animation.name)
            
        frame_no = anim_range.begin

        while frame_no <= anim_range.end:
            frame = Frame()
            animation.frames.append(frame)
            
            bpy.context.scene.frame_set(frame_no)
            
            for mesh_object in mesh_objects:   
                if mesh_object.location[2] < 0:
                    continue
                    
                frame_mesh = mesh_object.to_mesh(scene=bpy.context.scene, apply_modifiers=True, settings='PREVIEW')
                            
                num_tris = len(frame_mesh.polygons)           
                uvs = frame_mesh.uv_layers[frame_mesh.uv_textures.active.name].data
                for i in range(num_tris):
                    tri = frame_mesh.polygons[i]
                    image = frame_mesh.uv_textures.active.data[tri.index].image
                    vi0 = tri.vertices[0]
                    vi1 = tri.vertices[1]
                    vi2 = tri.vertices[2]                    
                    v0 = mesh_object.matrix_world * frame_mesh.vertices[vi0].co
                    v1 = mesh_object.matrix_world * frame_mesh.vertices[vi1].co
                    v2 = mesh_object.matrix_world * frame_mesh.vertices[vi2].co
                    uv0 = uvs[i*3].uv
                    uv1 = uvs[i*3+1].uv
                    uv2 = uvs[i*3+2].uv
                    
                    frame.append_vert(v0, uv0)
                    frame.append_vert(v1, uv1)
                    frame.append_vert(v2, uv2)
                    
                    image_id = clip.add_image(image.filepath[2:])
                    frame.append_face(image_id, v0, v1, v2)
            frame_no+=1

    clip_xml_tree = XML.ElementTree(clip.to_xml())
    clip_xml_tree.write(filepath)

    return {'FINISHED'}
                
