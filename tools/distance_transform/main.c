#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "edtaa4func.c"
#include "lodepng.h"

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;

unsigned char *make_distance_map( unsigned char *img, unsigned int width, unsigned int height, double radius )
{
    short * xdist = (short *)  malloc( width * height * sizeof(short) );
    short * ydist = (short *)  malloc( width * height * sizeof(short) );
    double * gx   = (double *) calloc( width * height, sizeof(double) );
    double * gy      = (double *) calloc( width * height, sizeof(double) );
    double * data    = (double *) calloc( width * height, sizeof(double) );
    double * outside = (double *) calloc( width * height, sizeof(double) );
    double * inside  = (double *) calloc( width * height, sizeof(double) );
    int i;

    double img_min = 255, img_max = -255;
    for( i=0; i<width*height; ++i)
    {
        double v = img[i];
        data[i] = v;
        if (v > img_max) img_max = v;
        if (v < img_min) img_min = v;
    }

    for( i=0; i<width*height; ++i)
    {
        data[i] = (img[i]-img_min)/img_max;
    }

    computegradient( data, width, height, gx, gy);
    edtaa4(data, gx, gy, width, height, xdist, ydist, outside);
    for( i=0; i<width*height; ++i)
        if( outside[i] < 0 )
            outside[i] = 0.0;

    memset(gx, 0, sizeof(double)*width*height );
    memset(gy, 0, sizeof(double)*width*height );
    for( i=0; i<width*height; ++i)
        data[i] = 1 - data[i];
    computegradient( data, width, height, gx, gy);
    edtaa4(data, gx, gy, width, height, xdist, ydist, inside);
    for( i=0; i<width*height; ++i)
        if( inside[i] < 0 )
            inside[i] = 0.0;

    unsigned char *out = (unsigned char *) malloc( width * height * sizeof(unsigned char) );
    for( i=0; i<width*height; ++i)
    {
        outside[i] -= inside[i];
        outside[i] = 128+outside[i]*(128.0/radius);
        if( outside[i] < 0 ) outside[i] = 0;
        if( outside[i] > 255 ) outside[i] = 255;
        out[i] = 255 - (unsigned char) outside[i];
    }

    free( xdist );
    free( ydist );
    free( gx );
    free( gy );
    free( data );
    free( outside );
    free( inside );
    return out;
}

int main( int argc, char **argv )
{
    double radius = 16;
    char *infile, *outfile;
    if (argc != 4) {
        printf("usage:\n");
        printf("distance_transform <SourcePNG> <OutputPNG> <Radius>");
    } else {
        infile = argv[1];
        outfile = argv[2];
        radius = atof(argv[3]);

        unsigned int width=0, height=0, depth=0;
        unsigned char *rgba_img;
        int i;
        lodepng_decode32_file(&rgba_img, &width, &height, infile);

        unsigned char *img = (unsigned char *)malloc(sizeof(unsigned char) * width * height);
        for (i = 0; i < width * height; ++i) {            
            img[i] = rgba_img[4 * i + 0];
        }

        printf("Image dimension: %d x %d\n", width, height);

        unsigned char *out = make_distance_map(img, width, height, radius);

        for (i = 0; i < width * height; ++i) {            
            rgba_img[4 * i + 0] = out[i];
            rgba_img[4 * i + 1] = out[i];
            rgba_img[4 * i + 2] = out[i];
            rgba_img[4 * i + 3] = 255;
        }        

        printf("Done, saving file.\n");

        lodepng_encode32_file(outfile, rgba_img, width, height);

        free(rgba_img);
        free(img);
        free(out);
        return 0;
    }
}
