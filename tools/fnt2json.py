# Tool converting Angelcode bitmap font generator text fnt to json

import sys
if len(sys.argv) != 5:
	print("fnt2raw <in.fnt> <shiftx> <shifty> <png|default>")
	sys.exit(0)

import fileinput

shiftx = int(sys.argv[2])
shifty = int(sys.argv[3])
texture = sys.argv[4]

out = {}
chars = []
out["glyphs"] = chars
for line in fileinput.input(sys.argv[1]):
    tokens = line.split()
    if len(tokens) > 0:
        if tokens[0] == "info":
            out["name"] = tokens[1].split("=")[1].split("\"")[1]
        if tokens[0] == "common":
            out["lineHeight"] = int(tokens[1].split("=")[1])
            out["base"] = int(tokens[2].split("=")[1])
        if tokens[0] == "page":		
			out["texture"] = tokens[2].split("=")[1].split("\"")[1] if texture == "default" else texture
        if tokens[0] == "char":
            charid = int(tokens[1].split("=")[1])
            x = int(tokens[2].split("=")[1]) + shiftx
            y = int(tokens[3].split("=")[1]) + shifty
            width = int(tokens[4].split("=")[1])
            height = int(tokens[5].split("=")[1])
            xoffset = int(tokens[6].split("=")[1])
            yoffset = int(tokens[7].split("=")[1])
            xadvance = int(tokens[8].split("=")[1])
            chars.append([charid,x,y,width,height,xoffset,yoffset,xadvance])

import json
print(json.dumps(out))
    
