import json
import pyamf
import os

from sys import argv
script, input_path = argv

for path, dirs, files in os.walk(input_path):
    files = [ fi for fi in files if fi.lower().endswith(('.json', '.full', '.js'))]
    for file in files:
        (f, extension) = os.path.splitext(file)
        print f

        write_path = os.path.join(path, f + ".amf")

        jsonfile = open(os.path.join(path, file))
        object = json.loads(jsonfile.read())
        jsonfile.close()
        amf = pyamf.encode(object, 3)
        amf_file = open(write_path,"wb")
        amf_bytearray = bytearray()
        amf_bytearray.extend(amf.read())
        amf_file.write(amf_bytearray)
